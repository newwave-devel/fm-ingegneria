<?php

class NW_Descriptors_Module extends NW_XmlDescriptor {

    public function decorators($class) {
        return $this->descriptor->$class->decorators->decorator;
    }

    public function classModel(NW_ClassURI &$classUri) {
        $class = $classUri->getClassName();
        return $this->descriptor->$class["model"];
    }

    public function classHelper(NW_ClassURI &$classUri) {
        $class = $classUri->getClassName();
        return $this->descriptor->$class["helper"];
    }

    public function connectionByName(NW_ClassURI &$classUri, $connectionName) {
        $class = $classUri->getClassName();
        return strval($this->descriptor->$class->db->connections->$connectionName);
    }

    public function table(NW_ClassURI &$classUri, $alias) {
        $class = $classUri->getClassName();
        return strval($this->descriptor->$class->db->tables->$alias);
    }

    public function classIsAvailableForExport(NW_ClassURI &$classUri) {
        $class = $classUri->getClassName();
        return ($this->descriptor->$class->db->integration->export["isExportable"] == 'Y');
    }

    public function classIsAvailableForImport(NW_ClassURI &$classUri) {
        $class = $classUri->getClassName();
        return ($this->descriptor->$class->db->integration->export["isImportable"] == 'Y');
    }

    public function getClassExportFormatsAvailable(NW_ClassURI &$classUri) {
        $class = $classUri->getClassName();
        return $this->descriptor->$class->db->integration->export->format;
    }

    public function getClassImportFormatsAvailable(NW_ClassURI &$classUri) {
        $class = $classUri->getClassName();
        return $this->descriptor->$class->db->integration->import->format;
    }

    public function classHasDefaultSortingOptions(NW_ClassURI &$classUri) {
        $class = $classUri->getClassName();
    }

    public function getClassSortingOptions(NW_ClassURI &$classUri, $sortname) {
        $class = $classUri->getClassName();
    }

    public function getClassBackendPageProperty(NW_ClassURI &$classUri, $page, $property) {
        $class = $classUri->getClassName();
        return $this->descriptor->$class->backend->pages->$page->params;
    }

    public function getBulkActions(NW_ClassURI &$classUri) {
        $class = $classUri->getClassName();
        return $this->descriptor->$class->backend->pages->$page->actions->bulk;
    }

    public function getItemActions(NW_ClassURI &$classUri) {
        $class = $classUri->getClassName();
        return $this->descriptor->$class->backend->pages->$page->actions->item;
    }

    public function getModuleRelease() {
        return $this["rel"];
    }

    public function getClassCallbacksFor(NW_ClassURI &$classUri, $action) {
        $class = $classUri->getClassName();
    }

    public function getClassAvailableStatuses(NW_ClassURI &$classUri) {
        $class = $classUri->getClassName();
        return $this->descriptor->$class->class_status;
    }

    public function getClassDefaultStatus(NW_ClassURI &$classUri) {
        $class = $classUri->getClassName();
        foreach ($this->getClassAvailableStatuses($classUri) as $status)
            if (isset($status["default"]) && $status["default"] == "Y")
                return $status;
        return false;
    }

}

?>
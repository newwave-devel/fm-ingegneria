<?php

class Pages_Model_Page_Observer_MainBlocks
{

    public function update(&$subject, $event)
    {
        switch ($event) {
            case Pages_Model_Page_Event::EVENT_BEFORE_SAVE:
                break;
            case Pages_Model_Page_Event::EVENT_AFTER_SAVE:
                $this->appendNewMainBlock($subject);
                break;
            case Pages_Model_Page_Event::EVENT_BEFORE_UPDATE:
                break;
            case Pages_Model_Page_Event::EVENT_AFTER_UPDATE:
                break;
        }
    }
    
    public function appendNewMainBlock(Pages_Model_Page &$subject)
    {
    	try {
    		$descriptor=NW_Pages_Page::getInstance()->page($subject->getTemplate());
    		foreach($descriptor->blocks->block as $block)
    		{
	    		if(isset($block['main']) && $block['main']=="Y")
	    		{
	    			$blockId = $block["id"];
	    			$newBlock = new Pages_Model_Block();
	    			$newBlock->setTemplate($blockId)
	    				->setLang($subject->getLang())
	    				->setStatus(Pages_Model_Block::STATUS_DRAFT)
	    				->setContents(array());
	    			$newBlock->setPage($subject->getId());
	    			$blockService = new Pages_Model_Block_Service();
	    			$blockService->save($newBlock);
	    			break;
	    		}
    		}
    	} catch(Exception $e)
    	{
    		throw $e;
    	}
    }
    
    
    private function _insert($label, $value, $page, $id)
    {
    	try{
    		$adapter = Zend_Db_Table::getDefaultAdapter();
    		$data = array(
    				"content" => $value,
    				"label" => $label,
    				"page" => $page,
    				"block" => $id
    		);
    		$adapter->insert("tbl_block_contents", $data);
    	}catch(Exception $e)
    	{
    		throw $e;
    	}
    }
    public function dropBlockContent(Pages_Model_Block $subject)
    {
    	try{
    		$blockId = $subject->getId();
    		$adapter = Zend_Db_Table::getDefaultAdapter();
    		$adapter->delete("tbl_block_contents", "block = $blockId" );
    		return $this;
    	} catch(Exception $e)
    	{
    		throw $e;
    	}
    }
}
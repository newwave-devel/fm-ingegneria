<?php

class NW_Models_Object_Form extends NW_Models_Form {

    private $_helper;
    

    public function __construct($classUri, $options) {
        parent::__construct($classUri, $options);
        $this->_helper = new NW_Models_Form_Helper($this);
    }

    public function create($fieldsName = array(), $fields_options = array()) {
        $model = NW_Application::__model($this->_classUri);

        foreach ($model->formElements() as $field)
            $this->addElement($field);
        $this->_helper->addModelFields($fieldsName, $fields_options);
        return $this;
    }
}

?>

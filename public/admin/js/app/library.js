function Library()
{
}

Library.chooseOne = function(itemRef)
{
	$("a.select.current").removeClass("current");
	$(itemRef).addClass("current");
}

Library.choose = function(itemRef)
{
	$(itemRef).toggleClass("current");
}

Library.addRemote = function(datas)
{
	if( typeof datas == 'object')
		var parsed = datas;
	else 
		var parsed = jQuery.parseJSON(datas);
	
	if(parsed.status == 'ok')
	{ 
		if(parsed.payload instanceof Array)
			var payload = parsed.payload.pop();
		else var payload = parsed.payload;
		
		var libraryController = parent.window.main_page.subPage().getLibraryController();
		return libraryController.addItem(payload);
	}
	return true;
}

Library.addLocal = function(datas)
{
	var items = $("a.select.current");
	var libraryController = parent.window.main_page.subPage().getLibraryController();
	var replaced = false;
	$(items).each(
		function()
		{
			var rel = $(this).attr("rel");
			$.get("/app/media/fromlibrary", {id : rel})
				.done(function(data)
				{
					var parsed = jQuery.parseJSON(data);
					if(parsed.status == 'ok')
					{ 
						if(parsed.payload instanceof Array)
							var payload = parsed.payload.pop();
						else var payload = parsed.payload;	
						libraryController.addItem(payload);
						replaced = true;
					}
				})
				.fail(function(){alert("There has been some errors processing the request");});
		}
	);
	if(replaced)
	{
		parent.window.main_page.notify("Immagini aggiunte con successo");
		parent.jQuery.fancybox.close();
	}
}

Library.replace = function()
{
	var items = $("a.select.current");
	var libraryController = parent.window.main_page.subPage().getLibraryController();
	 $(items).each(
		function()
		{
			var rel = $(this).attr("rel");
			$.get("/app/media/fromlibrary", {id : rel})
				.done(function(data)
				{
					var parsed = jQuery.parseJSON(data);
					if(parsed.status=='ok')
					{
						if(parsed.payload instanceof Array)
							var payload = parsed.payload.pop();
						else var payload = parsed.payload;
						libraryController.replaceItem(payload);
						parent.window.main_page.notify("Immagine sostituita con successo");
						parent.jQuery.fancybox.close();
					}
				})
				.fail(function(){alert("There has been some errors processing the request");});
		});
}


Library.crop = function()
{

	return function(responseText, statusText, xhr, $form)
	{
			if(statusText == 'success')
			{
				var parsed = jQuery.parseJSON(responseText);
				if(parsed.status=='ok')
				{
					if(parsed.payload instanceof Array)
						var payload = parsed.payload.pop();
					else var payload = parsed.payload;
					parent.window.main_page.subPage().getLibraryController().replaceItem(payload);
					parent.window.main_page.notify("Immagine sostituita con successo");
					parent.jQuery.fancybox.close();
				}
					
			}
		}
}

<?php

class App_Model_I18n  {

    protected $_id;
    protected $_lang;
    protected $_context;
    protected $_format;
    protected $_key;
    protected $_value;

    const FORMAT_TEXT = 'text';
    const FORMAT_WYSIWYG = 'wysiwyg';
    const FORMAT_TEXTAREA = 'textarea';

    /**
     * @param mixed $key
     */
    public function setKey($key)
    {
        $this->_key = $key;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->_key;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->_value = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->_value;
    }


    /**
     * @param mixed $context
     */
    public function setContext($context)
    {
        $this->_context = $context;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContext()
    {
        return $this->_context;
    }

    /**
     * @param mixed $format
     */
    public function setFormat($format)
    {
        $this->_format = $format;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFormat()
    {
        return $this->_format;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->_id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->_id;
        return $this;
    }

    /**
     * @param mixed $lang
     */
    public function setLang($lang)
    {
        $this->_lang = $lang;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLang()
    {
        return $this->_lang;
        return $this;
    }

    public function __construct()
    {
        $this->_context = '';
        $this->_format = self::FORMAT_WYSIWYG;
        $this->_id = null;
        $this->_lang = '';
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid user property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception("Invalid user property '$name'");
        }
        return $this->$method();
    }

    public function setOptions(array $options)
    {
        $methods = get_class_methods ( $this );
        foreach ( $options as $key => $value )
        {
            $method = 'set' . ucfirst ( $key );
            if (in_array ( $method, $methods ))
            {
                $this->$method ( $value );
            }
        }
        return $this;
    }

    public function getOptions()
    {
        return get_object_vars ( $this );
    }


}


<?php

class App_Model_User_Mapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('App_Model_DbTable_User');
        }
        return $this->_dbTable;
    }

    public function save(App_Model_User $user) {
        try {
            $data = $this->_toDb($user);
            
            $id = $user->getId();
            if (empty($id)) {
                unset($data['id']);
                return $this->getDbTable()->insert($data);
            } else {
                $this->getDbTable()->update($data, array('id = ?' => $id));
                return $id;
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function find($id, App_Model_User &$user) {
        try {
            $result = $this->getDbTable()->find($id);
            if (0 == count($result)) {
                return;
            }
            $row = $result->current();
            $this->_fromDb($user, $row); 
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function fetchAll(NW_Pages_Paginator &$paginator, $paramsSearch, $paramsOrder=null) {
    	try {
    		$table = $this->getDbTable();
    		$select = $table->select();
    
    		$this->_filterBySearchParam($select, $paramsSearch);
    		if(!empty($paginator) && !$paginator->getFetchAll())
    			$select->limit($paginator->itemsPerPage, $paginator->getStart());
    		if(!empty($paramsOrder))
    		{
    			foreach($paramsOrder as $key => $dir)
    			{
    				$sql = sprintf("%s %s", $key, strtoupper($dir));
    				$select->order($sql);
    			}
    		}
    		$entries = array();
    		
    		foreach ($table->fetchAll($select) as $row) {
    			$model = new App_Model_User();
    			$this->_fromDb($model, $row);
    			$entries[] = $model;
    		}
    		if(!empty($paginator))
    			$paginator->itemsCount = $this->count($select);
    			
    		return $entries;
    	} catch (Exception $e) {
    		throw $e;
    	}
    }
    
    public function fetchChilds(NW_Pages_Paginator &$paginator, $paramsSearch, $paramsOrder=null) {
    	try {
    		$table = $this->getDbTable();
    		$select = $table->select();
    
    		$this->_filterBySearchParam($select, $paramsSearch)
    			->_filterByUserParam($select);
    		if(!empty($paginator) && !$paginator->getFetchAll())
    			$select->limit($paginator->itemsPerPage, $paginator->getStart());
    		if(!empty($paramsOrder))
    		{
    			foreach($paramsOrder as $key => $dir)
    			{
    				$sql = sprintf("%s %s", $key, strtoupper($dir));
    				$select->order($sql);
    			}
    		}
    		$entries = array();
    
    		foreach ($table->fetchAll($select) as $row) {
    			$model = new App_Model_User();
    			$this->_fromDb($model, $row);
    			$entries[] = $model;
    		}
    		if(!empty($paginator))
    			$paginator->itemsCount = $this->count($select);
    		 
    		return $entries;
    	} catch (Exception $e) {
    		throw $e;
    	}
    }
    
    public function countBy($fields)
    {
    	try {
    		$select = $this->getDbTable()->select(Zend_Db_Table::SELECT_WITH_FROM_PART);
    		foreach($fields as $key => $value)
    			$select->where($key, $value);
    		return $this->count($select);
    	} catch (Exception $e) {
            throw $e;
        }
    }

    public function count(Zend_Db_Select $select=null) {
        try {
            $table = $this->getDbTable();
            if(!empty($select))
            {
            	$select->reset(Zend_Db_Select::COLUMNS)
                    ->reset(Zend_Db_Select::LIMIT_COUNT)
                    ->reset(Zend_Db_Select::LIMIT_OFFSET);
            } else{
            	$select = $this->getDbTable()->select(Zend_Db_Table::SELECT_WITH_FROM_PART);
            }
            $select->columns(array('num' => new Zend_Db_Expr('COUNT(*)')));
            return $table->fetchRow($select)->num;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function delete($id) {
        try {
            $this->getDbTable()->delete(array("id=?" => $id));
            return true;
        } catch (Exception $e) {
            throw $e;
        }
    }

    private function _toDb(App_Model_User $model) 
    {
        $map = array(
                'id' => $model->getId(),
                'date_created' => $model->getDate_created(),
                'name' => $model->getName(),
                'password' => $model->getPassword(),
                'salt' => $model->getSalt(),
                'username' => $model->getUsername(),
                'email' => $model->getEmail(),
                'activation_code' => $model->getActivationCode(),
                'status' => $model->getStatus(),
        		'pid' => $model->getPid(),
                'role' => is_object($model->getRole()) ? $model->getRole()->id : $model->getRole(),
         );
        return $map;
    }

    private function _fromDb(App_Model_User &$model, $row) {

        $model->setId($row->id)
                ->setUsername($row->username)
                ->setName($row->name)
                ->setPassword($row->password)
                ->setSalt($row->salt)
                ->setDate_created($row->date_created)
                ->setRole($row->findParentRow('App_Model_DbTable_Role'))
                ->setStatus($row->status)
                ->setEmail($row->email)
                ->setPid($row->pid)
                ->setActivationCode($row->activation_code);
    }
    
    private function _filterBySearchParam(Zend_Db_Table_Select &$select, $paramsSearch = array()) {
    	if (!empty($paramsSearch)) {
    		if (!empty($paramsSearch['name'])) {
    			$name = $paramsSearch['name'];
    			$select->where("tbl_users.name LIKE '%$name%'");
    		}
    		if (!empty($paramsSearch['role'])) 
    			$select->where("tbl_users.role = ?", $paramsSearch['role']);
    		if (!empty($paramsSearch['pid'])) 
    			$select->where("tbl_users.pid = ?", $paramsSearch['pid']);
    		if (!empty($paramsSearch['email']))
    		{
    			$email = $paramsSearch['email'];
    			$select->where("tbl_users.email LIKE '%$email%'");
    		}
    		if (!empty($paramsSearch['id']))
    			if(is_array($paramsSearch['id']))
    				$select->where("tbl_users.id IN (?)", $paramsSearch['id']);
    			else $select->where("tbl_users.id=?", $paramsSearch['id']);
    	}
    	return $this;
    }
    
    private function _filterByUserParam(Zend_Db_Table_Select &$select, $paramsSearch = array()) {
    		$select->where("tbl_users.pid=?", Zend_Auth::getInstance()->getStorage()->read()->getId());
    	return $this;
    }

}


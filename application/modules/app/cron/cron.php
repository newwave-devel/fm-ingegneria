<?php
try {    
		$PROC_ID = 'app';
    	$messageService = new App_Model_Message_Service();
    	$messageService->processQueue();
    	cronLog($PROC_ID, 'ok');

} catch (Exception $e) {
    cronLog($PROC_ID, 'ko', $e->getMessage());  
}
?>

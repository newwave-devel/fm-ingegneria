<?php

class Application_View_Helper_FormMultiCheckboxList extends Application_View_Helper_FormRadioList
{
	protected $_inputType = 'checkbox';
	
	protected $_isArray = true;
	
    public function formMultiCheckboxList($name, $value = null, $attribs = null,$options = null, $listsep = "")
    {
        return $this->formRadioList($name, $value, $attribs, $options, $listsep);
    }
}

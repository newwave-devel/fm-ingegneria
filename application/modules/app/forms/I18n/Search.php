<?php

class App_Form_I18n_Search extends Zend_Form {

    private $_contexts = '';
    private $_paginator;

    public function init() {
    	$this->setMethod('post')
    		    ->setAction("/app/i18n/search");
    	
    	$key = new NW_Form_Element_Text("keylike");
        $key -> setLabel("Label nel Codice")
        	->setRequired(false);

        $value = new NW_Form_Element_Text("value");
        $value -> setLabel("Valore Tradotto")
        			->setRequired(false);

        if(App_Model_Display_Service::getInstance()->isMultilanguage())
        {
            $language = new NW_Form_Element_Select('lang');
            $language->setMultiOptions(App_Model_Display_Service::getInstance()->langsAsSelect())
                ->setLabel("Langage");
        }
        else
            $language = new NW_Form_Element_Hidden('lang');

        $language->setValue(App_Model_Display_Service::getInstance()->lang());

        $context =  new NW_Form_Element_Select("context");
        $context->setLabel("Posizione nel codice")
            ->addMultiOption("", "Selezionare una posizione");

        foreach($this->_contexts as $con)
            $context->addMultiOption(trim($con), $con);

        $ipp = new NW_Form_Element_Hidden("ipp");

        $ipp->setDecorators(array("ViewHelper"));
        $page = new NW_Form_Element_Hidden("pag");
        $page->setDecorators(array("ViewHelper"));
        if(!empty($this->_paginator))
        {
            $ipp->setValue($this->_paginator->getItemsPerPage());
            $page->setValue($this->_paginator->getCurPage());
        }

        $this->addElements(array($key,$value, $language, $context, $ipp, $page));
        $this->getView()->jQuery()->addOnLoad('$("#frmSearch").ajaxForm({target: \'#main_content\'});');
    }



    public function setContexts($contexts)
    {
        $this->_contexts = $contexts ;
    }

    public function setPaginator($paginator)
    {

        $this->_paginator = $paginator ;
    }
    

    

}


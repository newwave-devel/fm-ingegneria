<?php
class App_Model_TransactionalEmail_Mapper extends App_Model_Message_Mapper {
	
	protected $_dbTable;
	
	public function setDbTable($dbTable) {
		if (is_string ( $dbTable )) {
			$dbTable = new $dbTable ();
		}
		if (! $dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception ( 'Invalid table data gateway provided' );
		}
		$this->_dbTable = $dbTable;
		return $this;
	}
	
	public function dbTable() {
		
		if (null === $this->_dbTable) {
			$this->setDbTable ( 'App_Model_DbTable_TransactionalEmail' );
		}
		return $this->_dbTable;
	}
		
	public function qFetchAll(NW_Pages_Paginator &$paginator, $paramsSearch, $paramsOrder = array(), $load = false) {
		try {			
			$select = parent::qFetchAll($paginator, $paramsSearch, $paramsOrder,$load);
			$this->_filterBySearchParam($select, $paramsSearch);
			return $select;
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	
	public function save(App_Model_TransactionalEmail $message) {
		try {
			$data = $this->_toDb ( $message );
			return $this->dbTable ()->insert ( $data );
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	
	public function count(Zend_Db_Select $select = null, $params = array()) {
		try {
			$select = parent::qCount($select);
			if(!empty($params))
				$this->_filterBySearchParam($select, $params, true);
			return $this->dbTable ()->fetchRow ( $select )->num;
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	private function _toDb(App_Model_TransactionalEmail $message) {
		$result = array ();
		$result ['message'] = $message->getId ();
		$result ['item'] = $message->getItem ();
		$result ['model'] = $message->getModel ();
		return $result;
	}
	
	
	private function _filterBySearchParam(Zend_Db_Select &$select, $paramsSearch, $is_querycount=false) {	
		$select->setIntegrityCheck(false);
		$tableName = $select->getPart(Zend_Db_Select::FROM);
		$tableName = is_array($tableName) ? key($tableName) : $tableName;
		if($is_querycount)
			$select->join("tbl_messages_transactional", "$tableName.id=tbl_messages_transactional.message", array());
		else $select->join("tbl_messages_transactional", "$tableName.id=tbl_messages_transactional.message", array("item", "message", "model"));
		
		if (! empty ( $paramsSearch )) {
			
			if (isset ( $paramsSearch ['item'] )) {
				if (! is_array ( $paramsSearch ['item'] ))
					$select->where ( "tbl_messages_transactional.item=?", $paramsSearch ['item'] );
				else
					$select->where ( "tbl_messages_transactional.item IN(?)", $paramsSearch ['item'] );
			}
			if (isset ( $paramsSearch ['message'] )) {
				if (! is_array ( $paramsSearch ['message'] ))
					$select->where ( "tbl_messages_transactional.message=?", $paramsSearch ['message'] );
				else
					$select->where ( "tbl_messages_transactional.message IN (?)", $paramsSearch ['message'] );
			}
			if (isset ( $paramsSearch ['model'] )) {
				$select->where ( "tbl_messages_transactional.model=?", $paramsSearch ['model'] );	
			}
		}
		return $this;
	}
	
	public function delete($paramsAsArray) {
		try {
			$where = array();
			foreach($paramsAsArray as $key => $value)
				$where[] = $this->dbTable()->getAdapter()->quoteInto($key, $value);
			
			$this->getDbTable()->delete($where);
			return true;
		} catch (Exception $e) {
			throw $e;
		}
	}
}
?>
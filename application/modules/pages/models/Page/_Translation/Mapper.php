<?php
class Pages_Model_Block_Mapper
{
	protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Pages_Model_DbTable_Block');
        }
        return $this->_dbTable;
    }

	public function find($id, Pages_Model_Block &$model)
	{
		try {
			$result = $this->getDbTable ()->find ( $id );
			if (0 == count ( $result )) {
				return;
			}
			$row = $result->current ();
			$this->_fromDb ( $model, $row );
		} catch ( Exception $e ) {
			throw $e;
		}
	}

	public function load($id, $lang)
	{

	}

	public function fetchAll(NW_Pages_Paginator &$paginator, $paramsSearch, $paramsOrder = null) {
		try {
			$table = $this->getDbTable ();
			$select = $table->select ();
				
			$this->_filterBySearchParam ( $select, $paramsSearch )->_filterByUser ( $select );
			if (! $paginator->getFetchAll ())
				$select->limit ( $paginator->itemsPerPage, $paginator->getStart () );
				
			if (! empty ( $paramsOrder )) {
				foreach ( $paramsOrder as $key => $dir ) {
					$sql = sprintf ( "%s %s", $key, strtoupper ( $dir ) );
					$select->order ( $sql );
				}
			}
			$entries = array ();
				
			foreach ( $table->fetchAll ( $select ) as $row ) {
				$model = Pages_Model_Block_Factory::getBlock($row->template);
//				$model = new Pages_Model_Block();
				$this->_fromDb ( $model, $row );
				$entries [] = $model;
			}
			$paginator->itemsCount = $this->count ( $select );
				
			return $entries;
		} catch ( Exception $e ) {
			throw $e;
		}
	}

	public function save(Pages_Model_Block $model)
	{
		try {
			$data = $this->_toDb($model);
			
			if (null === ($id = $model->getId()) || empty($id)) {
			$model->notify(Pages_Model_Block_Event::EVENT_BEFORE_SAVE);
				unset($data['id']);
				$this->getMaxOrder($data);
			$id = $this->getDbTable()->insert($data);
			$model->setId($id);
			$model->notify(Pages_Model_Block_Event::EVENT_AFTER_SAVE);
			return $id;
			} else {
				if(empty($data['order'])) $this->getMaxOrder($data);
				$model->notify(Pages_Model_Block_Event::EVENT_BEFORE_UPDATE);
				$this->getDbTable()->update($data, array('id = ?' => $id));
				$model->notify(Pages_Model_Block_Event::EVENT_AFTER_UPDATE);
				return $id;
			}
		} catch (Exception $e) {
			throw $e;
		}
	}

	private function getMaxOrder(&$data, $increment = true)
	{
		try {
		    $table = $this->getDbTable ();
			$select = $table->select();
			$select->from($table, array("IFNULL(MAX(`order`),1) AS ord"))
				->where('page=?', $data['page']);
			$result = $table->fetchRow($select);
			if($increment)
				$data['order'] = $result->ord +1 ;
			else 
				$data['order'] = $result->ord +1 ;
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	//@TODO: ADD support for observers in the correct way!
	public function delete($id)
	{
	    try {
	    	$item = new Pages_Model_Block();
	    	$this->find($id,$item);
	    	
	    	$temp = Pages_Model_Block_Factory::getBlock($item->getTemplate());
	    	$temp->setId($id)
	    		->setOptions($item->getOptions());
	        $table = $this->getDbTable();
	        $temp->notify(Pages_Model_Block_Event::EVENT_BEFORE_DELETE);
	        $where = $table->getAdapter()->quoteInto('id = ?', $id);
	        $table->delete($where);
	        $temp->notify(Pages_Model_Block_Event::EVENT_AFTER_DELETE);
	        return true;
	    } catch(Exception $e)
	    {
	        throw $e;
	    }
	}
	
	public function count(Zend_Db_Select $select) {
		try {
			$table = $this->getDbTable ();
			$select->reset ( Zend_Db_Select::COLUMNS )->reset ( Zend_Db_Select::LIMIT_COUNT )->reset ( Zend_Db_Select::LIMIT_OFFSET )->columns ( array (
					'num' => new Zend_Db_Expr ( 'COUNT(*)' )
			) );
			return $table->fetchRow ( $select )->num;
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	
	private function _filterByUser(Zend_Db_Table_Select &$select)
	{
// 		$loggedUser = Zend_Auth::getInstance()->getStorage()->read();
// 		$loggedUserId = $loggedUser->getId();
// 		return $this;
	}
	
	private function _filterBySearchParam(Zend_Db_Table_Select &$select, $paramsSearch = array()) {
		if (!empty($paramsSearch)) {
	
			if (!empty($paramsSearch['page']))
			{
				if(is_array($paramsSearch['page']))
					$select->where("page IN ?", $paramsSearch['page']);
				else $select->where("page = ?", $paramsSearch['page']);
			}
			if (!empty($paramsSearch['template']))
			{
				if(is_array($paramsSearch['template']))
					$select->where("template IN ?", $paramsSearch['template']);
				else $select->where("template = ?", $paramsSearch['template']);
			}
		}
		return $this;
	}
	
	private function _fromDb(Pages_Model_Block &$model, $row, $load=false) {
		$model->notify(Pages_Model_Block_Event::EVENT_BEFORE_LOAD);
		$model->setId($row->id)
			->setPage($row->page)
			->setTsCreation(null)
			->setTsDelete(null)
			->setTsUpdate(null)
			->setOrder($row->order)
			->setTemplate($row->template)
			->setContents(null)
			->setLang($row->lang)
			->setStatus($row->status);
		
		if($load)
		{
				
		}
	
		if(!is_null($row->ts_creation))
			$model->setTsCreation(NW_Utils_DateTime::strptime($row->ts_creation, "%Y-%m-%d"));
		if(!is_null($row->ts_update))
			$model->setTsUpdate(NW_Utils_DateTime::strptime($row->ts_update, "%Y-%m-%d"));
		if(!is_null($row->ts_delete))
			$model->setTsDelete(NW_Utils_DateTime::strptime($row->ts_delete, "%Y-%m-%d"));
		if(!empty($row->contents))
		{
			$serializedObj = $row->contents;
			$unserialized = unserialize($serializedObj);
			if($unserialized===false) $unserialized =='';
			$model->setContents($unserialized);	
		}
		$model->notify(Pages_Model_Block_Event::EVENT_AFTER_LOAD);
	}
	
	
	private function _toDb(Pages_Model_Block $model) {
		$creationDate = $model->getTsCreation();
		$updateDate = $model->getTsUpdate();
		$deleteDate = $model->getTsDelete();
	
		$contents = serialize($model->getContents());
		$data = array(
				'id' => $model->getId(),
				'page' => $model->getPage(),
				'ts_creation' => (is_null($creationDate) || $creationDate=='') ? date("Y-m-d h:i:s") : strftime("%Y-%m-%d %h:%i:%s", $creationDate),
				'ts_update' => (is_null($updateDate) || $updateDate=='') ? date("Y-m-d h:i:s") : strftime("%Y-%m-%d %h:%i:%s", $updateDate),
				'ts_delete' => (is_null($deleteDate) || $deleteDate=='') ? date("Y-m-d h:i:s") : strftime("%Y-%m-%d %h:%i:%s", $deleteDate),
				'order' => $model->getOrder(),
				'template' => $model->getTemplate(),
				'contents' => $contents,
				'lang' => $model->getLang(),
				'status' => $model->getStatus()
		);
		return $data;
	}


}
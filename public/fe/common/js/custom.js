// Asincronous Google Analytics
//var _gaq=[['_setAccount','UA-38724008-5'],['_trackPageview']];(function(d,t){var //g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.src='//www.google-analytics.com/ga.js';s.parentNode.insertBefore(g,s)}(document,'script'))
//*****************************

//*************************
// LOADING
//*************************
function loading() {
	$('#loader').fadeOut(600);
}

// *************************
// SHARE IT
// *************************
function share() {
	$('#shareme').sharrre({
						share : {
							twitter : true,
							facebook : true,
							googlePlus : true
						},
						template : '<div class="box"><div class="left">Share</div><div class="middle"><a href="#" class="facebook">f</a><a href="#" class="twitter">t</a><a href="#" class="googleplus">+1</a></div><div class="right">{total}</div></div>',
						enableHover : false,
						enableTracking : true,
						render : function(api, options) {
							$(api.element).on('click', '.twitter', function() {
								api.openPopup('twitter');
							});
							$(api.element).on('click', '.facebook', function() {
								api.openPopup('facebook');
							});
							$(api.element).on('click', '.googleplus',
									function() {
										api.openPopup('googlePlus');
									});
						}
					});

}

// ***********************************
// PEOPLE SIDEBAR CAROUSEL
// ***********************************

function people_carousel_sidebar() {

	function mycarousel_initCallback(carousel) {
		// Disable autoscrolling if the user clicks the prev or next button.
		carousel.buttonNext.bind('click', function() {
			carousel.startAuto(0);
		});

		carousel.buttonPrev.bind('click', function() {
			carousel.startAuto(0);
		});

		// Pause autoscrolling if the user moves with the cursor over the clip.
		carousel.clip.hover(function() {
			carousel.stopAuto();
		}, function() {
			carousel.startAuto();
		});
	}
	;

	jQuery(document).ready(function() {
		if(jQuery('#key_people').length!=0) {
			jQuery('#key_people').jcarousel({
				vertical : false,
				auto : 0,
				scroll : 1,
				wrap : 'last',
				initCallback : mycarousel_initCallback
			});
			document.getElementById('key_people').style.visibility = 'visible';
		}
	});
	

}

// *************************
// DROPDOWN MENU
// *************************
$(function() {

	$("#main_menu ul li a").hover(function() {
		$(this).addClass("current");
		$('#main_menu_panel').addClass("current_mainmenu");
	});

	$("#main_menu ul li a#menu1_link").hover(function() {
		$("#main_menu ul li a").removeClass("current");
		$("#main_menu ul li a#menu1_link").addClass("current");
		$('.menu').removeClass("current");
		$('#menu1').addClass("current");
	});

	$("#main_menu ul li a#menu2_link").hover(function() {
		$("#main_menu ul li a").removeClass("current");
		$("#main_menu ul li a#menu2_link").addClass("current");
		$('.menu').removeClass("current");
		$('#menu2').addClass("current");
	});

	$("#main_menu ul li a#menu3_link").hover(function() {
		$("#main_menu ul li a").removeClass("current");
		$("#main_menu ul li a#menu3_link").addClass("current");
		$('.menu').removeClass("current");
		$('#menu3').addClass("current");
	});

	$("#main_menu ul li a#menu4_link").hover(function() {
		$("#main_menu ul li a").removeClass("current");
		$("#main_menu ul li a#menu4_link").addClass("current");
		$('.menu').removeClass("current");
		$('#menu4').addClass("current");
	});

	$("#main_menu_panel").mouseleave(function() {
		close_mainmennu_panel();
	});

	$("#menu_controller").hover(function() {
		close_mainmennu_panel();
	});

	$("#language_menu").hover(function() {
		close_mainmennu_panel();
	});

	$("#logo").hover(function() {
		close_mainmennu_panel();
	});

});

function close_mainmennu_panel() {
	$('#main_menu_panel').removeClass("current_mainmenu");
	$("#main_menu ul li a").removeClass("current");
	$('.menu').removeClass("current");
}

// *************************
// FANCYBOX
// *************************
function fancybox() {
	$("a.zoom").fancybox({
		padding : 10,
		maxWidth : '95%',
		maxHeight : '98%',
		fitToView : true,
		autoSize : true,
		closeClick : false,
		openEffect : 'none',
		closeEffect : 'none'
	});

	$("a.single_image").fancybox();
}

// *************************
// SLIDER HOME
// *************************
function slideshow() {
	$('#slideshow').nivoSlider({
		effect : 'fold', // Specify sets like: 'fold,fade,sliceDown'
		slices : 7, // For slice animations
		animSpeed : 800, // Slide transition speed
		pauseTime : 5000, // How long each slide will show
		directionNav : true, // Next & Prev navigation
		controlNav : false, // 1,2,3... navigation
		controlNavThumbs : false, // Use thumbnails for Control Nav
		pauseOnHover : false
	// Stop animation while hovering
	});
}


// *************************
// PROJECTS ACCORDION
// *************************
function projects_accordion() {
$("#accordion .expanded").hide();
    $("a.opening").click(function(){
        $(this).next().slideToggle('fast', function(){
            $(this).prev("a.opening").toggleClass("active");
        });
    return false;
});
}
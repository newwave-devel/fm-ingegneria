<?php if($tagBlocks):?>
<section class="project_tags">

	<h4 class="title">Tag</h4>
	<ul>
	<?php 
		foreach($tagBlocks as $item)
			echo sprintf('<li><a href="discipline.php?id=%d">%s</a></li>', $item->getPage(), $item->_("title"));
	?>
	</ul>
</section>
<!--project_tags-->
<?php endif; ?>
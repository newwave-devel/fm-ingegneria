<?php
class Pages_Model_Block_Shared_Service
{
	protected $_mapper;

    public function getMapper() {
        return $this->_mapper;
    }

    public function __construct() {
        $this->_mapper = new Pages_Model_Block_Shared_Mapper();
    }
	
	public function find($paramID)
	{
		try{
			$model = new Pages_Model_Block();
			$this->_mapper->find($paramID, $model);
			return $model;
		} catch(Exception $e)
		{
			
		}
	}

	
	public function fetchAll(NW_Pages_Paginator &$paginator, $paramsSearch = array(), $paramsOrder = array(), $load = false)
	{
		try 
		{
			return $this->_mapper->fetchAll($paginator, $paramsSearch, $paramsOrder, $load);
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function save(Pages_Model_Block_Composable $block)
	{
		try {
			return $this->_mapper->save($block);
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	public function saveShared(Pages_Model_Page $page, $arrayFromForm)
	{
		try {
			$blocks = array();
			$pageId = $page->getId();
			foreach($arrayFromForm as $field => $datas)
			{
				if($field == 'page') continue;
				$ids_as_string = trim($datas['id']);
				
				if(empty($ids_as_string)) continue;
				$ids_as_arr = explode(",", $ids_as_string);
				if(!is_array($ids_as_arr) && !is_empty($ids_as_arr))
					$ids_as_arr = array($ids_as_arr);

				$pos = 0;
				foreach($ids_as_arr as $blockId)
				{
					$block = new Pages_Model_Block_Shared();
					$block->setPage($pageId)
						->setBlock($blockId)
						->setPosition($field)
						->setOrder(++$pos);
					$blocks[] = $block;
				}
				
			}
			return $this->_mapper->saveShared($blocks, true, $pageId);
		} catch (Exception $e) {
			throw $e;
		}
	}

	
	public function delete($id)
	{
	    try {
	        $this->getMapper()->delete($id);
	        return true;
	    } catch (Exception $e) 
	    {
	        throw $e;
	    }
	}
	public function dropPage($id)
	{
		try {
			$this->getMapper()->dropPage($id);
			return true;
		} catch (Exception $e)
		{
			throw $e;
		}
	}
	
	public function byPage($pageId, NW_Pages_Paginator &$paginator=null, $paramsSearch = array(), $paramsOrder = array(), $load = false)
	{
		try{
			if(empty($paginator))
				$paginator = NW_Pages_Paginator::fetchAll();
			
			$paramsSearch['page'] = $pageId;
			$result = $this->fetchAll($paginator,$paramsSearch,$paramsOrder,$load);
			if($paginator->itemsPerPage==1 )
			   return (!empty($result)) ? current($result) : array();
			return $result;
			    
		}catch(Exception $e)
		{
			throw $e;
		}
	}	

	
	
}
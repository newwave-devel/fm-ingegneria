<?
//$prepath = '../../../';
defined('APPLICATION_PATH')
        || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../application'));
// Define application environment
defined('APPLICATION_ENV')
        || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));
        
//defined('PUBLIC_PATH') ||
        define('PUBLIC_PATH', realpath(APPLICATION_PATH.'/../public'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
            realpath(APPLICATION_PATH . '/../library'),
            get_include_path(),
        )));
//
require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance();
//
//// Create application, bootstrap, and run
$application = new Zend_Application(
                APPLICATION_ENV,
                APPLICATION_PATH . '/configs/application.ini'
);
error_reporting(E_ALL);
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);
 

// Initialize and retrieve DB resource
$bootstrap = $application->getBootstrap();
$bootstrap->bootstrap();

function cronLog($proc_id, $status, $log='') {
    try {
        $adapter = Zend_Db_Table::getDefaultAdapter();

        $adapter->insert('tbl_cron_log', array('ts' => new Zend_Db_Expr('NOW()'),
            'proc_id' => $proc_id,
            'log' => $log,
            'status' => $status
        ));
    } catch (Exception $e) {
        //
    }
}


$opts = getopt('l:');
umask(0);
$cronLevel = ($opts['l'] === false) ? 0 : $opts['l'];
switch($cronLevel)
{
	case '0':
		require_once(APPLICATION_PATH . '/modules/certificates/cron/cron.php');
    case '5':
    	require_once(APPLICATION_PATH . '/modules/app/cron/cron.php');
    	break;
}


echo 'done';
?>
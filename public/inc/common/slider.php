<?php if($slides): ?>
<section id="home_slider">

	<section id="slideshow" class="nivoSlider">
	<?php 
	require_once("inc/tpls/blocks/slide/tpl.php");
	
	$common_slider_count = 1;
	$common_slider_imgs = '';
	$common_slider_captions = '';
	foreach($slides as $slide)
	{
		$file = $blockService->asset($slide->_('file'));
		$common_slider_imgs .= sprintf($tpl_img, $slide->_('link'), $file->getAssetUrl(), $common_slider_count);
		$common_slider_captions .= sprintf($tpl_caption, $common_slider_count, $slide->_('link'), $slide->_('description'));
		$common_slider_count += 1;
	}
	echo $common_slider_imgs;
	?>
		
	</section>
	<!--slideshow-->

	<?php echo $common_slider_captions; ?>

</section>
<!--home_slider-->
<?php endif; ?>
<?php
if($blocks):
	require_once("inc/tpls/blocks/list/tpl.php");
	require_once("inc/functions/blocks.php");
	$col_sx = '';
	$col_dx = '';
	$odd = true;
	foreach($blocks as $sharedBlock)
	{
		$page = $sharedBlock->getPage();
		if($odd)
		{
			$col_sx .= ($page->getTemplate() == 'news') ? renderListBlockNews($tpl_item_news, $sharedBlock, $blockService, $odd, true) : 
																		renderBlockProject($tpl_item_project, $sharedBlock, $blockService, $odd, true);
			$odd = false;
		} else {
			$col_dx .= ($page->getTemplate() == 'news') ? renderListBlockNews($tpl_item_news, $sharedBlock, $blockService, $odd, true) :
																	renderBlockProject($tpl_item_project, $sharedBlock, $blockService, $odd, true);
			$odd = true;
		}
		
	}
	?>
	<section class="countries_articles_container">
		<?php echo $col_sx ?>
		<?php echo $col_dx ?>
	</section>
	<?php unset($col_sx); ?>
	<?php unset($col_sx); ?>
	<?php unset($blocks); ?>
<?php endif; ?>
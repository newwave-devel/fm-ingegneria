<?php
/**
 * Description of Paginator
 *
 * @author Ale
 */
class NW_Pages_Paginator
{
    public $curPage;
    public $itemsPerPage;
    public $itemsCount;
    public $numPages;
    public $hasPages;
    public $start; 
    public $isAjax;
    public $targetId;
    public $fetchAll = false;
    public $ajaxAction = null;
    
    const REQUEST_PARAM_PAG = 'pag';
    const REQUEST_PARAM_ITEMS_PER_PAGE = 'ipp';
    
    const DEFAULT_ITEMS_PER_PAGE = 25;
    
    public function __construct()
    {
       $this->curPage = 1;
       $this->itemsPerPage = self::DEFAULT_ITEMS_PER_PAGE;
       $this->itemsCount = 0;
       $this->numPages = 1;
       $this->hasPages = false;
       $this->start = 0;
       $this->isAjax = false;
       $this->targetId = '';
       $this->fetchAll= false;
       $this->ajaxAction = null;
    }
    
    public static function __fromRequest(Zend_Controller_Request_Abstract &$request)
    {
        $item = new NW_Pages_Paginator();
        $curPage = $request->getParam(self::REQUEST_PARAM_PAG, 1);
        $ipp = $request->getParam(self::REQUEST_PARAM_ITEMS_PER_PAGE, self::DEFAULT_ITEMS_PER_PAGE);
        $curPage = empty($curPage) ? 1 : $curPage;
        $ipp = empty($ipp) ? self::DEFAULT_ITEMS_PER_PAGE: $ipp;
        $item->setCurPage($curPage)
                ->setItemsPerPage($ipp);
        return $item;
    }
    
    public static function fetchAll()
    {
    	$item = new NW_Pages_Paginator();
    	$item->setFetchAll(true);
    	return $item;
    }
    
    public static function fetchSingle()
    {
        $item = new NW_Pages_Paginator();
    	$item->setItemsPerPage(1);
    	return $item;
    }
    
    public function setFetchAll($fetchAll)
    {
    	$this->fetchAll = $fetchAll;
    	$this->itemsPerPage = self::DEFAULT_ITEMS_PER_PAGE;
    	return $this;
    }
    
    public function getFetchAll()
    {
    	return $this->fetchAll;
    }
    
    public function getIsAjax() {
        return $this->isAjax;
    }

    public function setIsAjax($isAjax, $targetId='', $action= '') {
        $this->isAjax = $isAjax;
        $this->targetId = $targetId;
        $this->ajaxAction = $action;
        return $this;
    }

    
	public function getAjaxAction() {
		return $this->ajaxAction;
	}

	
	public function setAjaxAction($ajaxAction) {
		$this->ajaxAction = $ajaxAction;
		return $this;
	}

	public function getTargetId() {
        return $this->targetId;
    }

    public function setTargetId($targetId) {
        $this->targetId = $targetId;
        return $this;
    }

    public function getCurPage() {
        return $this->curPage;
    }

    public function setCurPage($curPage) {
        $this->curPage = $curPage;
        return $this;
    }

    public function getItemsPerPage() {
        return $this->itemsPerPage;
    }

    public function setItemsPerPage($itemsPerPage) {
        $this->itemsPerPage = $itemsPerPage;
        return $this;
    }

    public function getItemsCount() {
        return $this->itemsCount;
    }

    public function setItemsCount($itemsCount) {
        $this->itemsCount = $itemsCount;
        return $this;
    }

    public function hasPages() {
        return $this->hasPages;
    }

    public function setHasPages($hasPages) {
        $this->hasPages = $hasPages;
        return $this;
    }

    public function getNumPages()
    {
        return $this->numPages; 
    }
    
    public function getStart()
    {
        return ($this->getCurpage() - 1) * $this->getItemsPerPage();
    }
    public function prepare()
    {
        $this->hasPages = ($this->getItemsCount () > $this->getItemsPerPage ());
        $this->numPages = ceil($this->itemsCount / $this->itemsPerPage);
        return $this;      
    }
    public function getId($param='')
    {
        return substr($this->targetId, 1).$param;
    }
   
}

?>

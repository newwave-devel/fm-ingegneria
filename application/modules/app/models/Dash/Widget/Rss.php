<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Repubblica
 *
 * @author Ale
 */
class App_Model_Dash_Widget_Rss extends App_Model_Dash_Widget {
    
    private $_uri;
    private $_channel;
    
    
    public function __construct($options) {
        $this->_uri = $options['uri'];
        $this->boxTitle = isset($options[static::BOXTITLE]) ? $options[static::BOXTITLE] : "";
        $this->boxPosition = isset($options[static::BOXPOSITION]) ? $options[static::BOXPOSITION] : '';
        $this->_fetchRss();
    }
    
    

    private function _fetchRss() {
        try {
            $this->_channel = new Zend_Feed_Rss($this->_uri);
            $this->boxTitle = $this->_channel->title();
            $this->boxContent = $this->parse();
        } catch (Exception $e) {
            $this->boxContent = "Cannot connect to " . $this->_uri;
        }
    }
    
    public function parse() {  
        $content = "<ul>";
        foreach ($this->_channel as $item)
            $content .= sprintf("<li>%s<a href='%s'> &raquo;</a></li>", $item->title(),$item->link());
        $content .= "</ul>";
        return $content;

    }

}

?>

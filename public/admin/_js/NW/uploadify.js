NW.Uploadify = (function () {
    // Initialize the application
    var items = {};
    
    uploadifier = function(itemref)
    {
        
        this.itemRef = '.uploadify#'+itemref;
        this.classSelector = '.'+itemref;
        this.fieldName = $(this.classSelector +'[name="fieldName"]').val();
        this.canSubmit = false;
        this.boundedFields = [];
        this.validFiles = 0;
        
        this.options = {
            'swf' : '/js/vendors/uploadify/uploadify.swf',
            'uploader' : 'upload',
            'auto' : false,
            'buttonClass' : '',
            'buttonCursor' : 'hand',
            'buttonText' : null,
            'checkExisting' : false,
            'cancelImg': '/js/vendors/uploadify/uploadify-cancel.png',
            'debug' : false,
            'fileObjName' : 'uploaded',
            'fileSizeLimit' : 0,
            'fileTypeDesc' : 'All Files',
            'fileTypeExts' : '*.*',
            'formData' : {},
            'folder' : null,
            'height' : 30,
            'method' : 'post',
            'multi' : false,
            'overrideEvents' : [],
            'preventCaching' : true,
            'progressData' : 'percentage',
            'queueID' : false,
            'queueSizeLimit' : 999,
            'removeCompleted' : true,
            'removeTimeout' : 3,
            'requeueErrors' : false,
            'successTimeout' : 30,
            'uploadLimit' : 0,
            'width' : 120,
            'onCancel' : null,
            'onClearQueue' : null,
            'onDestroy' : null,
            'onDialogClose' : null,
            'onDialogOpen' : null,
            'onDisable' : null,
            'onEnable' : null,
            'onFallback' : null,
            'onInit' : null,
            'onQueueCompleted' : null,
            'onSelect' : function(file) {
               this.validFiles +=1;
            },
            'onSelectError' :function(){
            	this.validFiles -=1;
            },
            'onSWFReady' : null,
            'onUploadCompleted' : null,
            'onUploadError' : null,
            'onUploadProgress' : null,
            'onUploadStart' : null ,
            'onUploadSuccess' : function(file, data, response) {console.log(data);},
            'cancel' : null,
            'destroy' : null,
            'disable' : null,
            'settings' : null,
            'stop' : null,
            'upload' : null
        };
    
    }
    
    uploadifier.prototype = {
        setOptions : function(options)
        {
            for(var key in options)
                this.options[key] = options[key];
            return this;
        },
        setOption : function(param, value)
        {
            this.options[param] = value;
            return this;
        },
        uploadifyItem : function(options)
        {   
            options = (options === undefined) ? this.options : options;
            var newOptions = [];
            for (var key in options)
            	{
            		if(options[key] == null) continue;
            			newOptions[key] = options[key];
            	}
            $(this.itemRef).uploadify(newOptions);
        },
        lateParam: function(param, value)
        {
            $(this.itemRef).uploadify('settings', param, value, true);
            return this;
        },
        bindFields: function()
        {
            var params = arguments;
            
            for(var i=0; i<params.length; i++)
                this.boundedFields.push(params[i]);

            return this;
        },
        //to be called before send
        bind: function()
        { 
            var formDatas = [];
            if(this.boundedFields.length != 0)
            {
                for(var i=0; i< this.boundedFields.length; i++)
                {
                    fieldName = this.boundedFields[i];
                    fieldRef = $("#" + fieldName);
                    fieldVal = jQuery.trim(($(fieldRef)).val());
                    
                    if(fieldRef.hasClass("required") && fieldVal =='')
                    {
                    	console.log("campo "+ fieldName+ "non valido");
                        this.canSubmit = false;
                    }
                    else 
                        formDatas[fieldName] = fieldVal;
                    console.log("binding.. "+ fieldName);
                }
                
            }
            return formDatas;
        },
        upload: function()
        {
            this.canSubmit = true;
            var formDatas = this.bind();
            console.log(formDatas);
            console.log(this.canSubmit);
            console.log(this);
            if(this.canSubmit)
            {
            	if(this.validFiles > 0)
                {
            		$(this.itemRef).uploadify('settings', 'formData', formDatas);
            		$(this.itemRef).uploadify("upload");
                }
            	else console.log("submitting anyway");
            }
            else alert("Check all fields");
        }
    }
    
    
    // Return the public facing methods for the App
    return {
        add: function (paramId) {
            items[paramId] = new uploadifier(paramId);
            return items[paramId];
        },
        setOption: function (paramId, property, value) {
            items[paramId].options[property] = value;
            return items[paramId];
        },
        setOptions: function (paramId, properties) {
            items[paramId].setOptions(properties);
            return items[paramId];
        },
        uploadifyItem: function(paramId)
        {
            items[paramId].uploadifyItem();
            return items[paramId];  
        },
        get: function(paramId){
            return items[paramId];
        }
    };
}());


var NW_Uploadify = NW.Uploadify;
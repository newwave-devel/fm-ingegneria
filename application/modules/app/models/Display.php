<?php
class App_Model_Display
{
	private $_lang;
	private $_website;
	private $_langs;
	
	public function __construct($lang='', $website='')
	{
		
	}
	
	public static function defaultDisplay()
	{
        global $application;
		$display = new self();
        $options = $application->bootstrap()->getOption('mosaic');
		$display->_website = $options["websites"]["default"];
		$display->_langs = $options["languages"];
		$display->_lang = $display->_langs[0];
		return $display;
	}
	
	public function getLang()
	{
		return $this->_lang;
	}
	
	public function getLangs()
	{
		return $this->_langs;
	}
	
	public function isMultilanguage()
	{
		return (count($this->_langs)!=0);
	}
	
	public function langsAsSelect()
	{
		$langs = array();
		foreach($this->_langs as $lang)
		{
			$langs[$lang] = strtoupper($lang);
		}
		return $langs;
	}
	
	public function setLang($lang)
	{
		$this->_lang = $lang;
		return $this;
	}
	
	public function setLangs($langs)
	{
		$this->_langs = $langs;
		return $this;
	}
	
	public function setWebsite($ws)
	{
		$this->_website = $ws;
		return $this;
	}
}
?>
<?php
	$basepath = dirname ( __FILE__ );
	// Define path to application directory
	defined ( 'APPLICATION_PATH' ) || define ( 'APPLICATION_PATH', realpath ( $basepath . '/../../application' ) );

	// Define application environment
	defined ( 'APPLICATION_ENV' ) || define ( 'APPLICATION_ENV', (getenv ( 'APPLICATION_ENV' ) ? getenv ( 'APPLICATION_ENV' ) : 'production') );

	defined ( 'APP_PATH' ) || define ( 'APP_PATH', APPLICATION_PATH . DIRECTORY_SEPARATOR . "../" );

	// Ensure library/ is on include_path
	set_include_path ( implode ( PATH_SEPARATOR, array (realpath ( APPLICATION_PATH . '/../library' ), get_include_path () ) ) );

	require_once 'Zend/Loader/Autoloader.php';

	// instantiate the loader
	$loader = Zend_Loader_Autoloader::getInstance ();

	$db = Zend_Db::factory('Pdo_Mysql', array(
			'host'     => 'localhost',
			'username' => 'root',
			'password' => '',
			'dbname'   => 'sealx'
	));
	Zend_Db_Table::setDefaultAdapter($db);
	
	$loader->registerNamespace ( 'NW_' )->registerNamespace ( "App_" );

	$moduleLoder = new Zend_Application_Module_Autoloader(
			array(
					'namespace'=>'Pages',
					'basePath'=> APPLICATION_PATH . '/modules/pages'
			));

	$moduleLoder = new Zend_Application_Module_Autoloader(
			array(
					'namespace'=>'App',
					'basePath'=> APPLICATION_PATH . '/modules/app'
			));
	$moduleLoder = new Zend_Application_Module_Autoloader(
			array(
					'namespace'=>'Profiles',
					'basePath'=> APPLICATION_PATH . '/modules/profiles'
			));
	
	

?>
<?php

/**
 * @author Ale
 * used only for reading db from db and translating
 */
class Pages_Model_Taxonomy_Row
{

   public $parent;
   public $dependent;
   public $taxonomy;
   public $order;
   public $invokerRole;
   
   public function __construct($options = array())
   {
   		$this->parent = isset($options['parent'])  ? $options['parent'] : '';
   		$this->dependent = isset($options['dependent'])  ? $options['dependent'] : '';
   		$this->taxonomy = isset($options['taxonomy'])  ? $options['taxonomy'] : '';
   		$this->order = isset($options['order'])  ? $options['order'] : '';
   		$this->invokerRole = isset($options['invokerRole'])  ? $options['invokerRole'] : '';
   }
/**
	 * @return the $parent
	 */
	public function getParent()
	{
		return $this->parent;
	}

/**
	 * @return the $dependent
	 */
	public function getDependent()
	{
		return $this->dependent;
	}

/**
	 * @return the $taxonomy
	 */
	public function getTaxonomy()
	{
		return $this->taxonomy;
	}

/**
	 * @return the $order
	 */
	public function getOrder()
	{
		return $this->order;
	}

/**
	 * @return the $invokerRole
	 */
	public function getInvokerRole()
	{
		return $this->invokerRole;
	}

/**
	 * @param field_type $parent
	 */
	public function setParent($parent)
	{
		$this->parent = $parent;
	}

/**
	 * @param field_type $dependent
	 */
	public function setDependent($dependent)
	{
		$this->dependent = $dependent;
	}

/**
	 * @param field_type $taxonomy
	 */
	public function setTaxonomy($taxonomy)
	{
		$this->taxonomy = $taxonomy;
	}

/**
	 * @param field_type $order
	 */
	public function setOrder($order)
	{
		$this->order = $order;
	}

/**
	 * @param field_type $invokerRole
	 */
	public function setInvokerRole($invokerRole)
	{
		$this->invokerRole = $invokerRole;
	}


   
    
    
}
function deleteAction(data, textStatus, jqXHR)
{
    var result = data.status;
    var itemId = data.payload.id;
    switch(result)
    {
        case 'success':
            var domQuery = "tr#"+itemId;
            $(domQuery).hide();
            break;
        default:
            alert("Si sono verificati degli errori");
            break;
    }
    
}

function togglestatusAction(data, textStatus, jqXHR)
{
    var result = data.status;
    var itemId = data.payload.id;
    var image = data.payload.image;
    switch(result)
    {
        case 'success':
            var domQuery = "a#togglestatus"+itemId;
            var itemRef = $(domQuery);
            itemRef.html('<img src="' + image +'" />');
            break;
        default:
            alert("Si sono verificati degli errori");
            break;
    }
    
}
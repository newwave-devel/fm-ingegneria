<?php
class Pages_Model_Page_Factory
{
	public static function getPage($pagetemplate)
	{
		$item = new Pages_Model_Page();
		$item->setTemplate($pagetemplate);
		NW_Pages_Page::getInstance()->attachObservers($item);
		return $item;
	}
}
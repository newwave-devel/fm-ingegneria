<?php
class Pages_Model_Block_Shared_Event
{
	const EVENT_BEFORE_LOAD = 7;
	const EVENT_AFTER_LOAD = 8;
    const EVENT_BEFORE_SAVE = 1;
    const EVENT_AFTER_SAVE = 2;
    const EVENT_BEFORE_UPDATE = 3;
    const EVENT_AFTER_UPDATE = 4;
    const EVENT_ADD = 5;
    const EVENT_TRANSLATE = 6;
    const EVENT_BEFORE_DELETE = 9;
    const EVENT_AFTER_DELETE = 10;
}
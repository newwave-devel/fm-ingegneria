<?php

abstract class NW_Adapters_Adapter
{

    private $_conn_name;

    public function __construct($namespace, $conn_name)
    {
        $this->_conn_name = $namespace . $conn_name;
    }

    public function enrollSingleton()
    {
        NW_Application::enrollSingleton($this, NW_ClassURI::fromString($this->_conn_name));
    }

}

?>
<?php
class Pages_Model_Page_Mapper {
	protected $_dbTable;
	
	private $_taxonomies = array ();
	private $_display = null;
	private $_asCollection = false;
	
	public function setDbTable($dbTable) {
		if (is_string ( $dbTable )) {
			$dbTable = new $dbTable ();
		}
		if (! $dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception ( 'Invalid table data gateway provided' );
		}
		$this->_dbTable = $dbTable;
		return $this;
	}
	
	public function getDbTable() {
		if (null === $this->_dbTable) {
			$this->setDbTable ( 'Pages_Model_DbTable_Page' );
		}
		return $this->_dbTable;
	}
	
	public function addTaxonomy(Pages_Model_Taxonomy $taxonomy) {
		try {
			$this->_taxonomies [] = $taxonomy;
			return $this;
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	
	public function setDisplay(App_Model_Display $display) {
		try {
			$this->_display = $display;
			return $this;
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	
	public function dropTaxonomies() {
		try {
			$this->_taxonomies = array ();
			return $this;
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	
	public function find($id, Pages_Model_Page $model) {
		try {
			$result = $this->getDbTable ()->find ( $id );
			if (0 == count ( $result )) {
				return;
			}
			$row = $result->current ();
			$this->_fromDb ( $model, $row );
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	public function load($id, $lang) {
	}
	
	public function setAsCollection()
	{
		$this->_asCollection = true;
		return $this;
	}
	
	public function requestCount($paramsSearch)
	{
		try{
			$table = $this->getDbTable ();
			$select = $table->select ()->from ( $table );
			$this->_filterBySearchParam ( $select, $paramsSearch )->_filterByUser ( $select );
			return $this->count ( $select );
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	/**
	 *
	 * @param NW_Pages_Paginator $paginator        	
	 * @param type $paramsSearch        	
	 * @return \Purchase_Model_OrderRequest, array
	 * @throws Exception
	 */
	public function fetchAll(NW_Pages_Paginator &$paginator, $paramsSearch, $paramsOrder = array('order' => 'asc'), $load) {
		try {
			$table = $this->getDbTable ();
			$select = $this->_select($table);
			
			$this->_filterBySearchParam ( $select, $paramsSearch )->_filterByUser ( $select )->_addTaxonomies ( $select );
			if (! $paginator->getFetchAll ())
				$select->limit ( $paginator->itemsPerPage, $paginator->getStart () );
			
			if (! empty ( $paramsOrder )) {
				foreach ( $paramsOrder as $key => $dir ) {
					$sql = sprintf ( "%s %s", $key, strtoupper ( $dir ) );
					$select->order ( $sql );
				}
			}
			
			$entries = $this->_populate($table->fetchAll ( $select ));
			
			$paginator->itemsCount = $this->count ( $select );
			$this->_clearCollection();
			return $entries;
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	
	private function _select($table)
	{
		if(!$this->_asCollection)
			return $table->select ()->from ( $table );
		else
			return $table->select ()->from ( $table,array("id"));
	}
	
	private function _populate($rows, $load=false)
	{
		$entries = array();
		if(!$this->_asCollection)
		{
			foreach ($rows as $row ) {
				$model = new Pages_Model_Page ();
				$this->_fromDb ( $model, $row, $load );
				$entries [] = $model;
			}
		} else {
			foreach ( $rows as $row )
				$entries [] = $row->id;
		}
		
		return $entries;
	}
	
	private function _clearCollection()
	{
		$this->_asCollection = false;
	}
	
	public function fetchAvailable(NW_Pages_Paginator &$paginator, $paramsSearch, $paramsOrder = null, $load = false) {
		try {
			$table = $this->getDbTable ();
			$select = $this->_select($table);
	
			$taxonomy = $this->_taxonomies [0];
	
			$paramsSearch ['type'] = $taxonomy->getRelated ();
	
			$this->_filterBySearchParam ( $select, $paramsSearch )->_filterByUser ( $select )->_addTaxonomies ( $select, true );
	
			if (! $paginator->getFetchAll ())
				$select->limit ( $paginator->itemsPerPage, $paginator->getStart () );
	
			if (! empty ( $paramsOrder )) {
				foreach ( $paramsOrder as $key => $dir ) {
					$sql = sprintf ( "%s %s", $key, strtoupper ( $dir ) );
					$select->order ( $sql );
				}
			}

			$entries = $this->_populate($table->fetchAll ( $select ));

			$paginator->itemsCount =$this->count ( $select );
			$this->_clearCollection();
			return $entries;
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	
	public function fetchDependent(NW_Pages_Paginator &$paginator, $paramsSearch, $paramsOrder = null, $load = false) {
		try {
			$table = $this->getDbTable ();
			$select = $this->_select($table);
			
			$taxonomy = $this->_taxonomies [0];

				
			$this->_filterBySearchParam ( $select, $paramsSearch )->_filterByUser ( $select )->_addTaxonomies ( $select );
				
			if (! $paginator->getFetchAll ())
				$select->limit ( $paginator->itemsPerPage, $paginator->getStart () );
				
			if (! empty ( $paramsOrder )) {
				foreach ( $paramsOrder as $key => $dir ) {
					$sql = sprintf ( "%s %s", $key, strtoupper ( $dir ) );
					$select->order ( $sql );
				}
			}

			$entries = $this->_populate($table->fetchAll ( $select ));

			$paginator->itemsCount = $this->count ( $select );
			$this->_clearCollection();	
			return $entries;
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	
	public function fetchParents(NW_Pages_Paginator &$paginator, $paramsSearch, $paramsOrder = array(), $load = false) {
		try {
			$table = $this->getDbTable ();			
			
			$taxonomy = $this->_taxonomies [0];
			$select = $this->_select($table);
				
			$this->_addTaxonomies ( $select, false );
			$this->_filterBySearchParam ( $select, $paramsSearch )->_filterByUser ( $select );
			if (! $paginator->getFetchAll ())
				$select->limit ( $paginator->itemsPerPage, $paginator->getStart () );
			
			if (! empty ( $paramsOrder )) {
				foreach ( $paramsOrder as $key => $dir ) {
					$sql = sprintf ( "%s %s", $key, strtoupper ( $dir ) );
					$select->order ( $sql );
				}
			}
            //echo "<!--".$select."-->";
			$entries = $this->_populate($table->fetchAll ( $select ));

			$paginator->itemsCount = $this->count ( $select );
			$this->_clearCollection();
			return $entries;
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	public function save(Pages_Model_Page &$model) {
		try {
			if (null === ($id = $model->getId ()) || empty ( $id )) {
			    $model->notify(Pages_Model_Page_Event::EVENT_BEFORE_SAVE);
			    $data = $this->_toDb ( $model );
				unset ( $data ['id'] );
				$this->getMaxOrder ( $data );
				$id = $this->getDbTable ()->insert ( $data );
				$model->setId($id);
				$model->notify(Pages_Model_Page_Event::EVENT_AFTER_SAVE);
				return $id;
			} else {
			    $model->notify(Pages_Model_Page_Event::EVENT_BEFORE_UPDATE);
			    $data = $this->_toDb ( $model );
				if (empty ( $data ['order'] ))
					$this->getMaxOrder ( $data );
				$this->getDbTable ()->update ( $data, array (
						'id = ?' => $id 
				) );
				$model->notify(Pages_Model_Page_Event::EVENT_AFTER_UPDATE);
				return $id;
			}
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	
	public function saveTranslation(Pages_Model_Page &$model) {
		try {
			    $model->notify(Pages_Model_Page_Event::EVENT_BEFORE_TRANSLATE); 
			    $data = $this->_toDb ( $model );
				unset ( $data ['id'] );
				$this->getMaxOrder ( $data );
				$id = $this->getDbTable ()->insert ( $data );
				$model->setId($id);
				$model->notify(Pages_Model_Page_Event::EVENT_AFTER_TRANSLATE);
				return $id;
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	private function getMaxOrder(&$data, $increment = true) {
		try {
			$table = $this->getDbTable ();
			$select = $table->select ();
			$select->from ( $table, array (
					"IFNULL(MAX(`order`),1) AS ord" 
			) )->where ( 'template=?', $data ['template'] );
			$result = $table->fetchRow ( $select );
			if ($increment)
				$data ['order'] = $result->ord + 1;
			else
				$data ['order'] = $result->ord + 1;
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	public function delete(Pages_Model_Page $page) 
	{
	    try {
	        $table = $this->getDbTable();
	        $blockService = new Pages_Model_Block_Service();
	        $taxonomyService = new Pages_Model_Taxonomy_Service();
	        $pageId  = $page->getId();
	        $table->getAdapter()->beginTransaction();
	        //empties blocks
	        $page->notify(Pages_Model_Page_Event::EVENT_BEFORE_DELETE);
	        foreach($blockService->byPage($page->getId()) as $block)
	            $blockService->delete($block->getId());
	        //empties taxonomies
	        $taxonomyService->dropPageFromTaxonomy($pageId);
	        
	        //drop tables
	        $where = $table->getAdapter()->quoteInto('id = ?', $pageId);
	        $table->delete($where);
	        
	        $this->getDbTable()->getAdapter()->commit();
	        $page->notify(Pages_Model_Page_Event::EVENT_AFTER_DELETE);
	        return true;
	   } catch ( Exception $e ) {
	       $this->getDbTable()->getAdapter()->rollback();
	       throw $e;
	   }
	    
	}
	public function count(Zend_Db_Select $select) {
		try {
			$table = $this->getDbTable ();
			$select->reset ( Zend_Db_Select::COLUMNS )->reset ( Zend_Db_Select::LIMIT_COUNT )->reset ( Zend_Db_Select::LIMIT_OFFSET )->columns ( array (
					'num' => new Zend_Db_Expr ( 'COUNT(*)' ) 
			) );
			
			return $table->fetchRow ( $select )->num;
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	private function _filterByUser(Zend_Db_Table_Select &$select) {
		// $loggedUser = Zend_Auth::getInstance()->getStorage()->read();
		// $loggedUserId = $loggedUser->getId();
		return $this;
	}
	
	private function _filterBySearchParam(Zend_Db_Table_Select &$select, $paramsSearch = array()) {
		$tableName = $select->getPart ( Zend_Db_Select::FROM );
		$tableName = is_array ( $tableName ) ? key ( $tableName ) : $tableName;
		
		if (! empty ( $paramsSearch )) {
			
			if (! empty ( $paramsSearch ['template'] )) {
				if (is_array ( $paramsSearch ['template'] ))
					$select->where ( "$tableName.template IN (?)", $paramsSearch ['template'] );
				else
					$select->where ( "$tableName.template = ?", $paramsSearch ['template'] );
			}
			if (! empty ( $paramsSearch ['type'] )) {
				if (is_array ( $paramsSearch ['type'] ))
					$select->where ( "$tableName.template IN (?)", $paramsSearch ['type'] );
				else
					$select->where ( "$tableName.template = ?", $paramsSearch ['type'] );
			}
			if (! empty ( $paramsSearch ['free'] ))
				$select->where ( "$tableName.label LIKE ?", '%' . $paramsSearch ['free'] . '%' );
			if (! empty ( $paramsSearch ['labelstrict'] ))
				$select->where ( "$tableName.label = ?", $paramsSearch ['labelstrict'] );
			if (! empty ( $paramsSearch ['lang'] ))
				$select->where ( "$tableName.lang = ?", $paramsSearch ['lang'] );
			else 
			{
				$lang = (empty($this->_display)) ? App_Model_Display_Service::getInstance()->lang() : $this->_display->getLang();
				$select->where ( "$tableName.lang = ?", $lang );
			}
			if (! empty ( $paramsSearch ['status'] )) {
				if (is_array ( $paramsSearch ['status'] ))
					$select->where ( "$tableName.status IN (?)", $paramsSearch ['status'] );
				else
					$select->where ( "$tableName.status = ?", $paramsSearch ['status'] );
			}
			if(isset($paramsSearch ['id']))
			{
				if (! empty ( $paramsSearch ['id'] )) {
					if (is_array ( $paramsSearch ['id'] ))
						$select->where ( "$tableName.id IN (?)", $paramsSearch ['id'] );
					else
						$select->where ( "$tableName.id = ?", $paramsSearch ['id'] );
				} else $select->where ( "$tableName.id = ?", '');
			}
			
		}
		return $this;
	}
	private function _fromDb(Pages_Model_Page &$model, $row, $load = false) {
		$model->setId ( $row->id )->setLabel ( $row->label )->setTscreation ( null )->setTsdelete ( null )
					->setTsupdate ( null )->setLang ( $row->lang )->setStatus ( $row->status )
					->setTemplate ( $row->template )->setOrder ( $row->order )
					->setTrx($row->trx);
		if (! empty ( $this->_taxonomies )) {
			foreach ( $this->_taxonomies as $taxnomy ) {
				$taxonomyName = $taxnomy->getName ();
				$relatedAliasLabel = sprintf ( "taxonomy-%s-label", $taxonomyName );
				$relatedAliasId = sprintf ( "taxonomy-%s-id", $taxonomyName );
				if (isset ( $row->$relatedAliasLabel ) && isset ( $row->$relatedAliasId ))
					$model->appendToTaxonomy ( $taxonomyName, array (
							$row->$relatedAliasId => $row->$relatedAliasLabel 
					) );
			}
		}
		if ($load) {
		}
		
		if (! is_null ( $row->ts_creation ))
			$model->setTsCreation ( NW_Utils_DateTime::strptime ( $row->ts_creation, "%Y-%m-%d" ) );
		if (! is_null ( $row->ts_update ))
			$model->setTsUpdate ( NW_Utils_DateTime::strptime ( $row->ts_update, "%Y-%m-%d" ) );
		if (! is_null ( $row->ts_delete ))
			$model->setTsDelete ( NW_Utils_DateTime::strptime ( $row->ts_delete, "%Y-%m-%d" ) );
	}
	private function _toDb(Pages_Model_Page $model) {
		$creationDate = $model->getTsCreation ();
		$updateDate = $model->getTsUpdate ();
		$deleteDate = $model->getTsDelete ();
		
		$data = array (
				'id' => $model->getId (),
				'label' => $model->getLabel (),
				'ts_creation' => (is_null ( $creationDate ) || $creationDate == '') ? date ( "Y-m-d h:i:s" ) : strftime ( "%Y-%m-%d %h:%i:%s", $creationDate ),
				'ts_update' => (is_null ( $updateDate ) || $updateDate == '') ? date ( "Y-m-d h:i:s" ) : strftime ( "%Y-%m-%d %h:%i:%s", $updateDate ),
				'ts_delete' => (is_null ( $deleteDate ) || $deleteDate == '') ? date ( "Y-m-d h:i:s" ) : strftime ( "%Y-%m-%d %h:%i:%s", $deleteDate ),
				'template' => $model->getTemplate (),
				'status' => $model->getStatus (),
				'lang' => $model->getLang (),
				'order' => $model->getOrder (),
		        'trx' => $model->getTrx()
		);
		return $data;
	}
	
	private function _addTaxonomies(Zend_Db_Table_Select &$select, $is_fetch_available=false) {
		try {
			if (empty ( $this->_taxonomies ))
				return;
			$select->setIntegrityCheck ( false );
			$count = 1;
			
			foreach ( $this->_taxonomies as $taxonomy )
			{
				$taxonomyName = $taxonomy->getName ();
				$role = $taxonomy->getInvokerrole ();
				$dependent = $taxonomy->getRelated ();
				$tbl_taxonomy_alias = "tbl_page_taxonomy_" . $count;
				$relationedRole = 'dependent';
				$invokerId = $taxonomy->getInvokerId ();
	
	
				if ($role == 'dependent')
				{
					$relationedRole = 'parent';
					if (empty ( $invokerId ))
					{
						if(!$is_fetch_available)
							throw new Exception ( "Cannot fetch parents if no id is provided" );
						else {
							break;
						}
					} else
					{
						//----
						$parentAlias = $taxonomy->getRelated();					
						//JOIN
	
						$select->joinLeft ( array ($tbl_taxonomy_alias => "tbl_page_taxonomy"), "`$tbl_taxonomy_alias`.`$relationedRole`=`tbl_pages`.`id`", array () );
						$select->where("`$tbl_taxonomy_alias`.`$role`=?",$taxonomy->getInvokerId ());
						$select->where("`tbl_pages`.`status` <> ?", 'deleted');
						$select->where("`$tbl_taxonomy_alias`.`taxonomy` = ?", $taxonomyName);
						//----
					}
				} else {
					
					$join_on_taxonomy = true;
					$relationedRole = 'dependent';
					
					if (! empty ( $invokerId ))
						$select->where ( "$tbl_taxonomy_alias.$role = ?", $taxonomy->getInvokerId () );
					else
						$join_on_taxonomy = false;
						
					$relatedAliasLabel = sprintf ( "taxonomy-%s-label", $taxonomyName );
					$relatedAliasId = sprintf ( "taxonomy-%s-id", $taxonomyName );
						
					if ($join_on_taxonomy) 
					{
						$select->joinLeft ( array ($tbl_taxonomy_alias => "tbl_page_taxonomy" ), "`$tbl_taxonomy_alias`.`$relationedRole`=`tbl_pages`.`id`", array () );
						
						$select->where("`$tbl_taxonomy_alias`.`taxonomy` = ?", $taxonomyName);
						$select->where ( "`tbl_pages`.`status` <> ?", 'deleted' );
						$count += 1;
					} else
						$select->where ( "`tbl_pages`.`status` <> ?", 'deleted' );
				}
			}
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	
	/*
	private function _addTaxonomies(Zend_Db_Table_Select &$select, $is_fetch_available=false) {
		try {
			if (empty ( $this->_taxonomies ))
				return;
			$select->setIntegrityCheck ( false );
			$count = 1;
			
			foreach ( $this->_taxonomies as $taxonomy ) 
			{
				$taxonomyName = $taxonomy->getName ();
				$role = $taxonomy->getInvokerrole ();
				$dependent = $taxonomy->getRelated ();
				$tbl_taxonomy_alias = "tbl_page_taxonomy_" . $count;
				$relationedRole = 'dependent';
				$invokerId = $taxonomy->getInvokerId ();
				
				
				if ($role == 'dependent') 
				{
					
					$relationedRole = 'parent';
					if (empty ( $invokerId ))
					{
						if(!$is_fetch_available)
							throw new Exception ( "Cannot fetch parents if no id is provided" );
						else {	
							break;
						}
					} else 
					{
						//----
						$parentAlias = $taxonomy->getRelated();
						$select->from ( array($parentAlias => "tbl_pages" ));
						$select->where("`$parentAlias`.`template` = ?",  $taxonomy->getRelated ());
						
						//JOIN
						
						$select->joinLeft ( array ($tbl_taxonomy_alias => "tbl_page_taxonomy"), "`$tbl_taxonomy_alias`.`$relationedRole`=`$parentAlias`.`id`", array () );
						$select->where("`$tbl_taxonomy_alias`.`$role`=?",$taxonomy->getInvokerId ());
						$select->where("`$parentAlias`.`status` <> ?", 'deleted');
						$select->where("`$tbl_taxonomy_alias`.`taxonomy` = ?", $taxonomyName);
						
						//----
					}
				} else {
					$join_on_taxonomy = true;
					$relationedRole = 'dependent';
					
					if (! empty ( $invokerId ))
						$select->where ( "$tbl_taxonomy_alias.$role = ?", $taxonomy->getInvokerId () );
					else
						$join_on_taxonomy = false;
					
					$relatedAliasLabel = sprintf ( "taxonomy-%s-label", $taxonomyName );
					$relatedAliasId = sprintf ( "taxonomy-%s-id", $taxonomyName );
					
					if ($join_on_taxonomy) {
						$select->joinLeft ( array ($tbl_taxonomy_alias => "tbl_page_taxonomy" ), "`$tbl_taxonomy_alias`.`$relationedRole`=`tbl_pages`.`id`", array () )
						->joinLeft ( array ($dependent => "tbl_pages" ), "`$tbl_taxonomy_alias`.`$relationedRole`=`$dependent`.id", 
									array (
											$relatedAliasLabel => $dependent . ".label", //was $dependent.label
											$relatedAliasId => $dependent . ".id" 
										) 
						);
						$select->where("`$tbl_taxonomy_alias`.`taxonomy` = ?", $taxonomyName);
						$select->where ( "`$dependent`.`status` <> ?", 'deleted' );
						$count += 1;
					} else
						$select->where ( "`tbl_pages`.`status` <> ?", 'deleted' );
				}
			}
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	*/
}
<?php
class Pages_Model_Translation_Mapper
{
	protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Pages_Model_DbTable_Translation');
        }
        return $this->_dbTable;
    }

	public function fetchAll($paramsSearch) {
		try {
			$table = $this->getDbTable ();
			$select = $table->select ();
				
			$this->_filterBySearchParam ( $select, $paramsSearch );
				
			$entries = array ();
			
			foreach ( $table->fetchAll ( $select ) as $row ) {
				$model = new Pages_Model_Translation();
				$this->_fromDb ( $model, $row );
				$entries [] = $model;
			}
				
			return $entries;
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	
	private function _filterBySearchParam(Zend_Db_Table_Select &$select, $paramsSearch = array()) {
		if (!empty($paramsSearch)) {
	
			if (!empty($paramsSearch['id']))
			{
				if(is_array($paramsSearch['id']))
					$select->where("id IN (?)", $paramsSearch['id']);
				else $select->where("id = ?", $paramsSearch['id']);
				unset($paramsSearch['id']);
			}
			else {
				$key = key($paramsSearch);
				if(!empty($paramsSearch[$key]))
				{
					if(is_array($paramsSearch[$key]))
						$select->where("$key IN (?)", $paramsSearch[$key]);
					else $select->where("$key = ?", $paramsSearch[$key]);
				}
			}
		}
		return $this;
	}
	
	private function _fromDb(Pages_Model_Translation &$model, $row, $load=false) {
		
		$model->setId($row->id);
		foreach(App_Model_Display_Service::getInstance()->langs() as $code)
			$model->addLang($code, $row->$code);
	}
	
	
	private function _toDb(Pages_Model_Translation $model) {
		$data = array('id' => $model->getId());
		foreach(App_Model_Display_Service::getInstance()->langs() as $code)
			$data[$lang] = $model->getLang($code);
		return $data;
	}


}
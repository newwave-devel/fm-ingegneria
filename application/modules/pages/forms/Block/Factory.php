<?php
class Pages_Form_Block_Factory
{
	public static function getForm($blockTemplate, $options = array())
	{
		if($blockTemplate instanceof Pages_Model_Block)
			$blockTemplate = $blockTemplate->getTemplate();
		
		$descriptor = NW_Pages_Block::getInstance()->block($blockTemplate);
		$form = isset($descriptor['form']) ? $descriptor['form'] : 'Pages_Form_Block';
		$model = isset($descriptor['model']) ? $descriptor['model'] : 'Pages_Form_Model';
		$service =isset($descriptor['service']) ? $descriptor['service'] : 'Pages_Form_Service';
			
		$form = new $form($options);
		$elements = array();
		$subform = new Zend_Form_SubForm("contents");
		$subform->setIsArray(true)->setDecorators(array('FormElements','Form'));
		
		foreach($descriptor->fields->children() as $field)
		{
			$fieldName = $field->getName();
			$fieldType = $field['type'];
			$required = ($field['required'] =='Y');
			$class = $field['class'];
			
			$id = sprintf("%s_%s", $fieldName, rand());
			$label = (string) $field->label;
			
			$options = (array) $field->params;
			$formElementName = sprintf("NW_Form_Element_%s", ucfirst($fieldType));
			$fElem = new $formElementName($fieldName, array("class" => $class, "id" => $id, "params" => $options, "blockname" => $blockTemplate, "fieldname" => $fieldName));

			if($required) $fElem->setRequired(true);
			$fElem->setLabel($label)
					->addDecorators(array(new NW_Form_Decorator_Clearer()));
			$elements[] = $fElem;
			if(method_exists($fElem, 'appendJs'))
				$fElem->appendJs();
		}
		$subform->addElements($elements);
		$form->setAction('/pages/block/save')
					->addSubForm($subform, 'contents');
		return $form;
	}
}
?>
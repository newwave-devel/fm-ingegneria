<?php
$pageId = 'people';
include "inc/required.php";
include "inc/common/contents.php";
?>

<?php include "inc/common/head.php" ?>


<!-- SCRIPT -->
<script type="text/javascript" src="/common/js/core.js"></script>
<script type="text/javascript" src="/common/js/jquery.sharrre.min.js"></script>
<script type="text/javascript" src="/common/js/jquery.nivo.slider.pack.js"></script>
<script type="text/javascript" src="/common/js/custom.js"></script>

<script type="text/javascript">	
                jQuery(window).load(function() {
                    // START
					loading();
					share();
                    slideshow();
					fancybox();
                });
        </script>

</head>

<body lang="it">

	<div id="loader"></div>
    	    	
        <?php include "inc/common/header.php"?>
        
        <section id="main_page">
		<section class="container">

			<section id="page_title">
				<h3><?php $translator->{"Pagina 'people'"}->__("People") ?></h3>
                    
                    <?php include "inc/common/social_share.php"?>
                    
                </section>
			<!--page_title-->

			<?php include "inc/people/main-slider-no-text.php"?>
			<!--home_slider-->


			<section id="main_data">

				<aside class="mainCol">
					<section id="discipline_text">
						<h3><?php echo $mainBlock->_('title')?></h3>
						<?php echo $mainBlock->_('description')?>
					</section>
					<!--discipline_text-->
				</aside>
				<!--mainCol-->

				<aside class="sidebar">
 					
 					<aside class="sidebar">
                   		<h5 class="view_people"><a href="company.php"><?php $translator->{"Pagina 'people'"}->__("View Company") ?></a></h5>
                 	</aside><!--sidebar-->
				</aside>
				<!--sidebar-->

			</section>
			<!--main_data-->

			<section class="key_people">
				<h5><?php $translator->{"Pagina 'people'"}->__("Partners") ?></h5>
				<?php include "inc/people/partners.php"?>
			</section>
			<!--key_people-->


			<section class="key_people">
				<h5><?php $translator->{"Pagina 'people'"}->__("Key Staff") ?></h5>
					<?php include "inc/people/keystaff.php"?>
				</ul>
			</section>
			<!--key_people-->

		</section>
		<!--container-->
	</section>
	<!--main_page-->
        
        <?php include "inc/common/footer.php"?>
		
	</body>

</html>
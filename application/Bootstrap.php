<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

	
    protected function _initViewItem() {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();
        $viewRenderer->setView($view);
        $view->addScriptPath(APPLICATION_PATH.'/views/scripts/');
        Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
    }

    protected function _initLocale() {
        try {
            setlocale(LC_TIME, 'ita', 'it_IT.utf8');
            $translate = new Zend_Translate('gettext', APPLICATION_PATH . '/languages/it/default.mo', 'it');
            date_default_timezone_set('Europe/Rome');
            Zend_Registry::set('Zend_Translate', $translate);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    protected function _initHelpers()
    {
        Zend_Controller_Action_HelperBroker::addPrefix('App_Helper');
    }

    protected function _initJQuery() {
        $view = $this->getResource('view');
        ZendX_JQuery::enableView($view);
        
        $view->jQuery()
                ->setLocalPath("/admin/js/jquery.js")
                ->setUiLocalPath("/admin/js/jquery-ui.min.js")
                ->enable()
                ->uiEnable();
 
        $view->addHelperPath('NW/View/Helper/JQuery', 'NW_View_Helper_JQuery_');  
    }

    protected function _initDoctype() {
        $view = $this->getResource('view');
        $view->doctype('HTML5');
    }

    protected function _initMeta() {

        $view = $this->getResource('view');
        $view->headMeta()->setCharset('UTF-8')
                ->appendName('viewport', 'width=1024')
                ->appendName('author', "Newwave snc - newwave-media.it")
                ->appendName('copyright', "Copyright 2013 Newwave snc All Rights Reserved.");
    }

    protected function _initHeadLink() {
        $view = $this->getResource('view');
        $view->headLink()
        	->appendStylesheet('/admin/js/vendors/albanx/real-ajax-uploader/examples/css/classicTheme/style.css')
                ->appendStylesheet('/admin/css/main.css')
               ;
               
    }
    
    protected function _initAcl()
    {
    	$helper= new App_Controller_Helper_Acl();
    	$helper->setRoles()->setResources()->setPrivileges()->setAcl();
    }
    
    protected function _initControllers() {
        $front = Zend_Controller_Front::getInstance();
        $front->setControllerDirectory(
                array(
                    'default' => APPLICATION_PATH . '/controllers',
                    'app' => APPLICATION_PATH . '/modules/app/controllers'
                )
        );
    }

    protected function _initPlugins() {
        $frontController = Zend_Controller_Front::getInstance();
        $frontController->registerPlugin(new Zend_Controller_Plugin_ErrorHandler())
        				->registerPlugin(new NW_Controller_Plugin_Auth_Check())
                        ->registerPlugin(new App_Controller_Plugin_Acl());
    }
    
    protected function _initLog()
    {
    	$writer = new Zend_Log_Writer_Stream(PUBLIC_PATH.'/var'.DIRECTORY_SEPARATOR.'log.txt');
    	$logger = new Zend_Log($writer);
    	Zend_Registry::set("Log", $logger);
    }
   

    protected function _initRoutes() {

        $frontController = Zend_Controller_Front::getInstance();

        $router = $frontController->getRouter();
        //if no user is logged, forwards to login, else forwards to dash
        $routeLogin = new Zend_Controller_Router_Route(
                 '/login',
                        array(
                            'module' => 'app',
                            'controller' => 'login',
                            'action' => 'index'
                        )
        );
        $routeError = new Zend_Controller_Router_Route(
        		'/error',
        		array(
        				'module' => 'default',
        				'controller' => 'error',
        				'action' => 'error'
        			)
        		);
        
        $routeAdminPanel = new Zend_Controller_Router_Route(
        		'/adminpanel',
        		array(
        				'module' => 'app',
        				'controller' => 'adminpanel',
        				'action' => 'index'
        		)
        );
        
        $router->addRoute('login', $routeLogin)
        		->addRoute('error', $routeError)
        		->addRoute('adminpanel',$routeAdminPanel);
        
    }
    
    protected function _initDisplay()
    {
//     	$this->bootstrap('session');
//     	$display = new Zend_Session_Namespace('Display');
    }

}
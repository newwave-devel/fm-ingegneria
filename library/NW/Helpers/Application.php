<?php

class NW_Helpers_Application
{   

    public static function getClassUri($module, $class)
    {
        return new NW_ClassURI($module, $class);
    }

    

    public static function getModuleDescriptor($classUri)
    {
        list($moduleName, $className) = self::explodeClassUri($classUri);
        return NW_Application::getSingleton('app/NW_ModuleManager')->getModuleDescriptor($moduleName);
    }


}

?>
<?php
class Pages_Form_Taxonomy_Factory
{
	public static function getForm(Pages_Model_Taxonomy $taxonomy, $options)
	{
		switch($taxonomy->getHtml())
		{
		    case 'select':
		    	return new Pages_Form_Taxonomy_Select($options);
		    case 'checkbox':
		    	return new Pages_Form_Taxonomy_Checkbox($options);
		}
	}
}
<?php

class NW_Models_Object_Decorators_Relationable extends NW_Models_Object_Decorator {

    private $_related;

    public function __construct($object) {
        $this->_object = $object;
        $this->_mapper = new NW_Models_Object_Decorators_Mapper_Relationable($this);
        $this->_related = $this->_mapper->getFieldsName();
    }

    public function __set($name, $value) {
        if (isset($this->_related->$name))
            $this->_related->$name = $value;
        else
            $this->getObject()->$name = $value;
        return $this;
    }

    public function __get($name) {
        if (isset($this->_related->$name))
            return $this->_related->$name;
        else
            return $this->getObject()->$name;
    }

    public function isRelationable() {
        return true;
    }

    public function doInsert($object) {
        try {
            return $this->getObject()->doInsert($object);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function updateRel($relName, $pid, $values) {
        try {
            $relType = NW_Application::getSingleton('defs/NW_Defs_Relations')->relation($relName, 'reltype');
            switch ($relType) {
                case '1,n':
                    return $this->insertSingle($relName, $pid, $values);
                case 'n,m':
                    return $this->insertMultiple($relName, $pid, $values);
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function insertSingle($relName, $pid, $values) {
        try {
            $query = $this->_mapper->insertSingle($relName, $pid, $values);
            $this->getAdapter()->performUpdateQuery($query);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function doUpdate(&$object) {
        try {
            return $this->getObject()->doUpdate($object);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function doLoadOne($params) {
        try {
            $query = $this->getObject()->doLoadOne($params);
            return $this->getMapper()->queryLoad($query);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function doLoadAll($params, NW_Pages_Paginator &$paginator, $query = '') {

        try {
            $query = $this->getObject()->doLoadAll($params, $paginator, $query);
            return $this->getMapper()->queryAll($query);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function doView($viewName, $params, &$paginator) {

        try {
            $query = $this->getObject()->doView($viewName, $params, $paginator);
            return $this->getMapper()->queryView($viewName, $query);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function toStdClass() {
        $stdClass = $this->getObject()->toStdClass();
        foreach ($this->_related as $k => $v)
            $stdClass->$k = (array) $v;
        return $stdClass;
    }

    public function formFields() {
        return $this->getObject()->formFields();
    }

    public function cloneNew(&$object) {
        return $this->getObject()->cloneNew($object);
    }

    public function doPorompompero() {
        return $this->getObject();
    }

}

?>

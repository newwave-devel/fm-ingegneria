<?php
class Pages_Model_Page_Composable_Service extends Pages_Model_Page_Service
{
	public function __construct() {
		parent::__construct();
	}
	
	public function saveComposable(Pages_Model_Page_Composable $page, $formValues)
	{
		try {
			$this->getMapper()->getDbTable()->getAdapter()->beginTransaction();
			parent::save($page);
			$pos = 0;
			$blockService = new Pages_Model_Block_Composable_Service();
			$blockService->dropPage($page->getId());
			foreach($formValues as $key => $vals)
			{
				if($key=='page')
					continue;
				$position = $key;
				foreach($vals as $foo => $id_as_string)
				{
					
					$id_as_array = explode(",", $id_as_string);
					if(!is_array($id_as_array)) continue;
					foreach($id_as_array as $blockId)
					{
						if(empty($blockId)) continue;
						$block = new Pages_Model_Block_Composable();
						$block->setPage($page->getId())
							->setPosition($position)
							->setBlock($blockId)
							->setOrder(++$pos);
						$blockService->save($block);
					}
				}
			}
			$this->getMapper()->getDbTable()->getAdapter()->commit();
		} catch (Exception $e) {
			$this->getMapper()->getDbTable()->getAdapter()->rollBack();
			throw $e;
		}
	}
	
	
}
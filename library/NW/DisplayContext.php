<?php

/**
 * Groups base display params, such as order by, order obj
 *
 * @author alessio
 *
 */
class NW_DisplayContext
{
    private $_classUri;
    private $_orderBy;
    private $_orderMethod;
    private $_view;

    const ORDERBY_AS_FOLLOW = 1;
    const USE_CLASS_DEFINED_SORTING = 2;
    const ORDER_BY_NONE = 3;

    public static function fromRequest($classUri, $action = 'index', $view = 'adminlist')
    {
        $classUri = ($classUri instanceof NW_ClassURI) ? $classUri : NW_ClassURI::fromString($classUri);
        $descriptor = NW_Application::getSingleton('app/NW_ModuleManager')->getModuleDescriptor($classUri);
        $sorting_name = $descriptor->getClassBackendPageProperty($classUri->_className, $action, 'sorting');

        $orderBy = '';
        $orderMethod = '';

        if (empty($sorting_name))
        {
            if ($descriptor->classHasDefaultSortingOptions($classUri->_className))
            {
                $orderMethod = self::USE_CLASS_DEFINED_SORTING;
                $orderBy = $descriptor->getClassSortingOptions($classUri->_className, 'default');
            } else
                $orderMethod = self::ORDER_BY_NONE;
        }
        else
        {
            $orderMethod = self::ORDERBY_AS_FOLLOW;
            $orderBy = $descriptor->getClassSortingOptions($classUri->_className, $sorting_name);
        }

        $displayContext = new NW_DisplayContext($classUri, $orderMethod);
        if ($orderBy != '')
            $displayContext->setOrderBy($orderBy);
        $displayContext->setView($view);
        return $displayContext;
    }

    public function __construct($classUri, $orderby = array(), $orderMethod=self::USE_CLASS_DEFINED_SORTING)
    {
        $classUri = ($classUri instanceof NW_ClassURI) ? $classUri : NW_ClassURI::fromString($classUri);
        $this->_classUri = $classUri;
        $this->_orderBy = $orderby;
        $this->_orderMethod = $orderMethod;
        $this->_start = 0;
        $this->_len = 0;
        $this->_active = '';
        $this->_deleted = 'N';
        $this->_owner = '';
        $this->_view = '';
    }

    public function setOrderBy($fields)
    {
        if ($this->_orderMethod == self::ORDERBY_AS_FOLLOW)
        {
            if (is_array($fields))
            {
                foreach ($fields as $f)
                    $this->_orderBy[] = $f;
            } else
                $this->_orderBy[] = $fields;
        }
        return $this;
    }

    public function applyClassOrderBy()
    {
       
        $moduleDescriptor = NW_Application::getSingleton('app/NW_ModuleManager')->getModuleDescriptor($classUri);
        if ($moduleDescriptor->classHasDefaultSortingOptions($classUri->_className))
        {
            foreach ($moduleDescriptor->getClassDefaultSortingOptions($classUri->_className) as $classSortingMethod)
                $this->_orderBy[] = $classSortingMethod;
        }
    }

    public function useOrderBy()
    {
        if (count($this->_orderBy) == 0)
            return false;
        return true;
    }

    public function getOrderBy()
    {
        return $this->_orderBy;
    }

    public function getOrderByForQuery()
    {
        print_r($this->_orderBy);
    }

    public function getView()
    {
        return $this->_view;
    }

    public function setView($view)
    {
        $this->_view = $view;
        return $this;
    }

    public function useView()
    {
        if ($this->_view == '')
            return false;
        return true;
    }

}

?>
<?php
class App_StatsController extends NW_Controller_Action 
{
public function init() {
        $username = 'alessio.dezotti@gmail.com';
        $password = 'sharktrap78';
        
        try {
            $client = Zend_Gdata_ClientLogin::getHttpClient($username, $password, Zend_Gdata_Analytics::AUTH_SERVICE_NAME);
            $this->service = new Zend_Gdata_Analytics($client);
            $this->dateEnd = new DateTime();
            $this->dateStart = new DateTime();
            $this->dateStart->sub(new DateInterval('P1M1D'));
        } catch (Exception $e) {
           throw $e;
        }
    }

    public function indexAction() {
        try {
            $this->_setAjaxAction();
            $this->view->currenPageName = "Google Analytics Reports";
            $this->_addJsSource("/admin/js/NW/highcharts.js");
            $this->_addJsSource("/admin/js/vendors/Highcharts/js/highcharts.js");
            $this->_addJsSource("/admin/js/vendors/Highcharts/js/modules/exporting.js");

            $this->_addWidget('mobile');
        } catch (Exception $e) {
            throw $e;
        }
    }

    private function _addWidget($action) {
        $jsOnload = sprintf("$('#graphContainer').load('/app/stats/%s');", $action);
        $this->view->jQuery()->addOnload($jsOnload);
    }

    public function mobileAction() {
        try {
            $this->_setAjaxAction();
            $this->_helper->layout->setLayout('ajaxcontent');
            $this->view->jQuery()->setRenderMode(ZendX_JQuery::RENDER_JQUERY_ON_LOAD);

            $query = new App_Model_Analytics_QueryBroker($this->service, '10376324');
            $query->addDimension(Zend_Gdata_Analytics_DataQuery::DIMENSION_DATE)
                    ->addDimension(Zend_Gdata_Analytics_DataQuery::DIMENSION_CITY)
                    ->addDimension(Zend_Gdata_Analytics_DataQuery::DIMENSION_COUNTRY)
                    ->addDimension(Zend_Gdata_Analytics_DataQuery::DIMENSION_CONTINENT)
                    ->addMetric(Zend_Gdata_Analytics_DataQuery::METRIC_VISITS)
                    ->addMetric(Zend_Gdata_Analytics_DataQuery::METRIC_PAGEVIEWS)
                    ->addMetric(Zend_Gdata_Analytics_DataQuery::METRIC_TIME_ON_SITE)
                    ->getQuery()
                    ->setStartDate($this->dateStart->format("Y-m-d"))
                    ->setEndDate($this->dateEnd->format("Y-m-d"))
                    ->setMaxResults(50);
            ;
            $results = $query->query();

            $visitCity = $results->getCartesian(Zend_Gdata_Analytics_DataQuery::DIMENSION_CITY, Zend_Gdata_Analytics_DataQuery::METRIC_VISITS);
            $visitCountry = $results->getCartesian(Zend_Gdata_Analytics_DataQuery::DIMENSION_COUNTRY, Zend_Gdata_Analytics_DataQuery::METRIC_VISITS);
            $visitContinent = $results->getCartesian(Zend_Gdata_Analytics_DataQuery::DIMENSION_CONTINENT, Zend_Gdata_Analytics_DataQuery::METRIC_VISITS);

            $visitCityPie = new App_Model_HighStats_Pie();
            $visitCountryPie = new App_Model_HighStats_Pie();
            $visitContinentPie = new App_Model_HighStats_Pie();



            $jsPie1 = $visitCityPie->setDataX($visitCity->x)
                    ->addY(array("data" => $visitCity->y, "name" => "Visite per citt�"))
                    ->limit(10)
                    ->setTitle('Visite per Citt�')
                    ->getJs();

            $jsPie2 = $visitCountryPie->setDataX($visitCountry->x)
                    ->addY(array("data" => $visitCountry->y, "name" => "Visite per nazione"))
                    ->setTitle('Visite per Nazione')
                    ->getJs();

            $jsPie3 = $visitContinentPie->setDataX($visitContinent->x)
                    ->addY(array("data" => $visitContinent->y, "name" => "Visite per continente"))
                    ->getJs();



            $this->addGraph('pie', 'city', $jsPie1, '', "Visite per citt�");
            $this->addGraph('pie', 'country', $jsPie2, 'left', "Visite per Nazione");
            $this->addGraph('pie', 'continent', $jsPie3, 'right', "Visite per Continente");
        } catch (Exception $e) {
            
             throw $e;
        }
    }

    private function addGraph($type, $id, $js, $pos, $title) {
        $paramsVarName = sprintf("params%s", $id);
        $chartName = sprintf("chart%s", $id);
        $this->view->jQuery()->addOnload("var $paramsVarName=$js;" . PHP_EOL);
        $this->view->jQuery()->addOnload("var $chartName = NW_Highcharts.addChart('$id', '$type').sync($paramsVarName);" . PHP_EOL);
        if (empty($this->view->blocks))
            $this->view->blocks = array();
        $this->view->blocks[] = array('id' => $id, 'pos' => $pos, "title" => $title);
    }
}
<?php
class App_ACL_Resources
{
	const CERTIFICATE_CERTIFICATE_INDEX = 'certificate.index';
	const CERTIFICATE_CERTIFICATE_LIST = 'certificate.list';
	const CERTIFICATE_CERTIFICATE_FLEETMANAGER = 'certificate.fleetmanager';
	const CERTIFICATE_CERTIFICATE_DASHLIST = 'certificate.dashlist';
	const CERTIFICATE_CERTIFICATE_SEARCH = 'certificate.search';
	const CERTIFICATE_CERTIFICATE_ADD = 'certificate.add';
	const CERTIFICATE_CERTIFICATE_EDIT = 'certificate.edit';
	const CERTIFICATE_CERTIFICATE_SAVE = 'certificate.save';
	const CERTIFICATE_CERTIFICATE_DELETE = 'certificate.delete';
	const CERTIFICATE_CERTIFICATE_EXPORTASPDF = 'certificate.exportaspdf';
	
	const PROFILES_FLEETMANAGER_INDEX = 'fleetmanager.index';
	
	const PURCHASE_ORDERS_ADD = 'orders.add';
	const PURCHASE_ORDERS_ADDROWS = 'orders.addrows';
	const PURCHASE_ORDERS_DPARECEIVED = 'orders.dpareceived';
	const PURCHASE_ORDERS_EDIT = 'orders.edit';
	const PURCHASE_ORDERS_FLEETMANAGERLIST = 'orders.fleetmanagerlist';
	const PURCHASE_ORDERS_INDEX = 'orders.index';
	const PURCHASE_ORDERS_LIST = 'orders.list';
	const PURCHASE_ORDERS_NOTIFYDPA = 'orders.notifydpa';
	const PURCHASE_ORDERS_PROCEED = 'orders.proceed';
	const PURCHASE_ORDERS_SAVE = 'orders.save';
	const PURCHASE_ORDERS_SEARCH = 'orders.search';
	
	const PURCHASE_QUOTE_ADD = 'quote.add';
	const PURCHASE_QUOTE_FETCHBYORDER = 'quote.fetchbyorder';
	const PURCHASE_QUOTE_INDEX = 'quote.index';
	const PURCHASE_QUOTE_SAVE = 'quote.save';
	
	
	const PURCHASE_ROWS_ADD = 'rows.add';
	const PURCHASE_ROWS_EDIT = 'rows.edit';
	const PURCHASE_ROWS_FETCHBYORDER = 'rows.fetchbyorder';
	const PURCHASE_ROWS_INDEX = 'rows.index';
	const PURCHASE_ROWS_SAVE = 'rows.save';
	
	
	const SHIPOWNERS_SHIPOWNER_ADD = 'shipowner.add';
	const SHIPOWNERS_SHIPOWNER_DASHLIST = 'shipowner.dashlist';
	const SHIPOWNERS_SHIPOWNER_DELETE = 'shipowner.delete';
	const SHIPOWNERS_SHIPOWNER_EDIT = 'shipowner.edit';
	const SHIPOWNERS_SHIPOWNER_INDEX = 'shipowner.index';
	const SHIPOWNERS_SHIPOWNER_SAVE = 'shipowner.save';
	const SHIPOWNERS_SHIPOWNER_TOSELECT = 'shipowner.toselect';
	
	const VESSELS_SHIPOWNER_ADD = 'vessel.add';
	const VESSELS_SHIPOWNER_DASHLIST = 'vessel.dashlist';
	const VESSELS_SHIPOWNER_DELETE = 'vessel.delete';
	const VESSELS_SHIPOWNER_EDIT = 'vessel.edit';
	const VESSELS_SHIPOWNER_INDEX = 'vessel.index';
	const VESSELS_SHIPOWNER_SAVE = 'vessel.save';
	const VESSELS_SHIPOWNER_TOSELECT = 'vessel.toselect';
	
	const APP_USERS_DASHLIST = 'users.dashlist';
	const APP_USERS_INDEX = 'users.index';
	const APP_USERS_LIST = 'users.list';
	const APP_USERS_SAVE = 'users.save';
	const APP_USERS_UPDATEMYPROFILE = 'users.updatemyprofile';
	
	
	const APP_INDEX_INDEX = 'index.index';
	const APP_INDEX_LOGOUT = 'index.logout';
	
	const APP_ERROR_INDEX = 'error.index';
	const APP_LOGIN_INDEX= 'login.index';
	
	
	const DASH_SHOW_INDEX= 'show.index';
	const DASH_SHOW_FLEETMANAGER= 'show.fleetmanager';
	
}
?>
<?php
class Pages_Model_Taxonomy_Service
{
	
    private $_mapper;
    
    public function __construct() {
    	$this->_mapper=new Pages_Model_Taxonomy_Mapper();
    }
	/**
	 * loads a taxonomy object by its simplexml descriptor
	 * @param Pages_Model_Taxonomy $taxonomy
	 * @throws Exception
	 * @return Pages_Model_Taxonomy
	 */
	public function load(Pages_Model_Taxonomy $taxonomy)
	{
		try {
			$taxonomyDescriptor = NW_Pages_Page::getInstance()->page($taxonomy->getInvoker())->taxonomies;
			$taxonomyName = $taxonomy->getName();
			$node = $taxonomyDescriptor->$taxonomyName;
 			$role = (string) $node['role'];
			
 			if($role == 'parent')
 				$taxonomy->setRelated((string) $node['dependent']);
			else 
				$taxonomy->setRelated((string) $node['parent']);
			$taxonomy->setInvokerrole($role);
			return $taxonomy;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function save(Pages_Model_Taxonomy_DTO $dto)
	{
	    try {
	        $this->_mapper->save($dto);
	        return true;
	    } catch (Exception $e) {
	        throw $e;
	    }
	}
	
	public function saveRow(Pages_Model_Taxonomy_Row $row)
	{
		try {
			$this->_mapper->saveRow($row);
			return true;
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	public function dropPageFromTaxonomy($pageId)
	{
	    try {
	        $this->_mapper->dropPageFromTaxonomy($pageId);
	        return true;
	    } catch (Exception $e) {
	        throw $e;
	    }
	}
	
	/**
	 * queries the taxonomy table to extract all items that has something to do with current page
	 * @param unknown $pageId
	 */
	public function taxonomyRows($pageId)
	{
		try {
			return $this->_mapper->taxonomyRows($pageId);
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	/**
	 * queries the taxonomy table to extract all items that has something to do with current page
	 * @param unknown $pageId
	 */
	public function translate(Pages_Model_Taxonomy_Row $row)
	{
		try {
			$pageId = ($row->getInvokerRole() == 'parent') ? $row->getDependent() : $row->getParent();
			$translationService = new Pages_Model_Page_Translation_Service();
			$pageService = new Pages_Model_Page_Service();
			$page = $pageService->find($pageId);
			$page->getTrx();
			return $this->_mapper->translate($row);
		} catch (Exception $e) {
			throw $e;
		}
	}
	
}
<?php
$tpl_news_accordion = <<<TPL
	<div class="level">
		<a class="opening %s" href="#">%s</a>
		<div class="expanded %s">
			<ul>
				%s
			</ul>
		</div>
		<!--/.expanded-->
	</div>
TPL;

$tpl_listItem = <<<TPL
	<article class="new">
		<a href="news-detail.php?id=%d" title="%s">
        	<img class="img" src="/assets/res.php?src=%s&w=365&h=180" alt="%s">
            <!-- <h5>Espace Public</h5> -->
            <h3>%s</h3>
            <h6>%s</h6>
            <p>%s</p>
            <div class="color_wrapper"></div>
        </a>
    </article><!--new-->
TPL;


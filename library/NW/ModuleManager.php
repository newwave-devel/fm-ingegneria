<?php

class NW_ModuleManager extends NW_XmlDescriptor
{
    private $_module_pool;
    private $_modules_enabled;
    

    const MODULE_DESCRIPTOR_FILENAME = 'module.xml';

    /**
     *
     * Activation of the ModuleManager is made by the ApplicationDescriptor.
     * Activation implies the registration of the autoloads
     * @param simplexmlobject $descriptor
     */
    public function __construct($descriptor)
    {
        try {
            if (empty($descriptor))
                throw new Exception("No Descriptor specified");
            parent::__construct($descriptor);
            $this->_module_pool = array();
            $this->_classes_pool = array();
            $this->_modules_enabled = array();
            $this->activateModules();
        } catch (Exception $e) {
            throw $e;
        }
    }

//    public function getMapFor(NW_ClassURI $classuri, $arr_fieldProperties=array())
//    {
//        return $this->getClassDescriptor($classuri)->getMap($arr_fieldProperties);
//    }
//
//    public function getRelationsFor(NW_ClassURI $classuri)
//    {
//        return $this->getClassDescriptor($classuri)->getRelationsName();
//    }

    /**
     *
     * Loads class Descriptor. Behaves as a singleton.
     * @param string $classuri
     * @throws Exception
     * @return NW_Descriptors_Object
     */
    public function classDescriptor(&$classuri)
    {
        if(is_string($classuri))
            $classuri = NW_ClassURI::fromString ($classuri);
        
        $module = $classuri->getPrefix();
        $class = $classuri->getClassName();
        if ($this->isModuleEnabled($module))
        {
            if (!isset($this->_classes_pool[$module]) || !isset($this->_classes_pool[$module][$class]) || empty($this->_classes_pool[$module][$class]))
                $this->_classes_pool[$module][$class] = $this->loadClassDescriptor($classuri);
            return $this->_classes_pool[$module][$class];
        } else
            throw new Exception("Module $module not enabled, therefore $class is not available");
    }

    /**
     *
     * Loads module.xml
     * @param unknown_type $moduleName
     * @throws Exception
     */
    public function getModuleDescriptor(&$classuri)
    {
        try {
            if(is_string($classuri))
                $classuri = NW_ClassURI::fromString ($classuri);
            
            $moduleName = $classuri->_prefix;
            if($moduleName == '')
                throw new Exception("Empty moduleName");
            if ($this->isModuleEnabled($moduleName))
            {
                if (!isset($this->_module_pool[$moduleName]) || empty($this->_classes_pool[$moduleName]))
                    $this->_module_pool[$moduleName] = $this->loadModuleDescriptor($moduleName);
                return $this->_module_pool[$moduleName];
            } else
                throw new Exception("Module '$moduleName' not enabled");
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     *
     * Loads class.xml within its module
     * @param unknown_type $moduleName
     * @param unknown_type $classname
     * @throws Exception
     * @return NW_Descriptors_Object
     */
    private function loadClassDescriptor($classuri)
    {
        try {
            if(is_string($classuri))
                $classuri = NW_ClassURI::fromString ($classuri);
            $moduleName = $classuri->getPrefix();
            $classname = $classuri->getClassName();

            $path = $this->path($moduleName, $this->getClassDescriptorFilename($classname));
            return NW_Descriptors_Object::fromFile ($path);
        } catch (Exception $e) {
            throw $e;
        }
    }

    private function loadModuleDescriptor($moduleName)
    {
        try {       
            $path = $this->path($moduleName, self::MODULE_DESCRIPTOR_FILENAME);
            return NW_Descriptors_Module::fromFile($path);
        } catch (Exception $e) {
            throw $e;
        }
    }

   
    

    public function isModuleEnabled($moduleName)
    {
        try {
            if (trim($moduleName) == '')
                throw new Exception("Empty moduleName");
            if (!isset($this->_modules_enabled[$moduleName]))
                throw new Exception("'$moduleName' not registered");
            return $this->_modules_enabled[$moduleName];
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function activateModules()
    {
        foreach($this->descriptor->module as $module)
        {
            $moduleName = strval($module["name"]);
            if($module['active']=='Y')
                $this->_modules_enabled[$moduleName] = true;
        }
    }

//    public function activateModules()
//    {
//        foreach ($this->toSimpleXml()->xpath("//modules/module[@active='Y']") as $mod)
//        {
//            $codepool = strval($mod['codepool']);
//            $modulename = strval($mod['name']);
//
//            $classPathForAutoload = $this->getModuleAutoloaderClassPath($codepool, $modulename);
//
//            $this->_modules_enabled[$modulename] = true;
//            $this->_modules_codepools[$modulename] = $codepool;
//            //if i want to use default module, i have no class for referencing, instantiating objects, etc..
//            if ($modulename != 'Default')
//            {
//                if (file_exists($classPathForAutoload))
//                {
//                    require_once($classPathForAutoload);
//                    //spl_autoload_register("$classNameForAutoload::autoload");
//                    //Logger::getLogger(__CLASS__)->info("registrato autoload: $classNameForAutoload::autoload");
//                } else
//                {
//                    throw new Exception("Cannot activate module $modulename, autoloadpath ('$classPathForAutoload') not found");
//                }
//            }
//        }
//    }

    /**
     * 
     * returns all modulenames along their activation status
     */
    public function listModules()
    {
        return $this->_modules_enabled;
    }
    
    private function path($moduleName, $file = '')
    {
        return APP_PATH . '/library/Local/' . $moduleName . '/etc/'.$file;
    }

    private function getClassDescriptorFilename($classname)
    {
        return $classname . '.xml';
    }
}

?>
<!-- ogni 6 li classe no_margin -->
<?php if($galleryBlocks ): ?>
<?php 
	$projects_gallery_count = 0;
	if(is_array($galleryBlocks)) $galleryBlocks = $galleryBlocks[0];
	$projects_gallery_assets = $blockService->assets($galleryBlocks->_('file'));
	if($projects_gallery_assets): 
?>
		<h4 class="title">Gallery</h4>
		<ul class="photo_gallery">
<?php
	foreach($projects_gallery_assets as $asset)
	{
		$extraclass=((++$projects_gallery_count%6)==0) ? 'class="no_margin"' : '';
		echo sprintf('<li %s><a class="zoom" rel="project" href="%s"
		title="%s"><img class="img" src="/assets/res.php?src=%s&w=160&h=160"><span class="color_wrapper"></span></a></li>',
		$extraclass,  $asset->getAssetUrl(), $mainBlock->_("title"), $asset->getAssetUrl());
	}
?>
		</ul>
		<?php endif; ?>
<?php endif; ?>
<?php

class Pages_Form_PageSearch extends Zend_Form {
	
    private $_type = '';
    private $_paginator;
    
    public function init() 
    {    
        $this->setMethod('post')
                ->setAction("/pages/page/search");
        
        $free = new NW_Form_Element_Text('free');
        $free->setLabel("Ricerca Libera")
                ->setFilters(array('StringTrim'))
                ->setAttrib('placeholder', "Testo da Cercare")
                ->setAttrib('class', "ricerca")
                ->setDecorators(array("Label", "ViewHelper"));
        
        $status = new NW_Form_Element_Select('status');
        $status->setLabel("Status")
                ->addMultiOption("", "Please Select One")
                ->addMultiOption("draft", "Bozza")
                ->addMultiOption("active", "Active");
        if(App_Model_Display_Service::getInstance()->isMultilanguage())
        {
        	$language = new NW_Form_Element_Select('lang');
        	$language->setMultiOptions(App_Model_Display_Service::getInstance()->langsAsSelect())
        		->setLabel("Langage");
        }
        else 
        	$language = new NW_Form_Element_Hidden('lang');
        	
        $language->setValue(App_Model_Display_Service::getInstance()->lang());
        $type = new NW_Form_Element_Hidden("type");
        $type->setValue($this->_type);
        $ipp = new NW_Form_Element_Hidden("ipp");
        
        $ipp->setDecorators(array("ViewHelper"));
        $page = new NW_Form_Element_Hidden("pag");
        $page->setDecorators(array("ViewHelper"));
        if(!empty($this->_paginator))
        {
        	$ipp->setValue($this->_paginator->getItemsPerPage());
        	$page->setValue($this->_paginator->getCurPage());
        }
        $this->addElements(array($free, $ipp,$status, $page, $type, $language));
       
        $this->getView()->jQuery()->addOnLoad('$("#frmSearch").ajaxForm({target: \'#main_content\'});');
    }
    
    public function setType($type)
    {
        $this->_type = $type ;
    }
    
    public function setPaginator($paginator)
    {
    	
    	$this->_paginator = $paginator ;
    }
}


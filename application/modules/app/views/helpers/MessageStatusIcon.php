<?php

class Zend_View_Helper_MessageStatusIcon extends Zend_View_Helper_Abstract {

    public $view;
  
    public function setView(Zend_View_Interface $view) {
        $this->view = $view;
    }

    public function messagestatusicon($status) {
    	switch($status)
    	{
	        case App_Model_Message::STATUS_NEW:
	        	return '<span class="icon_font">&#xf0e7;</span>';
	        case App_Model_Message::STATUS_SENT:
	        	return '<span class="icon_font">&#xf093;</span>';
	        case App_Model_Message::STATUS_RECEIVED:
	        	return '<span class="icon_font">&#xf019;</span>';
	        
	    }
    }

}

?>

<?php
class App_Model_Notification {
	
	private $_id;
	private $_to;
	private $_ts;
	private $_priority;
	private $_message;
	private $_read;
	
	const PRIORITY_LOW = 0;
	const PRIORITY_MEDIUM = 50;
	const PRIORITY_HIGH = 100;

	 public function getId() {
	  return $this->_id;
	 }
	 
	 public function setId($_id) {
	  $this->_id = $_id;
	  return $this;
	 }
 
	
	public function __construct() {
		$this->_to = array ();
		$this->_ts = time();
		$this->_priority = self::PRIORITY_LOW;
		$this->_message = '';
		$this->_read = false;
	}
	
	/**
	 *
	 * @return the unknown_type
	 */
	public function getTo() {
		return $this->_to;
	}
	
	/**
	 *
	 * @param unknown_type $_to        	
	 */
	public function setTo($_to) {
		$this->_to = $_to;
		return $this;
	}
	
	/**
	 *
	 * @return the unknown_type
	 */
	public function getTs() {
		return $this->_ts;
	}
	
	/**
	 *
	 * @param unknown_type $_ts        	
	 */
	public function setTs($_ts) {
		$this->_ts = $_ts;
		return $this;
	}
	
	/**
	 *
	 * @return the unknown_type
	 */
	public function getPriority() {
		return $this->_priority;
	}
	
	/**
	 *
	 * @param unknown_type $_priority        	
	 */
	public function setPriority($_priority) {
		$this->_priority = $_priority;
		return $this;
	}
	
	/**
	 *
	 * @return the unknown_type
	 */
	public function getMessage() {
		return $this->_message;
	}
	
	/**
	 *
	 * @param unknown_type $_message        	
	 */
	public function setMessage($_message) {
		$this->_message = $_message;
		return $this;
	}
	
	/**
	 *
	 * @return the unknown_type
	 */
	public function getRead() {
		return $this->_read;
	}
	
	/**
	 *
	 * @param unknown_type $_read        	
	 */
	public function setRead($_read) {
		$this->_read = $_read;
		return $this;
	}
	
	public function __set($name, $value) {
		$method = 'set' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid user property');
		}
		$this->$method($value);
	}
	
	public function __get($name) {
		$method = 'get' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception("Invalid user property '$name'");
		}
		return $this->$method();
	}
	
	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}
	
	public function getOptions()
	{
		return get_object_vars($this);
	}
}
<?php

class App_Helper_Alert extends Zend_Controller_Action_Helper_Abstract {
    
   
    public function canDisplay() {
        $message = $this->getActionController()->getRequest()->getParam("error", '');
        if($message!='')
        {
            $output = sprintf("MyAlert.display(dic.%s);", $message);
            $this->getActionController()->view
                    ->jQuery()->addOnLoad($output);
        }
    }
    
    public function display($message)
    {
    	$output = sprintf("console.log('ziolupo')", $message);
    	$this->getActionController()->view
    		->jQuery()->addJavascript($output);
    }
    
    public function direct()
    {
        return $this->canDisplay();
    }
}

?>

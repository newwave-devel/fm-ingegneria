function LibraryController(blockTemplate, target)
{
    this.blockTemplate= blockTemplate;
    this.target = target;
}

LibraryController.prototype.setTarget = function(target)
{
	this.target = target;
	return this;
}

LibraryController.prototype.parse = function(payload)
{
	var html = '';
	if(payload.is_image)
		return templates.imagepreview.printf(payload); 
	else
		return templates.filepreview.printf(payload);
	return html;
}

LibraryController.prototype.addItem= function(payload)
{
	//adds the current blockTemplate
	payload.blockTemplate = this.blockTemplate;
	return parent.window.main_page.subPage().append(this.target, this.parse(payload));
}

LibraryController.prototype.replaceItem= function(payload)
{
	//adds the current blockTemplate
	payload.blockTemplate = this.blockTemplate;
	return parent.window.main_page.subPage().replace(this.target, this.parse(payload));
}
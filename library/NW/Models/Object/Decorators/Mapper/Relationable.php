<?php

class NW_Models_Object_Decorators_Mapper_Relationable extends NW_Models_Object_Decorators_Mapper {

    private $fieldsname;

    public function __construct(NW_Models_Object_Decorators_Relationable &$decoratorRef) {
        $this->decoratorRef = $decoratorRef;
        $this->table = 'tbl_relations';
        $this->fieldsname = array();
        foreach ($this->decoratorRef->classDescriptor()->rels() as $fname)
            $this->fieldsname[] = strval($fname);
    }

    public function getFieldsName() {
        return $this->fieldsname;
    }

    public function insertSingle($relName, $pid, $values) 
    {
        $query = new NW_Db_Query_Update($this->decoratorRef->getMainTable());     
        $query->add(array($relName =>$values));
        $query->where(array("id = ?" => $pid));
        return $query;
        
    }

    public function queryUpdate(&$object) {
        //
    }

    public function fieldsInView($viewName) {
        $res = array();
        foreach ($this->fieldsname as $fName) {
            if ($this->decoratorRef->classDescriptor()->inView($viewName, $fName))
                $res[] = $fName;
        }
        return $res;
    }

    public function queryAll($query = '') {
        if ($query == '')
            $query = new NW_Db_Query_Select();

        foreach ($this->decoratorRef->classDescriptor()->rels() as $relationFieldName) {
            $relationFieldName = strval($relationFieldName);
            $relDefId = $this->decoratorRef->classDescriptor()->relDefId($relationFieldName);

            $cUri = $this->decoratorRef->classDescriptor()->relDef($relDefId, 'relClass');

            if (empty($relDefId))
                throw new Exception("Relation '$relationFieldName' ($relDefId) has no valid reldef! ");

            $query = NW_Application::__model($cUri)->loadByRelDef($query, $relDefId, $this->getDecorator()->getClassUri(), $this->getDecorator()->getMainTable());
        }
        return $query;
    }

    public function queryView($viewName, $query = '') {


        if ($query == '')
            $query = new NW_Db_Query_Select();

        $fieldsInView = $this->fieldsInView($viewName);
        if (!empty($fieldsInView)) {
            foreach ($fieldsInView as $relationFieldName) {
                $relationFieldName = strval($relationFieldName);

                $relDefId = $this->decoratorRef->classDescriptor()->relDefId($relationFieldName);

                $cUri = $this->decoratorRef->classDescriptor()->relDef($relDefId, 'relClass');

                if (empty($relDefId))
                    throw new Exception("Relation '$relationFieldName' ($relDefId) has no valid reldef! ");

                $query = NW_Application::__model($cUri)->loadByRelDef($query, $relDefId, $this->getDecorator()->getClassUri(), $this->getDecorator()->getMainTable());
            }
        }
        return $query;
    }

    public function queryLoad($query = '') {
        if ($query == '')
            $query = new NW_Db_Query_Select();

        foreach ($this->decoratorRef->classDescriptor()->rels() as $relationFieldName) {
            $relationFieldName = strval($relationFieldName);
            $relDefId = $this->decoratorRef->classDescriptor()->relDefId($relationFieldName);
            $cUri = $this->decoratorRef->classDescriptor()->relDef($relDefId, 'relClass');

            if (empty($relDefId))
                throw new Exception("Relation '$relationFieldName' has no valid reldef!");

            return NW_Application::__model($cUri)->loadByRelDef($query, $relDefId, $this->getDecorator()->getClassUri(), $this->getDecorator()->getMainTable());
        }
    }

}

?>

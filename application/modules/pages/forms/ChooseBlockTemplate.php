<?php

class Pages_Form_ChooseBlockTemplate extends NW_Form {

    protected $blocks;

    public function init()
    {
        $this->setMethod('post')
                ->setAttrib('class', 'form')
                ->setAction("/pages/page/addblock");

        $select = new NW_Form_Element_Select('template');
        $select->setRequired(true)
        	->setLabel("Choose a template")
        	->addMultiOption("", "Please Select One");
        	
        $pageId= new NW_Form_Element_Hidden('pageid');

        $position = new NW_Form_Element_Select("order");

        $position->setRequired(true)
            ->setLabel("Seleziona una posizione");
        $templates = array();

        $pos = 0;
        $first = true;
        foreach($this->blocks as $bl)
        {
            $pos = $bl->getOrder();
            if($first)
            {
                $position->addMultiOption($pos-1, "All'inizio");
                $first = false;
            }
            $template = $bl->getTemplate();

            if(array_search($template, $templates)===false)
            {

                $position->addMultiOption($pos, sprintf("Prima di '%s'", NW_Pages_Page::getInstance()->blockLabel($template)));
                $templates[] = $template;
            }
        }
        $position->addMultiOption($pos+1, "Alla fine");
        $this->addElements(array($select, $pageId, $position));
    }
    
    public function setTemplates($pageTemplate)
    {
    	$this->getElement('template')
    		->addMultiOptions(NW_Pages_Page::getInstance()->blocks($pageTemplate));
    }

    public function setBlocks($blocsk)
    {
        $this->blocks = $blocsk;
    }

}


NW.justGage= (function () {
    // Initialize the application
    var items = {};
    
    justGage = function(itemref)
    {
        this.options = {
            'id': null,
            'value': null,
            'min': null,
            'max': null,
            'title': null,
            'levelColorsGradient' : null,
            'titleFontColor':null,
            'valueFontColor':null,
            'showMinMax' : false,
            'gaugeWidthScale': null,
            'gaugeColor' : null,
            'showInnerShadow': null,
            'shadowOpacity': null,
            'shadowSize': null,
            'shadowVerticalOffset': null,
            'levelColors': null,
            'levelColorsGradient': null,
            'labelFontColor': null,
            'startAnimationTime': null,
            'startAnimationType': null,
            'refreshAnimationTime': null,
            'refreshAnimationType': null
        };
    };
    
    justGage.prototype = {
        setOptions : function(options)
        {
            for(var key in options)
                this.options[key] = options[key];
            return this;
        },
        setOption : function(param, value)
        {
            this.options[param] = value;
            return this;
        },
        render: function()
        {
        	params = [];
        	for(var key in this.options)
        	{
        		if(this.options[key]===null) continue;
        		params[key] = this.options[key];
        	}
        	return new JustGage(params);
        },
        setValues: function(min, max, value)
        {
        	this.options['min'] = min;
        	this.options['max'] = max;
        	this.options['value'] = value;
        	return this;
        }
        
    }
       
    // Return the public facing methods for the App
    return {
        add: function (paramId) {
            items[paramId] = new justGage(paramId);
            return items[paramId];
        },
        setOption: function (paramId, property, value) {
            items[paramId].options[property] = value;
            return items[paramId];
        },
        setOptions: function (paramId, properties) {
            items[paramId].setOptions(properties);
            return items[paramId];
        },
        get: function(paramId)
        {
            return items[paramId];
        }
    };
}());

var NW_justGage = NW.justGage;

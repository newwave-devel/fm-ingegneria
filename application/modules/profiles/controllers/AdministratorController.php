<?php

class Profiles_AdministratorController extends NW_Controller_Action {

    public function init() {
        $user = Zend_Auth::getInstance()->getIdentity();
      
        $this->view->user = $user;
        if ($this->_helper->Role() != App_Model_Role::ROLE_ADMINISTRATOR)
            $this->_redirect('/');
        
        App_Model_Display_Service::getInstance()->store();
        
        $this->addDefaultJs();
        $this->_addOnload("window.main_page = Mosaic_page.init();");       		
    }

    public function indexAction() {
        try {
        	$this->_helper->layout()->setLayout('internal');
        } catch (Exception $e) {
            throw $e;
        }
    }

}


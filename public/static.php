<?php
$pageId = 'static';
include "inc/required.php";
include "inc/common/contents.php";
?>
<?php include "inc/common/head.php" ?>


        <!-- SCRIPT -->

		<script type="text/javascript" src="/common/js/core.js"></script>
		<script type="text/javascript" src="/common/js/jquery.jcarousel.pack.js"></script>
        <script type="text/javascript" src="/common/js/jquery.sharrre.min.js"></script>
        <script type="text/javascript" src="/common/js/jquery.nivo.slider.pack.js"></script>
        <script type="text/javascript" src="/common/js/custom.js"></script>
        
        <script type="text/javascript">	
                jQuery(window).load(function() {
                    // START
					loading();
					share();
					people_carousel_sidebar();
                    slideshow();
					fancybox();
                });
        </script>
        
	</head>    
    
	<body lang="it">
    
    	<div id="loader"></div>
    	    	
        <?php include "inc/common/header.php"?>
        
        <section id="main_page">
        	<section class="container">

            	<section id="page_title">
                    <h3><?php echo $mainBlock->_("title")?></h3>
                    
                   <?php include "inc/common/social_share.php"?>
                    
                </section><!--page_title-->
                
                <section id="main_data">
                
                	<aside class="mainCol">
                    	<section id="discipline_text">
                            <h3><?php echo $mainBlock->_("subtitle")?></h3>
                            <?php echo $mainBlock->_("textsx")?>
                        </section><!--discipline_text-->
                    </aside><!--mainCol-->
                    

                
                </section><!--main_data-->
                                

                
            </section><!--container-->
        </section><!--main_page-->
        
        <?php include "inc/common/footer.php"?>
		
	</body>
	
</html>
<?php
$pageId = 'news-detail';
include "inc/required.php";
include "inc/common/contents.php";
?>


<?php include "inc/common/head.php" ?>
		
        
        <!-- SCRIPT -->
		<script type="text/javascript" src="/common/js/core.js"></script>
        <script type="text/javascript" src="/common/js/jquery.jcarousel.pack.js"></script>
        <script type="text/javascript" src="/common/js/jquery.sharrre.min.js"></script>
        <script type="text/javascript" src="/common/js/custom.js"></script>
        
        <script type="text/javascript">	
                jQuery(window).load(function() {
                    // START
					loading();
					people_carousel_sidebar();
					share();
					fancybox();
                });
        </script>
        
	</head>    
    
	<body lang="it">
    
    	<div id="loader"></div>
    	    	
        <?php include "inc/common/header.php"?>
        
        <section id="main_page">
        	<section class="container">
            
            	<section id="page_title">

                    <h4><?php echo str_replace("/", ".", $mainBlock->_("date")) ?></h4>
   
                    <?php include "inc/common/social_share.php"?>
                    
                </section><!--page_title-->
            
            	<?php include "inc/common/main-slider-no-text.php"?>
                
                <section id="main_data">
                	
                    <?php include "inc/news/datas.php" ?>
                
                	<aside class="mainCol">
                    	<section id="discipline_text">
                            <h3><?php echo $mainBlock->_("title")?></h3> 
                            <p><?php echo $mainBlock->_("description")?></p>
                        </section><!--discipline_text-->
                        
                       <?php 
                       	include "inc/common/downloads.php"; 
                       ?>
                    </aside><!--mainCol-->
                    
                    <aside class="sidebar">
                    
                    	<?php include "inc/news/people.php"; ?>
                    
                    </aside><!--sidebar-->
                
                </section><!--main_data-->
                                
                <section id="home_articles">
                	
                 <?php include "inc/news/gallery.php"?>
                  <!-- 
                    <h4 class="title">Video</h4>
                    
                    <iframe src="//player.vimeo.com/video/47845783?title=0&amp;byline=0&amp;portrait=0&amp;color=cccccc" width="1000" height="562" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    -->
                    
                    <?php include "inc/news/tags.php"?>
                    
                </section><!--home_articles-->
                
            </section><!--container-->
        </section><!--main_page-->
        
        <?php include "inc/common/footer.php"?>
		
	</body>
	
</html>
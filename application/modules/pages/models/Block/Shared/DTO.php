<?php
class Pages_Model_Block_Shared_DTO {
	
	public $assigned;
	public $availables;
	public $positions;
	public $pages;
	
	public function __construct() {
		$this->assigned = array();
		$this->availables = array ();
		$this->positions = array();
		$this->pages = array();
	}
	
	public function addAssigned($assigned)
	{
		foreach($assigned as $ass)
		{
			$blockId = $ass->getBlock();
			$position = $ass->getPosition();
			$order = $ass->getOrder();
			$this->pages[$ass->getPage()] = null;
			$block = null;
			if(isset($this->availables[$blockId]))
			{
				$block = $this->availables[$blockId];
				unset($this->availables[$blockId]);
			}
			if(!isset($this->assigned[$position]) || !is_array($this->assigned[$position]))
				$this->assigned[$position] = array();
			if(!empty($block))
				$this->assigned[$position][$order] = $block;
		}
	}
	
	public function loadPageLabels($pages)
	{
		foreach($pages as $pag)
			$this->pages[$pag->getId()] = $pag->getLabel(); 
		return $this;
	}

	public function getPagesIds()
	{
		return array_keys($this->pages);
	}
	
	public function getAssigned()
	{
		return $this->assigned;
	}
	
	public function itemsInPos($pos)
	{
		return isset($this->assigned[$pos]) ? $this->assigned[$pos] : array();
	}
	
	public function addPosition(SimpleXMLElement $pos)
	{
		$key = (string)$pos['id'];
		$val = (string) $pos;
		$this->positions[$key] = $val;
		return $this;
	}
	

	public function getPositions()
	{
		return $this->positions;
	}
	
	
	public function addAvailables($availables)
	{
		foreach($availables as $avail)
		{
			$this->availables[$avail->getId()] = $avail;
			$this->pages[$avail->getPage()] = null;
		}
		return $this;
	}
}
?>
<?php

class NW_Tags_Tag
{
    private $name;
    private $taggedId;
    private $taggedType;
    const TABLE = 'tbl_tags';
    
    function __construct($name) {
        $this->name = $name;
    }

    public function getName() {
        return trim($this->name);
    }

    public function setName($name) {
        $this->name = trim($name);
        return $this;
    }

    public function getTaggedId() {
        return $this->taggedId;
    }

    public function setTaggedId($taggedId) {
        $this->taggedId = $taggedId;
        return $this;
    }

    public function getTaggedType() {
        return $this->taggedType;
    }

    public function setTaggedType($taggedType) {
        $this->taggedType = $taggedType;
        return $this;
    }
    
    public function save()
    {
        try {
            return $this->_insert();
        } catch(Exception $e)
        {
            throw $e;
        }
    }
    
    public static function import($map)
    {
        $item = new NW_Tags_Tag;
        $item->setName($map['name'])
                ->setTaggedId($map['tagged_id'])
                ->setTaggedType($map['tagged_type']);
        return $item;
    }
    public function export()
    {
        $map = array();
        $map['name'] = $this->getName();
        $map['tagged_id'] = $this->getTaggedId();
        $map['tagged_type'] = $this->getTaggedType();
        return $map;
    }
    
    private function _insert()
    {
        try {
           $q = new NW_Db_Query_Insert(self::TABLE, NW_Db_Query_Insert::RETURN_TRUE_FALSE);
           $q->addItemsArray($this->export());
           NW_Application::getSingleton('database/default')->performInsertQuery($q);
           print $q->getQuery();
           return $this;
        } catch(Exception $e)
        {
            throw $e;
        }
    }
    
    public static function update($tagName, $taggedId)
    {
        try {
           $q = new NW_Db_Query_Update(self::TABLE, NW_Db_Query_Update::RETURN_TRUE_FALSE);
           $q->add($this->export())
                   ->where(array('tagged_id=?', $taggedId))
                   ->where(array('name=?', $tagName));
           
           NW_Application::getSingleton('database/default')->performUpdateQuery($q);
        } catch(Exception $e)
        {
            throw $e;
        }
    }
    
    public function delete()
    {
        try {
           $q = new NW_Db_Query_Delete();
           $q->delete()->from(self::TABLE)->where(array('name=?', $this->getName()))
                                            ->where(array('tagged_id=?', $this->getTaggedId()));
           
           NW_Application::getSingleton('database/default')->performDeleteQuery($q);
           return $this;
        } catch(Exception $e)
        {
            throw $e;
        }
    }


}
?>

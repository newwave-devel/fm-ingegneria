<?php

class Pages_Form_Taxonomy_Checkbox extends Zend_Form {
	
    protected $taxonomy;
    protected $availables = array(); 
    protected $selected = array();
    
    public function setTaxonomy(Pages_Model_Taxonomy $taxonomy)
    {
        $this->taxonomy = $taxonomy;
    }
    
    public function setAvailables($availables = array())
    {
        $this->availables = $availables;
    }
    
    public function setSelected($selected = array())
    {
    	$this->selected = $selected;
    }
    
    public function init() 
    {
        $formId = "form_tax_".$this->taxonomy->getName();
        $this->setMethod('post')
                ->setAttrib('class', 'form ajaxtaxonomysubmit')
                ->setAction("/pages/taxonomy/save")
                ->setAttrib("id", $formId);
        
        $pageId = new NW_Form_Element_Hidden('pageid');
        $pageId->addValidator('Digits',false)
        		->setRequired(true);
       
        $taxonomy = new NW_Form_Element_MultiCheckBox('related');
        if(!empty($this->selected))
        {
        	if(is_array($this->selected))
        	{
        		$sel = array();
        		foreach($this->selected as $s)
        			$sel[] = $s->getId();
        	} else {
        		$sel = $selected;
        	}
        	$taxonomy->setValue($sel);
        }
        $taxonomy->setRequired(false);
        
        $role = new NW_Form_Element_Hidden('role');
        $role->setValue($this->taxonomy->getInvokerrole());
        
        $taxonomyName = new NW_Form_Element_Hidden('taxonomy');
        $taxonomyName->setRequired(true)
            ->setValue($this->taxonomy->getName());
        
        

        if(!empty($this->availables))
        {
            foreach($this->availables as $avPage)
                $taxonomy->addMultiOption($avPage->getId(), $avPage->getLabel());
        }
        
        $this->addElements(array($pageId, $taxonomy, $role,$taxonomyName));
    }

    public function populate(array $values) 
    {    

    }
    
}


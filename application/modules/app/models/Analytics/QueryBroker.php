<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Visits
 *
 * @author Ale
 */
class App_Model_Analytics_QueryBroker {

    private $dimensions;
    private $metrics;
    private $service;
    private $query;
    private $id;
    

    public function __construct(Zend_Gdata_Analytics &$service, $id) {
        $this->service = $service;
        $this->dimensions = array();
        $this->metrics = array();
        $this->id = $id;
        $this->query = $this->service->newDataQuery()->setProfileId($this->id);
    }

    public function addDimension($dim) {
        $this->dimensions[] = $dim;
        return $this;
    }

    public function addMetric($met) {
        $this->metrics[] = $met;
        return $this;
    }
    
    public function getQuery()
    {
        return $this->query;
    }
    //refer to http://ga-dev-tools.appspot.com/explorer/ for a better understanding of code
    public function query() {
        $aggregates = new App_Model_Analytics_Aggregates($this->dimensions, $this->metrics);
        
        foreach ($this->dimensions as $dim)
            $this->query->addDimension($dim);

        foreach ($this->metrics as $met)
            $this->query->addMetric($met);

        foreach ($this->service->getDataFeed($this->query) as $row) {
            //extract all the "x" (dimensions)
            foreach ($this->dimensions as $dimensionName)
            {
                $currentDimensionValue = $row->getValue($dimensionName)->getValue();    //android browser, for example
                
                foreach($this->metrics as $metricName)
                {
                    $currentMetricValue = intval($row->getMetric($metricName)->getValue());
                    $aggregates->addY($dimensionName, $metricName, $currentDimensionValue, $currentMetricValue); //print("<li>$currentDimensionValue: $currentMetricValue</li>");
                }
            }
                
            
        }
        
        return $aggregates;
    }

}


class App_Model_Analytics_Aggregates
{
    private $aggregates;
    
    public function __construct($dimensions, $metrics)
    {
        $this->aggregates = array();
        foreach($dimensions as $dim)
        {
            $this->aggregates[$dim] = array();
            foreach($metrics as $met)
                $this->aggregates[$dim][$met] = array();
        }
    }
    
    public function Y($dimension, $metric, $xValue)
    {
        return $this->aggregates[$dimension][$metric][$xValue];
    }
    
    public function setY($dimension, $metric, $xValue, $yValue)
    {
        $this->aggregates[$dimension][$metric][$xValue] = $yValue;
    }
    
    public function addY($dimension, $metric, $xValue, $yValue, $default = 0)
    {
        if(!isset($this->aggregates[$dimension][$metric][$xValue])) 
            $this->aggregates[$dimension][$metric][$xValue] = $default;
        $this->aggregates[$dimension][$metric][$xValue] += $yValue;
    }
    
    public function getXs($dimension, $metric)
    {
        return array_keys($this->aggregates[$dimension][$metric]);
    }
    
    public function getYs($dimension, $metric)
    {
        return array_values($this->aggregates[$dimension][$metric]);
    }
    
    public function getCartesian($dimension, $metric)
    {
        return new App_Model_Analytics_Cartesian($this->getXs($dimension, $metric), $this->getYs($dimension, $metric)); 
    }
    
}

class App_Model_Analytics_Cartesian
{
    public $x;
    public $y;
    
    public function __construct($x, $y) {
        $this->x = $x;
        $this->y = $y;
    }
}
?>

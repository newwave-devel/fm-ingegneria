<?php

class Zend_View_Helper_Date extends Zend_View_Helper_Abstract {

    public $view;
  
    public function setView(Zend_View_Interface $view) {
        $this->view = $view;
    }

    public function date($format, $ts) {
    	if(!is_null($ts) && !empty($ts))
        	return strftime($format, $ts);
    	return $ts;
    }

}

?>

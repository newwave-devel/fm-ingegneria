<?php

/**
 * @author Ale
 *
 */
class Pages_Model_Taxonomy
{

    private $_name;

    private $_invoker;

    private $_invokerRole;

    private $_related;

    private $_invokerId;

    private $_label;
    
    private $_html;
    
    public static function fromXml(SimpleXMLElement $res, $pageTemplate)
    {
    	$html = (string)$res['html'];
    	$item =  new Pages_Model_Taxonomy($res->getName(), $pageTemplate, null, (string)$res->label, $html);
    	$item->setInvokerrole((string)$res['role']);
    	return $item;
    }
    
    public function __construct($taxonomyName='', $invoker='', $invokerId = null, $label = null, $html = '')
    {
        $this->_name = $taxonomyName;
        $this->_invoker = $invoker;
        $this->_invokerId = $invokerId;
        $this->_label= $label;
        $this->_html = $html;
    }

    /**
     *
     * @return the unknown_type
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     *
     * @param unknown_type $_name            
     */
    public function setName($_name)
    {
        $this->_name = $_name;
        return $this;
    }

    /**
     *
     * @return the unknown_type
     */
    public function getInvoker()
    {
        return $this->_invoker;
    }

    /**
     *
     * @param unknown_type $_invoker            
     */
    public function setInvoker($_invoker)
    {
        $this->_invoker = $_invoker;
        return $this;
    }

    /**
     *
     * @return the unknown_type
     */
    public function getInvokerrole()
    {
        return $this->_invokerRole;
    }

    /**
     *
     * @param unknown_type $_invokerRole            
     */
    public function setInvokerrole($_invokerRole)
    {
        $this->_invokerRole = $_invokerRole;
        return $this;
    }

    /**
     *
     * @return the unknown_type
     */
    public function getRelated()
    {
        return $this->_related;
    }

    /**
     *
     * @param unknown_type $_dependent            
     */
    public function setRelated($_dependent)
    {
        $this->_related = $_dependent;
        return $this;
    }

    /**
     *
     * @return the unknown_type
     */
    public function getInvokerid()
    {
        return $this->_invokerId;
    }

    /**
     *
     * @param unknown_type $_invokerId            
     */
    public function setInvokerid($_invokerId)
    {
        $this->_invokerId = $_invokerId;
        return $this;
    }

 public function getLabel() {
  return $this->_label;
 }
 
 public function setLabel($_label) {
  $this->_label = $_label;
  return $this;
 }

 public function getHtml() {
  return $this->_html;
 }
 
 public function setHtml($_html) {
  $this->_html = $_html;
  return $this;
 }
 
 
}
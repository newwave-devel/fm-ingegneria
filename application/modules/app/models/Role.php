<?php

class App_Model_Role
{
    protected $_id;
    protected $_role;
    //protected $_parent_id;
    const ROLE_ADMINISTRATOR = 1;
    const ROLE_EDITOR = 2;
    const ROLE_GUEST = 3;
    
    public function __construct(array $options = null)
    {
        $this->_id = null;
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
 
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid user property');
        }
        $this->$method($value);
    }
 
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid user property');
        }
        return $this->$method();
    }
 
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }
    
    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $id;
        return $this;
    }
    public function getRole()
    {
        return $this->_role;
    }
    public function setRole($role)
    {
        $this->_role = $role;
        return $this;
    }

    public function export($to = 'array')
    {
        switch($to)
        {
            case 'array' :
                $map = array();
                $map['id'] = $this->getId();
                $map['role'] = $this->getRole();
                return $map;
        }
    }
    
    
    
    public static function import($from)
    {
        $obj = new App_Model_Role();
        if(is_object($from)) {
        $obj->setId($from->id)
                ->setRole($from->role);
        } else {
            $obj->setId($from['id'])
                ->setRole($from['role']); 
        }
        return $obj;
    }   
    
    public function save(App_Model_Role &$mapper)
    {
        try {
            return $mapper->save($this);
        }catch(Exception $e)
        {
            throw $e;
        }
    }

}


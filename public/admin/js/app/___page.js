function page(pageId, blockOrder)
{
    this.pageId = pageId;
    this.blockOrder = blockOrder;
}

page.prototype.addBlock = function()
{
	$.fancybox.close();
	this.refreshBlocks();      
}

page.prototype.drop = function()
{
	if(confirm(dic.confirm_page_removal))
	{
		$.get("/pages/page/delete", {id: this.pageId})
			.done(function(data)
			{ 
				result = jQuery.parseJSON(data);
				if(result.status=="ok")
				{
					$.get(result.payload)
						.done(function(data){ window.my_dash_main_content.fadeOut(500).html(data).fadeIn(500);} )
						.fail(function(){alert("There has been some errors processing the request");});
				}
					
			} )
			.fail(function(){alert("There has been some errors processing the request");}); 
	}
}

page.prototype.dropBlock = function(blockId)
{
	if(confirm(dic.confirm_block_removal))
	{
		$.get("/pages/block/delete", {id: blockId})
			.done(function(data)
			{ 
				result = jQuery.parseJSON(data);
				if(result.status=="ok")
				{
					blockId = "fieldset#block"+blockId;
					$(blockId).fadeOut(250);
					displayNotification(dic.block_removed_successfully);
				}
					
			} )
			.fail(function(){alert("There has been some errors processing the request");}); 
	}
}

page.prototype.saveTaxonomy=function(responseText, statusText)
{
	if(responseText.status=='ok')
	{
		displayNotification(dic.taxnomy_updated_successfully);
	} else {
		notify(responseText);
	}
}

page.prototype.saveBlock = function(responseText, statusText)
{
	if(responseText.status=='ok')
	{
		page.refreshBlock(responseText.payload.id);
		displayNotification(dic.block_updated_successfully);
	} else {
		notify(responseText);
		//displayNotification(dic.block_updated_successfully);
	}
}

page.add = function(responseText, statusText)
{
	if(responseText.status=='ok')
	{
		var itemId = responseText.payload.id;
		page.refresh(itemId);
		displayNotification(dic.page_saved_successfully);
	} else {
		notify(responseText);
	}
}

page.refresh = function(itemId)
{
	$.get("/pages/page/edit", {id: itemId })
		.done(function(data){ window.my_dash_main_content.html(data);} )
		.fail(function(){alert("There has been some errors processing the request");});
}

page.prototype.refreshBlocks = function()
{
	$.get("/pages/page/edit", {id: this.pageId })
		.done(function(data){ window.my_dash_main_content.html(data);} )
		.fail(function(){alert("There has been some errors processing the request");});
}

page.refreshBlock = function(blockId)
{
	$.get("/pages/block/edit", {id: blockId })
		.done(function(data)
		{
			htmlBlockId = "fieldset#block"+blockId;
			$(htmlBlockId).fadeOut(1000).html(data).fadeIn(1000);
		} )
		.fail(function(){alert("There has been some errors processing the request");});
}

<?php
class Pages_Model_Block_Admin_Service
{
	protected $_service;
	private $_request;
	private $_view;

    public function getService() {
        return $this->_service;
    }

    public function __construct($request=null, &$view=null) {
        $this->_service = new Pages_Model_Block_Service();
        $this->_request = $request;
        $this->_view = $view;
    }
    
    public function edit($model = null, $return_view =false)
    {
    	try {
    		
    		$view = ($return_view) ? new stdClass() : $this->_getView();
    		if(empty($model))
    		{
    			$id = $this->_request->getParam("id");
    			$model = $this->_service->find($id);
    		}
    		else if($model instanceof Pages_Model_Block)
    		{
    			$id = $model->getId();
    		}
    		else 
    		{
    			$id = $model;
    			$model = $this->_service->find($id);
    		}
    		$blockId = $id; //even new blocks have an id 
    		$form = $this->blockForm($model, $blockId);
    		$form->populate($model->getOptions());
    		$view->form = $form;
    		$blockDescriptor = NW_Pages_Block::getInstance()->block($model->getTemplate());
    		$view->cannotDelete = (isset($blockDescriptor["main"]) && ($blockDescriptor["main"]=="Y"));
    		$view->blockTitle = NW_Pages_Page::getInstance()->blockLabel($model->getTemplate());
    		$view->blockId = $blockId;
    		$view->id = $id;
    		if($return_view)
    			return $view;
    	} catch (Exception $e) {
    		throw $e;
    	}
    }
    
    public function blockForm($block, $blockId)
    {
        return Pages_Form_Block_Factory::getForm($block, array("id" => $blockId));
    }
    
    private function _getView()
    {
        return $this->_view;
    }
	
    public function getRandomBlockId()
    {
    	return rand();
    }
    
    public function shared($sortDir = null)
    {
    	try {
    		$view = $this->_getView();
    		$pageId = $this->_request->getParam('page');
    		$pageService = new Pages_Model_Page_Service();
    		$page = $pageService->find($pageId);
    		
    		//loads datas from the descriptor
    		$descriptor = NW_Pages_Page::getInstance()->page($page->getTemplate());
    		$blocks =  $descriptor->shares->blocks->children();
    		$positions = $descriptor->shares->positions->children();
    		//loads datas from the descriptor
    		
    		$dto = new Pages_Model_Block_Shared_DTO();   		
    		$sharedService = new Pages_Model_Block_Shared_Service();
    		
    		$invokerId = $pageId;
    		//sets the positions
    		foreach($positions as $pos)
    			$dto->addPosition($pos);
    		
    		$form = new Pages_Form_Block_Shared(array("positions" => $positions));
    		$form->getElement("page")->setValue($pageId);

            $sort = ($sortDir) ?  array("ts_creation" => $sortDir, "page" => $sortDir) : array("ts_creation" => "DESC", "page" => "DESC");
    		
    		$blockIds = array();
    		foreach($blocks as $block)
    		{
    			
    			$filters = $block->filters->children();
    			
    			if($filters->count() == 0)
    			{
    				$paginator = NW_Pages_Paginator::fetchAll();
    				//loads all blocks with template $block['id']

    				$returned = $this->_service->fetchAll($paginator, array("template"=> $block['id'], 'status' => "active", "lang" => $page->getLang()), $sort);
    				$dto->addAvailables($returned);
    			} else
    			{
    				foreach($filters as $filter)
    				{
    					$filterType = $filter->getName();
    					switch($filterType)
    					{
    					    case 'taxonomy':
    					    	$taxonomyId = $filter['id'];
    					    	$whatToFetch = $filter['fetch'];
    					    	$invoker = $filter['invoker'];
    					    	//if I have a taxonomy, I need to fetch the pages related to it first
    					    	$taxonomy = new Pages_Model_Taxonomy($taxonomyId, $invoker, $invokerId);
    					    	$pageService->addTaxonomy($taxonomy);
    					    	$paginator = NW_Pages_Paginator::fetchAll();
    					    	$pages = ($whatToFetch == "parent") ? $pageService->fetchParents($paginator, array("lang" => $page->getLang(), "status" => "active")) : $pageService->fetchDependent($paginator, array("lang" => $page->getLang(), "status" => "active"));
    		
    					    	$pageService->dropTaxonomies();
    					    	$paginator = NW_Pages_Paginator::fetchAll();
    					    	$pageIds = array();
    					    	foreach($pages as $page)
    					    		$pageIds[] = $page->getId();
    					    	$returned = $this->_service->fetchAll($paginator, array("page" => $pageIds, 'template' => $block['id'], "lang" => $page->getLang(), "status" => "active"), $sort);
    					    	$dto->addAvailables($returned);
    					    	break;
    					}
    				}
    			}
    		
    		}
    		$assigned = $sharedService->byPage($pageId);
    		$dto->addAssigned($assigned);
    		
    		$paginator = NW_Pages_Paginator::fetchAll();
    		$pages = $pageService->fetchAll($paginator, array("id" => $dto->getPagesIds()));
    		$dto->loadPageLabels($pages);
    		$view->dto = $dto;
    		$view->form = $form;
    		$view->id = $pageId;
    	} catch(Exception $e)
    	{
    		throw $e;
    	}
    }
	
}
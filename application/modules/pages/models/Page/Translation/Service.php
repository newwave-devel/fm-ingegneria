<?php
class Pages_Model_Page_Translation_Service
{
	protected $_mapper;

    public function getMapper() {
        return $this->_mapper;
    }

    public function __construct() {
        $this->_mapper = new Pages_Model_Page_Translation_Mapper();
    }
	
	public function fetchAll($paramsSearch = array())
	{
		try 
		{
			return $this->_mapper->fetchAll($paramsSearch);
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function find($paramID)
	{
		try{
			$model = new Pages_Model_Page_Translation();
			$this->_mapper->find($paramID, $model);
			return $model;
		} catch(Exception $e)
		{
				
		}
	}
	
}
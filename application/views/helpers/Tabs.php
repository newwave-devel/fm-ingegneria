<?php
class Zend_View_Helper_Tabs extends Zend_View_Helper_Abstract
{
	public $view;
	
	public function setView(Zend_View_Interface $view) {
		$this->view = $view;
	}
	
    public function tabs($tabs)
    {      
        $html = '<ul>'.PHP_EOL;
        foreach ($tabs as $tabItem)
        	$html .= sprintf('<li><a href="%s">%s</a></li>', $tabItem['href'],$this->view->translate($tabItem['label'])).PHP_EOL;
        $html .= '<ul>'.PHP_EOL;
        return $html;
    }
}

?>

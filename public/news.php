<?php
$pageId = 'news';
include "inc/required.php";
include "inc/common/contents.php";
?>

<?php include "inc/common/head.php" ?>
        <!-- SCRIPT -->
		<script type="text/javascript" src="/common/js/core.js"></script>
        <script type="text/javascript" src="/common/js/jquery.sharrre.min.js"></script>
        <script type="text/javascript" src="/common/js/custom.js"></script>
        <script type="text/javascript" src="/common/js/jquery.formplugin.min.js"></script>
        <script type="text/javascript" src="/common/js/news-search.js"></script>
        <script type="text/javascript">	
                jQuery(window).load(function() {
                    // START
					loading();
					share();
					projects_accordion();
					pagination();
                    ajaxForm("#frmController", "#newsList");
					fancybox();
                });
        </script>

        
	</head>    
    
	<body lang="it">
    
    	<div id="loader"></div>
    	    	
        <?php include "inc/common/header.php"?>
        
        <section id="main_page" class="projects">
        	<section class="container">
            
            	<section id="page_title">
                    <h3><?php $translator->{"Pagina news"}->__("News") ?></h3>
                    
                    <?php include "inc/common/social_share.php"?>
                    
                </section><!--page_title-->

                    <aside class="mainCol">
                	    <?php include "inc/news/list/list.php"?>
                    </aside>
                    <aside class="sidebar">
                    
                        <?php  include "inc/news/list/accordion.php"?>

                        <form id="frmController" class="search_bar" action="newslist.ajax.php">
                            <input type="text" name="search" value="<?php echo  $filter_search ?>">
                            <input type="hidden" name="disciplines" value="<?php echo $filter_disciplines ?>">
                            <input type="hidden" id="curPage" name="pag" value="<?php echo $paginator->curPage ?>">
                            <input type="button" class="submit_btn" value="Search">
                        </form>
                    </aside><!--sidebar-->
                
                </section><!--main_data-->
                                
                
                
            </section><!--container-->
        </section><!--main_page-->
        
        <?php include "inc/common/footer.php"?>
		
	</body>
	
</html>
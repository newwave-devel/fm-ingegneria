<?php
class NW_Utils_DateTime
{
    public static function strptime($date, $format)
    {
        $strptime = strptime($date, $format);
        if($strptime!= false)
            return mktime($strptime['tm_hour'], $strptime['tm_min'], $strptime['tm_sec'], $strptime['tm_mon']+1, $strptime['tm_mday'], $strptime['tm_year']+1900);
        return false;
    }
}
?>

<?php

/**
 * Description of RetCode
 *
 * @author Ale
 */
class NW_View_RetCode {
     
    public $status;
    public $payload;
    
    const STATUS_ERROR = 'ko';
    const STATUS_SUCCESS = 'ok';
   
   
    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    public function getPayload() {
        return $this->payload;
    }

    public function setPayload($payload) {
        $this->payload = $payload;
        return $this;
    }
    
    public function addToPayload($key, $value) {
    	if(empty($this->payload)) 
    		$this->payload = new stdClass();
        $this->payload->$key = $value;
        return $this;
    }

    function __construct($status=self::STATUS_ERROR, $payload=null) {
        $this->status = $status;
        $this->payload = (empty($payload)) ? new stdClass() : $payload;
    }
    
    public function setStatusSuccess()
    {
    	$this->status = self::STATUS_SUCCESS;
    	return $this;
    }
    
    public function setStatusError()
    {
    	$this->status = self::STATUS_ERROR;
    	return $this;
    }
    
    public static function success($message='')
    {
    	$item = new NW_View_RetCode;
    	$item->status = self::STATUS_SUCCESS;
    	$item->payload = $message;
    	return $item;
    }
    
    public static function error($message='')
    {
    	$item = new NW_View_RetCode;
    	$item->status = self::STATUS_ERROR;
    	$item->payload = $message;
    	return $item;
    }


    
    
}

?>

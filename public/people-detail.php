<?php
$pageId = 'people-detail';
include "inc/required.php";
include "inc/common/contents.php";
?>


<?php include "inc/common/head.php" ?>
		
        
        <!-- SCRIPT -->
		<script type="text/javascript" src="/common/js/core.js"></script>
        <script type="text/javascript" src="/common/js/jquery.sharrre.min.js"></script>
        <script type="text/javascript" src="/common/js/custom.js"></script>
        
        <script type="text/javascript">	
                jQuery(window).load(function() {
                    // START
					loading();
					share();
					fancybox();
                });
        </script>
        
	</head>    
    
	<body lang="it">
    
    	<div id="loader"></div>
    	    	
        <?php include "inc/common/header.php"?>
        
        <section id="main_page">
        	<section class="container">
            
            	<section id="page_title">
                	<h4><?php echo $mainBlock->_("role")?></h4>
                    <h3><?php echo $mainBlock->_("title")?></h3>
                    
                    <?php include "inc/common/social_share.php"?>
                    
                </section><!--page_title-->
                
                <section id="main_data" class="people">
                	<?php include "inc/people/detail/image.php"; ?>
                	<aside class="mainCol">
                    	<section id="discipline_text">
                            <h3><?php $translator->{"Profilo People"}->__("Profile") ?></h3>
                            <?php echo $mainBlock->_("description")?>
                        </section><!--discipline_text-->
                    </aside><!--mainCol-->
                    
                </section><!--main_data-->
                               
                  
                    <?php include "inc/people/detail/tags.php"; ?>
                    
                    <section id="home_articles">
                    
                    <?php include "inc/people/detail/main-col.php"; ?>
                    
                </section><!--home_articles-->
                    
                
            </section><!--container-->
        </section><!--main_page-->
        
        <?php include "inc/common/footer.php"?>
		
	</body>
	
</html>
<?php

class Pages_TaxonomyController extends NW_Controller_Action
{
    private $_service;
    protected $_request;
    private $_display;

    public function init()
    {
        $this->_translateAdapter = Zend_Registry::get('Zend_Translate');
        $this->_request = $this->getRequest();
        $this->_service = new Pages_Model_Taxonomy_Service();
        $this->display = App_Model_Display_Service::getInstance();
    }

    public function selectAction()
    {
        try {
        	$this->_helper->switchContextByUser();
            $this->_setAjaxAction();
            if (! $this->_hasParam('pageid'))
                throw new Exception("Page id not found");
            if (! $this->_hasParam('name'))
                throw new Exception("Taxonomy name not found");
            $pageService = new Pages_Model_Page_Service();
            
            $pageId = $this->_getParam("pageid");
            $taxonomyName = $this->_getParam("name");
            
            $page = $pageService->find($pageId);
            $taxonomy = NW_Pages_Page::getInstance()->taxonomy($page->getTemplate(),$taxonomyName);
            
            if($taxonomy->getInvokerrole() == 'parent')
            {
	            $paginator = NW_Pages_Paginator::fetchAll();
	            $availables = $pageService->addTaxonomy($taxonomy)->fetchAvailable($paginator, array("lang" => $page->getLang()));
	            $pageService->dropTaxonomies();
	            
	            $paginator = NW_Pages_Paginator::fetchAll();
	            $taxonomy->setInvokerid($pageId);
	            $selected = $pageService->addTaxonomy($taxonomy)->fetchDependent($paginator,array("lang" => $page->getLang()));
            } else {
            	$paginator = NW_Pages_Paginator::fetchAll();
            	$availables = $pageService->addTaxonomy($taxonomy)->fetchAvailable($paginator, array("lang" => $page->getLang()));
            	$pageService->dropTaxonomies();
            	$paginator = NW_Pages_Paginator::fetchAll();
            	$taxonomy->setInvokerid($pageId);
            	$selected = $pageService->addTaxonomy($taxonomy)->fetchParents($paginator, array("lang" => $page->getLang()));
            }
            $form = Pages_Form_Taxonomy_Factory::getForm($taxonomy, array('taxonomy' => $taxonomy, 'availables' => $availables, 'selected' => $selected));
            $form->getElement("pageid")->setValue($pageId);
            $this->view->form = $form;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function saveAction()
    {
        try {
    		$this->_setJsonAction();		
    		$form = new Pages_Form_Taxonomy();
    		$status = new NW_View_RetCode();
    		
    		if ($this->_request ->isPost())
    		{
    			if ($form->isValid($this->_request->getPost()))
    			{
    			    $dto = new Pages_Model_Taxonomy_DTO($form->getValues());
    			    $this->_service->save($dto);
    			    $status->setStatusSuccess();
    			}
    			else {		
    				$errors = '';
    				foreach($form->getMessages() as $field => $fieldErrors)
    				    $errors .= sprintf("<strong>Campo %s</strong>: %s<br />", $field, current($fieldErrors));

    				$status->setPayload($errors);
    			}
    			$this->view->status = $status;
    		}
    	} catch(Exception $e)
    	{
    		$this->view->status = NW_View_RetCode::error($e->getMessage());
    	}
    }
}


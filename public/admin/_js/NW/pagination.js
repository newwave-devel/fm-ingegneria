NW.Pagination = (function () {
    // Initialize the application
    var items = {};
    
    pagination = function(formId, targetId)
    {
        //the form that actually sends the request
       this.formId = formId;
       this.targetId = targetId;
    }
    
    pagination.prototype = 
    {
        ajaxifyForm : function()
        {
            $(this.formId).ajaxForm(
            {
                target: this.targetId
            });
        },
        registerPageLink : function(pageElementClass)
        {
            var formId = this.formId;
            $("."+pageElementClass).click(function()
            {
                val = $(this).attr("rel");
                $(formId + " input[name='pag']").val(val);
                $(formId).submit();
                return false;
            });
        },
        registerIppSelect : function(selectName)
        {   
            var formId = this.formId;
            $("select[name='" +selectName+ "']").change(function()
            {
                val = $(this).val();
                $(formId + " input[name='pag']").val(1);
                $(formId + " input[name='ipp']").val(val);
                $(formId).submit();
            });
        },
        registerPageSelect: function(selectName)
        {
            var formId = this.formId;
            $("select[name='" + selectName + "']").change(function()
            {
                val = $(this).val();
                $(formId + " input[name='pag']").val(val);
                $(formId).submit();
            });
        }
    }  
    // Return the public facing methods for the App
    return {
        add: function (formId, targetId) {
            p = new pagination(formId, targetId);
            p.ajaxifyForm();
            items[formId] = p;
            return items[formId];
        },
        get: function(formId)
        {
            return items[formId];
        }     
    };
}());


var NW_Pagination = NW.Pagination;
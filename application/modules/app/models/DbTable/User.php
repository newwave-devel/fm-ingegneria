<?php

class App_Model_DbTable_User extends Zend_Db_Table_Abstract
{

    protected $_name = 'tbl_users';
    protected $_primary = 'id';
    
    protected $_referenceMap    = array(
        'Role' => array(
            'columns'           => 'role',
            'refTableClass'     => 'App_Model_DbTable_Role',
            'refColumns'        => 'id'
        )
        
    );
  
}


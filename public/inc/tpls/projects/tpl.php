<?php
$tpl_discipline_accordion = <<<TPL
	<div class="level">
		<a class="opening %s" href="#">%s</a>
		<div class="expanded %s">
			<ul>
				%s
			</ul>
		</div>
		<!--/.expanded-->
	</div>
TPL;

$tpl_countries_accordion = <<<TPL
	<div class="level">
		<a class="opening %s" href="#">%s</a>
		<div class="expanded %s">
			<ul>
				%s
			</ul>
		</div>
		<!--/.expanded-->
	</div>
TPL;

$tpl_listItem = <<<TPL
	<article class="project %s">
		<a href="project-detail.php?id=%d"> 
			<img class="img" src="/assets/res.php?src=%s&w=230&h=230" alt="%s">
			<h6>%s</h6>
			<h3>%s</h3>
			<div class="color_wrapper"></div>
		</a>
	</article>
TPL;


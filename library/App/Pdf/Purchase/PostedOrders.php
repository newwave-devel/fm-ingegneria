<?php
class App_Pdf_Purchase_PostedOrders extends App_Pdf
{
	public function __construct()
	{
		parent::__construct('utf-8', 'A4-L');
		$this->documentName = 'Registro_Ordini.pdf';
	}
	
	public function setHeader()
	{
		$html_header = <<<HTML
		<div>
			<table border="1" cellpadding="0" cellspacing="0" style="width: 863px;">
				<tbody>
					<tr>
						<td rowspan="3">
							<p><img src="common/img/docs/logo.gif" style="height: 72px; width: 235px" /></p>
						</td>
						<td rowspan="3">
							<p>REGISTRO DEGLI ORDINI DI SICUREZZA</p>
						</td>
						<td>
							<p>MOD PU 10 01/03</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>Rev.00 del 01/01/2012</p>
	
							<p>Pagina {PAGENO} di {nb}</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>DPA</p>
						</td>
					</tr>
				</tbody>
			</table>
		<p>&nbsp;</p>
	</div>
HTML;
		$this->SetHTMLHeader($html_header);
		return $this;
	}
	
}

?>
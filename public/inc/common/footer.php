<footer>
	<section class="container">
		<h3>F&M Ingegneria Spa</h3>
		<h5><?php $translator->{"Footer"}->__("2013 &copy; F&M Ingegneria S.p.A. - IT02916640275")?></h5>
		<ul>
		    <li><a href="/static.php?id=terms_conditions"><strong><?php $translator->{"Footer"}->__("TERMS AND CONDITIONS") ?></strong></a></li>
            <li>-</li>
			<li><a href="/static.php?id=privacy"><strong><?php $translator->{"Footer"}->__("LEGAL AND CORPORATE INFORMATION") ?></strong></a></li>
		</ul>
        <br />
        <ul class="credits">
        	<li>Total creative direction: <a href="http://www.tessariassociati.com" target="_blank" title="Tessari Associati">Tessari Associati</a> | Web development: <a href="http://www.newwave-media.it" target="_blank" title="Newwave Media">Newwave</a></li>
        </ul>
        
         <h5 id="fm_retail"><a href="http://www.fm-retail.de/" target="_blank">FM retail</a></h5>

		<section id="social_box">
			<h5><?php $translator->{"Footer"}->__("FOLLOW US")?>:</h5>
			<ul>
				<li><a id="twitter" href="https://twitter.com/FMingegneria" target="_blank">Twitter</a></li>
				<li><a id="facebook" href="https://www.facebook.com/pages/FM-Ingegneria/324170300928387?ref=stream" target="_blank">Facebook</a></li>
				<li><a id="youtube" href="https://www.youtube.com/channel/UCMeObaSywGH8QsJAsF5ANHg/feed" target="_blank">Youtube</a></li>
				<li><a id="linkedin" href="http://it.linkedin.com/pub/f-m-ingegneria-spa/8a/1a4/219/" target="_blank">Linkedin</a></li>
			</ul>
		</section>
		<!--social_box-->
	</section>
	<!--container-->
</footer>

<!--<section id="privacySlider">
<p>Utilizzando il nostro sito web, si acconsente all'impiego di cookies in conformità alla nostra <a class="zoom" data-fancybox-type="iframe" href="cookies.html">politica sui cookies</a> &nbsp;&nbsp;<a id="cookieBtn" href="#">Ho capito</a></p>
</section><!--privacySlider-->
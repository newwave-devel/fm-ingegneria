var dic = {
	confirm_block_removal: "Confermi la cancellazione del blocco?",
	confirm_file_deletion: "Confermi la cancellazione del file?",
	confirm_page_removal: "Confermi la cancellazione della pagina?",
	block_removed_successfully: "Blocco rimosso con successo",
	account_update_successfully: "Account aggiornato con successo",
    translation_updated_successfully: "Traduzione aggiornata con successo"
}



<?php

class NW_Defs_Relations extends NW_XmlDescriptor // NW_Descriptors_Descriptor //
{
    
    const ROLE_RELATING = 'relating';
    const ROLE_RELATED = 'related';

    const MODEL_CENTRALIZED = 'centralized';
    const MODEL_INTABLE = 'intable';

    //
    // returns the "relating" node
    public function relating($relDef, $attr = '')
    {
        if($attr == '')
            return $this->descriptor->$relDef->relating;
        $reldef = $this->descriptor->$relDef->relating;
        return strval($reldef[$attr]);
    }
    //
    // returns the "related" node
    public function related($relDef, $attr = '')
    {
        if($attr == '')
        return $this->descriptor->$relDef->related;
        $reldef = $this->descriptor->$relDef->related;
        return strval($reldef[$attr]);
    }

    public function relation($relDef, $attr)
    {
        $reldef = $this->descriptor->$relDef;
        return strval($reldef[$attr]); 
    }

}

?>
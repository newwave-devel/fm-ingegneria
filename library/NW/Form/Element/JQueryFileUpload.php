<?php

/**
 * Description of Hidden
 *
 * @author Alessio
 */
class NW_Form_Element_JQueryFileUpload extends Zend_Form_Element_Hidden
{
	private $_hasFile = false;
	
    public function __construct($spec, $options = null)
    {
        parent::__construct($spec, $options);
        
        $this->setDecorators(array(new NW_Form_Decorator_JQueryFileUpload()));     
        $this->getView()->jQuery()->addJavascriptFile('/admin/js/vendors/jQuery-File-Upload/js/jquery.iframe-transport.js');
        $this->getView()->jQuery()->addJavascriptFile('/admin/js/vendors/jQuery-File-Upload/js/jquery.fileupload.js');
        $this->getView()->jQuery()->addJavascriptFile('/admin/js/NW/NW.js')
        							->addJavascriptFile('/admin/js/NW/JQueryFileUpload.js');
        $this->getView()->jQuery()->addOnload(sprintf("var %s = NW_JQueryFileUpload.add('%s');", $this->getName(), $this->getName()));
        $this->getView()->jQuery()->addOnload(sprintf("%s.fileupload();", $this->getName()));
    }

    public function setHasFile()
    {
    	$this->_hasFile = true;
    	return $this;
    }
    
    public function hasFile()
    {
    	return $this->_hasFile;
    }

    
}

?>

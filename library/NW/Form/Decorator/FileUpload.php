<?php

class NW_Form_Decorator_FileUpload extends NW_Form_Decorator
{

	public function render($content)
	{
		$element = $this->getElement();
		 
		if (!$element instanceof Zend_Form_Element)
		{
			return $content;
		}
		if (null === $element->getView())
		{
			return $content;
		}
	
		$separator = $this->getSeparator();
		$placement = $this->getPlacement();
		$label = $this->buildLabel();
		$input = $this->buildInput();
		$errors = $this->buildErrors();
	
	
		$desc = $this->buildDescription();
		$extra_class = $this->getOption('class');
		if (is_array($extra_class))
			$itemClass = empty($extra_class) ? 'formBlock' : sprintf("input %s", implode(" ", $extra_class));
		else
			$itemClass = empty($extra_class) ? 'formBlock' : sprintf("input %s", $extra_class);
	
		$buttonLabel = ($element->hasFile()) ? sprintf('<span>%s</span>','Override existing file') : sprintf('<span>%s</span>','Upload...');
		$buttonEraseHtml = ($element->hasFile()) ? sprintf('<span class="cancel">%s</span>','Cancel File') : '';
		$buttonClass = ($element->hasFile()) ? 'btn btn-danger fileinput-button"' : 'btn btn-success fileinput-button"';
		
		switch ($placement)
		{
		    case (self::PREPEND):
		    	return $this->getOutput() . $separator . $content;
		    case (self::APPEND):
		    default:
		    	return $content . $separator . $this->getOutput();
		}
	}
	
	public function getOutput()
	{
		$id = $this->getElement()->getAttrib("id");
		$fieldName = $this->getElement()->getAttrib("fieldname");
		$blockName = $this->getElement()->getAttrib("blockname");
		$tpl = <<<TPL
		
		<section class="gallery">
            <h4 class="title">Files</h4>
            <section class="previews" id="%s" rel="%s">
				%s
			</section>
			<div class="clearer"></div>
                
				<nav class="button_toolbar">
					<a class="btn btn-default" onclick="window.main_page.subPage().libraryAddFromLocal(this)"><span class="icon_font">&#xf108;</span>Add from local</a>
					<a class="btn btn-default" onclick="window.main_page.subPage().libraryAddFromLibrary(this)"><span class="icon_font">&#xf03e;</span>Add from library</a>
					<div class="clearer"></div>
				</nav><!--button_toolbar-->
       </section><!--gallery-->
TPL;
		return sprintf($tpl, $id, $blockName, $this->_preview($id));
	}
	
	
	
	private function _preview($id)
	{
		$mediaService = new App_Model_Asset_Service();
		$paginator = NW_Pages_Paginator::fetchAll();
		$value =  $this->getElement()->getValue();
		$uploadedFilesHtml = '';
		
		
		if(!empty($value))
		{
			if(strpos($value, ',') !== false)
				$value = explode(",", $value);
			$paramsSearch = array("ids" => $value);
			$uploadedFilesHtml = '';
			foreach($mediaService->fetchAll($paginator, $paramsSearch) as $media)
				$uploadedFilesHtml .= App_Model_Asset_Service::preview($media, $id,$this->getElement()->getAttrib("blockname"));
		}
		return $uploadedFilesHtml;
	}

}

?>
<?php

class App_AdminpanelController extends NW_Controller_Action {

    public function init() {
    }
    /**
     * redirects user to /app/usertypecontroller 
     * @throws Exception
     */
    public function indexAction() {
        try {
        	//loads the permissions
        	//App_Model_Permission_Manager::load();
            switch ($this->_helper->Role()) {
                case App_Model_Role::ROLE_ADMINISTRATOR:
                    $this->_forward(null, 'administrator', 'profiles');
                    break;
                case App_Model_Role::ROLE_EDITOR:
                    $this->_forward(null, 'editor', 'profiles');
                    break;
                case App_Model_Role::ROLE_GUEST:
                    $this->_forward(null, 'guest', 'profiles');
                    break;
            }
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function logoutAction()
    {
    	App_Model_Permission_Manager::destroy();
    	Zend_Auth::getInstance()->clearIdentity();
    	$this->_redirect("/");
    }

}


<?php
class App_Model_User_Service {

    protected $_mapper;
    
    public function getMapper()
    {
    	return $this->_mapper;
    }
    
    public function __construct(){
    	$this->_mapper = new App_Model_User_Mapper();
    }

    public function delete($paramId)
    {
    	try {
    		$user = $this->find($paramId);
    		$permissionService = New App_Model_Permission_Service();
    		$this->getMapper()->getDbTable()->getAdapter()->beginTransaction();
    		$permissionService->reset($user);
    		$this->_mapper->delete($paramId);
    		$this->getMapper()->getDbTable()->getAdapter()->commit();
    	} catch(Exception $e)
    	{
    		$this->getMapper()->getDbTable()->getAdapter()->rollBack();
    		throw $e;
    	}
    }
    
    public function save(App_Model_User $user, App_Model_Permissions $permission=null, $resetPassword = false) {
        try {
        	$permissionService = New App_Model_Permission_Service();
        	$this->getMapper()->getDbTable()->getAdapter()->beginTransaction();
        	
        	if($resetPassword)
        		$user->assignNewPassword($user->getPassword());
        	
        	if(!empty($permission))
        	{
        		$id = $this->_mapper->save($user);
        		$user->setId($id);
        		//refresh users (now has id)
        		$permission->setUser($user);
        		$permissionService->save($permission);		
        	} else {
        		$id= $this->_mapper->save($user);
        	}
        	$this->getMapper()->getDbTable()->getAdapter()->commit();
        	return $id;
        } catch (Exception $e) {
        	$this->getMapper()->getDbTable()->getAdapter()->rollBack();
            throw $e;
        }
    }

    public function find($id) {
        try {
        	$model = new App_Model_User();
        	$this->_mapper->find($id, $model);
        	return $model;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function fetchAll(NW_Pages_Paginator &$paginator, $paramsSearch = array(), $paramsOrder = array())
    {
    	try {
    		return $this->_mapper->fetchAll($paginator, $paramsSearch, $paramsOrder);
    	} catch(Exception $e)
    	{
    		throw $e;
    	}
    }
    
    public function fetchChilds(NW_Pages_Paginator &$paginator, $paramsSearch = array(), $paramsOrder = array())
    {
    	try {
    		$paramsSearch = array("pid" => Zend_Auth::getInstance()->getStorage()->read()->getId());
    		return $this->_mapper->fetchChilds($paginator, $paramsSearch, $paramsOrder);
    	} catch(Exception $e)
    	{
    		throw $e;
    	}
    }
    
    public function fetchByIds(&$paginator, $ids)
    {
    	if(empty($paginator))
    	{
    		$paginator = new NW_Pages_Paginator();
    		$paginator->setFetchAll(true);
    	}
    	$params = array("id" => $ids);
    	return $this->fetchAll($paginator, $params);
    }
    
    public function checkUniqueness($username, $email)
    {
    	try {
    		return ($this->_mapper->countBy(
    					array("username=?" => $username,
    							"email=?" => $email
    		))==0);
    	} catch(Exception $e)
    	{
    		throw $e;
    	}
    }
    /**
     * retrieves list of users that are related to vessel 
     * @param unknown $vesselId
     * @throws Exception
     */
    public function getUserByVessel($vesselId, $userRole=null, $load=true)
    {
    	try {
    		$permService = new App_Model_Permission_Service();
    		$userIds = $permService->usersOfVessel($vesselId,$userRole);
    		if(!empty($userIds))
    			return ($load)? $this->fetchAll(NW_Pages_Paginator::fetchAll(), array("id" => $userIds)) : $userIds;
    		return array();
    	} 
    	catch(Exception $e)
    	{
    		throw $e;
    	}
    }
   

}

?>
<?php
$pageId = 'contacts';
include "inc/required.php";
include "inc/common/contents.php";
?>
<?php include "inc/common/head.php" ?>
		
        
        <!-- SCRIPT -->
		<script type="text/javascript" src="common/js/core.js"></script>
        <script type="text/javascript" src="common/js/jquery.sharrre.min.js"></script>
        <script type="text/javascript" src="common/js/custom.js"></script>
        
        <script type="text/javascript">	
                jQuery(window).load(function() {
                    // START
					loading();
					share();
                });
        </script>
        
	</head>    
    
	<body lang="it">
    
    	<div id="loader"></div>
    	    	
        <?php include "inc/common/header.php"?>
        
        <section id="main_page">
        	<section class="container">
            
            	<section id="page_title">
                    <h3>Contacts</h3>
                    
                     <?php include "inc/common/social_share.php"?>
                    
                </section><!--page_title-->
                
                <?php 
                
                	foreach($addresses as $address)
                	{
                		$img = $address->_('file');
                		echo sprintf($tpl_address, $blockService->asset($img)->getAssetUrl(), $address->_('description'));
                	}
                	
                ?>
            </section><!--container-->
        </section><!--main_page-->
        
        <?php include "inc/common/footer.php"?>
		
	</body>
	
</html>
<?php
class Pages_Model_Taxonomy_Mapper
{
	protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Pages_Model_DbTable_Taxonomy');
        }
        return $this->_dbTable;
    }

	

	public function save(Pages_Model_Taxonomy_DTO $model)
	{
		try {
			$datas = $this->_toDb($model);
			$this->getDbTable()->getAdapter()->beginTransaction();
			$role=$model->getInvokerRole();
			$this->delete($model->getTaxonomyName(), $model->getInvokerId(), $model->getInvokerRole());
			
			if($role == 'parent')
			{
				if(is_array($datas['dependent']))
				{
					$deps = $datas['dependent'];
					
					foreach($deps as $dep)
					{
						$datas['dependent'] = $dep;
						$this->getDbTable()->insert($datas);
					}
				} 
				else 
				{
					if(!empty($datas['dependent']) && ($datas['dependent'] != '') && ($datas['dependent'] != 0))
						$this->getDbTable()->insert($datas);
				}
			}
			else
			{
				if(!empty($datas['parent']))
				{
					if(is_array($datas['parent']))
					{
						$deps = $datas['parent'];
						//unset($datas['dependent']);	
						foreach($deps as $dep)
						{
							$datas['parent'] = $dep;
							if(empty($dep)) continue;
							$this->getDbTable()->insert($datas);
						}
					} else {
						
						if(!empty($datas['dependent']) && ($datas['dependent'] != '') && ($datas['dependent'] != 0))
							$this->getDbTable()->insert($datas);
					}
				}
			}
			
			$this->getDbTable()->getAdapter()->commit();
			return true;
		} catch (Exception $e) {
		    $this->getDbTable()->getAdapter()->rollback();
			throw $e;
		}
	}

	
	public function saveRow(Pages_Model_Taxonomy_Row $model)
	{
		try {
				$data = array(
				    'parent' => $model->getParent(), 
					'dependent' => $model->getDependent(),
					'taxonomy' => $model->getTaxonomy(),
					'order' => $model->getOrder()
				);
				Zend_Registry::get("Log")->debug(var_export($data, true));
				$this->getDbTable()->insert($data);
			return true;
		} catch (Exception $e) {
			throw $e;
		}
	}
	

	public function delete($taxonomyName, $invokerId, $invokerRole)
	{
	    try
	    {
	        $table = $this->getDbTable();
	        $where[] = $table->getAdapter()->quoteInto('taxonomy = ?',  $taxonomyName);
	        $where[] = $table->getAdapter()->quoteInto("$invokerRole = ?",  $invokerId);
	        $table->delete($where);
	        return true;
	    } catch(Exception $e)
	    {
	        throw $e;
	    }
	}
	
	public function dropPageFromTaxonomy($pageId)
	{
	    try
	    {
	        $table = $this->getDbTable();
	        $where = $table->getAdapter()->quoteInto('parent = ? OR dependent= ?',  $pageId, $pageId);
	        $table->delete($where);
	        return true;
	    } catch(Exception $e)
	    {
	        throw $e;
	    }
	}

	public function taxonomyRows($pageId)
	{
		try {
			$table = $this->getDbTable();
			$select = $table->select()
				->where('parent = ? OR dependent= ?', $pageId, $pageId);
			$result = array();
			foreach ( $table->fetchAll ( $select ) as $row ) {
				$t = new Pages_Model_Taxonomy_Row($row);
				if($pageId == $row->parent)
					$t->setInvokerRole('parent');
				else $t->setInvokerRole('dependent');
				$result[] =$t;
			}
			return $result;
		} catch(Exception $e)
	    {
	        throw $e;
	    }   
	}
	
	
	
	private function _toDb(Pages_Model_Taxonomy_DTO $model) {
		
	    $values = $model->getValues();
        
        $parent = ($model->getInvokerRole() == 'parent') ? $model->getInvokerId() : $model->getValues();
        $dependent = ($model->getInvokerRole() == 'dependent') ? $model->getInvokerId() : $model->getValues();
        $data = array(
            'parent' => $parent,
            'dependent' => $dependent,
            'taxonomy' => $model->getTaxonomyName(),
            'order' => 1
        );
        return $data;

	}
	
// 	private function _fromDb($row, Pages_Model_Taxonomy_Row &$model) {
// 		$invokerId = $model->getInvokerId();
// 		$parent = $row->parent;
// 		$related = $row->dependent;
// 		$value = $related;
		
// 		if(!empty($invokerId))
// 		{
// 			$role = ($parent == $invokerId) ? 'parent' : 'dependent';
// 			$value = ($role=='parent') ? $related : $parent;
// 		}
		
// 		$model->setTaxonomyName($row->taxonomy)
// 			->setInvokerRole($role)
// 			->setOrder($row->order)
// 			->setValue($value);
// 	}


}
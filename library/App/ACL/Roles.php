<?php
class App_ACL_Roles
{
	const FLEETMASTER = 2;
    const SHIPOWNER = 3;
    const SHIPOFFICER = 4;
    
    
    public static function defaultRole()
    {
    	return self::FLEETMASTER;
    }
}
?>
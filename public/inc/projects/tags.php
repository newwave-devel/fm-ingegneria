<?php 
	$project_tags_taxonomies = array();
	foreach($peoplesBlocks as $block)
		$project_tags_taxonomies[] = array('title' => $block->_("title"), 'link' => sprintf('people-detail.php?id=%d', $block->getPage()));
	foreach($disciplinesBlocks as $block)
		$project_tags_taxonomies[] = array('title' => $block->_("title"), 'link' => sprintf('discipline.php?id=%d', $block->getPage()));
	foreach($disciplinesCatBlocks as $block)
		$project_tags_taxonomies[] = array('title' => $block->_("title"), 'link' => "#");
		//$project_tags_taxonomies[] = array('title' => $block->_("title"), 'link' => sprintf('homepages.php?id=%d', $block->getPage()));
	shuffle($project_tags_taxonomies);
?>
<?php if(!empty($project_tags_taxonomies)) : ?>
<section class="project_tags">

	<h4 class="title">Tag</h4>
	<ul>
	<?php 
		foreach($project_tags_taxonomies as $item)
			echo sprintf('<li><a href="%s">%s</a></li>', $item['link'], $item['title']);
	?>
	</ul>
</section>
<!--project_tags-->
<?php endif; ?>
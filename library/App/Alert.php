<?php
class App_Alert
{
	public $level;
	public $message;
	
	const LEVEL_WARNING = 'warning';
	const LEVEL_ERROR = 'error';
	const LEVEL_NOTIFY = 'notify';
	
	public function __construct($message, $level = self::LEVEL_NOTIFY)
	{
		$this->level = $level;
		$this->message = $message;
	}
	/**
	 * @return the $level
	 */
	public function getLevel() {
		return $this->level;
	}

	/**
	 * @return the $message
	 */
	public function getMessage() {
		return $this->message;
	}

	/**
	 * @param field_type $level
	 */
	public function setLevel($level) {
		$this->level = $level;
	}

	/**
	 * @param field_type $message
	 */
	public function setMessage($message) {
		$this->message = $message;
	}

	
}

?>
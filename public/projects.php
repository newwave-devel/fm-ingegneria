<?php
$pageId = 'projects';
include "inc/required.php";
include "inc/common/contents.php";
?>
<?php include "inc/common/head.php" ?>

<!-- SCRIPT -->


<script type="text/javascript" src="/common/js/core.js"></script>
<script type="text/javascript" src="/common/js/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="/common/js/jquery.sharrre.min.js"></script>
<script type="text/javascript" src="/common/js/custom.js"></script>
<script type="text/javascript" src="/common/js/jquery.formplugin.min.js"></script>
<script type="text/javascript" src="/common/js/project-search.js"></script>
<script type="text/javascript">
    jQuery(window).load(function () {
        // START
        loading();
        share();
        projects_accordion();
        pagination();
        ajaxForm("#frmController", "#prjList");
        $("#tabs").tabs();
		fancybox();
    });

</script>

<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="/common/js/clustering.js"></script>
<?php include "common/js/map.js.php" ?>


</head>

<body lang="it">
<div id="loader"></div>
<?php include "inc/common/header.php" ?>
<section id="main_page" class="projects">
    <section class="container">
        <section id="page_title">
            <h3>
                <?php $translator->{"Pagina 'Progetti'"}->__("Projects") ?>
            </h3>
            <?php include "inc/common/social_share.php" ?>
        </section>
        <!--page_title-->


        <section id="home_countries">
            <div id="tabs">

                <div id="tab_menu">
                    <ul>
                        <li><a href="#tabs-1"><?php $translator->{"Pagina 'Progetti'"}->__("Projects Maps") ?></a></li>
                        <li><a href="#tabs-2"><?php $translator->{"Pagina 'Progetti'"}->__("Projects List") ?></a></li>
                    </ul>
                </div>
                <!--tab_menu-->

                <div id="tabs-1">
                    <div id="map"></div>
                </div>
                <!--tabs-1-->

                <div id="tabs-2">
                    <aside class="mainCol">
                        <?php include "inc/projects/list/list.php" ?>
                    </aside>
                    <aside class="sidebar">
                        <?php include "inc/projects/list/accordion.php" ?>
                        <form id="frmController" class="search_bar" action="projectlist.ajax.php">
                            <input type="text" name="search" value="<?php echo $filter_search ?>">
                            <input type="hidden" name="disciplines" value="<?php echo $filter_disciplines ?>">
                            <input type="hidden" name="countries" value="<?php echo $filter_countries ?>">
                            <input type="hidden" id="curPage" name="pag" value="<?php echo $paginator->curPage ?>">
                            <input type="button" class="submit_btn" value="Search">
                        </form>
                    </aside>
                    <!--sidebar-->
                </div>
                <!--tabs-2-->

            </div>
            <!--tabs-->
        </section>
        <!--home_countries-->


        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>


    </section>
    <!--main_data-->

</section>
<!--container-->
</section>
<!--main_page-->

<?php include "inc/common/footer.php" ?>
</body>
</html>
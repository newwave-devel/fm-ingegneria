<?php
class Pages_Model_Page_Event
{
    const EVENT_BEFORE_SAVE = 1;
    const EVENT_AFTER_SAVE = 2;
    const EVENT_BEFORE_UPDATE = 3;
    const EVENT_AFTER_UPDATE = 4;
    const ADD_BLOCK = 5;
    const DROP_BLOCK = 6;
}
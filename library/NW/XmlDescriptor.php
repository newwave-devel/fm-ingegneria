<?php

class NW_XmlDescriptor {

    protected $descriptor;

    public static function fromFile($descriptor_path) {
        try {
            if (file_exists($descriptor_path) && is_readable($descriptor_path)) {
                $descriptor = simplexml_load_file($descriptor_path);
                if ($descriptor === false)
                    throw new Exception("Invalid Xml received");
                return new static($descriptor);
            }
            else
                throw new Exception("-->Descriptor at $descriptor_path not found");
        } catch (Exception $e) {
            print $e->getMessage();
        }
    }

    protected static function fromString($string) {
        return new static(simplexml_load_string($string));
    }

    protected static function fromDom(DOMNode $node) {
        return new static($node);
    }

    public function __toString() {
        return strval($this);
    }

    public function __construct(SimpleXMLElement $descriptor) {
        try {
            if (empty($descriptor))
                throw new Exception("Empy descriptor received");
            $this->descriptor = $descriptor;
        } catch (Exception $e) {
            print $e->getMessage();
        }
    }

    public function dump() {
        return $this->descriptor->saveXML();
    }

}
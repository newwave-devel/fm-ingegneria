<?php if(is_array($gallery_header) && !empty($gallery_header[0])) : ?>
    <?php if($gallery_header[0]->_('file') != '' ): ?>
    <section id="home_slider">
        <section id="slideshow" class="nivoSlider">
            <?php foreach($blockService->assets($gallery_header[0]->_('file')) as $asset): ?>
                <img src="<?php echo $asset->getAssetUrl()?>" title="#htmlcaption1" alt="" />
            <?php endforeach; ?>
        </section>
    </section>
    <!--home_slider-->
    <?php endif; ?>
<?php endif; ?>
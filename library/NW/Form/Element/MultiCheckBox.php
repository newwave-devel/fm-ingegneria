<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Hidden
 *
 * @author Alessio
 */
class NW_Form_Element_MultiCheckBox extends Zend_Form_Element_MultiCheckbox
{
    public function __construct($spec, $options = null)
    {
        parent::__construct($spec, $options);
        $this->setAttrib('helper', 'formMultiCheckboxList');
        $this->setDecorators(array(new NW_Form_Decorator_MultiCheckBox()));
    }
}

?>

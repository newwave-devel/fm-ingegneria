<?php

class App_Model_Dash_Widget {

    public $boxPosition;
    public $boxTitle;
    public $boxContent;
    public $clearBoth = false;
    
    const BOXPOSITION = 'boxPosition';
    const BOXTITLE = 'boxTitle';

    public function getBoxPosition() {
        return $this->boxPosition;
    }

    public function setBoxPosition($boxPosition) {
        $this->boxPosition = $boxPosition;
        return $this;
    }

    public function getBoxTitle() {
        return $this->boxTitle;
    }

    public function setBoxTitle($boxTitle) {
        $this->boxTitle = $boxTitle;
        return $this;
    }

    public function getBoxContent() {
        return $this->boxContent;
    }

    public function setBoxContent($boxContent) {
        $this->boxContent = $boxContent;
        return $this;
    }
    
    public function render()
    {
        return $this;
    }
    
    public function setclearBoth()
    {
        $this->clearBoth = true;
        return $this;
    }

    public static function getWidget($type, $options=array()) {
        try {
            $className = sprintf("App_Model_Dash_Widget_%s", ucfirst($type));
            if (!class_exists($className))
                throw new Exception("Widget $type not found");
            return new $className($options);
        } catch (Exception $e) {
            throw $e;
        }
    }

}


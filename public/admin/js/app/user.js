function user(userId)
{
    this.userId = userId;
}

user.prototype.addfromDash = function(responseText, statusText, xhr, $form)
{
	var response = JSON.parse(responseText);
	if(response.status=='ok' || response.status=='ko')
	{
		window.main_page.notify(response);
		$.get("/app/users/list").done(function(data){$("#userslist").html(data).fadeIn(550)});
	}
	else {
		response.status='ko';
		response.payload='there has been some errors';
		window.main_page.notify(response);
	}
}

user.prototype.updatemyprofile = function(responseText, statusText, xhr, $form)
{
	var response = JSON.parse(responseText);
	if(response.status=='ok' || response.status=='ko')
		window.main_page.notify(response);
	else {
			response.status='ko';
			response.payload='there has been some errors';
			window.main_page.notify(response);
		}
}

user.refreshList = function()
{
    $.get("/app/users/list/")
        .done(function(data){ $('#userslist', parent.document).html(data);} )
        .fail(function(){alert("There has been some errors processing the request");});
        
}

user.remove = function(userId)
{
    $.post('/app/users/delete', {id: userId})
        .done(function(){user.refreshList();})
        .fail(function(){alert("There has been some errors processing the request");});
    return false;
}
user.update = function(userId)
{
	$.fancybox.close();
	user.refreshList();
}
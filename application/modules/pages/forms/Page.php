<?php

class Pages_Form_Page extends Zend_Form {
	
    public function init() 
    {
        $this->setMethod('post')
                ->setAttrib('class', 'form ajaxpagesubmit')
                ->setAction("/pages/page/save")
                ->setAttrib("id", "page_form");
        
        $label = new NW_Form_Element_Text('label');
        $label->addValidator('Regex',false, array('/([a-zA-Z0-9 \-_]+)/'))
                ->setFilters(array('StringTrim'))
                ->setRequired(true)
                ->setLabel("Label");

        $lang = new NW_Form_Element_Select('lang');
        $lang ->setFilters(array('StringTrim'))
                ->setRequired(true);
        $lang->addMultiOptions(App_Model_Display_Service::getInstance()->langsAsSelect());
        $lang->setLabel("Lingua");
        $id = new NW_Form_Element_Hidden('id');
        $id->addValidator('Digits',false)
        		->setRequired(false);
        $template = new NW_Form_Element_Hidden('template');
        $template->addValidator('Regex',false, array('/([a-zA-Z0-9 \-_]+)/'))
        	->setRequired(true);
        
        $status = new NW_Form_Element_Select('status');
        $status->setRequired(true)
                ->setLabel("Status")
                ->addMultiOption("", "Please Select One")
                ->addMultiOption("draft", "Bozza")
                ->addMultiOption("active", "Active")
                ->addMultiOption("delete", "Cancelled");       
        $order = new NW_Form_Element_Hidden("order");
        
        $this->addElements(array($label, $lang, $id, $status, $template, $order));
    }

    public function populate(array $values) 
    {    
        $AdaptedValues['label'] = $values["_label"];
        $AdaptedValues['lang'] = $values["_lang"];
        $AdaptedValues['id'] = $values["_id"];
        $AdaptedValues['status'] = $values["_status"];
        $AdaptedValues['template'] = $values["_template"];
        $AdaptedValues['order'] = $values["_order"];
        parent::populate($AdaptedValues);
    }
    
    public function getValues($suppressArrayNotation = false) {
        $values = parent::getValues($suppressArrayNotation);
        return $values;
        
    }
}


<?php

class Pages_View_Helper_PageStatus extends Zend_View_Helper_Abstract {

    public $view;
    private $_status;

    public function setView(Zend_View_Interface $view) {
        $this->view = $view;
    }

    public function pageStatus($status) {
        $this->_status = $status;
        return $this;
    }

    public function icon() {
        switch ($this->_status) {
            case Pages_Model_Page::STATUS_ACTIVE:
                return '<span class="icon_font">#</span>';
            case Pages_Model_Page::STATUS_DRAFT:
                return '<span class="icon_font">#</span>';
            case Pages_Model_Page::STATUS_DELETED:
                return '<span class="icon_font">"</span>';
        }
    }

    public function linkClass() {
        switch ($this->_status) {
            case Pages_Model_Page::STATUS_ACTIVE:
                return 'class = "ajax certificato ok"';
            case Pages_Model_Page::STATUS_DRAFT:
                return 'class = "ajax certificato rinnovare"';
            case Pages_Model_Page::STATUS_DELETED:
                return 'class = "ajax certificato urgente"';
        }
    }

}

?>

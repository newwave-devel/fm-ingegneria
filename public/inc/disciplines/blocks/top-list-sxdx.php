<?php
if($blocks):
	$isBottom = isset($isBottom) ? $isBottom : false;
	require_once("inc/tpls/blocks/list/tpl.php");
	require_once("inc/functions/blocks.php");
	$col_sx = '';
	$col_dx = '';
	foreach($blocks as $sharedBlock)
	{
		$page = $sharedBlock->getPage();
		$pos = $sharedBlock->getPosition();
		$odd = ($pos=='sx');
		if($odd)
			$col_sx .= ($page->getTemplate() == 'news') ? renderSharedBlockNews($tpl_item_news, $sharedBlock, $blockService, $odd, $isBottom) : 
																		renderSharedBlockProject($tpl_item_project, $sharedBlock, $blockService, $odd, $isBottom);
		else
			$col_dx .= ($page->getTemplate() == 'news') ? renderSharedBlockNews($tpl_item_news, $sharedBlock, $blockService, $odd, $isBottom) :
																	renderSharedBlockProject($tpl_item_project, $sharedBlock, $blockService, $odd, $isBottom);
		
	}
	?>
	<aside id="mainCol">
		<?php echo $col_sx ?>
	</aside>
	<aside id="sidebar">
		<?php echo $col_dx ?>
	</aside>
	<?php unset($col_sx); ?>
	<?php unset($col_sx); ?>
	<?php unset($blocks); ?>
<?php endif; ?>

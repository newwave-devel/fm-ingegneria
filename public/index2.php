<?php
$pageId = 'homepage-map';
include "inc/required.php";
include "inc/common/contents.php";
?>


<?php include "inc/common/head.php" ?>
		
        
        <!-- SCRIPT -->
		<script type="text/javascript" src="/common/js/core.js"></script>
        <script type="text/javascript" src="/common/js/jquery.nivo.slider.pack.js"></script>
        <script type="text/javascript" src="/common/js/custom.js"></script>
        
        
        
        <script type="text/javascript">	
                jQuery(window).load(function() {
                    // START
					loading();
					slideshow();
                });
        </script>
       
   
	</head>    
    
	<body lang="it">
    
    	<div id="loader"></div>
    	    	
        <?php include "inc/common/header.php"?>
        
        
        <div id="menu_controller"></div>
        <section id="main_menu_panel">
			<?php include "inc/common/menu/panel.php"?>
        </section><!--main_menu_panel-->
        <?php include "inc/common/menu/main.php"?>
        
        <section id="main_page" class="home">
        	<section class="container">
            
            	<?php include "inc/common/slider.php"; ?>
                
                
                
                
                	
                 <!-- QUESTA E' LA PARTE NUOVA -->
                 
                 
                 	<section id="home_articles">
                    
                    	<section class="row">
                    
                          <article class="article ">
                          <a href="news-detail.php?id=382" title="VENICE EXPO GATE STARTS TAKING FORM"> <img class="img" src="/assets/res.php?src=/assets/70dfb1e2e949b6b54d6c30e50226881b_1000x500.jpg&h=320" alt="VENICE EXPO GATE STARTS TAKING FORM">
                          <h6>News</h6>
                          <h3>VENICE EXPO GATE STARTS TAKING FORM</h3>
                          <h4>19.09.2014</h4>
                          <p>The installation of the steel roof structure of Venice Expo Gate started. The roof trusses compose a 100 x 100 m square, laying over 12 tapered columns. Columns lay on a 32 x 32 m grid, with only 4 columns inside the exhibition hall and the remaining 8 around the perimeter; this solution provides a wide column free space ...</p>
                          <div class="color_wrapper"></div>
                          </a>
                          </article>
                          
                          
                          <article class="article small last">
                          <a href="news-detail.php?id=339" title="First meters of Chile Pavillion"> <img class="img" src="/assets/res.php?src=/assets/70ebe7b18d755ccdb0e802c16432836a_1000x500.jpg&w=320&h=320" alt="First meters of Chile Pavillion">
                          <h6>News</h6>
                          <h3>First meters of Chile Pavillion</h3>
                          <h4>04.07.2014</h4>
                          <p>First meters of Chile Pavillion at Expo 2015
                          
                          &quot;Sobre su visita ...</p>
                          <div class="color_wrapper"></div>
                          </a>
                          </article>
                      
                      </section><!--row-->
                      
                      
                      
                      <section class="row">
                      
                      <article class="article small">
                      <a href="news-detail.php?id=339" title="First meters of Chile Pavillion"> <img class="img" src="/assets/res.php?src=/assets/70ebe7b18d755ccdb0e802c16432836a_1000x500.jpg&w=320&h=320" alt="First meters of Chile Pavillion">
                      <h6>News</h6>
                      <h3>First meters of Chile Pavillion</h3>
                      <h4>04.07.2014</h4>
                      <p>First meters of Chile Pavillion at Expo 2015
                      
                      &quot;Sobre su visita ...</p>
                      <div class="color_wrapper"></div>
                      </a>
                      </article>
                      
                      
                      <article class="article last">
                      <a href="news-detail.php?id=382" title="VENICE EXPO GATE STARTS TAKING FORM"> <img class="img" src="/assets/res.php?src=/assets/70dfb1e2e949b6b54d6c30e50226881b_1000x500.jpg&h=320" alt="VENICE EXPO GATE STARTS TAKING FORM">
                      <h6>News</h6>
                      <h3>VENICE EXPO GATE STARTS TAKING FORM</h3>
                      <h4>19.09.2014</h4>
                      <p>The installation of the steel roof structure of Venice Expo Gate started. The roof trusses compose a 100 x 100 m square, laying over 12 tapered columns. Columns lay on a 32 x 32 m grid, with only 4 columns inside the exhibition hall and the remaining 8 around the perimeter; this solution provides a wide column free space ...</p>
                      <div class="color_wrapper"></div>
                      </a>
                      </article>
                      
                      </section><!--row-->
                      
                      
                    
                    </section><!--home_articles-->
                                  
                 
                 <!-- FINE PARTE NUOVA -->
                 
                 
                 
                 
                 
                 
                 
                    
                      
                <section id="home_countries">
                	
                    
                </section><!--home_countries-->
                
                <br>
                
              
                
            </section><!--container-->
        </section><!--main_page-->
        
        <?php include "inc/common/footer.php"?>
		
	</body>
    
    
	
</html>
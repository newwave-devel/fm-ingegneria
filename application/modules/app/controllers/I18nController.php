<?php
class App_I18nController extends NW_Controller_Action
{
    private $_service;

    public function init() {
        $this->_service = new App_Model_I18n_Service();
        $this->_translateAdapter = Zend_Registry::get('Zend_Translate');
    }

    public function indexAction()
    {
        try {
            $this->_setAjaxAction()
                ->_addJsSource('/admin/js/app/translations.js');
            $paginator = NW_Pages_Paginator::__fromRequest ( $this->_request );
            $paginator->setIsAjax ( true, "#main_content" );
            $lang = $this->_getParam ( "lang", App_Model_Display_Service::getInstance ()->lang () );

            $paramsSearch = array ("lang" => $lang );
            $pageSearchForm = new App_Form_I18n_Search( array ("contexts" => $this->_service->contextsAsSelect(),"paginator" => $paginator ) );
            $this->view->items  = $this->_service->fetchAll ( $paginator, $paramsSearch );
            $this->view->paginator = $paginator->prepare ();
            $this->view->formSearch = $pageSearchForm;
            $this->view->pageTitle = $this->_translateAdapter->translate("Gestione Etichette di testo");
        } catch(Exception $e)
        {
            throw $e;
        }
    }

    public function editAction()
    {
        try {
            $this->_setIframeAction();
            $id = $this->_getParam("id");

            $item = $this->_service->find($id);
            $paginator = NW_Pages_Paginator::fetchAll();
            $siblingItems = $this->_service->fetchAll($paginator, array("key" => $item->getKey(), "context" => $item->getContext()));
            $form = new App_Form_I18n_Item(array("format" => $item->getFormat(), "siblings" =>$siblingItems));
            $form->populate($item->getOptions());
            $form->getElement('siblings')->setValue($item->getId());
            $this->view->form = $form;
        } catch(Exception $e)
        {
            throw $e;
        }
    }

    public function updateAction()
    {
        try {
            $this->_setIframeAction();

            $paginator = NW_Pages_Paginator::fetchAll();
            $siblingItems = $this->_service->fetchAll($paginator, array("key" => $this->_getParam("key"), "context" => $this->_getParam("context")));
            $form = new App_Form_I18n_Item(array("format" => $this->_getParam("format"), "siblings" =>$siblingItems));

            if ($this->_request ->isPost())
            {
                if ($form->isValid($this->_request->getPost()))
                {
                    $values = $form->getValues();
                    $model = new App_Model_I18n();
                    $model->setOptions($values);
                    $itemId = $this->_service->save($model);
                    $this->_addOnload("parent.window.main_page.getTranslationController().updateValue();");
                    $this->view->form = $form;
                } else {
                    $this->view->form = $form;
                }
            }
        } catch(Exception $e)
        {
            $this->view->status = NW_View_RetCode::error($e->getMessage());
        }
    }
//return item value so can be replaced
    public function refreshitemAction()
    {
        try{
            $this->_setJsonAction();
            $result = new NW_View_RetCode();
            if(!$this->_hasParam('paramId')) throw new Exception();
            $id = $this->_getParam('paramId');
            $item = $this->_service->find($id);
            if(!empty($item))
            {
                $result->setStatus(NW_View_RetCode::STATUS_SUCCESS)
                    ->setPayload(array("id" => $item->getId(), "value" => $item->getValue()));
            } else {
                $result->setStatus(NW_View_RetCode::STATUS_ERROR)
                    ->setPayload("no item found");
            }
            $this->view->result = $result;
        } catch(Exception $e)
        {
            throw $e;
        }
    }

    public function searchAction() {
        try {
            $this->_setAjaxAction()
                ->_addJsSource('/admin/js/app/translations.js');
            $paginator =  NW_Pages_Paginator::__fromRequest($this->_request);
            $paginator->setIsAjax ( true, "#main_content" );

            $pageSearchForm = new App_Form_I18n_Search( array ("contexts" => $this->_service->contextsAsSelect(), 'paginator' => $paginator) );


            $params = array();
            if ($this->_request->isPost())
            {
                if ($pageSearchForm->isValid($this->_request ->getPost()))
                {
                    $params = $pageSearchForm->getValues();
                    $this->_addOnload('NW_Tablesorter.register(".tabella").sort()');

                    $this->view->items  = $this->_service->fetchAll ( $paginator, $params );
                    $this->view->paginator = $paginator->prepare ();
                    $this->view->formSearch = $pageSearchForm;
                    $this->view->pageTitle = $this->_translateAdapter->translate("Ricerca");
                }
                else
                    $this->_forward('list');
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

}
<?php
class Pages_Model_Page_DTO_Page_List {
	
	public $contents;
	
	public function __construct($pages=array(), $translations=array()) {
		$this->_pages = $pages;
		$this->_translations = $translations;
		$this->contents = array();
	}
	
	/**
	 *
	 * @return the unknown_type
	 */
	public function getPages() {
		return $this->_pages;
	}
	
	
	public function setPages($pages) {

		foreach($pages as $p)
			$this->contents[$p->getId()]['item'] = $p;
		return $this;
	}
	
	public function pageIds()
	{
		return array_keys($this->contents);
	}
	
	public function setTranslations($trx, $lang=null) {
		$lang = (empty($lang)) ? App_Model_Display_Service::getInstance()->lang() : $lang;
		foreach($trx as $t)
		{
			$item = $t->getLang($lang);
			if(isset($this->contents[$item]))
				$this->contents[$item]['langs'] = $t->getLangs();
		}		
		return $this;
	}
	
}
?>
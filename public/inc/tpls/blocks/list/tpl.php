<?php	
	$tpl_item_project = <<<TPL
		<article class="article %s">
			<a href="project-detail.php?id=%d" title="%s"> <img class="img" src="%s" alt="%s">
				<h6>%s</h6>
				<h3>%s</h3>
				<p>%s</p>
				<div class="color_wrapper"></div>
			</a>
		</article>
TPL;
	
	$tpl_item_news = <<<TPL
		<article class="article %s">
			<a href="news-detail.php?id=%d" title="%s"> <img class="img" src="%s" alt="%s">
				<h6>%s</h6>
				<h3>%s</h3>
				<h4>%s</h4>
				<p>%s</p>
				<div class="color_wrapper"></div>
			</a>
		</article>
TPL;

?>
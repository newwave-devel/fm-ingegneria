<?php
class Pages_Model_Page_Admin_Composable_Service extends Pages_Model_Page_Admin_Service
{
	public function __construct($request, &$view) {
		parent::__construct($request, $view);
	}
	
	public function saveComposable(Pages_Model_Page_Composable $page, $formValues)
	{
		try {
			$this->getMapper()->getDbTable()->getAdapter()->beginTransaction();
			parent::save($page);
			$pos = 0;
			$blockService = new Pages_Model_Block_Composable_Service();
			$blockService->dropPage($page->getId());
			foreach($formValues as $key => $vals)
			{
				if($key=='page')
					continue;
				$position = $key;
				foreach($vals as $foo => $id_as_string)
				{
					
					$id_as_array = explode(",", $id_as_string);
					if(!is_array($id_as_array)) continue;
					foreach($id_as_array as $blockId)
					{
						if(empty($blockId)) continue;
						$block = new Pages_Model_Block_Composable();
						$block->setPage($page->getId())
							->setPosition($position)
							->setBlock($blockId)
							->setOrder(++$pos);
						$blockService->save($block);
					}
				}
			}
			$this->getMapper()->getDbTable()->getAdapter()->commit();
		} catch (Exception $e) {
			$this->getMapper()->getDbTable()->getAdapter()->rollBack();
			throw $e;
		}
	}
	
	public function edit()
	{
		try {
			$view = $this->_getView();
			$id = $this->_request->getParam('id');
			$page = $this->_service->find($id);
	
			//loads datas from the descriptor
			$descriptor = NW_Pages_Page::getInstance()->page($page->getTemplate());
			$blocks = NW_Pages_Page::getInstance()->blocks($page->getTemplate(),false);
			$positions = $descriptor->positions->children();
	
			//loads datas from the descriptor
			$dto = new Pages_Model_Block_Composable_DTO();
			$blockService = new Pages_Model_Block_Service();
			$composableService= new Pages_Model_Block_Composable_Service();
	
			$invokerId = $id;
			//sets the positions
			foreach($positions as $pos)
				$dto->addPosition($pos);
			$form = new Pages_Form_Block_Shared(array("positions" => $positions));
			$form->getElement("page")->setValue($id);
			//if behave is specified, loads the parent item
			if(isset($descriptor['behaves']) && !empty($descriptor['behaves']))
			{
				$parentPageLabel = $descriptor['behaves'];
				$pageService = new Pages_Model_Page_Service();
				$parentPage = $pageService->first(array("labelstrict" => $parentPageLabel, "lang" => $page->getLang()));
				if(empty($parentPage)) throw new Exception("No item with $parentPageLabel available for the current language");
				//if I have a behaves, I need to specify that it will act like it is the specified item, so i set accordingly the invokerId
				$invokerId= $parentPage->getId();
			}
	
			$blockIds = array();
			foreach($blocks as $block)
			{
				$filters = $block->filters->children();
	
				if($filters->count() == 0)
				{
					$paginator = NW_Pages_Paginator::fetchAll();
					//loads all blocks with template $block['id']
					$returned = $blockService->fetchAll($paginator, array("template"=>$block['id'], 'status' => "active", "lang" => $page->getLang()));
					$dto->addAvailables($returned);
				} else
				{
					foreach($filters as $filter)
					{
						$filterType = $filter->getName();
						switch($filterType)
						{
						    case 'taxonomy':
						    	$taxonomyId = $filter['id'];
						    	$whatToFetch = $filter['fetch'];
						    	$invoker = $filter['invoker'];
						    	//if I have a taxonomy, I need to fetch the pages related to it first
						    	$taxonomy = new Pages_Model_Taxonomy($taxonomyId, $invoker, $invokerId);
						    	$this->_service->addTaxonomy($taxonomy);
						    	$paginator = NW_Pages_Paginator::fetchAll();
						    	$pages = ($whatToFetch == "parent") ? $this->_service->fetchParents($paginator, array("lang" => $page->getLang(), "status" => "active")) : $this->_service->fetchDependent($paginator, array("lang" => $page->getLang(), "status" => "active"));
						    	 
						    	$this->_service->dropTaxonomies();
						    	$paginator = NW_Pages_Paginator::fetchAll();
						    	$pageIds = array();
						    	foreach($pages as $page)
						    		$pageIds[] = $page->getId();
						    	$returned = $blockService->fetchAll($paginator, array("page" => $pageIds, 'template' => $block['id'], "lang" => $page->getLang(), "status" => "active"));
						    	$dto->addAvailables($returned);
						    	break;
						}
					}
				}
	
			}
			$assigned = $composableService->byPage($id);
			$dto->addAssigned($assigned);
			$view->dto = $dto;
			$view->form = $form;
			$view->id = $id;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
}
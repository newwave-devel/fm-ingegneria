<?php
class Pages_Model_Block_Main_Service
{
	protected $_mapper;

    public function getMapper() {
        return $this->_mapper;
    }

    public function __construct() {
        $this->_mapper = new Pages_Model_Block_Main_Mapper();
    }

	
	public function query(NW_Pages_Paginator &$paginator, $paramsSearch=array(), $fields=array(), $order=array())
	{
		try 
		{
			return $this->_mapper->query($paginator, $paramsSearch, $fields, $order);
		} catch(Exception $e)
		{
			throw $e;
		}
	}
}
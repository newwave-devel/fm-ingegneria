<?php if($staff): ?>
<ul>
	<?php 
		$people_keystaff_count =0;
		foreach($staff as $sp):
			$spblock = $sp->getBlock();
			$role = $spblock->_("role");
			$name = $spblock->_("title");
			$img = $spblock->_("file");
			$src = $blockService->asset($img);
			$classNomargin = (++$people_keystaff_count%3 ==0) ? 'class="no_margin"' : '';
			echo sprintf($tpl_people_keystaff,  $classNomargin, $sp->getPage()->getId(), $src->getAssetUrl(), $name, $role, $name);
		endforeach;
	?>
</ul>
<?php endif; ?>

<?php

class NW_Application
{

    /**
     * All Singletons Enlisted in the application
     * @var array
     */
    private static $_singletons_pool = null;
    private static $_obj_registry = null;

    /**
     * returns Singleton by name.
     * Every object is created by mean of its constructor.
     * @param string $item
     */

    /**
     * Is the main entry point of the whole app.
     * Uses ApplicationDescriptor to detect and load the environment and the current runlevel.
     * Actually, it
     * 		-	Enable Logs.
     * 		-	Performs all necessary includes / requires
     * 		- 	Enables __autoload
     * 		-	Makes all Params available to request.
     * 		-	Enrolls to the singleton pools the email and database resources
     *
     * @param $runlevelname
     */
    public static function runFor($runlevelname)
    {
        try {
//            self::enableLogging();

            $appDescriptor = NW_Descriptors_Application::load();
            $env = $appDescriptor->getEnvironment();
            $runlevel = $appDescriptor->loadRunLevel($runlevelname);

            NW_Application::enrollSingleton($appDescriptor, NW_ClassURI::fromString('app/NW_Descriptors_Application'));
            NW_Application::enrollSingleton($env, NW_ClassURI::fromString('app/NW_Environment'));
            NW_Application::enrollSingleton($runlevel, NW_ClassURI::fromString('app/NW_CurrentRunlevel'));
            NW_Application::enrollSingleton($appDescriptor->loadModuleManager(), NW_ClassURI::fromString('app/NW_ModuleManager'));
            
            //relationdefs
            $relationsDef = NW_Defs_Relations::fromFile(APP_PATH.'etc/reldefs.xml');        
            NW_Application::enrollSingleton($relationsDef, NW_ClassURI::fromString('defs/NW_Defs_Relations'));
            $typeDefs = NW_Defs_Type::fromFile(APP_PATH.'etc/typedefs.xml');
            NW_Application::enrollSingleton($typeDefs, NW_ClassURI::fromString('defs/NW_Defs_Type'));
            
            $env->run();
            $runlevel->run();
 //           Logger::getLogger(__CLASS__)->info("NW_Application::run() ended correctly");
        } catch (Exception $e) {
 //           Logger::getLogger(__CLASS__)->error($e->getMessage());
            throw $e;
        }
    }

    public static function getSingleton($classuri)
    {
        try {
            if (!$classuri instanceof NW_ClassURI)
                $classuri = NW_ClassURI::fromString($classuri);

            $prefix = $classuri->getPrefix();
            $classname = $classuri->getClassName();
            if (!isset(self::$_singletons_pool[$prefix][$classname]) || empty(self::$_singletons_pool[$prefix][$classname]))
            {
                if (class_exists($classname))
                    self::$_singletons_pool[$prefix][$classname] = new $classname();
                else
                    throw new Exception("Class '$classname' not found for '" . $classuri->toString()."'");
            }
            return self::$_singletons_pool[$prefix][$classname];
        } catch (Exception $e) {
 //           Logger::getLogger(__CLASS__)->error($e->getMessage());
            throw $e;
        }
    }

    /**
     *
     * Can be used to register Singletons in the singletons pool.
     * Item must be in the form $namespace / $classname
     * @param object $item
     * @param string $location
     */
    public static function enrollSingleton($item, NW_ClassURI $classuri)
    {
        try {
            if (empty($item))
                throw new Exception("No item to enroll in $location");
            $prefix = $classuri->getPrefix();
            $classname = $classuri->getClassName();
           
            if (!isset(self::$_singletons_pool[$prefix][$classname]) || self::$_singletons_pool[$prefix][$classname] == null)
                self::$_singletons_pool[$prefix][$classname] = $item;
                
        } catch (Exception $e) {
 //           Logger::getLogger(__CLASS__)->error("Error enrolling in " . $classuri->toString());
            throw $e;
        }
    }

    /**
     * returns path to /app directory
     * Enter description here ...
     */
    public static function getRootPath()
    {
        return APP_PATH . DIRECTORY_SEPARATOR;
    }

    public static function getCurrentRunlevel()
    {
        return NW_Application::getSingleton(NW_ClassURI::fromString("app/NW_CurrentRunlevel"));
    }

//    private static function enableLogging()
//    {
//        require_once APP_PATH . 'libs/log4php/Logger.php';
//        Logger::configure(APP_PATH . 'libs/log4php/xml/log4php.properties.xml');
//    }

    public static function dumpSingletonsPool()
    {
        $res = array();
        foreach (self::$_singletons_pool as $namespace => $classes)
        {
            foreach ($classes as $classname => $object)
                $res[] = " $namespace / $classname ";
        }
        return $res;
    }

    public static function __new($className, $options=array())
    {
        try {
            if ($className instanceof NW_ClassURI)
                $classUri = $className;
            else
                $classUri = NW_ClassURI::fromString($className);

            if ($classUri->is_singleton())
                throw new Exception("Cannot use __new for instantiating singletons, use enroll instead");

            $realclassname = $classUri->projectToZendClassName($classUri);
            
            return new $realclassname($options);
        } catch (Exception $e) {
            throw $e;
        }
    }
    //should be definitive
    public static function __model($classUri)
    {
        try {
            $classUri = ($classUri instanceof NW_ClassURI) ? $classUri : NW_ClassURI::fromString($classUri);
            
            if ($classUri->is_singleton())
                throw new Exception("Cannot use __new for instantiating singletons, use enroll instead");
            $cname = $classUri->getClassName();
            $cmodule = $classUri->getPrefix();
           
            if (!isset(self::$_obj_registry[$cmodule][$cname]) || empty(self::$_obj_registry[$cmodule][$cname]))
            {
                $item = new NW_Models_Object_Object($classUri);
                
                foreach(NW_Application::getSingleton("app/NW_ModuleManager")->getModuleDescriptor($classUri)->decorators($cname) as $dec)
                {
                    $dec_fullname = sprintf("NW_Models_Object_Decorators_%s", ucfirst($dec));
                    $item = new $dec_fullname($item);
                }
                self::$_obj_registry[$cmodule][$cname] = $item;
            }
            return self::$_obj_registry[$cmodule][$cname];
            
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public static function getTime()
    {
        return time();
    }

}

?>
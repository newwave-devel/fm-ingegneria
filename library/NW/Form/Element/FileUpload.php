<?php

/**
 * Description of Hidden
 *
 * @author Alessio
 */
class NW_Form_Element_FileUpload extends Zend_Form_Element_Hidden
{
	private $_hasFile = false;
	
    public function __construct($spec, $options = null)
    {
        parent::__construct($spec, $options);
        $this->setDecorators(array(new NW_Form_Decorator_FileUpload()));
    }
    
    public function setHasFile()
    {
    	$this->_hasFile = true;
    	return $this;
    }
    
    public function hasFile()
    {
    	return $this->_hasFile;
    }
    //needs to force value to a string..
    public function setValue($value)
    {
    	if(is_array($value))
    		$value = (implode(",", $value));
    	parent::setValue($value);
    }
    
    
}

?>

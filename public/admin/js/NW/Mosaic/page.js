	Mosaic.Page = (function() {
	var subpage;
	var my_sidebar_list;
	var my_sidebar_link;
	var my_main_content;
	var my_ajax;
	var widget_collapse;
	var pageId;
	var my_notifications_bar;
	var my_notifications_body;
    var translationController;
	
	Page = function() {		
		this.subpage = undefined;
		this.my_sidebar_list = $('#main_menu ul li');
		this.my_sidebar_link = $('#main_menu ul li a');
		this.my_main_content = $('#main_content');
		this.my_notifications_bar = $('#notifications-bar');
		this.my_notifications_body = $('#notifications-body');
		this.my_ajax = $('a.ajax');
		this.widget_collapse = $('a.widget_collapse');
        this.translationController = undefined;
	}

	Page.prototype = 
	{
			mainmenu: function() 
			{
				var self = this;
				$(document).on("click", "#main_menu ul li a", function() 
				{    
					var checkElement = $(this).next();
				    
					self.my_sidebar_list.removeClass('current');
					self.my_sidebar_link.removeClass('current');
				    $(this).closest('li').addClass('current');	
				    
				    
				    if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
				      $(this).closest('li').removeClass('current');
				      checkElement.slideUp(250);
				    }
				    
				    if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
				      $('#main_menu ul ul:visible').slideUp(250);
				      checkElement.slideDown(200);
				    }
				    
				    return (!checkElement.is('ul'));
				});
				return this;
			},
			addBlock: function()
			{
				$.fancybox.close();
				this.refreshBlocks();      
			},
			drop: function()
			{
				var self = this;
				if(confirm(dic.confirm_page_removal))
				{
					$.get("/pages/page/delete", {id: this.pageId})
						.done(function(data)
						{ 
							result = jQuery.parseJSON(data);
							if(result.status=="ok")
							{
								$.get(result.payload)
									.done(function(data){ self.my_main_content.fadeOut(500).html(data).fadeIn(500);} )
									.fail(function(){self.notify("There has been some errors processing the request");});
							}
								
						} )
						.fail(function(){alert("There has been some errors processing the request");}); 
				}
			},
			removeFile: function(itemRef)
			{
				var self = this;
				if(confirm(dic.confirm_file_deletion))
				{
					$.get("/app/media/delete/",{id: $(itemRef).attr("rel")})
						.done(function(data){ 
							result = jQuery.parseJSON(data);
							if(result.status=='ok')
								{
									$(itemRef).closest("div.picture").fadeOut(function() { $(this).remove(); });
									return false;
								}
							else self.notify(response.payload);
						} )
						.self.notify(function(){alert("There has been some errors processing the request");});
				}
			},
			ajaxlink: function() 
			{
				var self = this;
				$(document).on("click", "a.ajax", function() 
				{
					var el = $(this);
				    var mylink = el.attr('href');
						
					self.my_main_content.html('<img class="loader_img" src="/admin/img/ajax-loader.gif" />');
			        
			        $(this).addClass("current");
			        $.get(mylink).done(function(data){
			            self.my_main_content.html(data).fadeIn(550);
			        })
			        .fail(function(data){
			            self.my_main_content.html(data.responseText).fadeIn(550);
			        });
					return false;
			    });
				return this;
			},
			subPage: function ()
			{
				if(this.subpage == undefined)
					this.subpage=Mosaic_subpage.getNew();
				return this.subpage;
			},
			gestione_notifiche: function() {
				var self = this;
				$(document).on("click", "#notification_center > .notification > .close", function() {
					$(this).parent().fadeOut(550);
					return false;
				});
				return this;
			},
			collapseWidget: function() {
				var self = this;
				$(document).on("click", "a.widget_collapse", function () {
					//var el = $(this).parent().closest(".widget").children(".collapsible");
					$(this).closest("section.widget").children(".collapsible").slideToggle(200);
					return false;
				});
				return this;
			},
			myFancybox: function()
			{
				$(document).on("focusin", ".zoom", function () {
					jQuery(".zoom").not(".fancybox").fancybox({
				        maxWidth	: 980,
						minWidth	: 860,
				        fitToView	: true,
				        width		: '75%',
				        autoSize	: true,
				        closeClick	: false,
				        openEffect	: 'none',
				        closeEffect	: 'none'
				    });
				});
				return this;
			},
			previews: function()
			{
				$(document).on("focusin", ".preview", function () {
					jQuery(".preview").not(".fancybox").fancybox({
				        maxWidth	: 980,
						minWidth	: 860,
				        fitToView	: false,
				        width		: '75%',
				        autoSize	: false,
				        closeClick	: false,
				        openEffect	: 'none',
				        closeEffect	: 'none'
				    });
				});
				return this;
			},
			//opens fancybox
			fancybox: function(url)
			{
				$.fancybox({
					maxWidth	: 980,
					minWidth	: 860,
			        fitToView	: true,
			        width		: '75%',
			        autoSize	: true,
			        closeClick	: false,
			        openEffect	: 'none',
			        closeEffect	: 'none',
			        type: 'iframe',
			        href: url
				});
			},
			refreshBlock : function(blockId, target)
			{
				var self = this;
				$.get("/pages/block/edit", {id: blockId })
					.done(function(data)
					{
						htmlBlockId = "#"+target;
						$(htmlBlockId).fadeOut("slow", function(){
						    $(htmlBlockId).replaceWith(data);
						    $(htmlBlockId).fadeIn("slow");
						});
					} )
					.fail(function(){self.notify("There has been some errors processing the request");});
			},
			selectTemplate: function(pageId)
			{
				this.fancybox("pages/page/selecttemplate/id/"+pageId);
			},
			refreshBlocks : function()
			{
				var self = this;
				$.get("/pages/page/edit", {id: this.pageId })
					.done(function(data){ self.my_main_content.html(data);} )
					.fail(function(){self.notify("There has been some errors processing the request");});
			},
			dropBlock: function(paramId)
			{
				if(confirm(dic.confirm_block_removal))
				{
					$.get("/pages/block/delete", {id: paramId})
						.done(function(data)
						{ 
							result = jQuery.parseJSON(data);
							if(result.status=="ok")
							{
								blockId = "#block_"+paramId;
								$(blockId).fadeOut(250);
								self.notify(dic.block_removed_successfully);
							}
								
						} )
						.fail(function(){self.notify("There has been some errors processing the request");}); 
				}
			},
			notify: function(message)
			{	
				if(message instanceof Object) 
				{
					var messageclass = (message.status===undefined) ? 'notify' : message.status;
					this.my_notifications_body.attr("class", messageclass);
					this.my_notifications_body.html(message.payload);
				} else {
					this.my_notifications_body.attr("class", 'notify');
					this.my_notifications_body.html(message);
				}
				this.my_notifications_bar.stop(false, true).animate({'top': '0' }, 500).delay(5000).animate({'top': '-70' }, 500);
			},
			setPageId : function(id)
			{
				this.pageId = id;
				return this;
			},
			reload: function()
			{
				var self=this;
				$.get("/pages/page/edit", {id: this.pageId})
					.done(function(data){ self.my_main_content.fadeOut(200).html(data).fadeIn(200);} )
					.fail(function(){self.notify("There has been some errors processing the request");});
				return this;
			},
			ajaxBlockSubmit: function()
			{
				var self = this;
				$(document).on("submit", "form.ajaxblocksubmit", function () {
					var blockId = ($(this).attr("id"));
					
					$(this).ajaxSubmit
							({
							 success: function(responseText, statusText, xhr, $form)
										{
											var formId = "#" + $form.attr('id');
											var blockId = $(formId).parent().parent().attr("id");
											if(statusText == 'success') //received a reply from server
											{
												var retCode = responseText.status;
												if(retCode == 'ko') self.notify(responseText.payload);
												else self.refreshBlock(responseText.payload.id, blockId);
											}
										},
							 beforeSerialize: function()
	                                          { 
	                                          	if (typeof CKEDITOR != 'undefined') 
	                                            { 
	                                          		for( instance in CKEDITOR.instances ) 
	                                          			CKEDITOR.instances[instance].updateElement();
	                                            }
	                                          }, 
	                            dataType: "json" 
							}); 
					return false;
				});
				return this;
			},
			userblockSubmit: function()
			{
				var self = this;
				$(document).on("submit", "form.ajaxuser", function () {
					$(this).ajaxSubmit
							({
								success: function(responseText, statusText, xhr, $form)
										{
											if(statusText == 'success')
											{
												var retCode = responseText.status;
												if(retCode == 'ko') self.notify(responseText.payload);
												else self.notify(dic.account_update_successfully);
											}
										},
							 dataType: "json" 
							}); 
					return false;
				});
				return this;
			},
			ajaxPageSubmit: function()
			{
				var self = this;
				$(document).on("submit", "form.ajaxpagesubmit", function () {
					$(this).ajaxSubmit
							({
								success: function(responseText, statusText, xhr, $form)
										{
											if(statusText == 'success') //received a reply from server
											{
												var retCode = responseText.status;
												if(retCode == 'ko') self.notify(responseText.payload);
												else {
													var pageId = responseText.payload.id;
													self.setPageId(pageId).reload();
												}
													
											}
										},
	                            dataType: "json" 
							}); 
					return false;
				});
				return this;	
			},
			saveSharedBlocks: function(pageId)
			{
				var self = this;			
				$(".target").each(function()
				{
					var pos = $(this).attr("rel");
					if(pos == undefined) return;
					var input = $("#"+pos+"-id");
					input.val("");
					$(this).children().each(
						function()
						{
							var rel = $(this).attr("rel");
							if(input.val() == '')
								input.val(rel);
							else
								input.val(input.val() + "," + rel);
						});
				});
				
				$("#composableform").ajaxSubmit(
				{
					success: function(responseText, statusText, xhr, $form)
					{
						if(statusText == 'success') //received a reply from server
						{
							var retCode = responseText.status;
							if(retCode == 'ko') self.notify(responseText.payload);
							else {
								self.notify(responseText.payload);
							}
								
						}
					},
					dataType: "json" 
				});
			},
			ajaxTaxonomySubmit: function()
			{
				var self = this;
				$(document).on("submit", "form.ajaxtaxonomysubmit", function () {
					$(this).ajaxSubmit
							({
								success: function(responseText, statusText, xhr, $form)
										{
											if(statusText == 'success') //received a reply from server
											{
												var retCode = responseText.status;
												if(retCode == 'ko') self.notify(responseText.payload);
												else {
													$( "#tabs" ).tabs( "option", "active", 0 );
												}
													
											}
										},
	                            dataType: "json" 
							}); 
					return false;
				});
				return this;	
			},
            getTranslationController: function(itemId)
            {
                if(typeof itemId !='undefined')
                    this.translationController = new TranslationController(itemId);
                return this.translationController;
            }

	}
	
	return {
		init: function()
		{
			var page = new Page();
			page.mainmenu()
				.ajaxlink()
				.gestione_notifiche()
				.collapseWidget()
				.myFancybox()
				.ajaxBlockSubmit()
				.ajaxPageSubmit()
				.ajaxTaxonomySubmit()
				.userblockSubmit()
				.previews();
			return page;
		}
		
	}
}());

var Mosaic_page = Mosaic.Page;
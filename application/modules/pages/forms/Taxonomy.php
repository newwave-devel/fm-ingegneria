<?php

class Pages_Form_Taxonomy extends Zend_Form {
	
    public function init() 
    {
        $this->setMethod('post')
                ->setAttrib('class', 'edit_form')
                ->setAction("/pages/taxonomy/save");
        $this->getView()->jQuery()->addOnLoad('$(".edit_form").ajaxForm({success: currentPage.saveTaxonomy, dataType: "json" });');
        $pageId = new NW_Form_Element_Hidden('pageid');
        $pageId->addValidator('Digits',false)
        		->setRequired(true);
       
        $related = new NW_Form_Element_Hidden('related');
        $related->setRequired(false);
        $role = new NW_Form_Element_Hidden('role');
        $taxonomy = new NW_Form_Element_Hidden('taxonomy');
        $taxonomy->setRequired(true);
        $this->addElements(array($pageId, $related, $role,$taxonomy));
    }
    
    
}


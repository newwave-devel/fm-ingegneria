<?php

class NW_Defs_Type extends NW_XmlDescriptor //NW_Descriptors_Descriptor
{
    //const DESCRIPTOR_PATH = 'etc/typedefs.xml';
    const FIELD_MODEL_DB_BASE = 1;
    const FIELD_MODEL_DB_EXTENDED = 2;

    const DISPLAY_TYPE_LIST = 1;


    public function dbModel($field, $fieldModel = self::FIELD_MODEL_DB_BASE)
    {
        $res = $this->descriptor->$field->database;
        if (self::FIELD_MODEL_DB_BASE)
            return strval($res);
    }

    public function appModel($field)
    {
        $res = $this->descriptor->$field;
        return strval($res['appmodel']);
    }

    public function formModel($field)
    {
        $res = $this->descriptor->$field;
        return strval($res['formmodel']);
    }

    //@TODO: verify whether it would be better to put this in AdminListObject
    //reasons for moving it: -> just because regards the list
    //reasons for keeping: ->just because this knows about all typedefs
    public function renderFor(&$field, $type, $display=self::DISPLAY_TYPE_LIST)
    {
        switch ($display)
        {
            case self::DISPLAY_TYPE_LIST:
                return $this->renderForList($field, $type);
        }
    }

    public function renderForList($field, $type)
    {
        switch ($type)
        {
            case 'string':
                $value = substr($field, 0, 150);
                break;
            case 'text':
                $value = substr($field, 0, 150);
                break;
            case 'active':
                $value = ($active == 'Y');
                break;
            default:
                $value = substr($field, 0, 150);
                break;
        }
        return $value;
    }

}

?>
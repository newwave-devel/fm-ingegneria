<?php

class App_Model_Dash_Graph {

    public $graphType;
    public $graphTitle;
    public $multi;
    public $metrics;
    public $values;
    public $clearBoth = false;

    function __construct($options = array()) {
        $this->graphType = isset($options['graphType']) ? $options['graphType'] : '';
        $this->graphTitle = isset($options['graphTitle']) ? $options['graphTitle'] : '';
        $this->multi = isset($options['multi']) ? $options['multi'] : false;
        $this->metrics = isset($options['metrics']) ? $options['metrics'] : array();
        $this->values = isset($options['values']) ? $options['values'] : array();
        $this->clearBoth = isset($options['clearBoth']) ? $options['clearBoth'] : false;
    }

    public function getGraphType() {
        return $this->graphType;
    }

    public function setGraphType($graphType) {
        $this->graphType = $graphType;
        return $this;
    }

    public function getGraphTitle() {
        return $this->graphTitle;
    }

    public function setGraphTitle($graphTitle) {
        $this->graphTitle = $graphTitle;
        return $this;
    }

    public function isMulti() {
        return $this->multi;
    }

    public function setMulti($multi) {
        $this->multi = $multi;
        return $this;
    }

    public function getMetrics() {
        return $this->metrics;
    }

    public function setMetrics($metrics) {
        $this->metrics = $metrics;
        return $this;
    }
    
    public function getValues() {
        return $this->values;
    }

    public function setValues($values, $label) {
       $this->values[$label] = $values;
        return $this;
    }

    public function getClearBoth() {
        return $this->clearBoth;
    }

    public function setClearBoth($clearBoth) {
        $this->clearBoth = $clearBoth;
        return $this;
    }

    public static function getGraph($type, $options = array()) {
        try {
            $className = sprintf("App_Model_Dash_Graph_%s", ucfirst($type));
            if (!class_exists($className))
                throw new Exception("Widget $type not found");
            return new $className($options);
        } catch (Exception $e) {
            throw $e;
        }
    }

}


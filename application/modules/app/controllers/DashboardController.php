<?php
class App_DashboardController extends NW_Controller_Action 
{
    public function indexAction() {
         $this->_setAjaxAction();
         $this->_helper->SwitchContextByUser();
         $pageService = new Pages_Model_Page_Service();

         $contents = App_Model_Dash_Widget::getWidget('contentsCount', array('type' => App_Model_Dash_Widget_ContentsCount::TYPE_HEADER))
                ->addValue("Pages",  $pageService->count(), 'left')
                ->addValue("Draft",  $pageService->count(array('status' => Pages_Model_Page::STATUS_DRAFT)), 'left')
                ->render();
        
//         $portfolio = App_Model_Dash_Widget::getWidget('portfolio')
//                 ->setBoxTitle("News from Newwave media agency")
//                 ->setBoxPosition("right");
                
        $rss1 = App_Model_Dash_Widget::getWidget("rss", array("uri" => 'http://www.gostructural.com/RSS'));      
        $rss1->setBoxTitle("News from http://www.gostructural.com");
                                                                    
        $rss2 = App_Model_Dash_Widget::getWidget('rss', array('uri'  => 'http://www.engineeringnews.co.za/page/home/feed'));
        $rss2->setBoxTitle("News from http://www.engineeringnews.co.za");
 
        $this->view->widgets = array($contents, $rss1, $rss2);
        
    }
}
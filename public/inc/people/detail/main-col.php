<?php
require_once("inc/tpls/blocks/list/tpl.php");
require_once("inc/functions/blocks.php");

$people_detail_main_col_contents = array();
$people_detail_main_col_contents_projects= array();
$people_detail_main_col_contents_news = array();
$people_detail_main_col_contents_html_sx = '';
$people_detail_main_col_contents_html_dx = '';
foreach($projectsBlocks as $pb)
	$people_detail_main_col_contents_projects[] = $pb->getId();


foreach($newsBlocks as $pb)
	$people_detail_main_col_contents_news[] = $pb->getId();
$people_detail_main_col_contents = array_merge(array_values($projectsBlocks), array_values($newsBlocks));

if($people_detail_main_col_contents !== false )
{

	shuffle($people_detail_main_col_contents);
	
	$odd = true;
	foreach($people_detail_main_col_contents as $item)
	{
		$blockId = $item->getId();
		$is_project = (in_array($blockId,$people_detail_main_col_contents_projects));
		if($is_project)
		{
			if($odd)
				$people_detail_main_col_contents_html_sx .= renderBlockProject($tpl_item_project, $item, $blockService, $odd);
			else 
				$people_detail_main_col_contents_html_dx .= renderBlockProject($tpl_item_project, $item, $blockService, $odd);
		}
		else 
		{
			if($odd)
				$people_detail_main_col_contents_html_sx .= renderBlockNews($tpl_item_news, $item, $blockService, $odd);
			else 
				$people_detail_main_col_contents_html_dx .= renderBlockNews($tpl_item_news, $item, $blockService, $odd);
		}
		$odd = !$odd;
	}

    if($people_detail_main_col_contents_html_sx != '' && $people_detail_main_col_contents_html_dx!= '')
    {
        echo sprintf('<h4 class="title">%s</h4>',  $translator->{"Profilo People"}->_t("Related Contents"));

    ?>
            <aside id="mainCol">
            <?php echo $people_detail_main_col_contents_html_sx ?>
            </aside>
            <aside id="sidebar">
            <?php echo $people_detail_main_col_contents_html_dx ?>
            </aside>
    <?php
        }
    }
?>
<?php

class NW_Media_Uploader {

    private $_apply_rename;
    private $_assetType;

    const ASSETS_PATH = '/assets';
    const TEMP_PATH = '/assets/temp';
    const APPLY_RENAME = true;
    const DO_NOT_RENAME = false;

    public function __construct($assetType, $apply_rename = self::APPLY_RENAME) {
        $this->_apply_rename = $apply_rename;
        $this->_assetType = $assetType;
    }

    private function _setAttributes(App_Model_Asset &$asset) {
        $absPath = $asset->getAssetPath();
        if (file_exists($absPath)) {
            $asset->setFilesize(filesize($absPath));
            
            switch ($this->_assetType) {
                case App_Model_Asset::MEDIA_TYPE_IMAGE:

                    $img_attributes = getimagesize($absPath);
                    if ($img_attributes) {
                        $asset->setAttribute('type', $img_attributes[2])
                                ->setAttribute('wh', $img_attributes[3])
                                ->setAttribute('mime', $img_attributes['mime'])
                                ->setAttribute('width', $img_attributes[0])
                                ->setAttribute('height', $img_attributes[1]);
                    }
                    break;
                default:
                    $attributes = stat($absPath);

                    $fileExtension = pathinfo($absPath, PATHINFO_EXTENSION);
                    if ($attributes && $fileExtension) {
                        $asset->setAttribute('size', $attributes['size'])
                                ->setAttribute('extension', $fileExtension);
                    }
                    break;
            }
        }
        return false;
    }

    public function upload(&$fileRef) {
        try {
            $tempFile = $fileRef['tmp_name'];
            $realName = $fileRef['name'];
            $fileName = $realName;
            $targetPath = $this->_getAbsPath($fileName);
            if ($this->_apply_rename) {
                $fileName = $this->_rename($targetPath);
                $targetPath = $this->_getAbsPath($fileName);
            }

            $targetFile = str_replace('//', '/', $targetPath);
            $urlPath = str_replace('//', '/', $this->_getUrlPath($fileName));
            if (!move_uploaded_file($tempFile, $targetFile))
                throw new Exception("Cannot move uploaded file");

            switch ($_FILES['uploaded']['error']) {
                case 0:
                    $asset = new App_Model_Asset();
                    $asset->setAssetName($fileName)
                            ->setUploadName($realName)
                            ->setAssetPath($targetPath)
                            ->setAssetUrl($urlPath)
                            ->setType($this->_assetType);
                    $this->_setAttributes($asset);
                    return $asset;
                case 1:
                    throw new Exception("The file is bigger than this PHP installation allows");
                case 2:
                    throw new Exception("The file is bigger than this form allows");
                case 3:
                    throw new Exception("Only part of the file was uploaded");
                case 4:
                    throw new Exception("No file was uploaded");
                case 6:
                    throw new Exception("Missing a temporary folder");
                case 7:
                    throw new Exception("Failed to write file to disk");
                case 8:
                    throw new Exception("File upload stopped by extension");
                default:
                    throw new Exception("unknown error " . $fileRef['error']);
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    private function _getAbsPath($fname) {
        return sprintf("%s%s/%s", PUBLIC_PATH, self::ASSETS_PATH, $fname);
    }

    private function _getUrlPath($fname) {
        return sprintf("%s/%s", self::ASSETS_PATH, $fname);
    }

    private function _rename($targetFile) {
        try {
            $pathinfo = pathinfo($targetFile);
            if (isset($pathinfo['extension']))
                return sprintf("%s.%s", md5(time() . $pathinfo['filename']), $pathinfo['extension']);
            return sprintf("%s", md5(time() . $pathinfo['filename']));
        } catch (Exception $e) {
            throw $e;
        }
    }

    public static function unlink($fname) {
        try {
            if (!is_file($fname))
            	return true; //has already been removed
               // throw new Exception("File '$fname' not found..sounds pretty strange!");
            if (!is_writable($fname))
                throw new Exception("Cannot remove file '$fname'");
            if (!unlink($fname))
                throw new Exception("Unknown Error deleting file '$fname'");
            return true;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public static function vaultAbsPath($fname)
    {
    	return sprintf("%s%s/%s", PUBLIC_PATH, self::TEMP_PATH, $fname);
    }
    
    public static function vaultRelPath($fname)
    {
    	return sprintf("%s/%s", self::TEMP_PATH, $fname);
    }
    
    public static function isImage($path)
    {
    	$a = getimagesize($path);
    	$image_type = $a[2];
    
    	if(in_array($image_type , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP)))
    	{
    		return true;
    	}
    	return false;
    }
    
    public static function exists($path)
    {
    	return file_exists($path);
    }

}

?>

<?php

/**
 * Description of Link
 *
 * @author Ale
 */
class Zend_View_Helper_Link_Ajax_Action extends Zend_View_Helper_Abstract
{
    public function link_ajax_action($action, $id, $content='', $options=array())
    {      
        $tpl = '<a href="%s" %s>%s</a>';
        
        $route = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();  
        
        $itemId = (isset($options['id'])) ? sprintf(' id="%s%d %s" ', $action, $id, $options['id']) : sprintf(' id="%s%d" ',$action, $id);
        $itemClass = (isset($options['class'])) ? sprintf(' class="action %s %s" ', $action, $options['class']) : sprintf(' class="action %s" ', $action);
        $itemRel = (isset($options['rel'])) ? sprintf(' rel="%s" ', $options['rel']) : '';
       
        $anchor = $this->view->url(array("action" => $action, "id" => $id), $route);
        $this->view->headScript()->appendFile("/js/admin/NW/actions.js");
        $this->view->headScript()->appendFile("/js/admin/NW/callbacks/actions.js");
        $this->view->jQuery()->addOnload(sprintf("NW_Actions.registerGet('%s', %sAction);", $action, $action));
        $content = (empty($content)) ? sprintf('<img src="/img/admin/icons/actions/%s.png" />', $action) : $content;
        return sprintf($tpl, $anchor, $itemId.$itemClass.$itemRel, $content);
    }
    
    

}

?>

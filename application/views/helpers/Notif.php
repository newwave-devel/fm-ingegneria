<?php

class Zend_View_Helper_Notif extends Zend_View_Helper_Abstract {
    public $view;
    
    public function setView(Zend_View_Interface $view) {
    	$this->view = $view;
    }
    
    public function notif($message, $status='info') {
    	$html = sprintf('<div class="notif %s">', $status);
        $html .=  $message; 
        $html .=  '<a href="#" class="close">x</a>';
        $html .= '</div>';    
        return $html;
    }

   
}

?>

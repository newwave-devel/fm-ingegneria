<?php if($paginator->hasPages()): ?>
<nav id="pagination">
	<ul>
		<?php 
			$curPage = $paginator->curPage; 
			$lastPage = $paginator->numPages;
			$prev = ($curPage > 1) ? $curPage-1 : 1;
			$next = ($curPage < $lastPage) ? $curPage +1 : $lastPage;
		?>
		<li><a class="prev pagination" href="#" rel="<?php echo $prev ?>">Prev</a></li>
		<li><a class="current" href=""><?php echo $curPage ?></a></li>
		<li>di <strong><?php echo $lastPage ?></strong></li>
		<li><a class="next pagination" href="#" rel="<?php echo $next ?>">Next</a></li>
	</ul>
</nav>
<?php endif; ?>
<!--pagination-->

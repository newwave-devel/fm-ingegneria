<?php

class NW_Models_Object_Mapper {

    private $fieldsname;
    protected $decoratorRef;
    protected $table;

    public function __construct(NW_Models_Object_Object &$decoratorRef) {

        $this->decoratorRef = $decoratorRef;
        $this->table = $this->decoratorRef->moduleDescriptor()->table($this->decoratorRef->getClassUri(), "mainTable");
        $this->fieldsname = $this->decoratorRef->classDescriptor()->byAttribute("translatable", "N");
    }

    public function getFieldsName() {
        return $this->fieldsname;
    }

//    public function fieldIsNotTranslatable($field)
//    {
//        return in_array($field, $this->fieldsname);
//    }

    public function fieldsInView($viewName) {
        $res = array();
        foreach ($this->fieldsname as $fName) {
            if ($this->decoratorRef->classDescriptor()->inView($viewName, $fName))
                $res[] = $fName;
        }
        return $res;
    }

    public function queryInsert($object) {
        $query = new NW_Db_Query_Insert($this->table);
        foreach ($this->fieldsname as $f)
            $query->addItem($f, $object->$f);
        $query->addItem('id', $object->id);
        return $query;
    }

    public function queryUpdate(&$object) {
        $query = new NW_Db_Query_Update($this->table);
        foreach ($this->fieldsname as $f)
            $query->add(array($f => $object->$f));
        $query->where(array("id=?" => $object->id));
        return $query;
    }

    public function queryAll($params, NW_Pages_Paginator &$paginator) {
        $query = new NW_Db_Query_Select();
        $query->select($this->getFieldsName(), $this->table)
                ->from($this->table);
        if (!empty($params))
            $query->where($params);
        $query->limit($paginator->getStart(), $paginator->itemsPerPage);
        return $query;
    }

    public function queryView($viewName, $params, &$paginator) {
        $query = new NW_Db_Query_Select();
        $fieldsInView = $this->fieldsInView($viewName);
        if (!empty($fieldsInView))
            $query->select($fieldsInView, $this->table);
        $query->from($this->table);
        if (!empty($params))
            $query->where($params);
        if (!empty($paginator))
            $query->limit($paginator->getStart(), $paginator->itemsPerPage);
        return $query;
    }

    public function queryLoad($params = '') {
        $query = new NW_Db_Query_Select();
        $query->select($this->getFieldsName(), $this->table)
                ->from($this->table)
                ->limit(0, 1);
        if (!empty($params))
            $query->where($params);
        return $query;
    }

    protected function getDecorator() {
        return $this->decoratorRef;
    }

    public function getTable() {
        return $this->table;
    }

    public function queryLoadByRelDefInTable(NW_Db_Query_Select &$query, $relationName, $viewName, $callerTable) {
        if ($query == '')
            $query = new NW_Db_Query_Select();
        $tableAlias = $this->tableAlias($relationName);

        foreach ($this->decoratorRef->classDescriptor()->view($viewName) as $field) {
            if (in_array($field, $this->fieldsname)) {
                $fieldAlias = $this->fieldAlias($field, $relationName);
                $query->select(array($field => $fieldAlias), $tableAlias);
            }
        }

        $query->select(array('id' => $this->fieldAlias('id', $relationName)), $tableAlias);
        $query
                ->leftJoinOn(array($this->table => $tableAlias), sprintf("%s.id=%s.%s", $tableAlias, $callerTable, $relationName));
    }

    protected function fieldAlias($field, $relationName) {
        return sprintf("`%s.%s`", $relationName, $field);
    }

    protected function tableAlias($relationName, $table = '') {
        $table = ($table == '') ? $this->table : $table;
        return sprintf("_%s_%s", $relationName, $table);
    }

}

?>
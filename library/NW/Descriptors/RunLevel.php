<?php

class NW_Descriptors_RunLevel extends NW_XmlDescriptor
{

    public function includes()
    {
        return $this->descriptor->includes->include;
    }

    public function defines()
    {
        return $this->descriptor->defines->define;
    }

    public function autoloadPool()
    {
        return $this->descriptor->autoload_pool->dir;
    }

    public function param($group, $param_name)
    {
        return $this->descriptor->params->xpath(sprintf('/group[@name="%s"]/param[@name="%s"]', $group, $param_name));
    }

    /**
     * each runlevels has a set of includes, has its set of autoload pools,
     * and makes available application params,
     * Enter description here ...
     */
    public function run()
    {
        try {
            $this->addIncludePath();
            $this->doDefines();
            $this->addInclude();
            //performs autoload
            spl_autoload_register('NW_Descriptors_RunLevel::autoload');
        } catch (Exception $e) {
            throw $e;
        }
    }

    private function addInclude()
    {
        foreach ($this->includes() as $node)
        {
            $mode = $node["mode"];
            $file = APPLICATION_PATH . $node;

            if (!file_exists($file))
            {
                throw new Exception($file . " Not Found");
            }
//            Logger::getLogger(__CLASS__)->info("Including: $file");
            switch ($mode)
            {
                case 'require_once':
                    require_once $file;
                    break;
                case 'require':
                    require $file;
                    break;
                case 'include':
                    include $file;
                    break;
                case 'include_once':
                    include_once $file;
                    break;
                default:
                    break;
            }
        }
    }

    private static function autoLoad($class)
    {
        try {
            
            $tokens = (explode("_", $class)); 
            $classname = array_pop($tokens).".php";
 //           $dir = implode(DIRECTORY_SEPARATOR, $tokens);
//            Logger::getLogger(__CLASS__)->debug("AUTOLOADER: searching for $dir");
            Zend_Loader::loadFile($classname, APPLICATION_PATH."/models/", true);   
                      
        } catch (Exception $e) {
            print $e->getMessage();
        }
    }

    private function doDefines()
    {
        foreach (NW_Application::getCurrentRunLevel()->defines() as $node)
        {
            $define = $node["varname"];
            $value = $node;
            define($define, $value);
        }
    }

    private function addIncludePath()
    {
        try {
            $all_include_path = get_include_path();
            foreach (NW_Application::getCurrentRunLevel()->getIncludePath() as $node)
            {
                $filepath = APP_PATH . $node;
                if (file_exists($filepath) && is_readable($filepath))
                    $all_include_path .= ":" . $filepath;
                else
                    throw new Exception("$filepath not Found");
            }
            set_include_path($all_include_path);
        } catch (Exception $e) {
            throw $e;
        }
    }

    private function getIncludePath()
    {
        return $this->descriptor->include_path->dir;
    }

}
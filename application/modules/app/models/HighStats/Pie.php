<?php
/**
 * Description of Pie
 *
 * @author Ale
 */
class App_Model_HighStats_Pie extends App_Model_HighStats_Chart {
    
    private $type= 'pie';
    
    public function __construct($options = array()){
        parent::__construct($options);
    }
    
    public function getJs() {
        $this->options['series'] = (isset($this->options['series'])) ? $this->options['series'] : array();
        $this->options['series'][0]['type'] = $this->type;
        $this->options['series'][0]['data'] = array();
        $this->_prepareDatas();
        return json_encode($this->options);
    }
    
    private function _prepareDatas()
    {
        $series = $this->getCurSeries(0);
        foreach($this->getDataX() as $pos => $x)
             $this->options['series'][0]['data'][] = array($x, $series->y($pos));
        foreach($series->getOptions() as $k => $v)
            $this->options['series'][0][$k] = $v;
        $this->addOption('plotOptions', array('pie' => array('allowPointSelect' => true)));
    }
    
   
}

?>

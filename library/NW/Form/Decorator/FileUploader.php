<?php

class NW_Form_Decorator_FileUploader extends NW_Form_Decorator
{
	public function render($content)
	{
		$element = $this->getElement();
		 
		if (!$element instanceof Zend_Form_Element)
		{
			return $content;
		}
		if (null === $element->getView())
		{
			return $content;
		}
		return $this->getOutput();
	}
	
	public function getOutput()
	{
		$tpl = '<div class="formBlock">';
		$tpl .= '<div id="%s"></div>';
		$tpl .= "<script>";
		$tpl .= $this->_js();
		$tpl .= "</script>";
		$tpl .= "</div>";
		return sprintf($tpl, $this->getElement()->getAttrib("id"));
	}
	
	private function _js()
	{
		$element = $this->getElement();
		$id = $element->getAttrib('id');
		$options = array("url" => "/app/media/upload", "chunkSize" => 0, "success" => $this->_success(), "finish" => $this->_finish());
		$extra_options = $element->getAttrib("params");
		//parse the extra options
		$full_options = (is_array($extra_options) && !empty($extra_options)) ? array_merge($options, $extra_options) : $options; 
		$res = array();
		foreach($full_options as $opt => $value)
		{
			switch($opt)
			{
				case 'data':
					$temp = new stdClass();
					$temp->blockId = $element->getAttrib("target"); 
 					foreach($value->children() as $item)
						$temp->{$item->getName()} = (string) $item;;
					$res['data'] = $temp;
					break;
				case 'finish':
			    case 'success':
			    	$res[$opt] = new Zend_Json_Expr($value);
			    	break;
			    default:
			    	$res[$opt] = $value;
			    	break;
			}
		}
		
		$json = Zend_Json::encode($res,false,array('enableJsonExprFinder' => true));
		$json = Zend_Json::prettyPrint($json, array("indent" => " "));
		
    	return sprintf("$('#%s').ajaxupload(%s);", $id, $json);
	}
	
	private function _success()
	{
		$tpl = <<<TPL
		function(file_name)	{
			var response = this.xhr.response;
			Library.addRemote(response);
		}
TPL;
		return sprintf($tpl);
	}
	private function _finish()
	{
		$tpl = <<<TPL
		function(file_names, file_obj)
		{
			alert("files caricati con successo");
		}
TPL;
		return sprintf($tpl);
	}
	

}

?>
<?php
require_once('mpdf.php');

class App_Pdf extends mPDF
{
	protected $documentName;
	
	public function __construct($mode='',$format='A4',$default_font_size=0,$default_font='',$mgl=15,$mgr=15,$mgt=16,$mgb=16,$mgh=9,$mgf=9, $orientation='P')
	{
		parent::__construct($mode,$format,$default_font_size,$default_font,$mgl,$mgr,$mgt,$mgb,$mgh,$mgf,$orientation);
		$this->SetTopMargin(50);
		$this->setHeader();
	}
	
	public function download($response, $filename, $pdfString)
	{
		$response->setHeader('Cache-Control', 'public', true)
			->setHeader('Content-Description', 'File Transfer', true)
			->setHeader('Content-Disposition', 'attachment; filename='.$filename, true)
			->setHeader('Content-Type', 'application/pdf', true)
			->setHeader('Content-Transfer-Encoding', 'binary', true)
			->appendBody($pdfString);
	}
	
	public function setView($view, $viewPath)
	{
		$this->WriteHTML($view->render($viewPath));
		return $this;
	}
	
	public function getDocumentName()
	{
		return $this->documentName;
	}
}

?>
<?php

class App_LoginController extends Zend_Controller_Action
{
    private $_translate = '';

    protected function _getAuthAdapter()
    {
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);

        $authAdapter->setTableName('tbl_users')
                ->setIdentityColumn('username')
                ->setCredentialColumn('password')
                ->setCredentialTreatment('SHA1(CONCAT(?,salt))');
        
        return $authAdapter;
    }

    public function init()
    {
        Zend_Layout::getMvcInstance()->setLayout('login');
        $this->_translate = Zend_Registry::get ('Zend_Translate');
    }

    public function indexAction()
    {
        $form = new Application_Form_Login();
        $request = $this->getRequest();
        if ($request->isPost())
        {
            if ($form->isValid($request->getPost()))
            {
                if ($this->_process($form->getValues()))
                    $this->_redirect('/adminpanel');
                else //invalid credentials
                {	
                	$this->view->msg = $this->_translate->_("Unauthorized Access");
                	$this->view->form = $form;
                }
            } else 
            	{	
	               $this->view->msg = $this->_translate->_("Invalid data submitted");
	               $this->view->form = $form;
	            }
        } else //request is not post
        $this->view->form = $form;
    }

    protected function _process($values)
    {
        // Get our authentication adapter and check credentials
        $adapter = $this->_getAuthAdapter();
        $adapter->setIdentity($values['username']);
        $adapter->setCredential($values['password']);

        $auth = Zend_Auth::getInstance();
        $result = $auth->authenticate($adapter);
        if ($result->isValid())
        { 
            $real_user = new App_Model_User();
            $real_user->setOptions((array) $adapter->getResultRowObject());
            $auth->getStorage()->write($real_user);
            return true;
        }
        return false;
    }
}


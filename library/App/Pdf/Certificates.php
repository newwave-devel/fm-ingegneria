<?php
class App_Pdf_Certificates extends App_Pdf
{
	public function __construct()
	{
		parent::__construct();
		$this->documentName = 'ElencoDocumentazione.pdf';
	}
	
	public function setHeader()
	{
		$html_header = <<<HTML
		<div>
			<table border="1" cellpadding="0" cellspacing="0" style="width: 863px;">
				<tbody>
					<tr>
						<td rowspan="3">
							<p><img src="common/img/docs/logo.gif" style="height: 72px; width: 235px" /></p>
						</td>
						<td rowspan="3">
							<p>ELENCO DOCUMENTAZIONE</p>
						</td>
						<td>
							<p>MOD PU 07 02/02</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>Rev.02 del 01/01/2013</p>
	
							<p>Pagina {PAGENO} di {nb}</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>DPA</p>
						</td>
					</tr>
				</tbody>
			</table>
		<p>&nbsp;</p>
	</div>
HTML;
		$this->SetHTMLHeader($html_header);
		return $this;
	}
	
}

?>
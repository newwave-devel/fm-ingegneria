<?php

class App_Model_DbTable_TransactionalEmail extends Zend_Db_Table_Abstract
{

    protected $_name = 'tbl_messages_transactional';
    protected $_primary = 'id';
    
    protected $_referenceMap    = array(
    		'Message' => array(
    				'columns'           => 'message',
    				'refTableClass'     => 'App_Model_DbTable_Message',
    				'refColumns'        => 'id'
    		)
    
    );
  
}


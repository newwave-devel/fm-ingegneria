<?php

abstract class NW_Models_Object_Decorator {

    protected $_object;
    protected $_mapper;

    public function __construct($object) {
        $this->_object = $object;
    }

    public function getClassUri() {
        return $this->getObject()->getClassUri();
    }

    public function getId() {
        return $this->getObject()->getId();
    }

    public function setId($id) {
        return $this->getObject()->setId($id);
    }

    public function getMainTable() {
        return $this->getObject()->getMainTable();
    }

    public function __set($name, $value) {
        $this->getObject()->$name = $value;
    }

    public function __get($name) {
        return $this->getObject()->$name;
    }

    public function stdClass() {
        return $this->toStdClass();
    }

    public function getMapper() {
        return $this->_mapper;
    }

    public function getDecorator($decoratorName) {
        try {
            $realDecoratorName = sprintf("NW_Models_Object_Decorators_%s", ucfirst($decoratorName));
            //$object = $this->getObject();
            if ($this instanceof $realDecoratorName)
                return $this;
            if ($this instanceof NW_Models_Object_Object)
                throw new Exception("No decorator $decoratorName found in this object");
            return $this->getObject()->getDecorator($decoratorName);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function dumpDecorators()
    {
        try {
            print "<li>". get_class( $this ) . "</li>";
            if ($this instanceof NW_Models_Object_Object)
                return;
            else return $this->getObject()->dumpDecorators();
        } catch(Exception $e)
        {
            throw $e;
        }
    }

    public function isCatalogable() {
        if ($this->getObject() instanceof NW_Models_Object_Decorator)
            return $this->getObject()->isCatalogable();
        return false;
    }

    public function isTranslatable() {
        if ($this->getObject() instanceof NW_Models_Object_Decorator)
            return $this->getObject()->isTranslatable();
        return false;
    }

    public function isOwnable() {
        if ($this->getObject() instanceof NW_Models_Object_Decorator)
            return $this->getObject()->isOwnable();
        return false;
    }

    public function isRevisionable() {
        if ($this->getObject() instanceof NW_Models_Object_Decorator)
            return $this->getObject()->isRevisionable();
        return false;
    }

    public function isRelationable() {
        if ($this->getObject() instanceof NW_Models_Object_Decorator)
            return $this->getObject()->isRelationable();
        return false;
    }

    //DATABASE METHODS

    public function save() {
        if ($this->getId() == -1)
            return $this->insert();
        else
            return $this->update();
    }

    public function insert($object) {
        try {
            $this->getAdapter()->transactionStart();
            $this->doInsert($object);
            $this->getAdapter()->transactionCommit();
        } catch (Exception $e) {
            $this->getAdapter()->transactionRollBack();
            throw $e;
        }
    }

    public function update($object) {
        try {
            $this->getAdapter()->transactionStart();
            $this->doUpdate($object);
            $this->getAdapter()->transactionCommit();
        } catch (Exception $e) {
            $this->getAdapter()->transactionRollBack();
            throw $e;
        }
    }

    public function load($params) {
        try {
            $query = $this->doLoadOne($params)->getQuery();
            $result = (object) $this->getAdapter()->fetchAssociative($query);
            return NW_Application::__new($this->getClassUri(), $result);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function all($params = array(), NW_Pages_Paginator &$paginator) {
        try {

            $selectQuery = $this->doLoadAll($params, $paginator);
            $countQuery = $selectQuery->toCountQuery();
            $contents = $this->getAdapter()->fetchRowsAssociative($selectQuery->getQuery());
            $paginator->itemsCount = $this->getAdapter()->fetchResult($countQuery);

            foreach ($contents as $res)
                $results[] = NW_Application::__new($this->getClassUri(), $res);

            return $results;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function view($viewName, $params=array(), &$paginator = null)
    {
        try {
            $selectQuery = $this->doView($viewName, $params, $paginator);
            $countQuery = $selectQuery->toCountQuery();
            $contents = $this->getAdapter()->fetchRowsAssociative($selectQuery->getQuery());
            $paginator->itemsCount = $this->getAdapter()->fetchResult($countQuery);
            return $contents;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function loadByRelDef(NW_Db_Query_Select &$query, $relname, NW_ClassURI $callerUri, $callerTable) {
        try {
            //detect what i can return.
            $reldefsdescriptor = NW_Application::getSingleton('defs/NW_Defs_Relations');
            $reltype = $reldefsdescriptor->relation($relname, 'reltype'); //1,n or n,1 or n,m
            
            //if the caller is relating.. i am related!
            $myrole = ($reldefsdescriptor->relation($relname, 'relating') == $callerUri) ? NW_Defs_Relations::ROLE_RELATED : NW_Defs_Relations::ROLE_RELATING;
            //$fieldsToExtract = $reldefsdescriptor->getForeignKeys($relname, $myrole);

            if ($reltype == '1,n') {
                if ($myrole == NW_Defs_Relations::ROLE_RELATED) {
                    $view = $reldefsdescriptor->relating($relname, 'view');
                    $this->getObject()->doLoadByRelDefInTable($query, $relname, $view, $callerTable);

                    return $query;
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function getObject() {
        return $this->_object;
    }

    protected function getAdapter() {
        return $this->getObject()->getAdapter();
    }

    public function formElements() {
        $fields = $this->getObject()->formFields();
        return $fields;
    }
    
    public function classDescriptor()
    {
        return $this->getObject()->classDescriptor();
    }
    
     public function moduleDescriptor()
     {
         return $this->getObject()->moduleDescriptor();
     }
     
     public function emptyObject()
     {
         $curi = $this->getClassUri()->projectToZendClassName();
         $foo = new $curi();
         return $this->cloneNew($foo);
     }

}

?>

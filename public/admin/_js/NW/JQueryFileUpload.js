NW.JQueryFileUpload = (function () {
    // Initialize the application
    var items = {};
    
    jqfu = function(refToHidden)
    {
    	//this way it is private and visible from the class
       var hiddenFieldRef = refToHidden;
       var jqXHR = null;
       
       this.getjqXHR = function()
       {
    	  return jqXHR;
       };
       
       
       this.options = {
        		dataType: 'json',
        		url:"/app/media/upload",
        		paramName:  '',
        		formData: '',
        		add: function(e, data)
        		{
        			var buttonsRef = "#showhide-"+hiddenFieldRef;
        			$("#progress-"+hiddenFieldRef).fadeIn("200");
        			$(buttonsRef).fadeIn(200);
        			jqXHR = data.submit();
        		},
        		done: function (e, data) 
        		{
			 		if(data.result.status=="ko")
			 		{
						alert("Upload failed, possible reason: " + data.result.payload );
						$("#progress-"+hiddenFieldRef).hide();
						$("#showhide-"+hiddenFieldRef).hide();
			 		}
					else 
					{
						var ids = new Array(); 
						var uploadedRef = "#uploaded-" + hiddenFieldRef;
						for(pos in data.result.payload)
						{
							delete_url = data.result.payload[pos].delete_url;
							name = data.result.payload[pos].name;
							size = data.result.payload[pos].size;
							type = data.result.payload[pos].type;
							url = data.result.payload[pos].url;
							id = data.result.payload[pos].id;
							ids.push(id);
							var onclickAction = "NW_JQueryFileUpload.get('" + hiddenFieldRef +"').remove("+id+", this)";
							
							$(uploadedRef).append(	'<li>' + name + 
														' | ' + size + 
														'<span class="icon_font">'+
															'<a onclick="'+onclickAction+'" href="#">4</a>' +
														'</span>'+
													'</li>');
						}
						
						
						$("input[name='" +  hiddenFieldRef + "']").val(ids.join(","));
					}
			 		$("#progress-"+hiddenFieldRef).delay("850").fadeOut("250");
					$("#showhide-"+hiddenFieldRef).fadeOut("250");
		        },
				fail: function (e, data) 
				{
					if(data.errorThrown=='abort')
						alert("Operation aborted");
					else alert("There has been errors contacting the server");
					$("#progress-"+hiddenFieldRef).hide();
					$("#showhide-"+hiddenFieldRef).hide();
				},
				progressall: function (e, data) {
		            var progress = parseInt(data.loaded / data.total * 100, 10);
		            var progressdisplayer = "#progress-"+hiddenFieldRef+ " .progress-bar";
		            $(progressdisplayer).css('width',progress + '%');
				}     
        };
    
    }
    
    jqfu.prototype = 
    {  	
        setOptions : function(options)
        {
            for(var key in options)
                this.options[key] = options[key];
            return this;
        },
        setOption : function(param, value)
        {
            this.options[param] = value;
            return this;
        },
        fileupload : function(options)
        {   
        	if(options instanceof Array)
        	{
        		var newOptions = [];
        		for (var key in options)
        		{
        			if(options[key] == null) continue;
            			newOptions[key] = options[key];
        		}
        		$(".fileupload").fileupload(newOptions)
        	} else 
        		$(".fileupload").fileupload(this.options);
            
        },
        remove: function(paramId, itemRef)
        {
        	$.get("/app/media/delete",{paramId:paramId})
        		.done(function(data)
        		{ 
        			var result=jQuery.parseJSON(data);
        			if(result.status=="ok")
        				$(itemRef).parent().parent().fadeOut(200);
        			else alert(result.payload);})
        		.fail(function(){alert("There has been some errors processing the request");});
        },
        cancelUpload: function()
        {
        	this.getjqXHR().abort();
        },
    }
    
    
    // Return the public facing methods for the App
    return {
        add: function (paramId) {
            items[paramId] = new jqfu(paramId);
            items[paramId].setOption('paramName', paramId);
            
            return items[paramId];
        },
        setOption: function (paramId, property, value) {
            items[paramId].options[property] = value;
            return items[paramId];
        },
        setOptions: function (paramId, properties) {
            items[paramId].setOptions(properties);
            return items[paramId];
        },
        fileupload: function(paramId)
        {
        	
            items[paramId].fileupload();
            return items[paramId];  
        },
        get: function(paramId){
            return items[paramId];
        }
    };
}());


var NW_JQueryFileUpload = NW.JQueryFileUpload;
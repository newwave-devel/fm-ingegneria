<?php

class App_Model_I18n_Mapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('App_Model_DbTable_I18n');
        }
        return $this->_dbTable;
    }

    public function save(App_Model_I18n $model) {
        try {
            $data = $this->_toDb($model);
            
            $id = $model->getId();
            if (empty($id)) {
                unset($data['id']);
                return $this->getDbTable()->insert($data);
            } else {
                $this->getDbTable()->update($data, array('id = ?' => $id));
                return $id;
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function find($id, App_Model_I18n &$model) {
        try {
            $result = $this->getDbTable()->find($id);
            if (0 == count($result)) {
                return;
            }
            $row = $result->current();
            $this->_fromDb($model, $row);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function fetchAll(NW_Pages_Paginator &$paginator, $paramsSearch, $paramsOrder) {
    	try {
    		$table = $this->getDbTable();
    		$select = $table->select();
    
    		$this->_filterBySearchParam($select, $paramsSearch);
    		if(!empty($paginator) && !$paginator->getFetchAll())
    			$select->limit($paginator->itemsPerPage, $paginator->getStart());

            if(!empty($paramsOrder))
    		{
    			foreach($paramsOrder as $key => $dir)
    			{
    				$sql = sprintf("%s %s", $key, strtoupper($dir));
    				$select->order($sql);
    			}
    		}
    		$entries = array();

    		foreach ($table->fetchAll($select) as $row) {
    			$model = new App_Model_I18n();
    			$this->_fromDb($model, $row);
    			$entries[] = $model;
    		}
    		if(!empty($paginator))
    			$paginator->itemsCount = $this->count($select);
    			
    		return $entries;
    	} catch (Exception $e) {
    		throw $e;
    	}
    }

    public function count(Zend_Db_Select $select=null) {
        try {
            $table = $this->getDbTable();
            if(!empty($select))
            {
            	$select->reset(Zend_Db_Select::COLUMNS)
                    ->reset(Zend_Db_Select::LIMIT_COUNT)
                    ->reset(Zend_Db_Select::LIMIT_OFFSET);
            } else{
            	$select = $this->getDbTable()->select(Zend_Db_Table::SELECT_WITH_FROM_PART);
            }
            $select->columns(array('num' => new Zend_Db_Expr('COUNT(*)')));
            return $table->fetchRow($select)->num;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function delete($id) {
        try {
            $this->getDbTable()->delete(array("id=?" => $id));
            return true;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function contextAsSelect()
    {
        try{
            $select = $this->getDbTable()->select();
            $select->distinct()->from($this->getDbTable(), array("context"))
                ->order(array("context ASC"));
            $results = array();

            foreach($this->getDbTable()->fetchAll($select) as $result)
                $results[] = $result->context;
            return $results;
        } catch(Exception $e)
        {
            throw $e;
        }
    }

    private function _toDb(App_Model_I18n $model)
    {
        $map = array(
                'id' => $model->getId(),
                'context' => $model->getContext(),
                'format' => $model->getFormat(),
                'lang' => $model->getLang(),
                'key' => $model->getKey(),
                'value' => $model->getValue()
         );

        return $map;
    }

    private function _fromDb(App_Model_I18n &$model, $row) {

        $model->setId($row->id)
                ->setContext($row->context)
                ->setFormat($row->format)
                ->setKey($row->key)
                ->setValue($row->value)
                ->setLang($row->lang);


    }
    
    private function _filterBySearchParam(Zend_Db_Table_Select &$select, $paramsSearch = array()) {
    	if (!empty($paramsSearch)) {
    		if (!empty($paramsSearch['lang']))
    			$select->where("tbl_i18n.lang = ?",  $paramsSearch['lang']);
    		if (!empty($paramsSearch['key']))
    			$select->where("tbl_i18n.key = ?", $paramsSearch['key']);
            if (!empty($paramsSearch['keylike']))
            {
                $value = $paramsSearch['keylike'];
                $select->where("tbl_i18n.key LIKE ?", "%$value%" );
            }
    		if (!empty($paramsSearch['value']))
            {
                $value = $paramsSearch['value'];
    			$select->where("tbl_i18n.value LIKE ?", "%$value%" );
            }
    		if (!empty($paramsSearch['context']))
    		    $select->where("tbl_i18n.context = ?", $paramsSearch['context']);
    	}
    	return $this;
    }

}


<?php

class Zend_View_Helper_Tooltip extends Zend_View_Helper_Abstract
{
    
    public function tooltip($text)
    {
        return sprintf('<a href="#" class="tooltip" title="%s"><span class="note_icon icon_font">5</span></a>',$text);
    }
    
    
 
}

?>

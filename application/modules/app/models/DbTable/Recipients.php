<?php

class App_Model_DbTable_Recipients extends Zend_Db_Table_Abstract
{

    protected $_name = 'tbl_messages_recipients';
    protected $_primary = 'id';
    
    protected $_referenceMap    = array(
    		'Message' => array(
    				'columns'           => 'message',
    				'refTableClass'     => 'App_Model_DbTable_Message',
    				'refColumns'        => 'id'
    		)
    );
  
}


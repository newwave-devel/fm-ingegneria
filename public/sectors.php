<?php
$pageId = 'sectors';
include "inc/required.php";
include "inc/common/contents.php";
?>


<?php include "inc/common/head.php" ?>
		
        
        <!-- SCRIPT -->
		<script type="text/javascript" src="/common/js/core.js"></script>
        <script type="text/javascript" src="/common/js/jquery.nivo.slider.pack.js"></script>
        <script type="text/javascript" src="/common/js/custom.js"></script>
        
        <script type="text/javascript">	
                jQuery(window).load(function() {
                    // START
					loading();
					slideshow();
					$("#tabs").tabs();
					fancybox();
                });
        </script>
        
	</head>    
    
	<body lang="it">
    
    	<div id="loader"></div>
    	    	
        <?php include "inc/common/header.php"?>
        
        <section id="main_page">
        	<section class="container">
            
            	<?php include "inc/common/slider.php"; ?>
                
                <section id="home_articles">
                	
                    <h4 class="title">Related contents</h4>
                    
                    <?php include "inc/homepages/main-col.php"?>
                    
                </section><!--home_articles-->
                
                
                <section id="home_countries">
                	
                	<?php include "inc/homepages/countries.php" ?>
                    
                </section><!--home_countries-->
                
            </section><!--container-->
        </section><!--main_page-->
        
        <?php include "inc/common/footer.php"?>
		
	</body>
	
</html>
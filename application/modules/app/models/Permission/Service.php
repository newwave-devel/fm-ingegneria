<?php
class App_Model_Permission_Service
{
	protected $_mapper;
	
	public function getMapper()
	{
		return $this->_mapper;
	}
	
	public function __construct(){
		$this->_mapper = new App_Model_Permission_Mapper();
	}
	
	
	
	public function load(App_Model_User $user=null)
	{
	try {
		if(empty($user))
			$user = Zend_Auth::getInstance()->getIdentity();
		$permissions = new App_Model_Permissions();
		$permissions->setUser($user);
		$this->_mapper->load($permissions);
		return $permissions;
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	
	public function grant(App_Model_User $user, $resourcesIds)
	{
		try {
			$this->_mapper->grant($user, $resourcesIds);
			return $this;
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	public function deny(App_Model_User $user, $resourcesIds)
	{
		try {
			$this->_mapper->deny($user, $resourcesIds);
			return $this;
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	public function reset(App_Model_User $user)
	{
		try {
			$this->_mapper->reset($user);
			return $this;
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	public function save(App_Model_Permissions $perm)
	{
		try {
			$this->reset($perm->getUser());
			$this->grant($perm->getUser(), $perm->getAllowed());
        return $this;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
}
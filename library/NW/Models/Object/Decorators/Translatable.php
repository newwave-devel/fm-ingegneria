<?php

class NW_Models_Object_Decorators_Translatable extends NW_Models_Object_Decorator {

    public $_lang;
    protected $_contents;
    
    public function __construct($object, $lang = 'it') {
        $this->_object = $object;
        $this->_lang = $lang;
        $this->_mapper = new NW_Models_Object_Decorators_Mapper_Translatable($this);
        $temp = array();
        foreach ($this->_mapper->getFieldsName() as $fieldName)
            $temp[$fieldName] = '';
        $this->_contents = (object) $temp;
    }

    public function __set($name, $value) {
        if($name == 'lang') $this->setLang ($value);
        if (isset($this->_contents->$name))
            $this->_contents->$name = $value;
        else
            $this->getObject()->$name = $value;
        return $this;
    }

    public function __get($name) {
        if($name == 'lang') return $this->getLang();
        if (isset($this->_contents->$name))
            return $this->_contents->$name;
        else
            return $this->getObject()->$name;
    }

    public function getLang() {
        return $this->_lang;
    }

    public function setLang($lang) {
        $this->_lang = $lang;
        return $this;
    }

    public function isTranslatable() {
        return true;
    }

    public function getTranlatedContent() {
        return $this->_contents;
    }
    
    public function fieldIsTranslatable($field)
    {
        return $this->getMapper()->fieldIsTranslatable($field);
    }

    public function doInsert($object) {
        try {
            $object = $this->getObject()->doInsert($object);
            $query = $this->getMapper()->queryInsert($object);
            $this->getAdapter()->performInsertQuery($query);
            print "<li>".$query->getQuery()."</li>";
            return $object;
        } catch (Exception $e) {
            throw $e;
        }
    }
    //must cancel before, and add anyway 
    public function doUpdate(&$object) {
        try {
            $querydrop = $this->getMapper()->queryDrop($object);
            $this->getAdapter()->performDeleteQuery($querydrop);
            $queryInsert = $this->getMapper()->queryInsert($object);
            $this->getAdapter()->performInsertQuery($queryInsert);
            print "<li>".$queryInsert->getQuery()."</li>";
            return $this->getObject()->doUpdate($object);
        } catch (Exception $e) {
           
            throw $e;
        }
    }

    public function doLoadOne($params) {
        try {
            $query = $this->getObject()->doLoadOne($params);        
            return $this->getMapper()->queryLoad($query);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function doLoadAll($params, NW_Pages_Paginator &$paginator) {
        try {
            $query = $this->getObject()->doLoadAll($params, $paginator);
            return $this->getMapper()->queryAll($query);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function doView($viewName, $params, &$paginator) {
        try {
            $query = $this->getObject()->doView($viewName, $params, $paginator);
            return $this->getMapper()->queryView($viewName, $query);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function doLoadByRelDefInTable(NW_Db_Query_Select &$query, $relationName, $view, $callerTable)
    {
        try {
            $this->getObject()->doLoadByRelDefInTable($query, $relationName, $view, $callerTable);
            $this->getMapper()->queryLoadByRelDefInTable($query, $relationName, $view, $callerTable);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function formFields()
    {
        $fields = $this->getObject()->formFields();
        $lang = new NW_Form_Element_Select("lang");
        $lang->setLabel("Lingua")
            ->addMultiOption("it", "Italiano")
                ->addMultiOption("en", "English");
        $fields[] = $lang;
        return $fields;
    }
    
    public function cloneNew(&$object)
    {
        $object->lang = $this->getLang();
        foreach($this->_contents as $fName => $fValue)
            $object->$fName = $fValue;
        return $this->getObject()->cloneNew($object);
    }

}
?>

<?php

class NW_Models_Object_Decorators_Taggable extends NW_Models_Object_Decorator {

    /**
     *
     * Object tags.
     * 
     * @var array
     */
    public $_tags;

    public function __construct($object, $tags = array()) {
        $this->_object = $object;
        $this->_tags = $tags;
    }
    
    public function __set($name, $value) {
        switch($name)
        {
            case 'tags':
                    array_push($this->_tags,$value);
                break;
            default:
                $this->getObject()->$name = $value;
        }
        return $this;
    }
    
     public function __get($name) {
        switch($name)
        {
            case 'tags':
                return $this->_tags;
                break;
            default:
                return $this->getObject()->$name;
        }
        
    }

    public function export(&$object) {
        $res = array();
        $res['tags'] = $object->tags;
        return $res;
    }

    public function isTaggable() {
        return true;
    }

    public function doInsert($object) {
        try {
            $query = $this->getMapper()->queryInsert($object);
            print "<li>".$query->getQuery()."</li>";
            $id = $this->getAdapter()->performInsertQuery($query);
            $object->id = $id;
            return $this->getObject()->doInsert($object);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function doUpdate(&$object) {
        try {
            $query = $this->getMapper()->queryUpdate($object);
            $this->getAdapter()->performUpdateQuery($query);
            print "<li>".$query->getQuery()."</li>";
            return $this->getObject()->doUpdate($object);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function doLoadOne($params) {
        try {
            $query = $this->getObject()->doLoadOne($params);
            return $this->getMapper()->queryLoad($query);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function doLoadAll($params, NW_Pages_Paginator &$paginator) {
        try {
            $query = $this->getObject()->doLoadAll($params, $paginator);
            return $this->getMapper()->queryAll($query);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function doView($viewName, $params, &$paginator) {
        try {
            $query = $this->getObject()->doView($viewName, $params, $paginator);
            return $this->getMapper()->queryView($viewName, $query);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function doLoadByRelDefInTable(NW_Db_Query_Select &$query, $relationName, $view, $callerTable)
    {
        try {
            $this->getObject()->doLoadByRelDefInTable($query, $relationName, $view, $callerTable);
            $this->getMapper()->queryLoadByRelDefInTable($query, $relationName);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
//    public function toStdClass()
//    {
//        $stdClass = $this->getObject()->toStdClass();
//        $stdClass->status = $this->getStatus();
//        $this->ts_insert = $this->getTsInsert();
//        $this->ts_update = $this->getTsUpdate();
//        $this->ts_delete = $this->getTsDelete();
//        return $stdClass;
//    }
//    
    public function formFields()
    {
        $fields = $this->getObject()->formFields();
        $status = new NW_Form_Element_Select("status");
        $status->setLabel("Stato di pubblicazione");
        $status->addMultiOption(self::STATUS_ACTIVE, "active")
                ->addMultiOption(self::STATUS_DRAFT, "draft");
        $fields[] = $status;
        return $fields;
    }
    
    public function cloneNew(&$object)
    {
        $object->status = $this->getStatus();
        $object->ts_insert = $this->getTsInsert();
        $object->ts_update = $this->getTsUpdate();
        $object->ts_delete = $this->getTsDelete();
        return $this->getObject()->cloneNew($object);
    }
}


?>

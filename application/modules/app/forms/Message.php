<?php

class App_Form_Message extends Zend_Form {

	public function init() {
		$this->setMethod('post')
			->setAttrib('class', 'form')
			->setAction("/app/mailbox/save");
		
		$to = new NW_Form_Element_Text("to");
		$to -> setLabel("To")
			->setRequired(true)
			->addValidator('Regex',false, array('/([a-zA-Z0-9@._\-,;]+)/'))
			->setFilters(array('StringTrim'));
		$cc = new NW_Form_Element_Text("cc");
		$cc -> setLabel("Cc")
			->setRequired(false)
			->addValidator('Regex',false, array('/([a-zA-Z0-9@._\-,;]+)/'))
			->setFilters(array('StringTrim'));
		$bcc = new NW_Form_Element_Text("bcc");
		$bcc ->  setLabel("Bcc")
			->setRequired(false)
			->addValidator('Regex',false, array('/([a-zA-Z0-9@._\-,;]+)/'))
			->setFilters(array('StringTrim'));
		$subject = new NW_Form_Element_Text('subject');
		$subject->setLabel("Subject")
			->setRequired(true)
			->addValidator('Regex',false, array('/([a-zA-Z 0-9._\-,;]+)/'))
			->setFilters(array('StringTrim'));
		$body = new NW_Form_Element_CKEditor('body');
		$body->setLabel("Message")
			->setRequired(true)
			->setFilters(array('StringTrim'));
		$id = new NW_Form_Element_Hidden("id");
		$submit = new NW_Form_Element_Submit("submit");
		$submit->setValue("Send Mail")
			->setAttrib('class', 'btn btn-success');
		
		$this->addElements(array($to,$cc, $bcc, $subject, $body, $id, $submit));
	}

	public function populate(array $values)
	{
		if(isset($values['_recipients']))
		{
			$recipients = array("to" => array(), "cc" => array(), "bcc" => array());
			
			foreach($values['_recipients'] as $rec)
			{
				switch ($rec->getSendas())
				{
				    case App_Model_Message_Recipient::SEND_AS_TO:
				    	$recipients["to"][] = $rec->getAddress();
				    	break;
			    	case App_Model_Message_Recipient::SEND_AS_CC:
			    		$recipients["cc"][] = $rec->getAddress();
			    		break;
		    		case App_Model_Message_Recipient::SEND_AS_BCC:
		    			$recipients["bcc"][] = $rec->getAddress();
		    			break;		
				}
			}
			$values["to"] = implode(",", $recipients["to"]);
			$values["cc"] = implode(",", $recipients["cc"]);
			$values["bcc"] = implode(",", $recipients["bcc"]);
		}
		
		$AdaptedValues['to'] = $values["to"];
		$AdaptedValues['cc'] = $values["cc"];
		$AdaptedValues['bcc'] = $values["bcc"];
		$AdaptedValues['body'] = utf8_encode($values["_body"]);
		$AdaptedValues['subject'] =$values["_subject"];
		$AdaptedValues['id'] = $values["_id"];
		
		parent::populate($AdaptedValues);
	}
	
	public function getValues($suppressArrayNotation = false)
	{
		$values = parent::getValues($suppressArrayNotation);
// 		$values["body"] = $values['body'];
// 		$values['subject'] = $values["subject"];
		return $values;
	}
	
	public function setReadOnly($elements=array())
	{
		if(empty($elements))
		{
			foreach($this->getElements() as $elemName)
				$elemName->setAttrib('disabled', 'disabled');
			return $this;
		}
		foreach($elements as $elementName)
			$this->getElement($elementName)->setAttrib('disabled', 'disabled');
		return $this;
	}

}


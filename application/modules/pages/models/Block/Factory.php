<?php
class Pages_Model_Block_Factory
{
	public static function getBlock($blocktemplate)
	{
		//$descriptor = NW_Pages_Block::getInstance()->block($blocktemplate);
		$item = new Pages_Model_Block();
		$item->setTemplate($blocktemplate)
				->setStatus(Pages_Model_Block::STATUS_DRAFT)
				->setContents(array());
		NW_Pages_Block::getInstance()->attachObservers($item);
		return $item;
	}
}
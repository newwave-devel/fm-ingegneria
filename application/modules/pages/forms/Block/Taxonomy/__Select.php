<?php
class Pages_Form_Block_Taxonomy_Single
{
	public function init() 
    {
        $this->setMethod('post')
                ->setAttrib('class', 'edit_form')
                ->setAction("/pages/taxonomy/save");

        $id = new NW_Form_Element_Hidden('id');
        $id->addValidator('Digits',false)
        		->setRequired(true);
        $taxonomyService = new Pages_Model_Taxonomy_Service();
              
        $taxonomy = new NW_Form_Element_Select('taxonomy');
        $taxonomy->setRequired(true)
        	->addMultiOption("", "Please Select One");
        
        foreach($taxonomyService->fetchAvailable() as $page)
        	$taxonomy->addMultiOption($page->getId(), $page->getLabel());
        
        $this->addElements(array( $id, $taxonomy));
    }
	
}
?>
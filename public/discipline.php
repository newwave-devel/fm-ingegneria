<?php
$pageId = 'discipline';
include "inc/required.php";
include "inc/common/contents.php";
?>
<?php include "inc/common/head.php" ?>
		
        
        <!-- SCRIPT -->
		<script type="text/javascript" src="common/js/core.js"></script>
        <script type="text/javascript" src="common/js/jquery.jcarousel.pack.js"></script>
        <script type="text/javascript" src="common/js/jquery.sharrre.min.js"></script>
        <script type="text/javascript" src="common/js/custom.js"></script>
        <script type="text/javascript" src="/common/js/jquery.nivo.slider.pack.js"></script>
        
        <script type="text/javascript">	
                jQuery(window).load(function() {
                    // START
					loading();
					people_carousel_sidebar();
					share();
                    slideshow();
					fancybox();
                });
        </script>
        
	</head>    
    
	<body lang="it">
    
    	<div id="loader"></div>
    	    	
        <?php include "inc/common/header.php"?>
        
        <section id="main_page">
        	<section class="container">
            
            	<section id="page_title">
            		<?php include "inc/common/breadcrumbs.php"?>
                    <h3><?php echo $mainBlock->_('title')?></h3>
                    
                   <?php include "inc/common/social_share.php"?>
                    
                </section><!--page_title-->

            	<?php include "inc/common/slider.php"?>
                
                <section id="main_data">
                	
                	<aside class="mainCol">
                		<?php if($mainBlock->_('description')): ?>
                    	<section id="discipline_text">
                            <p><?php echo $mainBlock->_('description')?></p>
                        </section><!--discipline_text-->
                        <?php endif; ?>
                        <?php include "inc/common/downloads.php"?>
                    </aside><!--mainCol-->
                    
                    <aside class="sidebar">
                    
                    	<?php include "inc/disciplines/people.php"; ?>
                    
                    </aside><!--sidebar-->
                
                </section><!--main_data-->
                                
                
                    
                    <?php include "inc/disciplines/main-col.php"?>             
                    
                
            </section><!--container-->
        </section><!--main_page-->
        
        <?php include "inc/common/footer.php"?>
		
	</body>
	
</html>
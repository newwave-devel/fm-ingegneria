<?php

class Zend_View_Helper_Nicesubstring extends Zend_View_Helper_Abstract {
    public $view;
    
    public function setView(Zend_View_Interface $view) {
    	$this->view = $view;
    }
    
    public function nicesubstring($string, $length, $replace = '...', $strip_tags=true) {
    	if(strlen($string) <= $length) return $string;
    	$length = $length - strlen($replace);
        
        if ($strip_tags) $string=strip_tags($string);
        $tok = strtok($string, " ");
       
        $count = 0;
        $result = "";

        while($tok !== false && $count <= $length)
        {
        	$result .= $tok." ";
        	$count += strlen($tok." ");
         	$tok = strtok(" ");
        }
        return utf8_encode($result.$replace);
    }

   
}

?>

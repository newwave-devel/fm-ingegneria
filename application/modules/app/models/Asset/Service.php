<?php
class App_Model_Asset_Service {
	protected $_mapper;
	
	public function getMapper() {
		return $this->_mapper;
	}
	
	public function __construct() {
		$this->_mapper = new App_Model_Asset_Mapper();
	}
	
	public function find($id)
	{
		try {
			$model = new App_Model_Asset();
			$this->_mapper->find($id, $model);
			return $model;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function fetchAll(NW_Pages_Paginator &$paginator, $paramsSearch=array(), $paramsOrder=array())
	{
		try {
			return $this->_mapper->fetchAll($paginator, $paramsSearch, $paramsOrder);
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function fetchParents(NW_Pages_Paginator &$paginator, $paramsSearch=array(), $paramsOrder=array())
	{
		try {
			$paramsSearch["pid"] = 0;
			return $this->_mapper->fetchAll($paginator, $paramsSearch, $paramsOrder);
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function fetchChilds(NW_Pages_Paginator &$paginator, $parentId, $paramsSearch=array(), $paramsOrder=array())
	{
		try {
			$paramsSearch["pid"] = $parentId;
			return $this->_mapper->fetchAll($paginator, $paramsSearch, $paramsOrder);
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function delete($id)
	{
		try {
			$model = $this->find($id);
			//try to delete item
			$path = $model->getAssetPath();
			if(NW_Media_Uploader::unlink($path))
				$this->_mapper->drop($id);
			return true;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function save(App_Model_Asset $asset)
	{
		try {
			return $this->_mapper->save($asset);
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	
	
	
	//image manipulation functions
	/**
	 * receives an asset, create, saves and return the new one.
	 * @param App_Model_Asset $asset
	 * @param unknown $width
	 * @param unknown $height
	 * @throws Exception
	 */
	public function autoResize(App_Model_Asset $asset, $width, $height)
	{
		try{
			if (extension_loaded('gmagick'))
			{				
				$image = new Gmagick();
				$image->readImage($asset->getAssetPath());
				$image->resizeImage($width, $height, null, 1, true);
				$resized = $this->_fork($asset, $width, $height);
				$image->writeImage($resized->getAssetPath());
				$image->destroy();
				$resized->setFilesize(App_Model_Asset_Service::getFilesizeReadable($resized->getAssetPath()));
				$id = $this->save($resized);
				$resized->setId($id);
				return $resized;
			} else {						
				$image = new Zebra_Image();
				$image->preserve_aspect_ratio = true;
				$image->source_path =$asset->getAssetPath();
				$resized = $this->_fork($asset, $width, $height);
				$image->target_path = $resized->getAssetPath();
				$image->resize($width, $height);
				$resized->setFilesize(App_Model_Asset_Service::getFilesizeReadable($resized->getAssetPath()));
				$id = $this->save($resized);
				$resized->setId($id);
				return $resized;
			}
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function cropAndFit(App_Model_Asset $asset,$x, $y, $x2, $y2,$w,$h)
	{
		try {
			if (extension_loaded('gmagick'))
			{
				$image = new Gmagick();
				$image->readImage($asset->getAssetPath());
				$resized = $this->_fork($asset, $w, $h);
				
				$cropW = $x2-$x;
				$cropH = $y2-$y;
				$image->cropimage($cropW, $cropH, $x, $y);
				$image->resizeImage($w, $h, null, 1, true);
				$image->writeImage($resized->getAssetPath());
				$image->destroy();
				$resized->setFilesize(App_Model_Asset_Service::getFilesizeReadable($resized->getAssetPath()));
				$id = $this->save($resized);
				$resized->setId($id);
				return $resized;
			} else {
			
				$image = new Zebra_Image();
				$image->source_path = $asset->getAssetPath();
				
				$parentId = $asset->getId();
				$cropped = $this->_fork($asset, $w, $h);
				$cropped->setId($parentId);
				$image->target_path = $cropped->getAssetPath();	
				
				$image->crop(intval($x), intval($y), intval($x2), intval($y2));
				//-- now i have the cropped one!
				$image2 = new Zebra_Image();
				$image2->source_path = $cropped->getAssetPath();
				$resized = $this->_fork($cropped, $w, $h);
				$image2->target_path = $resized->getAssetPath();
				$image2->resize($w, $h, ZEBRA_IMAGE_CROP_TOPLEFT);
				$resized->setFilesize(App_Model_Asset_Service::getFilesizeReadable($resized->getAssetPath()));
				$id = $this->save($resized);
				$resized->setId($id);
				return $resized; 
			}
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	private function _fork(App_Model_Asset $originalAsset, $width, $height)
	{
		$assetId = $originalAsset->getId(); //copy below is tricky: i need to save the id before!
		$newAsset = $originalAsset;
		$suffix = sprintf("_%sx%s", $width, $height);
		$newName = self::safeName($originalAsset->getUploadName(), $suffix);
		
		$newAssetPath = dirname($originalAsset->getAssetPath()).DIRECTORY_SEPARATOR.$newName;
		$newAssetUrl = dirname($originalAsset->getAssetUrl()).DIRECTORY_SEPARATOR.$newName;
		
		$newAsset->setAssetName($newName)
			->setId(null)
			->setAssetPath($newAssetPath)
			->setAssetUrl($newAssetUrl)
			->setAttribute("width", $width)
			->setAttribute("height", $height)
			->setPid($assetId);
		return $newAsset;
	}
	
// 	public function crop($assetId, $options = array())
// 	{
// 		try {
// 			$asset = $this->find($assetId);
// 			$image = new Zebra_Image();
// 			$image->source_path = $asset->getAssetPath();
// 			$parentName = $asset->getAssetName();
// 			$finalName = self::safeName($asset->getAssetName());
// 			$file_destname = rand();//;
			
// 			$destPath =  str_replace($parentName, $file_destname, $asset->getAssetPath());
// 			$destUrl =   str_replace($parentName, $file_destname, $asset->getAssetUrl());
// 			$image->target_path = $file_destname;
			
// 			if(!$image->crop($options['x'], $options['y'], $options['x2'], $options["y2"]))
// 				throw new Exception($image->error);

// 			$assetId = $asset->getId();
// 			$filesize = App_Model_Asset_Service::getFilesizeReadable( $asset->getAssetPath());
// 			$properties = getimagesize (  $asset->getAssetPath() );
				
// 			$asset->setId(null)
// 			->setAssetName($file_destname)
// 			->setAssetUrl($destUrl)
// 			->setAssetPath($destPath)
// 			->setPid($assetId)
// 			->setAttribute('size', $filesize)
// 			->setAttribute ( 'type', $properties [2] )
// 			->setAttribute ( 'wh', $properties [3] )
// 			->setAttribute ( 'width', $properties [0] )
// 			->setAttribute ( 'height', $properties [1] )
// 			->setAttribute( 'is_image', true);
// 			return $this->save($asset);
			
// 		} catch(Exception $e)
// 		{
// 			throw $e;
// 		}
// 	}
	
	/* @TODO: need to specify better the paths! */
// 	public function resize(App_Model_Asset $asset, $options = array())
// 	{
// 		try{
// 			$image = new Zebra_Image();
// 			$image->source_path = $asset->getAssetPath();
// 			$parentName = $asset->getAssetName();
// 			$suffix = sprintf("_%sx%s", $options['w'], $options['h']);
// 			$file_destname = self::safeName($asset->getAssetName(), $suffix);
// 			$options['m'] = isset($options['m']) ? $options['m'] : ZEBRA_IMAGE_CROP_TOPLEFT;
// 			//--- REFACTOR ASAP
// 			$destPath =  str_replace($parentName, $file_destname, $asset->getAssetPath());
// 			$destUrl =   str_replace($parentName, $file_destname, $asset->getAssetUrl());
// 			$image->target_path = $destPath;
			
// 			if(isset($options['preserve_aspect_ratio']))
// 				$image->preserve_aspect_ratio = $options['preserve_aspect_ratio'];
// 			if(isset($options['enlarge_smaller_images']))
// 				$image->enlarge_smaller_images = $options['enlarge_smaller_images'];
// 			if(isset($options['preserve_time']))
// 				$image->preserve_time = $options['preserve_time'];
			
// 			if (!$image->resize($options['w'], $options['h'], $options['m'])) 
// 				throw new Exception($image->error);
			
// 			$assetId = $asset->getId();
// 			$filesize = App_Model_Asset_Service::getFilesizeReadable($destPath);
// 			$properties = getimagesize ( $destPath );
			
// 			$asset->setId(null)
// 				->setAssetName($file_destname)
// 				->setAssetUrl($destUrl)
// 				->setAssetPath($destPath)
// 				->setPid($assetId)
// 				->setAttribute('size', $filesize)
// 				->setAttribute ( 'type', $properties [2] )
// 				->setAttribute ( 'wh', $properties [3] )
// 				->setAttribute ( 'width', $properties [0] )
// 				->setAttribute ( 'height', $properties [1] )
// 				->setAttribute( 'is_image', true);
// 			return $this->save($asset);
// 		} catch(Exception $e)
// 		{
// 			throw $e;
// 		}
// 	}
	
	public static function safeName($fName, $suffix='')
	{
		$pathinfo = pathinfo($fName);
            if (isset($pathinfo['extension']))
                return sprintf("%s%s.%s", md5(time() . $pathinfo['filename']), $suffix,$pathinfo['extension']);
            return sprintf("%s", md5(time() . $pathinfo['filename']));
	}
	/**
	 * gets the preview link used in admin
	 * @TODO: best fitted in an helper
	 */
	public static function preview(App_Model_Asset $media, $blockId = null, $blockName = null)
	{
		$tpl_preview = <<<TPL_PREVIEW
				
				<div class="picture">
                	<a class="fancybox.image preview" href="%s"><img class="img" src="/assets/res.php?src=%s&w=120&h=80&zc=2"></a>
                    <h5>%s</h5>
                    <h6>%s</h6>
                    <ul class="actions">
                    	<li><a class="edit_icon" onclick="window.main_page.subPage().modifyLibraryItem(this,%s,'%s')" href="#"><span class="icon_font">&#xf040;</span></a></li>
                        <li><a class="delete_icon" href="#" onclick="window.main_page.removeFile(this);return false;"><span class="icon_font">&#xf014;</span></a></li>
                    </ul>
					<input type="hidden" value="%s" name="contents[file][]" rel="%d"/>
                </div><!--picture-->
TPL_PREVIEW;
		
		$tpl_download = 
				'<div class="picture file">
                    <h5>%s</h5>
                    <h6>%s</h6>
                    <ul class="actions">
                    	<li><a class="edit_icon" href="%s"><span class="icon_font">&#xf019;</span></a></li>
                        <li><a class="delete_icon" onclick="window.main_page.removeFile(this);return false;"><span class="icon_font">&#xf014;</span></a></li>
                    </ul>
					<input type="hidden" value="%s" name="contents[file][]" rel="%d" />
                </div><!--file-->';
		
		$html = '';
		$path = $media->getAssetPath();
		$url = $media->getAssetUrl();
		
		if(NW_Media_Uploader::exists($path)) 
		{
			if(NW_Media_Uploader::isImage($path))
			{
				$html .= sprintf($tpl_preview, $url, $url,  $media->getUploadName(), $media->getFilesize(), $media->getId(), $blockName, $media->getId(),$media->getId());
			}
			else $html .= sprintf($tpl_download, $media->getUploadName(), $media->getFilesize(), $url, $media->getId(),$media->getId(),$media->getId());
		}
		return $html;
	}
	
	public static function getFilesizeReadable($file, $max = null, $system = 'si', $retstring = '%01.1f %s') {
		// Pick units
		if (!file_exists($file))
			return 0;
		$size = filesize($file);
		$systems['si']['prefix'] = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
		$systems['si']['size'] = 1000;
		$systems['bi']['prefix'] = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB');
		$systems['bi']['size'] = 1024;
		$sys = isset($systems[$system]) ? $systems[$system] : $systems['si'];
	
		// Max unit to display
		$depth = count($sys['prefix']) - 1;
		if ($max && false !== $d = array_search($max, $sys['prefix'])) {
			$depth = $d;
		}
	
		// Loop
		$i = 0;
		while ($size >= $sys['size'] && $i < $depth) {
			$size /= $sys['size'];
			$i++;
		}
	
		return sprintf($retstring, $size, $sys['prefix'][$i]);
	}

}
?>
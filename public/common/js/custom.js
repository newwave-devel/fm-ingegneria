function cookieLaw() {
	setCookie("cookieLaw", "cookieLaw", 30, true);
}

function setCookie(cname, cvalue, exdays, isRoot) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
	var strCookie = cname + "=" + cvalue + "; " + expires;	
	if(true){	strCookie += "; path=/";	}
    document.cookie = strCookie;
	//console.log("cookie set");
}

function cookieControl() {
	if (document.cookie.indexOf("cookieLaw=")<0) {	
		//console.log("non ho cookie");
		$("#privacySlider").delay(600).fadeIn(450);
	}
	
	else if (document.cookie.indexOf("cookieLaw=")>=0) {	
	//console.log("ho cookie");
	$("#privacySlider").fadeOut(450);
	}
}
  
 $(document).ready(function() {
	cookieControl();

	$('#cookieBtn').click(function() {
		cookieLaw();
		cookieControl();
		return false;
	});
});




  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-38724008-12', 'fm-ingegneria.com');
  ga('send', 'pageview');

//*****************************

//*************************
// LOADING
//*************************
function loading() {
	$('#loader').fadeOut(600);
}

// *************************
// SHARE IT
// *************************
function share() {
	$('#shareme').sharrre({
	share : {
		twitter : true,
		facebook : true,
		googlePlus : true
	},
	template : '<div class="box"><div class="left">Share</div><div class="middle"><a href="#" class="facebook">f</a><a href="#" class="twitter">t</a><a href="#" class="googleplus">+1</a></div><div class="right">{total}</div></div>',
	enableHover : false,
	enableTracking : true,
	//enableCounter: false,
	render : function(api, options) {
		$(api.element).on('click', '.twitter', function() {
			api.openPopup('twitter');
		});
		$(api.element).on('click', '.facebook', function() {
			api.openPopup('facebook');
		});
		$(api.element).on('click', '.googleplus',
				function() {
					api.openPopup('googlePlus');
				});
	}
	});

}

// ***********************************
// PEOPLE SIDEBAR CAROUSEL
// ***********************************

function people_carousel_sidebar() {
    var curPage = 1;
	function mycarousel_initCallback(carousel) {
		// Disable autoscrolling if the user clicks the prev or next button.
		carousel.buttonNext.bind('click', function() {
            if($("#chunkmax").val() > curPage)
            {
                curPage += 1;
                $("#carousel_pagination > strong").html(curPage);
			    carousel.startAuto(0);
            }
		});

		carousel.buttonPrev.bind('click', function() {
            if(curPage <= 1) return;
            curPage -= 1;
            $("#carousel_pagination > strong").html(curPage);
			carousel.startAuto(0);
		});

		// Pause autoscrolling if the user moves with the cursor over the clip.
		carousel.clip.hover(function() {
			carousel.stopAuto();
		}, function() {
			carousel.startAuto();
		});
	}
	;

	jQuery(document).ready(function() {
		if(jQuery('#key_people').length!=0) {
			jQuery('#key_people').jcarousel({
				vertical : false,
				auto : 0,
				scroll : 1,
				initCallback : mycarousel_initCallback
			});
			document.getElementById('key_people').style.visibility = 'visible';
		}
	});
	

}

// *************************
// DROPDOWN MENU
// *************************
$(function() {

	$("#main_menu ul li a").hover(function() {
		$(this).addClass("current");
		$('#main_menu_panel').addClass("current_mainmenu");
	});

	$("#main_menu ul li a#menu1_link").hover(function() {
		$("#main_menu ul li a").removeClass("current");
		$("#main_menu ul li a#menu1_link").addClass("current");
		$('.menu').removeClass("current");
		$('#menu1').addClass("current");
	});

	$("#main_menu ul li a#menu2_link").hover(function() {
		$("#main_menu ul li a").removeClass("current");
		$("#main_menu ul li a#menu2_link").addClass("current");
		$('.menu').removeClass("current");
		$('#menu2').addClass("current");
	});

	$("#main_menu ul li a#menu3_link").hover(function() {
		$("#main_menu ul li a").removeClass("current");
		$("#main_menu ul li a#menu3_link").addClass("current");
		$('.menu').removeClass("current");
		$('#menu3').addClass("current");
	});

	$("#main_menu ul li a#menu4_link").hover(function() {
		$("#main_menu ul li a").removeClass("current");
		$("#main_menu ul li a#menu4_link").addClass("current");
		$('.menu').removeClass("current");
		$('#menu4').addClass("current");
	});

	$("#main_menu_panel").mouseleave(function() {
		close_mainmennu_panel();
	});

	$("#menu_controller").hover(function() {
		close_mainmennu_panel();
	});

	$("#language_menu").hover(function() {
		close_mainmennu_panel();
	});

	$("#logo").hover(function() {
		close_mainmennu_panel();
	});

});

function close_mainmennu_panel() {
	$('#main_menu_panel').removeClass("current_mainmenu");
	$("#main_menu ul li a").removeClass("current");
	$('.menu').removeClass("current");
}

// *************************
// FANCYBOX
// *************************
function fancybox() {
	$("a.zoom").fancybox({
		padding : 10,
		maxWidth : '95%',
		maxHeight : '98%',
		fitToView : true,
		autoSize : true,
		closeClick : false,
		openEffect : 'none',
		closeEffect : 'none'
	});

	$("a.single_image").fancybox();
}

// *************************
// SLIDER HOME
// *************************
function slideshow() {
	var total = $('#slideshow img').length;
                    var rand = Math.floor(Math.random()*total);
	$('#slideshow').nivoSlider({
		startSlide:rand,
		effect : 'fade', // Specify sets like: 'fold,fade,sliceDown'
		slices : 7, // For slice animations
		animSpeed : 800, // Slide transition speed
		pauseTime : 5000, // How long each slide will show
		directionNav : true, // Next & Prev navigation
		controlNav : false, // 1,2,3... navigation
		controlNavThumbs : false, // Use thumbnails for Control Nav
		pauseOnHover : false
	// Stop animation while hovering
	});
}


// *************************
// PROJECTS ACCORDION
// *************************
function projects_accordion() {
$("#accordion .expanded").hide();
    $("a.opening").click(function(){
        $(this).next().slideToggle('fast', function(){
            $(this).prev("a.opening").toggleClass("active");
        });
    return false;
});
}

function pagination()
{
	jQuery(document).ready(function() {
		$(".pagination").click(function(){
			var gotoPage=$(this).attr("rel");
			$("#curPage").val(gotoPage);
			$("#frmController").submit();
			return false;
		});
	});
}

function ajaxForm(formId, target)
{
    jQuery(document).ready(function() {
        var options = {target: target, replaceTarget: true}
        $(formId).ajaxForm(options);
    });
}

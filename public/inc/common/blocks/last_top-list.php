<?php
if($blocks):
	$isBottom = isset($isBottom) ? $isBottom : false;
	require_once("inc/tpls/blocks/list/tpl.php");
	require_once("inc/functions/blocks.php");

    $counter = 3;
    $news = array();
    $projects = array();
    
	
	foreach($blocks as $sharedBlock)
	{
		$page = $sharedBlock->getPage();
		if($page->getTemplate() == 'news')
		{
			$text = renderSharedBlockNews($tpl_item_news, $sharedBlock, $blockService, false, false, $isBottom);
			$news[] = $text;
			
		}		
		else
		{
			$text = renderSharedBlockProject($tpl_item_project, $sharedBlock, $blockService, true, true, $isBottom);
			$projects[] = $text;
			
		}
	}
	?>
    <section id="home_articles">
        <?php for($i=0; $i< $counter; $i++): ?>
            <section class="row">
                <?php echo $news[$i]; ?>
                <?php echo $projects[$i]; ?>
            </section>
        <?php endfor; ?>
    </section>
<?php endif; ?>
function TranslationController(mainItemId)
{

    this.mainItemId = mainItemId;
    this.currentItemId = undefined;
}

TranslationController.prototype.translate = function()
{
    var link = 'app/i18n/edit/id/'+this.mainItemId;
    window.main_page.fancybox(link);
}

TranslationController.prototype.translateSibling = function(itemId)
{
    this.currentItemId = itemId;
    var link = 'app/i18n/edit/id/'+itemId;
    parent.window.main_page.fancybox(link);
}

TranslationController.prototype.updateValue = function()
{
    $.post("app/i18n/refreshitem",{paramId: this.mainItemId})
        .done(function(data)
        {
            result = jQuery.parseJSON(data);
            if(result.status=="ok")
            {
                content = result.payload.value;
                itemId =  result.payload.id;
                itemRef = '#value_'+itemId;

                $(itemRef).fadeOut(250).html(content).fadeIn(250);
                window.main_page.notify(dic.translation_updated_successfully);
            }
        })
        .fail(function() {window.main_page.notify(dic.translation_updated_successfully);})
}
<?php

class App_Model_Asset_Mapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('App_Model_DbTable_Asset');
        }
        return $this->_dbTable;
    }
    

    public function fetchAll(NW_Pages_Paginator &$paginator, $paramsSearch = array()) {
    	try {
			$table = $this->getDbTable ();
			$select = $table->select ();
			
			$this->_filterBySearchParam ( $select, $paramsSearch );
			if (! $paginator->getFetchAll ())
				$select->limit ( $paginator->itemsPerPage, $paginator->getStart () );
				
			if (! empty ( $paramsOrder )) {
				foreach ( $paramsOrder as $key => $dir ) {
					$sql = sprintf ( "%s %s", $key, strtoupper ( $dir ) );
					$select->order ( $sql );
				}
			}
			$entries = array ();
			foreach ( $table->fetchAll ( $select ) as $row ) {
				$model = new App_Model_Asset();
				$this->_fromDb ( $model, $row );
				$entries [] = $model;
			}
			$paginator->itemsCount = $this->count ( $select );
				
			return $entries;
		} catch ( Exception $e ) {
			throw $e;
		}
    }
    
    public function find($id, App_Model_Asset &$asset) {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $this->_fromDb($asset, $row);          
    }

    
    public function count(Zend_Db_Select $select) {
    	try {
    		$table = $this->getDbTable ();
    		$select->reset ( Zend_Db_Select::COLUMNS )->reset ( Zend_Db_Select::LIMIT_COUNT )->reset ( Zend_Db_Select::LIMIT_OFFSET )->columns ( array (
    				'num' => new Zend_Db_Expr ( 'COUNT(*)' )
    		) );
    		
    		return $table->fetchRow ( $select )->num;
    	} catch ( Exception $e ) {
    		throw $e;
    	}
    }

    public function save(App_Model_Asset $asset) {
        try {
            $data = $this->_toDb($asset);
            if ('' == ($id = $asset->getId())) {
                unset($data['id']);
                return $this->getDbTable()->insert($data);
            } else {
                $this->getDbTable()->update($data, array('id = ?' => $id));
            }
        } catch (Exception $e) {
            throw $e;
        }
    }


    public function drop($id) {
        try {
            $asset = new App_Model_Asset();
            $this->find($id, $asset);
           
            $where = $this->getDbTable()->getAdapter()->quoteInto("id=?", $id);
            $this->getDbTable()->delete($where);
            return true;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public static function _fromDb(App_Model_Asset &$obj, $map) {
        
        $obj->setAssetPath($map['asset_path'])
                ->setAssetName($map['asset_name'])
                ->setAssetUrl($map['asset_url'])
                ->setAttributes(json_decode($map['attributes']))
                ->setCreation_dt($map['creation_dt'])
                ->setId($map['id'])
                ->setUploadName($map['upload_name'])
                ->setPid($map['pid'])
                ->setType($map['type'])
                ->setFilesize($map['filesize'])
                ->setMime($map['mime'])
                ->setExtension($map['extension']);
    }

    public function _toDb(App_Model_Asset $obj) {
        $map = array(
            'asset_path' => $obj->getAssetPath(),
            'asset_url' =>  $obj->getAssetUrl(),
            'asset_name' =>  $obj->getAssetName(),
            'attributes' =>  json_encode($obj->getAttributes()),
            'creation_dt' =>  $obj->getCreation_dt(),
            'upload_name' =>  $obj->getUploadName(),
            'id' =>  $obj->getId(),
            'pid' =>  $obj->getPid(),
            'type' =>  $obj->getType(),
        	'mime' => $obj->getMime(),
        	'extension' => $obj->getExtension(),
            'filesize' =>  $obj->getFilesize()
         );
        return $map;
    }
    
    private function _filterBySearchParam(Zend_Db_Table_Select &$select, $paramsSearch = array()) {
    	if (!empty($paramsSearch)) {
    
    		if (!empty($paramsSearch['ids']))
    			$select->where("id IN (?)", $paramsSearch['ids']);
    		if (!empty($paramsSearch['type']))
    		{
    			if(is_array($paramsSearch['type']))
    				$select->where("type IN ?", $paramsSearch['type']);
    			else $select->where("type = ?", $paramsSearch['type']);
    		}
    		if (!empty($paramsSearch['asset_name']))
    			$select->where("asset_name = ?", $paramsSearch['asset_name']);
    		if (isset($paramsSearch['pid']))
    		{
    			$select->where("pid = ?", $paramsSearch['pid']);
    		}
    		if (!empty($paramsSearch['id']))
    		{
    			if(is_array($paramsSearch['id']))
    				$select->where("id IN ?", $paramsSearch['id']);
    			else $select->where("id = ?", $paramsSearch['id']);
    		}
    		if (!empty($paramsSearch['extension']))
    		{
    			if(is_array($paramsSearch['extension']))
    				$select->where("extension IN ?", $paramsSearch['extension']);
    			else $select->where("extension = ?", $paramsSearch['extension']);
    		}
    	}
    	return $this;
    }

}

?>

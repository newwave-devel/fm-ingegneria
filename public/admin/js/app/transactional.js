function transactional(model, itemId)
{
	this.model = model;
	this.itemId = itemId;
}

transactional.prototype.requeue = function(messageId)
{
	var model = this.model;
	var itemId = this.itemId;
	$.post("/app/mailbox/requeue/", {paramId: messageId})
		.done(function(data)
			{
				var response = JSON.parse(data);
				if(response.status=='ok')
				{
					$.get("/app/transactional/sent/",{model:model, item:itemId})
						.done(function(data) { $("#sent").fadeOut().html(data).fadeIn();})
						.fail(function() { alert("There has been some errors processing the request"); });
				}
				else alert("There has been some errors processing the request");	
		})
		.fail(function(){alert("There has been some errors processing the request");});
}
transactional.prototype.updateMail = function()
{
	$.fancybox.close();
	$.get("/app/transactional/sent/",{model: this.model, item:this.itemId})
		.done(function(data) { $("#sent").fadeOut().html(data).fadeIn();})
		.fail(function() { alert("There has been some errors processing the request"); });
}

transactional.prototype.dropIncoming = function(messageId)
{
	var model = this.model;
	var itemId = this.itemId;
	$.post("/app/mailbox/delete/", {paramId: messageId})
		.done(function(data){
			var response = JSON.parse(data);
			if(response.status=='ok')
			{
				$.get("/app/transactional/incoming/",{model:model, item:itemId})
					.done(function(data) { $("#incoming").fadeOut().html(data).fadeIn();})
					.fail(function() { alert("There has been some errors processing the request"); });
			}
			else alert("There has been some errors processing the request");
		})
    	.fail(function(){alert("There has been some errors processing the request");});
}

transactional.prototype.dropSent = function(messageId)
{
	var model = this.model;
	var itemId = this.itemId;
	$.post("/app/mailbox/delete/", {paramId: messageId})
		.done(function(data){
			var response = JSON.parse(data);
			if(response.status=='ok')
			{
				$.get("/app/transactional/sent/",{model:model, item:itemId})
					.done(function(data) { $("#sent").fadeOut().html(data).fadeIn();})
					.fail(function() { alert("There has been some errors processing the request"); });
			}
			else alert("There has been some errors processing the request");
		})
    	.fail(function(){alert("There has been some errors processing the request");});
}

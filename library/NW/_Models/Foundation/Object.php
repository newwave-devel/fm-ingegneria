<?php

abstract class NW_Models_Foundation_Object {

    /**
     *
     * Associative array representing the internal status of the object.
     * @var array
     */
    protected $_map;

    /**
     *
     * Associative array representing relations of the object.
     * @var unknown_type
     */
    protected $_related;

    /**
     *
     * Lang in which content is stored.
     * @var char(2)
     */
    protected $lang;

    /**
     *
     * Owner's Id
     * @var int
     */
    protected $owner;

    /**
     *
     * Item id
     * @var int
     */
    protected $id;

    /**
     *
     * Object status.
     * Avaialable values are 'active', 'draft', 'revision', 'deleted'
     * @var string
     */
    protected $status;

    /**
     *
     * Creation Timestamp (string, formatted for mysql insertion)
     * @var string
     */
    protected $ts_insert;

    /**
     *
     * Update Timestamp (string, formatted for mysql insertion)
     * @var string
     */
    protected $ts_update;

    /**
     *
     * Deleted Timestamp (string, formatted for mysql insertion)
     * @var string
     */
    protected $ts_delete;

    const STATUS_DRAFT = 'draft';
    const STATUS_REVISION = 'revision';
    const STATUS_ACTIVE = 'active';
    const STATUS_DELETED = 'deleted';
    const SOURCE_DB = "db";
    const SOURCE_REQUEST = 'request';
    const SOURCE_MAP = "map";
    const SOURCE_FORM = "form";
    const SOURCE_JSON = "json";

    protected function __construct(NW_ClassURI $classUri) {
        try {
            static::$_classUri = $classUri;
            $zendName = $classUri->projectToZendClassName();
            $this->_map = NW_Application::getSingleton('app/NW_ModuleManager')->getMapFor($classUri);
            $this->_related = NW_Application::getSingleton('app/NW_ModuleManager')->getRelationsFor($classUri);
            $this->lang = 'it';
            $this->owner = '1';
            $this->id = '-1';
            $this->status = NW_Application::getSingleton('app/NW_ModuleManager')->getModuleDescriptor($classUri)->getClassDefaultStatus($zendName);
            $this->ts_insert = date("Y-m-d h:i:s");
            $this->ts_update = date("Y-m-d h:i:s", 0);
            $this->ts_delete = date("Y-m-d h:i:s", 0);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function get($item) {
        if (array_key_exists($item, $this->_map))
            return $this->_map[$item];
    }

    public function set($item, $value) {
        if (array_key_exists($item, $this->_map))
            $this->_map[$item] = $value;
        return $this;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getId() {
        return $this->id;
    }

    public function setLang($lang) {
        $this->lang = $lang;
        return $this;
    }

    public function getLang() {
        return $this->lang;
    }

    public function setOwner($owner) {
        $this->owner = $owner;
        return $this;
    }

    public function getOwner() {
        return $this->owner;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setTsInsert($ts) {
        $this->ts_insert = $ts;
        return $this;
    }

    public function getTsInsert() {
        return $this->ts_insert;
    }

    public function setTsUpdate($ts) {
        $this->ts_update = $ts;
        return $this;
    }

    public function getTsUpdate() {
        return $this->ts_update;
    }

    public function setTsDelete($ts) {
        $this->ts_delete = $ts;
        return $this;
    }

    public function getTsDelete() {
        return $this->ts_delete;
    }

    protected function setRelationValue($relationName, $fields) {
        if (isset($this->_related[$relationName])) {
            foreach ($fields as $row) {
                $id = $row['id'];
                unset($row['id']);
                $this->_related[$relationName][$id] = $row;
            }
        }
        return $this;
    }

    public function export($format) {
        switch ($format) {
            case self::SOURCE_JSON:
                if (function_exists('json_encode'))
                    return json_encode($this);
                break;
            case self::SOURCE_MAP:
                 $map = $this->_map;
                foreach (get_object_vars($this) as $k => $v)
                {
                    if($k == '_map') continue;
                    $map[$k] = $this->$k;
                    
                }
                return $map;
                break;
            case self::SOURCE_DB:
                $map = $this->_map;
                array_walk($map, 'addslashes');
                foreach (get_object_vars($this) as $k => $v)
                    $map[$k] = addslashes ($v);
                return $map;
                break;
            case 'string':
                return serialize($this);
                break;
        }
    }

    public function import($source, $param) {
        
        switch ($source) {
            case self::SOURCE_REQUEST:
                if (get_magic_quotes_gpc()) {
                    foreach ($this->_map as $k => $v) {
                        if (filter_has_var($requestType, $k)) {
                            $this->_map[$k] = filter_input($requestType, $k);
                        }
                    }
                } else {
                    foreach ($this->_map as $k => $v) {
                        if (filter_has_var($requestType, $k)) {
                            $v = addslashes(filter_input($requestType, $k));
                            $this->_map[$k] = $v;
                        }
                    }
                }
                if (filter_has_var($requestType, 'id'))
                    $this->id = filter_input($requestType, 'id');
                if (filter_has_var($requestType, 'owner'))
                    $this->owner = filter_input($requestType, 'owner');
                break;
            case self::SOURCE_DB:
                foreach ($param as $prop => $val)
                    $this->set($prop, stripslashes($val));

                if ($this->_hasTranslatedFields())
                    $this->setLang($param['lang']);

                $param['status'] = (isset($param['status'])) ? $param['status'] : 'active';
                $this->setOwner($param['owner'])
                        ->setStatus($param['status'])
                        ->setId($param['id']);

                if (isset($param['ts_insert']))
                    $this->setTsInsert($param['ts_insert']);
                if (isset($param['ts_update']))
                    $this->setTsUpdate($param['ts_update']);
                if (isset($param['ts_delete']))
                    $this->setTsDelete($param['ts_delete']);
                break;
            case self::SOURCE_MAP:
                foreach ($param as $prop => $val)
                    $this->set($prop, $val);

                if ($this->_hasTranslatedFields())
                    $this->setLang($map['lang']);

                $param['status'] = (isset($param['status'])) ? $param['status'] : 'active';
                $this->setOwner($param['owner'])
                        ->setStatus($param['status'])
                        ->setId($param['id']);

                if (isset($param['ts_insert']))
                    $this->setTsInsert($param['ts_insert']);
                if (isset($param['ts_update']))
                    $this->setTsUpdate($param['ts_update']);
                if (isset($param['ts_delete']))
                    $this->setTsDelete($param['ts_delete']);
            case self::SOURCE_FORM:
                break;
        }
       
        return $this;
    }

    static public function getClassName() {
        return __CLASS__;
    }

    static public function getClassUri() {
        if (static::$_classUri == null)
            static::$_classUri = NW_Helpers_Application::getClassUri(static::CLASS_MODULE, __CLASS__);
        return static::$_classUri;
    }

    private function _hasTranslatedFields() {
        return NW_Application::getSingleton('app/NW_ModuleManager')
                        ->getModuleDescriptor($this->getClassUri())
                        ->classHasTranslatedFields($this->getClassUri()->projectToZendClassName());
    }

}
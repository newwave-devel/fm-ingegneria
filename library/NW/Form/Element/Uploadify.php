<?php

class NW_Form_Element_Uploadify extends Zend_Form_Element_Button {

    public function __construct($spec, $options = array()) {
        $uploadify_options = isset($options['uploadifyOptions']) ? $options['uploadifyOptions'] : array();
        $default_options = isset($options['options']) ? $options['options'] : array();

        parent::__construct($spec, $default_options);

        $this->_initUploadifyOptions($uploadify_options);
        $this->setDecorators(array('ViewHelper'));
    }

    private function _initUploadifyOptions($uploadifyOptions) {

        $itemId = sprintf("uplodify_%s", $this->getAttrib('id'));
        $this->getView()->jQuery()->addOnload(sprintf("var %s = NW_Uploadify.add('%s');", $itemId, $this->getAttrib('id')));
        $options = array();
        $bindParams = array();
        $lateParams = array();

        foreach ($uploadifyOptions as $k => $v) {
            switch ($k) {
                case 'formData':
                    if (!isset($options['formData']))
                        $options['formData'] = array();
                    foreach ($v as $param => $value)
                        $options['formData'][$param] = $value;
                    break;
                case 'lateParam':
                    foreach ($v as $param => $value) {
                        $value = (is_string($value)) ? "'$value'" : $value;
                        $value = (is_bool($value) && !$value) ? 0 : $value;
                        $lateParams[] = sprintf("lateParam('%s', %s)", $param, $value);
                    }
                    break;
                case 'bindFormField':
                    $v = sprintf("'%s'", implode("','", $v));
                    $bindParams[] = sprintf("bindFields(%s)", $v);
                    break;
                case 'onUploadSuccess':
                    $code = sprintf('function(file, data, response) { %s }', $v);
                    $bindParams[] = sprintf("setOption('%s', %s)", $k, $code);
                    break;
                default:
                    $v = (is_string($v)) ? "'$v'" : $v;
                    $v = (is_bool($v) && !$v) ? 0 : $v;
                    $bindParams[] = sprintf("setOption('%s', %s)", $k, $v);
                    break;
            }
        }
        $encoded_options = empty($options) ? '' : json_encode($options);
        $bindFunctions = (empty($bindParams)) ? '' : implode(".", $bindParams);
        $this->getView()->jQuery()->addOnload(sprintf("%s.%s.uploadifyItem(%s);", $itemId, $bindFunctions, $encoded_options));
        if (!empty($lateParams))
            $this->getView()->jQuery()->addOnload(sprintf("%s.%s;", $itemId, implode(".", $lateParams)));
    }

}

?>

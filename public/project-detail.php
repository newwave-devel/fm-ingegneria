<?php
$pageId = 'project-detail';
include "inc/required.php";
include "inc/common/contents.php";
?>

<?php include "inc/common/head.php" ?>
        
        <!-- SCRIPT -->
		<script type="text/javascript" src="/common/js/core.js"></script>
        <script type="text/javascript" src="/common/js/jquery.jcarousel.pack.js"></script>
        <script type="text/javascript" src="/common/js/jquery.sharrre.min.js"></script>
        <script type="text/javascript" src="/common/js/custom.js"></script>
        
        <script type="text/javascript">	
                jQuery(window).load(function() {
                    // START
					loading();
					people_carousel_sidebar();
					share();
					fancybox();
                });
        </script>
        
	</head>    
    
	<body lang="it">
    
    	<div id="loader"></div>
    	    	
        <?php include "inc/common/header.php"?>
        
        <section id="main_page">
        	<section class="container">
            
            	<section id="page_title">

                    <h3><?php echo $mainBlock->_("title")?></h3>
                    
                    <?php include "inc/common/social_share.php"?>
                    
                </section><!--page_title-->
            
            	<?php include "inc/common/main-slider-no-text.php"?>
                
                <section id="main_data">
                	
                    <?php include "inc/projects/datas.php" ?>
                
                	<aside class="mainCol">
                    	<section id="discipline_text">
                            <h3><?php $translator->{"Dettaglio Progetto"}->__("Descrizione") ?></h3>
                            <p><?php echo $mainBlock->_("description")?></p>
                        </section><!--discipline_text-->
                        
                       <?php 
                       	include "inc/common/downloads.php"; 
                       ?>
                    </aside><!--mainCol-->
                    
                    <aside class="sidebar">
                    
                    	<?php include "inc/projects/people.php"; ?>
                    
                    </aside><!--sidebar-->
                
                </section><!--main_data-->
                                
                <section id="home_articles">
                	
                 <?php include "inc/projects/gallery.php"?>

                   <?php include "inc/projects/video.php" ?>
                    
                    <?php include "inc/projects/tags.php"?>
                    
                </section><!--home_articles-->
                
            </section><!--container-->
        </section><!--main_page-->
        
        <?php include "inc/common/footer.php"?>
		
	</body>
	
</html>
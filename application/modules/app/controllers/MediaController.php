<?php
class App_MediaController extends NW_Controller_Action {
	
	private $_assetpath = '/assets';
	
	public function init() {
		$this->_translateAdapter = Zend_Registry::get('Zend_Translate');
	}
	
	public function uploadAction() {
		try 
		{
			$datas = array();
			$this->_setJsonAction();

			$adapter = new Zend_File_Transfer_Adapter_Http ();
			$destination_dir = PUBLIC_PATH . $this->_assetpath;
			
			if (! file_exists ( $destination_dir ))
				mkdir ( $destination_dir );
			$targetBlockID = '';
			if($this->_hasParam("blockId"))
				$targetBlockID = str_replace("#", "", $this->_getParam("blockId"));
			$adapter->setDestination ( $destination_dir )
						->addValidator('ExcludeExtension', false, array('php', 'exe'));
			$assetService = new App_Model_Asset_Service ();
			$width ='';
			$height = '';
			$method = '';
			//collect width & height
			if($this->_hasParam("width"))
				$width = $this->_getParam("width");
			if($this->_hasParam("height"))
				$height = $this->_getParam("height");
			if($this->_hasParam("method"))
				$method = $this->_getParam("method");
			
			foreach ($adapter->getFileInfo () as $file => $info ) 
			{
				$name = $this->getRequest()->getParam('ax-file-name');
				$safeName = App_Model_Asset_Service::safeName ( $name );	
				$fullPath = $destination_dir . DIRECTORY_SEPARATOR . $safeName;
				$adapter->addFilter ( 'rename', $fullPath );			
				
				if (! $adapter->isUploaded ( $file )) throw new Exception("File has not been uploaded");
				
				if (! $adapter->isValid ( $file )) 
				{
					$error = implode(PHP_EOL, $adapter->getErrors());
					throw new Exception($error);
				}
					
				if (! $adapter->receive ( $file )) throw new Exception ( "Error receiving file"); 
				
				$asset = new App_Model_Asset ();
				$asset->setAssetName ( $safeName )
						->setAssetPath ( $fullPath )
						->setAssetUrl ( $this->_assetpath . DIRECTORY_SEPARATOR . $safeName )
						->setUploadName( basename($name))
						->setMime($adapter->getMimeType($file) )
						->setFilesize($adapter->getFileSize ( $file ))
						->setAttribute ( 'mime', $adapter->getMimeType($file))
						->setAttribute( 'extension', pathinfo( $fullPath, PATHINFO_EXTENSION))
						->setAttribute( 'is_image', false)
						->setExtension(pathinfo( $fullPath, PATHINFO_EXTENSION))
						->setType(App_Model_Asset::MEDIA_TYPE_DOC);
				//if is image i get some more info
				$img_attributes = getimagesize ( $fullPath );
				if ($img_attributes) 
				{
					$asset->setAttribute ( 'type', $img_attributes [2] )
						->setAttribute ( 'wh', $img_attributes [3] )
						->setAttribute ( 'width', $img_attributes [0] )
						->setAttribute ( 'height', $img_attributes [1] )
						->setAttribute( 'is_image', true)
						->setType(App_Model_Asset::MEDIA_TYPE_IMAGE);
				}	
				//and, whatever it is, i save it
				$id = $assetService->save ( $asset );
				$asset->setId($id);
				//now, if it is an image, and i have info on it, i create the copy and i return this
				if($img_attributes && ($width != '') && ($height != '') )
					$asset = $assetService->autoResize($asset, $width, $height, $method);//$this->__autoResizes($asset, $width, $height,$method);
			}
			$outcome =  new NW_View_RetCode(NW_View_RetCode::STATUS_SUCCESS,$this->_assetPayload($asset, $targetBlockID)); 
			$this->view->output = json_encode ( $outcome );
			
		} catch ( Exception $e ) {
			$outcome =  new NW_View_RetCode(NW_View_RetCode::STATUS_ERROR, $e->getMessage()); 
			$this->view->output = json_encode ( $outcome );
		}
	}
	
	public function deleteAction() {
		try{
			$this->_setJsonAction();
			
			$service = new App_Model_Asset_Service();
			$assetId = $this->_request->getParam ( 'paramId' );
			if($service->delete($assetId))
				$this->view->output = NW_View_RetCode::success();
			else $this->view->output = NW_View_RetCode::error("There has been errors deleting the resource");
		}catch(Exception $e)
		{
			$this->view->output = NW_View_RetCode::error($e->getMessage());
		}
	}
	
	public function addtolibraryAction()
	{
		try{
			$this->_setIframeAction();
			if(!$this->_hasParam('block')) throw new Exception("No block id received");			
			if(!$this->_hasParam('blockId')) throw new Exception("No blockId id received");
			
			$blockTemplate = $this->_getParam('block');
			$blockId = $this->_getParam('blockId');
			
			$field = 'file';
			$block = $this->_getParam("block");
			$blockId = $this->_getParam("blockId");
			$form = new App_Form_Asset(array("block" => $block, "field" => $field, "blockId" => $blockId));
			$this->view->form = $form;
			$this->_helper->switchContextByUser();
			
		}catch(Exception $e)
		{
			$this->view->output = NW_View_RetCode::error($e->getMessage());
		}
	}
	
	public function uploaditemsAction()
	{
		try{
			$this->_setIframeAction();	
			$this->_helper->switchContextByUser();
			$form = new App_Form_Asset();
			$this->view->form = $form;	
		}catch(Exception $e)
		{
			$this->view->output = NW_View_RetCode::error($e->getMessage());
		}
	}
	
	public function editAction()
	{
		try{
			$this->_setIframeAction();
			$this->_helper->switchContextByUser();
			if(!$this->_hasParam('id')) throw new Exception("No id");
			if(!$this->_hasParam('blockName')) throw new Exception("No blockName");
			$id = $this->_getParam("id");
			$blockTemplate = $this->_getParam("blockName");
			
			$assetService = new App_Model_Asset_Service ();
			$currentAsset =$assetService->find($id);
			$this->view->pid = $id;
			if($currentAsset->getPid() != 0) 
			{
				$paginator = NW_Pages_Paginator::fetchAll();
				$this->view->entries = $assetService->fetchChilds($paginator, $currentAsset->getPid());
				$this->view->pid = $currentAsset->getPid();
				$this->view->editLink = '/app/media/fork/pid/'.$currentAsset->getPid();
			} else {
				$paginator = NW_Pages_Paginator::fetchAll();
				$this->view->entries = $assetService->fetchChilds($paginator, $currentAsset->getId());
				$this->view->pid = $currentAsset->getPid();
				$this->view->editLink = '/app/media/fork/pid/'.$currentAsset->getId();
			}
			
			$this->view->blockName = $blockTemplate;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function fromlibraryAction()
	{
		try{
			$this->_setJsonAction();
			
			if(!$this->_hasParam('id')) throw new Exception("No id");
			//if(!$this->_hasParam('blockId')) throw new Exception("No blockId");
			$mediaService = new App_Model_Asset_Service();
			$asset = $mediaService->find($this->_getParam("id"));
			$outcome =  new NW_View_RetCode(NW_View_RetCode::STATUS_SUCCESS,$this->_assetPayload($asset));
			$this->view->output = json_encode ( $outcome );
		}catch(Exception $e)
		{
			$this->view->output = NW_View_RetCode::error($e->getMessage());
		}
	}
	
	public function libraryAction()
	{
		try{
			$this->_setIframeAction();
			$this->_helper->switchContextByUser();
			if(!$this->_hasParam('blockId')) throw new Exception("No blockId");
			$blockId = $this->_getParam('blockId');
			//$this->_addJsSource("/admin/js/app/library.js")
			$this->_addOnload("library = new Library('$blockId');");
			$assetService = new App_Model_Asset_Service ();
			$paginator = new NW_Pages_Paginator();
            $paginator->setItemsPerPage(20);
			$this->view->entries = $assetService->fetchParents($paginator, array("type" => App_Model_Asset::MEDIA_TYPE_DOC));
		}catch(Exception $e)
		{$this->view->entries = $assetService->fetchParents($paginator);	
			$this->view->output = NW_View_RetCode::error($e->getMessage());
		}
	}
	
	public function galleryAction()
	{
		try{
			$this->_setIframeAction()
                ->_addJsSource('/admin/js/NW.js')
                ->_addJsSource('/admin/js/NW/pagination.js');
			$this->_helper->switchContextByUser();
			if(!$this->_hasParam('blockId')) throw new Exception("No blockId");
			if(!$this->_hasParam('block')) throw new Exception("No block template");
			$blockId = $this->_getParam('blockId');
			//$blockTemplate = $this->_getParam('block');
			$assetService = new App_Model_Asset_Service ();
            $paginator = NW_Pages_Paginator::__fromRequest ( $this->_request );
            $ipp = $this->_getParam("ipp", 18);
            $paginator->setItemsPerPage($ipp)
                    ->setIsAjax(false);
            $this->view->paginationController = 'frm_pagination';
			$this->view->entries = $assetService->fetchParents($paginator, array("type" => App_Model_Asset::MEDIA_TYPE_IMAGE));
            $this->view->paginator = $paginator->prepare ();
            $this->view->blockId = $blockId;
            //$this->view->blockTemplate = $blockTemplate;
		}catch(Exception $e)
		{
			$this->view->output = NW_View_RetCode::error($e->getMessage());
		}
	}
	
	public function appgalleryAction()
	{
		try{
			$this->_setAjaxAction();
			$assetService = new App_Model_Asset_Service ();
            $paginator = NW_Pages_Paginator::__fromRequest ( $this->_request );
            $ipp = $this->_getParam("ipp", 50);
            $paginator->setItemsPerPage($ipp)
                ->setIsAjax(true, '#main_content');
			$this->view->entries = $assetService->fetchParents($paginator, array("type" => App_Model_Asset::MEDIA_TYPE_IMAGE));
            $this->view->paginationController = 'frm_pagination';
            $this->view->paginator = $paginator->prepare ();
		}catch(Exception $e)
		{
			$this->view->output = NW_View_RetCode::error($e->getMessage());
		}
	}
	
	public function applibraryAction()
	{
		try{
			$this->_setAjaxAction();
			$assetService = new App_Model_Asset_Service ();
			$paginator = NW_Pages_Paginator::fetchAll();
			$this->view->entries = $assetService->fetchParents($paginator, array("type" => App_Model_Asset::MEDIA_TYPE_DOC));
		}catch(Exception $e)
		{
			$this->view->output = NW_View_RetCode::error($e->getMessage());
		}
	}
	
	public function forkAction()
	{
		try {
			$this->_setIframeAction();
			$this->_helper->switchContextByUser();
			if(!$this->_hasParam('pid')) throw new Exception("No id");
			//if(!$this->_hasParam('blockId')) throw new Exception("No blockId");
			if(!$this->_hasParam('blockName')) throw new Exception("No blockName");
			$pid = $this->_getParam("pid");
			//$blockId = $this->_getParam("blockId");
			$blockName = $this->_getParam("blockName");
			$params = NW_Pages_Block::getInstance()->field($blockName, "file")->params->data;
			
			$assetService = new App_Model_Asset_Service ();
			//$this->view->blockId = $blockId;
			$asset = $assetService->find($pid);
			
			//setting view property
			$width = $asset->getAttribute("width");
			$height = $asset->getAttribute("height");
			$resize = false;
			$whratio = floatval($width/$height);
			$scaledown = 1;
			if($width > 800 || $height > 800)
			{
				$resize = true;
				//must resize
				if($width > 800)
				{
					$width = 800;
					$height = $width *(1/$whratio);
				}
				if($height > 800)
				{
					$height = 800;
					$width = $height/$whratio;
				}
				$scaledown = floatval($asset->getAttribute("width")/$width );
			}
			//setting view property
			$requestWidth = (string) $params->width;
			$requestHeight = (string) $params->height;
			$aspectRatio = floatval($requestWidth/$requestHeight);
			
			$this->view->entry = $asset;
			$this->view->width = $width;
			$this->view->height = $height;
			$this->view->resize = $resize;
			$this->view->scaledown = $scaledown;
			$this->view->whratio = $aspectRatio;//$whratio;
			$this->view->requestWidth = $requestWidth;
			$this->view->requestHeight = $requestHeight;
			
			$this->_addJsSource('/admin/js/vendors/deepliquid/jcrop/js/jquery.Jcrop.min.js')
				->_addOnload("$('.coords').ajaxForm({success: Library.crop()})");
			
		}catch(Exception $e)
		{
			$this->view->output = NW_View_RetCode::error($e->getMessage());
		}
	}
	
	public function resizeAction()
	{
		try{
			$this->_setJsonAction();
			$this->_helper->switchContextByUser();
			if(!$this->_hasParam("id")) throw new Exception("no file id found");
			if(!$this->_hasParam("w")) throw new Exception("no width specified");
			if(!$this->_hasParam("h")) throw new Exception("no height specified");
			$id = $this->_getParam("id");
			$width = $this->_getParam("w");
			$height = $this->_getParam("h");
			$assetService = new App_Model_Asset_Service ();
			$asset = $assetService->find($id);
			$id = $assetService->resize($asset, array("w" => $width, "h" => $height, "m" => $method));
			
			$newAsset = $this->_createCopy($asset, $width, $height, ZEBRA_IMAGE_CROP_TOPLEFT);//$asset = $assetService->find($id);
			$datas[] = $output;
			$outcome =  new NW_View_RetCode(NW_View_RetCode::STATUS_SUCCESS,$this->_assetPayload($newAsset, $id));
			$this->view->output = $outcome;
		}catch(Exception $e)
		{
			$this->view->output = NW_View_RetCode::error($e->getMessage());
		}
	}
	
	public function cropAction()
	{
		try{
			$this->_setJsonAction();
			$this->_helper->switchContextByUser();
			if(!$this->_hasParam("id")) throw new Exception("no file id found");
			if(!$this->_hasParam("x")) throw new Exception("no x specified");
			if(!$this->_hasParam("y")) throw new Exception("no y specified");
			if(!$this->_hasParam("w")) throw new Exception("no w specified");
			if(!$this->_hasParam("h")) throw new Exception("no h specified");
			if(!$this->_hasParam("x2")) throw new Exception("no x2 specified");
			if(!$this->_hasParam("y2")) throw new Exception("no y2 specified");
			//if(!$this->_hasParam("scaledown")) throw new Exception("no scaledown specified");
			//if(!$this->_hasParam("blockId")) throw new Exception("no blockid specified");
			$whAspectRatio = floatval($this->_getParam("w") / $this->_getParam("h"));
			$id = $this->_getParam("id");
			
			$blockId = $this->_getParam("blockId");
			
			$x = $this->_getParam("x");
			$y = $this->_getParam("y");
			$x2 = $this->_getParam("x2");
			$y2 = $this->_getParam("y2");
			$w = $this->_getParam("w");
			$h = $this->_getParam("h");
			
			$assetService = new App_Model_Asset_Service ();
			$asset = $assetService->find($id);
			$newAsset = $assetService->cropAndFit($asset, $x, $y, $x2, $y2,$w,$h);
			$outcome =  new NW_View_RetCode(NW_View_RetCode::STATUS_SUCCESS,$this->_assetPayload($newAsset, $blockId));
			$this->view->output = $outcome;
		}catch(Exception $e)
		{
			$this->view->output = NW_View_RetCode::error($e->getMessage());
		}
	}
	private function _createCopy($asset, $width, $height,$method=ZEBRA_IMAGE_CROP_CENTER)
	{
		try{
			$assetService = new App_Model_Asset_Service ();
			$id = $assetService->resize($asset, array("w" => $width, "h" => $height, "m" => $method));
			return $assetService->find($id);
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	private function _assetPayload(App_Model_Asset $asset, $blockId=false)
	{
		$output = new stdClass ();
		$output->outcome = 'success';
		$output->name = basename($asset->getUploadName());
		$output->size = $asset->getFilesize();
		$output->type = $asset->getMime();
		$output->delete_url = '/app/media/delete/id/' . $asset->getId();
		$output->url = $asset->getAssetUrl ();
		$output->is_image = $asset->getAttribute('is_image');
		$output->extension = $asset->getExtension();
		$output->id = $asset->getId();
		$output->parentId = $asset->getPid();
		if($blockId)
			$output->blockId = $blockId;
		return $output;
	}
}
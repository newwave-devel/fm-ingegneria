<?php if($partners): ?>
<ul>
	<?php 
		$people_partners_count =0;
		foreach($partners as $pp):
			$pblock = $pp->getBlock();
			$role = $pblock->_("role");
			$name = $pblock->_("title");
			$img = $pblock->_("file");
			$src = $blockService->asset($img);
			$classNomargin = (++$people_partners_count%3==0) ? 'class="no_margin"' : ''; 
			echo sprintf($tpl_people_partners, $classNomargin, $pp->getPage()->getId(), $src->getAssetUrl(), $name, $role, $name);
		endforeach;
	?>
</ul>
<?php endif; ?>

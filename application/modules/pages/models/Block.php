<?php

class Pages_Model_Block
{

    private $_id;

    private $_lang;

    private $_tsCreation;

    private $_tsUpdate;

    private $_tsDelete;

    private $_page;

    private $_order;

    private $_template;

    private $_contents;

    private $_status;

    const STATUS_ACTIVE = 'active';

    const STATUS_DRAFT = 'draft';

    private $_observers;
    
    public function __construct()
    {
    	$this->_observers = new SplObjectStorage();
    }
    /**
     *
     * @return the unknown_type
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     *
     * @param unknown_type $_id            
     */
    public function setId($_id)
    {
        $this->_id = $_id;
        return $this;
    }

    /**
     *
     * @return the unknown_type
     */
    public function getTsCreation()
    {
        return $this->_tsCreation;
    }

    /**
     *
     * @param unknown_type $_tsCreation            
     */
    public function setTsCreation($_tsCreation)
    {
        $this->_tsCreation = $_tsCreation;
        return $this;
    }

    /**
     *
     * @return the unknown_type
     */
    public function getTsUpdate()
    {
        return $this->_tsUpdate;
    }

    /**
     *
     * @param unknown_type $_tsUpdate            
     */
    public function setTsUpdate($_tsUpdate)
    {
        $this->_tsUpdate = $_tsUpdate;
        return $this;
    }

    /**
     *
     * @return the unknown_type
     */
    public function getTsDelete()
    {
        return $this->_tsDelete;
    }

    /**
     *
     * @param unknown_type $_tsDelete            
     */
    public function setTsDelete($_tsDelete)
    {
        $this->_tsDelete = $_tsDelete;
        return $this;
    }

    /**
     *
     * @return the unknown_type
     */
    public function getPage()
    {
        return $this->_page;
    }

    /**
     *
     * @param unknown_type $_page            
     */
    public function setPage($_page)
    {
        $this->_page = $_page;
        return $this;
    }

    /**
     *
     * @return the unknown_type
     */
    public function getOrder()
    {
        return $this->_order;
    }

    /**
     *
     * @param unknown_type $_order            
     */
    public function setOrder($_order)
    {
        $this->_order = $_order;
        return $this;
    }

    /**
     *
     * @return the unknown_type
     */
    public function getTemplate()
    {
        return $this->_template;
    }

    /**
     *
     * @param unknown_type $_template            
     */
    public function setTemplate($_template)
    {
        $this->_template = $_template;
        return $this;
    }

    public function getContents()
    {
        return $this->_contents;
    }

    public function setContents($_contents)
    {
        $this->_contents = $_contents;
        return $this;
    }

    public function getLang()
    {
        return $this->_lang;
    }

    public function setLang($_lang)
    {
        $this->_lang = $_lang;
        return $this;
    }

    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || ! method_exists($this, $method)) {
            throw new Exception('Invalid certificates property: ' . $name);
        }
        $this->$method($value);
    }

    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || ! method_exists($this, $method)) {
            throw new Exception('Invalid certificates property: ' . $name);
        }
        return $this->$method();
    }

    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function getOptions()
    {
        return get_object_vars($this);
    }

    /**
     *
     * @return the unknown_type
     */
    public function getStatus()
    {
        return $this->_status;
    }

    /**
     *
     * @param unknown_type $_status            
     */
    public function setStatus($_status)
    {
        $this->_status = $_status;
        return $this;
    }
    
    public function _($name)
    {
        if(isset($this->_contents[$name]))
            return $this->_contents[$name];
        return null;
    }
    
    public function attach($observer)
    {
    	$this->_observers->attach($observer);
    }
    
    public function detach($observer)
    {
    	$this->_observers->detach($observer);
    }
    
    public function notify($event, $params=array())
    {
    	foreach($this->_observers as $observer)
    		$observer->update($this, $event, $params);
    }
}
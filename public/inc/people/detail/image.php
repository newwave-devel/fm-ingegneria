<?php
$peopleImage = $mainBlock->_ ( "file" );
if (! empty ( $peopleImage )) :
	?>
<img
	src="<?php echo $blockService->asset($peopleImage)->getAssetUrl()?>"
	alt="<?php echo $mainBlock->_("title")?>" class="img people">
<?php endif; ?>
<h2>Cookies statement</h2>

<p>
<strong>About cookies</strong><br />
Websites use smart and useful techniques to enhance user-friendliness and to make the site as interesting as possible for each visitor. One of the best known techniques involves cookies. Cookies can be used by website owners or third parties - like advertisers - who communicate through the website you visit.</p>

<p>Cookies are small text files that websites store on users’ computers. The information contained in cookies can also be used to track your surfing on different websites that use the same cookie.</p>

<p>We feel it is essential for you to be informed about the cookies that our website uses and for what purpose they are used. The purpose is threefold: we want to guarantee your privacy, the user friendliness, and the financing of our website as best as we can.</p>

<p>Cookies are classified according to duration and who sets them.</p>

<p>
<strong>Session cookies</strong><br />
A session cookie is temporarily stored in your computer’s memory while you are surfing on a site, for example to keep a track of which language you have chosen. Session cookies are not stored on your computer for a long period of time, but always disappear when you shut down your web browser.
<br /><br />
<strong>Persistent or tracker cookies</strong><br />
A persistent cookie saves a file on your computer for a long time; this kind of cookie has an expiry date. Persistent cookies help websites remember your information and settings when you visit them in the future. This result in faster and more convenient access since, for example, you don't have to login again.<br />When the expiry date has passed, the cookie is automatically deleted when you return to the website that created it.<br /><br />
<strong>First party cookies</strong><br />
These are set by the website itself (the same domain as in the browser’s address bar), and can only be read by that site. These cookies are commonly used to store information such as your preferences, for use when you re-visit the site.<br /><br />
<strong>Third party cookies</strong><br />
These are set by different domains from the one shown on the browser address bar that is by an organisation other than the website owner.<br />Cookies that, for example, are used to collect information for advertisements and custom content and also for web statistics may be ‘third-party cookies’.<br />As third-party cookies make it possible to generate more comprehensive surveys of user surfing habits, they are deemed to be more sensitive from the perspective of integrity; for this reason, most web browsers allow you to adjust your settings to not accept third-party cookies.
</p>


<p>
<strong>List of usedcookies, including those of third parties</strong><br />
Cookie proprietari (cookieLaw)<br />
Google Analytics (_ga, _gat)
Google Maps (AID, APISID, HSID, NID, OGP, PREF, SAPISID, SID, SNID, SSID, __utma, __utmz, _ga, PAIDCONTENT, WMC, GAPS)
</section><!--elenco_cookies-->
</p>


<p>
<strong>How to manage and disable cookies</strong><br />
To restrict, block or delete cookies just adjusting the settings of your web browser. The procedure is slightly different than the type of browser used. For detailed instructions, click on the link of your browser.<br /><br />
<a href="http://support.microsoft.com/kb/196955" target="_blank">Internet Explorer</a><br />
<a href="http://support.mozilla.org/it/kb/Gestione%20dei%20cookie?redirectlocale=en-US&redirectslug=Cookies" target="_blank">Firefox</a><br />
<a href="https://support.google.com/chrome/answer/95647?hl=it" target="_blank">Google Chrome</a><br />
<a href="http://www.opera.com/browser/tutorials/security/privacy/" target="_blank">Opera</a><br />
<a href="http://support.apple.com/kb/PH5042" target="_blank">Safari</a><br />
<a href="http://support.apple.com/kb/HT1677?viewlocale=it_IT" target="_blank">Safari iOS</a>
</p>
<p>If your browser is not among those offered, select "Help" on your web browser for information on how to proceed.</p>
<p>Warning: it is possible that disabling cookies the user no longer has access to certain content and can not fully appreciate the functionality of our websites.</p>
<p>By using the site without rejecting cookies and similar technologies online, users consent to the use of these technologies on our part for the collection and processing of information.</p>

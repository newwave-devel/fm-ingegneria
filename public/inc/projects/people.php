<?php if($peoplesBlocks): 
	require_once "inc/tpls/peoples/tpl.php";
	$peoples_chunks = array_chunk($peoplesBlocks, 4);
?>
<section id="key_people" style="visibility: hidden">
	<h5>Key People</h5>
	<ul>
	<?php 
		foreach($peoples_chunks as $chunk):
			echo "<li>";
			foreach($chunk as $peop)
			{
				$img = $blockService->asset($peop->_("file"))->getAssetUrl();
				echo sprintf($tpl_people_side_list, $peop->getPage(), $img, $peop->_("title"),  $peop->_("role"),  strtoupper($peop->_("title")));
			}
			echo "</li>";
		endforeach;
	?>
	</ul>
	<span id="carousel_pagination"><strong>1</strong> di <em><?php echo count($peoples_chunks) ?></em></span>
    <input type="hidden" id="chunkmax" value="<?php echo count($peoples_chunks) ?>" />
</section>
<!--key_people-->
<?php endif; ?>
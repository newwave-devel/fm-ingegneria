<?php
class App_Model_I18n_Collection
{
    private $_context;
    private $_lang;
    private $_i18nService;
    private $_labels;
    private $_new;

    public function __construct($refToService, $context, $lang)
    {
        $this->_service = $refToService;
        $this->_lang = $lang;
        $this->_context = $context;
        $this->_load();
    }

    private function _load()
    {
        try
        {
            $paginator = NW_Pages_Paginator::fetchAll();
            foreach($this->_service->fetchAll($paginator, array("context" => $this->_context, "lang" =>  $this->_lang)) as $i18nEntry)
                $this->_labels[$i18nEntry->getKey()] = $i18nEntry->getValue();
        } catch(Exception $e)
        {
            throw $e;
        }
    }

    public function __($key, $format = App_Model_I18n::FORMAT_WYSIWYG)
    {
        $key = trim($key);
        if(!isset($this->_labels[$key]))
        {
            foreach(App_Model_Display_Service::getInstance()->langs() as $langCode)
            {
                $new = new App_Model_I18n();
                $new->setContext($this->_context)
                    ->setKey($key)
                    ->setValue($key)
                    ->setLang($langCode)
                    ->setFormat($format);
                $this->_service->save($new);
            }
            echo $key;
        }
        else
            echo $this->_labels[$key];
    }

    public function _t($key, $format = App_Model_I18n::FORMAT_WYSIWYG)
    {
        $key = trim($key);
        if(!isset($this->_labels[$key]))
        {
            foreach(App_Model_Display_Service::getInstance()->langs() as $langCode)
            {
                $new = new App_Model_I18n();
                $new->setContext($this->_context)
                    ->setKey($key)
                    ->setValue($key)
                    ->setLang($langCode)
                    ->setFormat($format);
                $this->_service->save($new);
            }
            return $key;
        }
        return $this->_labels[$key];
    }
}
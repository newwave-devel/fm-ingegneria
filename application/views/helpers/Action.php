<?php

/**
 * Description of Link
 *
 * @author Ale
 */
class Zend_View_Helper_Action extends Zend_View_Helper_Abstract {

    public function action($action, $options = array()) {
        $route = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();
        $params['action'] = $action;
        $params['controller'] = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
        $params['module'] = Zend_Controller_Front::getInstance()->getRequest()->getModuleName();
        return $this->view->url($params, $route, true);
    }

}

?>

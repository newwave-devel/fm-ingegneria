<?php if($jobs): ?>
    <?php foreach($jobs as $job): ?>
        <article class="contact_box">
            <section class="career">
                <h6><?php echo str_replace("/", ".",  $job->_("date")); ?></h6>
                <h3><?php echo $job->_("title") ?></h3>
                <p><?php echo $job->_("description") ?></p>
            </section><!--career-->

            <a class="file" data-rel="external"
                href="mailto:<?php $translator->{"Pagina 'Careers'"}->__("Indirizzo Destinatario CV") ?>?subject=<?php echo $job->_("title") ?>">
                    <?php $translator->{"Pagina 'Careers'"}->__("Apply") ?>
            </a>

        </article><!--contact_box-->
    <?php endforeach; ?>
    <?php else: ?>
        <?php $translator->{"Pagina 'Careers'"}->__("Nessun risultato trovato"); ?>
    <?php endif; ?>

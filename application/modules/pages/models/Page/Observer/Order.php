<?php

class Pages_Model_Page_Observer_Order
{

    public function update(&$subject, $event)
    {
        switch ($event) {
            case Pages_Model_Page_Event::EVENT_BEFORE_SAVE:
                break;
            case Pages_Model_Page_Event::EVENT_AFTER_SAVE:
                break;
            case Pages_Model_Page_Event::EVENT_BEFORE_UPDATE:
                break;
            case Pages_Model_Page_Event::EVENT_AFTER_UPDATE:
                break;
        }
    }
}
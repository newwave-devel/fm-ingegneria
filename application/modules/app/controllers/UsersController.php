<?php

class App_UsersController extends NW_Controller_Action {
    private $_service;
    
    public function init() {
        $this->_service = new App_Model_User_Service();
        $this->_translateAdapter = Zend_Registry::get('Zend_Translate');
    }

    public function indexAction() {
        // action body
    }

    public function dashlistAction() {
        try {
        	$this->_setAjaxAction();
        	$this->_helper->SwitchContextByUser();
        	
        	switch ($this->_helper->Role()) 
        	{              
                case App_Model_Role::ROLE_ADMINISTRATOR:
                	$frmAdduser = new App_Form_User();
                	$this->view->adduser = $frmAdduser;
                	$paginator = new NW_Pages_Paginator();
                	$this->view->entries = $this->_service->fetchChilds($paginator);//$this->_asyncRequest("#list", "/app/users/list");
                    break;
                case App_Model_Role::ROLE_EDITOR:
                case App_Model_Role::ROLE_GUEST:
                	break;
            }
            $frmMyProfile = new App_Form_MyProfile();
            $me = $this->_service->find($this->_helper->LoggedUser()->getId());
            $frmMyProfile->populate($me->getOptions());
            $this->view->myprofile = $frmMyProfile;
            
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function listAction() {
        try {
            $this->_setAjaxAction();
            $paginator = new NW_Pages_Paginator();
            $paginator->setFetchAll(true);
           // $this->_addOnload("fancybox()");
            $this->view->entries = $this->_service->fetchChilds($paginator);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function updatemyprofileAction() {
        try {
            $this->_setJsonAction();
            $request = $this->getRequest();
            $form = new App_Form_MyProfile();
            
            $status = new NW_View_RetCode();
            $curLoggedUser = $this->_service->find($this->_helper->LoggedUser()->getId());
            $oldPassword = $curLoggedUser->getPassword();
            if ($request->isPost()) 
            {
                if ($form->isValid($request->getPost())) 
                {
                    //check against password
                    if($curLoggedUser->checkPassword($form->getValue('oldpassword')))
                    {
                    	$curLoggedUser->setOptions($form->getValues());
                    	//if does not set a new pass
                    	$resetPassword = ($form->getValue('password') != '');
                    	// overrides form value with the old one
                    	if(!$resetPassword) $curLoggedUser->setPassword($oldPassword);
                    	$this->_service->save($curLoggedUser, null, $resetPassword);
                    	$status->setStatusSuccess()
                    		->setPayload($this->_translateAdapter->translate("Personal datas updated successfully"));
                    }
                    else {
                    	$status->setStatusError()
                    		->setPayload($this->_translateAdapter->translate("Your old password is invalid"));
                    }
                   
                } else {
                    $errors = '';
    				foreach($form->getMessages() as $field => $msg)
    				{
    				    if(is_array($msg))
    				    {
    				        foreach($msg as $label => $error)
    				            if(is_array($error))
    				                $errors .= sprintf("<strong>Campo %s</strong>: %s<br />", $field, current($error));
    				            else $errors .= sprintf("<strong>Campo %s</strong>: %s<br />", $field, $error);
    				    } else 
    				        $errors .= sprintf("<strong>Campo %s</strong>: %s<br />", $field, $msg);
    				}
    				$status->setPayload($errors);
                }
                $this->view->status = $status;
            } else {
            	$status->setStatusError()
            	->setPayload("There has been some errors, please try again later");
            $this->view->status = $status;
            }
        } catch (Exception $e) {
            throw $e;
        }
    }
    //@TODO: needs to be refactored, logic in controller is not good
    public function saveAction()
    {
    	try {
    		$this->_setJsonAction();
    		$request = $this->getRequest();
    		$form = new App_Form_User();
    		
    		$status = new NW_View_RetCode();
    		if ($request->isPost())
    		{
    			if ($form->isValid($request->getPost()))
    			{
    				$permissions = new App_Model_Permissions();
    				
    				$model = new App_Model_User();
    				if(!$this->_service->checkUniqueness($form->getValue('username'), $form->getValue('email')))
    					throw new Exception($this->_translateAdapter->translate("Duplicate username or email"));
    				
    				$model->setOptions($form->getValues())->setPid($this->_helper->LoggedUser()->getId());
    				$allowed = null;
    				
                    $itemId = $this->_service->save($model, $permissions, true);
                    $status->setStatusSuccess()
                    	->setPayload($this->_translateAdapter->translate("User added successfully"));
    			}
    			else {		
    				throw new Exception("Please check fields: ". $form->firstError());
    			}
    			$this->view->status = $status;
    		}
    	} catch(Exception $e)
    	{
    		$this->view->status = NW_View_RetCode::error($e->getMessage());
    	}
    }
    
    
    public function updateAction()
    {
    	try {
    		$this->_setIframeAction();
    		$request = $this->getRequest();
    		$form = new App_Form_User();
    		$form->getElement('password')->setRequired(false);
     		$form->getElement('checkpassword')->setRequired(false);
    		$status = new NW_View_RetCode();
    		if ($request->isPost())
    		{
    			if ($form->isValid($request->getPost()))
    			{
    				
    				if ($this->_hasParam('id') && ($id=$request->getParam('id'))!='') {
    					$model = $this->_service->find($id);
    					$oldPassword = $model->getPassword();
    				}
    				else {
    					$model = new App_Model_User();
    					if(!$this->_service->checkUniqueness($form->getValue('username'), $form->getValue('email')))
    						throw new Exception($this->_translateAdapter->translate("Duplicate username or email"));
    				}
    				$model->setOptions($form->getValues())->setPid($this->_helper->LoggedUser()->getId());
    
    				$resetPassword = ($form->getValue('password') != '');
    				if(!$resetPassword)
    					$model->setPassword($oldPassword);
    				$itemId = $this->_service->save($model, $permissions, $resetPassword);
    				$this->_addOnload('parent.user.update()');
    			}
    			else {
    				$this->view->form = $form; //if form is invalid...
    			}
    		}
    	} catch(Exception $e)
    	{
    		$this->view->status = NW_View_RetCode::error($e->getMessage());
    	}
    }
    
    public function editAction()
    {
    	try {
    		$this->_setIframeAction();
    		if(!$this->_hasParam('id')) throw new Exception("no id recevied");
    		$id = $this->_getParam('id');
    		$form = new App_Form_User();
    		$form->setAction('/app/users/update');
    		$user = $this->_service->find($id);
     		$form->populate($user->getOptions());
     		$form->getElement('id')->setValue($id);
     		$form->getElement('password')->setRequired(false);
     		$form->getElement('checkpassword')->setRequired(false);
    		$this->view->form = $form;
    	} catch(Exception $e)
    	{
    		throw $e;
    	}
    }
    
    public function deleteAction()
    {
    	try {
    		$this->_setAjaxAction();
    		if(!$this->_hasParam('id')) throw new Exception("no id received");
    		$id = $this->_getParam('id');
    		$this->_service->delete($id);
    	} catch(Exception $e)
    	{
    		throw $e;
    	}
    }
}


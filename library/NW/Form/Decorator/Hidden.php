<?php
class NW_Form_Decorator_Hidden extends NW_Form_Decorator
{
	
public function render($content)
    {
        $element = $this->getElement();
        if (!$element instanceof Zend_Form_Element)
        {
            return $content;
        }
        if (null === $element->getView())
        {
            return $content;
        }

        $separator = $this->getSeparator();
        $placement = $this->getPlacement();
        
        $input = $this->buildInput();
        switch ($placement)
        {
            case (self::PREPEND):
                return $input . $separator . $content;
            case (self::APPEND):
            default:
                return $content . $separator . $input;
        }
    }
	
	
}

?>
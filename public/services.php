<?php
$pageId = 'company';
include "inc/required.php";
include "inc/common/contents.php";
?>
<?php include "inc/common/head.php" ?>


        <!-- SCRIPT -->

		<script type="text/javascript" src="/common/js/core.js"></script>
		<script type="text/javascript" src="/common/js/jquery.jcarousel.pack.js"></script>
        <script type="text/javascript" src="/common/js/jquery.sharrre.min.js"></script>
        <script type="text/javascript" src="/common/js/jquery.nivo.slider.pack.js"></script>
        <script type="text/javascript" src="/common/js/custom.js"></script>
        
        <script type="text/javascript">	
                jQuery(window).load(function() {
                    // START
					loading();
					share();
					people_carousel_sidebar();
                    slideshow();
					fancybox();
                });
        </script>
        
	</head>    
    
	<body lang="it">
    
    	<div id="loader"></div>
    	    	
        <?php include "inc/common/header.php"?>
        
        <section id="main_page">
        	<section class="container">
            
            	<section id="page_title">
                    <h3><?php echo $mainBlock->_("title")?></h3>
                   <?php include "inc/common/social_share.php"?>
                </section><!--page_title-->
                
                
                <section id="main_data">
                	
                    <article class="services_box">
                    	<img class="img" src="">
                        <nav class="services_list">
                        	<h4>Buildings</h4>
                            <ul>
                            	<li><a href="">Architectural Design</a></li>
                                <li><a href="">Structural Design</a></li>
                                <li><a href="">MEP Design</a></li>
                                <li><a href="">Geology and Soil Mechanic Engineering</a></li>
                                <li><a href="">Restoration and Refurbishment</a></li>
                                <li><a href="">Buildings Specialistc Consultancy</a></li>
                            </ul>
                        </nav><!--services_list-->
                    </article><!--services_box-->
                    
                    <article class="services_box">
                    	<img class="img" src="">
                        <nav class="services_list">
                        	<h4>Buildings</h4>
                            <ul>
                            	<li><a href="">Architectural Design</a></li>
                                <li><a href="">Structural Design</a></li>
                                <li><a href="">MEP Design</a></li>
                                <li><a href="">Geology and Soil Mechanic Engineering</a></li>
                                <li><a href="">Restoration and Refurbishment</a></li>
                                <li><a href="">Buildings Specialistc Consultancy</a></li>
                                <li><a href="">Restoration and Refurbishment</a></li>
                            </ul>
                        </nav><!--services_list-->
                    </article><!--services_box-->
                    
                    <article class="services_box">
                    	<img class="img" src="">
                        <nav class="services_list">
                        	<h4>Buildings</h4>
                            <ul>
                            	<li><a href="">Architectural Design</a></li>
                                <li><a href="">Structural Design</a></li>
                                <li><a href="">MEP Design</a></li>
                                <li><a href="">Geology and Soil Mechanic Engineering</a></li>
                                <li><a href="">Restoration and Refurbishment</a></li>
                                <li><a href="">Buildings Specialistc Consultancy</a></li>
                            </ul>
                        </nav><!--services_list-->
                    </article><!--services_box-->
                    
                    <article class="services_box">
                    	<img class="img" src="">
                        <nav class="services_list">
                        	<h4>Buildings</h4>
                            <ul>
                            	<li><a href="">Architectural Design</a></li>
                                <li><a href="">Structural Design</a></li>
                                <li><a href="">MEP Design</a></li>
                            </ul>
                        </nav><!--services_list-->
                    </article><!--services_box-->
                	
                </section><!--main_data-->
                
            </section><!--container-->
        </section><!--main_page-->
        
        <?php include "inc/common/footer.php"?>
		
	</body>
	
</html>
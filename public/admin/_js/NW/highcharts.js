NW.Highcharts = (function () {
    // Initialize the application
    var items = {};
    var divRef = '';
    var logEnabled = true;
    
    chart = function(itemDivId)
    {
        this.divRef = "#"+itemDivId;
        this.options = {
            'chart' : {},
            'colors' : [{}],
            'credits': {},
            'exporting': {},
            'globals': '',
            'labels': {},
            'lang': {},
            'legend': {},
            'loading': {},
            'navigation': {},
            'pane': {},
            'plotOptions': {},
            'series': [],
            'subtitle' : {},
            'title' : {},
            'tooltip': {},
            'xAxis' : {},
            'yAxis': {}
        };
        
        this.setChartObjectParam = function(param, value)
        {
            this.options['chart'][param] = value;
            return this;
        }
                                
        this.addColor = function(value)
        {
            this.options['colors'].push(value);
            return this;
        }
                        
        this.setColors = function(colors)
        {
            this.options['colors'] = colors;
            return this;
        }
        this.setCreditsParam = function(param, value)
        {
            this.options['credits'][param] = value;
            return this;
        }
        this.setTitleParam = function(param,value)
        {
            this.options['title'][param] = value;
            return this;
        }
        this.setSubTitleParam = function(param,value)
        {
            this.options['subtitle'][param] = value;
            return this;
        }
        this.setXAxisParam = function(param,value)
        {
            this.options['xAxis'][param] = value;
            return this;
        }
        this.setXAxisCategories = function(arrCateg)
        {
            this.options['xAxis']['categories'] = arrCateg;
            return this;
        }  
        this.setYAxisParam = function(param,value)
        {
            this.options['yAxis'][param] = value;
            return this;
        }  
        this.addSeries = function(name, datas)
        {
            var newserie = {
                name: name, 
                data:data
            };
            this.options['series'].push(newserie);
            return this;
        }
        this.debug = function(optionField)
        {
            if(this.logEnabled)
            {
                if(optionField != undefined)
                    console.log(this.options[optionField]);
                else console.log(this);
            }
                           
        }
    }
    
    chart.prototype = {
        getOption : function(optionName)
        {
            return this.options[optionName];
        },       
        setChart : function(renderTo, type, marginRight, marginBottom)
        {
            this.options['chart'] = {
                'renderTo': renderTo, 
                'type': type, 
                'marginRight':marginRight, 
                'marginBottom':marginBottom
            };
        },
        getChart : function()
        {
            this.getChart();
        },
        setTitle : function(text, x)
        {
            this.options['title'] = {
                'text': text, 
                'x': x
            };
        },
        setSubtitle : function(text, x)
        {
            this.options['subtitle'] = {
                'text': text, 
                'x': x
            };
        },
        setXAxis: function(categories)
        {
            this.debug('xAxis');
            this.setXAxisCategories(categories);
        },
        addXAxis: function(param)
        {
            this.options['categories'].push(param);
        },
        setYAxis: function(title)
        {
            this.options['yAxis']['title']['text'] = title;
        },
        plot: function()
        {
            new Highcharts.Chart(this.options);
            console.log(this.options);
            return this;
        },
        sync: function(data)
        {
            var itemRef = this;
            $.each(data, function(index, value) {
                    itemRef.options[index] = value;
            });
            itemRef.plot();
        },
        async: function(paramAction)
        {
            var itemRef = this;
            
            $.post(paramAction,{}, function(data)
            {
                $.each(data, function(index, value) {
                    itemRef.options[index] = value;
                });
                itemRef.plot();
            }, 'json');
        },
        debug: function()
        {
            return this.debug();
        }
        
    }
    
    
    // Return the public facing methods for the App
    return {
        addChart: function (paramId, type) {
            items[paramId] = new chart(paramId);
            items[paramId].setChartObjectParam('renderTo', paramId);
            if(type != undefined)
                items[paramId].setChartObjectParam('type', type);
            return items[paramId];
        },
        
        debug: function(paramId)
        {
            return items[paramId].debug();
        },
        plot: function(paramId)
        {
            items[paramId].plot;
            return items[paramId];
        }
    };
}());


var NW_Highcharts = NW.Highcharts;
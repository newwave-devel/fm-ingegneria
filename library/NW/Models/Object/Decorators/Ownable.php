<?php

class NW_Models_Object_Decorators_Ownable extends NW_Models_Object_Decorator {

    public $_owner;

    public function __construct($object, $owner = 1) {
        $this->_object = $object;
        $this->_owner = $owner;
        $this->_mapper = new NW_Models_Object_Decorators_Mapper_Ownable($this);
    }

    public function getOwner() {
        return $this->_owner;
    }

    public function setOwner($res) {
        $this->_owner = $res;
        return $this;
    }

    public function isOwnable() {
        return true;
    }

    public function __get($name) {
        if ($name == 'owner')
            return $this->getOwner();

        return $this->getObject()->$name;
    }

    public function __set($name, $value) {
        if ($name == 'owner')
            return $this->setOwner($value);

        $this->getObject()->$name = $value;
        return $this;
    }

    public function doInsert($object) {
        try {
            $object = $this->getObject()->doInsert($object);
            $query = $this->getMapper()->queryInsert($object);
            $this->getAdapter()->performInsertQuery($query);
            print "<li>".$query->getQuery()."</li>";
            return $object;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function doUpdate(&$object) {
        try {
            
            $query = $this->getMapper()->queryUpdate($object);
            $this->getAdapter()->performUpdateQuery($query);
            print "<li>".$query->getQuery()."</li>";
            return $this->getObject()->doUpdate($object);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function doLoadOne($params) {
        try {
            $query = $this->getObject()->doLoadOne($params);
            return $this->getMapper()->queryLoad($query);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function doLoadAll($params, NW_Pages_Paginator &$paginator) {
        try {
            $query = $this->getObject()->doLoadAll($params, $paginator);
            return $this->getMapper()->queryAll($query);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function doView($viewName, $params, &$paginator) {
        try {
            $query = $this->getObject()->doView($viewName, $params, $paginator);
            return $this->getMapper()->queryView($viewName, $query);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function doLoadByRelDefInTable(NW_Db_Query_Select &$query, $relationName, $view, $callerTable) {
        try {
            $this->getObject()->doLoadByRelDefInTable($query, $relationName, $view, $callerTable);
            $this->getMapper()->queryLoadByRelDefInTable($query, $relationName);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function toStdClass() {
        $stdClass = $this->getObject()->toStdClass();
        $stdClass->owner = $this->getOwner();
        return $stdClass;
    }

    public function formFields() {
        $fields = $this->getObject()->formFields();
        $fields[] = new NW_Form_Element_Hidden("owner");
        return $fields;
    }
    
    public function cloneNew(&$object)
    {
        $object->owner = $this->getOwner();
        return $this->getObject()->cloneNew($object);
    }

}

?>

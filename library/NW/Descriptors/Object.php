<?php

class NW_Descriptors_Object extends NW_XmlDescriptor {

    public function vars() {
        return $this->descriptor->vars->var;
    }

    public function rels() {
        return $this->descriptor->vars->rel;
    }

    public function view($viewName, $type = "") {
        try {
        if(!isset($this->descriptor->views->$viewName))
            throw new Exception("View $viewName not defined");
        if($type == '')
        {
            foreach($this->descriptor->views->$viewName->var as $item)
                $res[] = strval($item);
            return $res;
        }
        foreach($this->descriptor->views->$viewName->var as $item)
            if($item['type'] == $type)
                $res[] = strval($item);
        return $res;
        } catch(Exception $e)
        {
            throw $e;
        }
    }
    
    public function inView($viewName, $fieldName)
    {
       try { 
           if(!isset($this->descriptor->views->$viewName))
                throw new Exception("View $viewName not defined");
           foreach($this->descriptor->views->$viewName->var as $item)
               if(strval($item)==$fieldName) return true;
           return false;
           } catch(Exception $e)
        {
            throw $e;
        }
    }

    public function relDef($relname, $attr = '') {
        if($attr == '')
            return $this->descriptor->reldefs->$relname;
        else 
        {
            $reldef = $this->descriptor->reldefs->$relname;
            return strval($reldef[$attr]);
        }
    }

    public function relDefId($relname) {
        $reldef = $this->descriptor->reldefs->$relname;
        return strval($reldef["reldef"]);
    }
    
    public function varDef($varname)
    {
        return $this->descriptor->vardefs->$varname;
    }

    public function varDefId($varname) {
        $vardef = $this->descriptor->vardefs->$varname;
        return strval($vardef["vardef"]);
    }

    private function defaultVal($item) {
        $node = $this->descriptor->vardefs->$item->defaultvalue;
        if ($node["eval"] == "N")
            return strval($node);
        else {
            $code = strval($node);
            if ($node["type"] == "php") {
                str_replace('$', '\$', $code);
                $txt = '$val = ' . $code . ';';
                return $txt;
            }
        }
    }

    public function byAttribute($attr, $val, $viewname = '') {
        $fields = array();
        foreach ($this->descriptor->xpath("//*[@$attr='$val']") as $field)
            $fields[] = $field->getName();
        if ($viewname == '')
            return $fields;
        $fieldsInView = $this->view($viewname);
        foreach ($fieldsInView as $pos => $viewField) {
            if (!in_array(strval($viewField), $fields))
                unset($fieldsInView[$pos]);
        }
        return $fieldsInView;
    }

    public function attribute($field, $attribute) {
        return $this->descriptor->vardefs->$field[$attribute];
    }
    
//    public function property($field, $context, $value='') {
//        if($value='')
//            return $this->descriptor->vardefs->$field->$context;
//        return $this->descriptor->vardefs->$field->$context[$value];
//    }

    /*
     * I use reldef attribute to extract relation name;
     * It's not clean programming, i admit :)
     */

    public function fieldIsRelation($field) {
        return isset($this->descriptor->obj->reldefs->$field);
    }

    public function getLibraryItems() {
        
    }

}
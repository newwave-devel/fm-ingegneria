<?php
	$basepath = dirname ( __FILE__ );
	// Define path to application directory
	defined ( 'APPLICATION_PATH' ) || define ( 'APPLICATION_PATH', realpath ( $basepath . '/../../application' ) );

	// Define application environment
	defined ( 'APPLICATION_ENV' ) || define ( 'APPLICATION_ENV', (getenv ( 'APPLICATION_ENV' ) ? getenv ( 'APPLICATION_ENV' ) : 'production') );

	defined ( 'APP_PATH' ) || define ( 'APP_PATH', APPLICATION_PATH . DIRECTORY_SEPARATOR . "../" );

	// Ensure library/ is on include_path
	set_include_path ( implode ( PATH_SEPARATOR, array (realpath ( APPLICATION_PATH . '/../library' ), get_include_path () ) ) );

	defined('PUBLIC_PATH') ||
	    define('PUBLIC_PATH', APPLICATION_PATH."/../public");
	
	require_once 'Zend/Application.php';
	
	$application = new Zend_Application(
	    APPLICATION_ENV,
	    APPLICATION_PATH . '/configs/application.ini'
	);
	$application->bootstrap();
	//---- SETTING DISPLAY
	$display = new App_Model_Display();
 	$display->setWebsite(0);
	$display->setLangs(array("en"));
	$display->setLang("en");
	
	//---- INITTING MODEL
	$pageService = new Pages_Model_Page_Service();
	$pageService->setDisplay($display);
	$blockService = new Pages_Model_Block_Service();

    //initting translator
    $translator = App_Model_I18n_Manager::getInstance($display->getLang());
?>
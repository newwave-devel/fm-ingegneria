jQuery(document).ready(function() {
    $(".submit_btn").click(function()
    {
        var params = new Array();
        $(".filter.disciplines.current").each(function(){
            if($(this).attr("rel")!='')
            params.push($(this).attr("rel"));
        });
        $(":hidden[name='disciplines']").val(params.join(","));

        var params = new Array();
        $(".filter.countries.current").each(function(){
            if($(this).attr("rel")!='')
            params.push($(this).attr("rel"));
        });
        $(":hidden[name='countries']").val(params.join(","));
        $("#frmController").submit();
    });

    $(".filter").click(function()
    {
        $("#curPage").val(1);
        if($(this).hasClass("disciplines"))
        {
            $(".disciplines.current").removeClass("current");
            $(this).toggleClass("current");
            $(":hidden[name='disciplines']").val($(this).attr("rel"));
            $("#frmController").submit();
        }
        else if($(this).hasClass("countries"))
        {
            $(".countries.current").removeClass("current");
            $(this).toggleClass("current");
            $(":hidden[name='countries']").val($(this).attr("rel"));
            $("#frmController").submit();
        }

        return false;
    })
});
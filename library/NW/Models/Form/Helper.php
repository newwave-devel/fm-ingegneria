<?php

class NW_Models_Form_Helper
{
    private $_objectDescriptor;
    private $_form;
    

    public function __construct(NW_Models_Form &$form, $options=null)
    {
        $this->_form = $form;
        $this->_objectDescriptor =  NW_Application::getSingleton("app/NW_ModuleManager")->classDescriptor($form->getClassUri());
    }

    public function addModelFields($fieldsName=array(), $fields_options=array())
    {
        if (empty($fieldsName))
            $fieldsName = $this->_objectDescriptor->vars();
      
        foreach ($this->_objectDescriptor->vars() as $fieldName)
        {   
            $fieldName = strval($fieldName);
            $varDef = $this->_objectDescriptor->varDefId($fieldName);
            if($varDef== 'image')
                continue;
            
            $fieldOptions = isset($fields_options[$fieldName]) ? $fields_options[$fieldName] : array();
            $element = $this->getField($fieldName, $varDef, $fieldOptions);
            $this->_form->addElement($element);
        }
        
        return $this;
    }

    public function getForm()
    {
        return $this->_form;
    }

    private function getField($fName, $varDef, $fieldOptions)
    {
        $options = array();
        $fieldMeta = $this->_objectDescriptor->varDef($fName)->form;
        
        $options['required'] = ($fieldMeta['required'] == 'Y');
        $options['label'] = strval($fieldMeta->label);
        $options['validators'] = empty($fieldMeta->validators) ? array() : $fieldMeta->validators;
        $options['errormsg'] = array();
        
        return $this->_form->getField($fName, $varDef, array_merge($options, $fieldOptions));
    }

}

?>
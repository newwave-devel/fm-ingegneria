<?php

class NW_Models_Object_Decorators_Mapper_Revisionable extends NW_Models_Object_Decorators_Mapper {

    public function __construct(NW_Models_Object_Decorators_Revisionable &$decoratorRef)
    {
        $this->decoratorRef = $decoratorRef;
        $this->table = 'tbl_revs';
    }
    
    public function queryInsert(&$object)
    {
        $query = new NW_Db_Query_Insert($this->table);
        $query->addItem("revId", $object->revId);
        $query->addItem("id", $object->id);
        return $query;
    }
    
    public function queryUpdate(&$object)
    {
        $query = new NW_Db_Query_Update($this->table);
            $query->add(array("revId" => $object->revId));
            $query->where(array("id=?" => $object->id));
        return $query;
    }
    
    public function queryLoad($query = '')
    {
        if ($query=='') $query = new NW_Db_Query_Select();
        return $query->select("revid", $this->table)
                ->innerJoinOn($this->getDecorator()->getMainTable(), sprintf("%s.id = %s.id", $this->getDecorator()->getMainTable(),$this->table));
    }
    
    public function queryAll($query='')
    {
        if ($query=='') $query = new NW_Db_Query_Select();
        return $query->select("revid", $this->table)->innerJoinOn($this->getDecorator()->getMainTable(), sprintf("%s.id = %s.id", $this->getDecorator()->getMainTable(),$this->table));
    }
    
    public function queryView($viewName, $query='')
    {
        if ($query=='') $query = new NW_Db_Query_Select();
        return $query->select("revid", $this->table)->innerJoinOn($this->getDecorator()->getMainTable(), sprintf("%s.id = %s.id", $this->getDecorator()->getMainTable(),$this->table));
    }
    
    public function queryLoadByRelDefInTable(NW_Db_Query_Select &$query)
    {
        
    }
}
?>

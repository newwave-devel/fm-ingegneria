<?php

class Zend_View_Helper_TimThumb extends Zend_View_Helper_Abstract
{
    const DEFAULT_ADMIN_WIDTH = 120;
    const DEFAULT_ADMIN_HEIGHT = 80;
    const DEFAULT_COMPRESSION = 90;
    const TT_PATH = '/libs/vendors/tt.php';
    
    public function timThumb($image, $width='', $height='', $compression='')
    {
        $width = ($width == '') ? self::DEFAULT_ADMIN_WIDTH : $width;
        $height = ($height == '') ? self::DEFAULT_ADMIN_HEIGHT : $height;
        $compression = ($compression == '') ? self::DEFAULT_COMPRESSION : $compression;
        return sprintf("%s?src=%s&w=%d&h=%d&zc=%d", self::TT_PATH, $image, $width, $height, $compression);
    }
    
    
 
}

?>

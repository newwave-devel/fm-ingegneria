<?php

class NW_Models_Object_Decorators_Mapper_Taggable extends NW_Models_Object_Decorators_Mapper
{
    
    public function __construct(NW_Models_Object_Decorators_Taggable &$decoratorRef)
    {
        $this->decoratorRef = $decoratorRef;
        $this->table = 'tbl_tags';
    }
    
    public function queryInsert(&$object)
    {
        $query = new NW_Db_Query_Insert($this->table, NW_Db_Query_Insert::RETURN_ID);
        $query->addItemsArray($this->getDecorator()->export($object));
        return $query;
    }
    
    public function queryUpdate(&$object)
    {   
        $query = new NW_Db_Query_Update($this->table);
        $query->add($this->getDecorator()->export($object));
        $query->where(array("id=?" => $object->id));
        return $query;
    }
    
    public function queryLoad($query = '')
    {
        if ($query=='') $query = new NW_Db_Query_Select();
        $query->select("*", $this->table)->innerJoinOn($this->table, sprintf("%s.id=%s.id", $this->table, $this->getDecorator()->getMainTable()));
        return $query;
    }
    
    public function queryAll($query='')
    {
        if ($query=='') $query = new NW_Db_Query_Select();
        return $query->select("*", $this->table)->innerJoinOn($this->table, sprintf("%s.id=%s.id", $this->table, $this->getDecorator()->getMainTable()));
    }
    
    public function queryView($viewName, $query='')
    {
        if ($query=='') $query = new NW_Db_Query_Select();
        return $query->select("*", $this->table)->innerJoinOn($this->table, sprintf("%s.id=%s.id", $this->table, $this->getDecorator()->getMainTable()));
    }
    
    public function queryLoadByRelDefInTable(NW_Db_Query_Select &$query, $relationName)
    {
        if ($query=='') $query = new NW_Db_Query_Select();
        $tablealias = $this->tableAlias($relationName);
        $maintablealias =  $this->tableAlias($relationName, $this->getDecorator()->getMainTable());
        $query->leftJoinOn(array($this->table => $tablealias),
                                sprintf("%s.id = %s.id AND %s.status ='%s'", $tablealias, $maintablealias, $tablealias, NW_Models_Object_Decorators_Catalogable::STATUS_ACTIVE));
    }
}

?>

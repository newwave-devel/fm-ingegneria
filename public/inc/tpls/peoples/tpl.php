<?php
$tpl_people_side_list = <<<TPL
	<a href="people-detail.php?id=%d"> <img class="img" src="/assets/res.php?src=%s&w=110&h=110" alt="%s" />
		<h6>%s</h6>
		<h5>%s</h5>
		<div class="color_wrapper"></div>
	</a>
TPL;

$tpl_people_keystaff = <<<TPL
	<li %s>
		<a href="people-detail.php?id=%d">
			<img class="img" src="/assets/res.php?src=%s&w=110&h=110" alt="%s" />
			<h6>%s</h6>
			<h5>%s</h5>
			<div class="color_wrapper"></div>
		</a>
	</li>
TPL;

$tpl_people_partners = <<<TPL
	<li %s>
		<a href="people-detail.php?id=%d">
			<img class="img" src="/assets/res.php?src=%s&w=110&h=110" alt="%s" />
			<h6>%s</h6>
			<h5>%s</h5>
			<div class="color_wrapper"></div>
		</a>
	</li>
TPL;
?>
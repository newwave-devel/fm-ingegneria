NW.Actions = (function () {
    // Initialize the application
    var actionlist = {};
    
    getAction = function(actionName, callBack, returnMethod)
    {
        var itemRef = "a.action."+actionName;
        this.caller = $(itemRef);
        this.actionName=actionName;
        this.callBack = callBack;
        this.returnMethod = returnMethod;
        this.logEnabled = false;
    }
    
    getAction.prototype = {
        register : function()
        {
            var callBackFn = this.callBack;
            var returnMethod = this.returnMethod;
            this.log();
            $(this.caller).click(
                function() 
                { 
                    var handler = $(this).attr('href');
                    $.get(handler,{}, callBackFn, returnMethod); 
                    return false;
                });
                
        }, 
        log: function()
        {
            if(this.logEnabled)
                console.log(this);
        }
    }
    
    
    // Return the public facing methods for the App
    return {
        registerGet: function (actionName, callBack, returnMethod) {
            callBack = (callBack == undefined) ? actionName : callBack;
            returnMethod = (returnMethod == undefined ) ? 'json' : returnMethod;
            actionlist[actionName] = new getAction(actionName, callBack, returnMethod);
            actionlist[actionName].register();
        }
    };
}());


var NW_Actions = NW.Actions;
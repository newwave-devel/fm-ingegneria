<?php

class Pages_Model_Page_Admin_Service
{

	protected $_service;

	protected $_request;

	protected $_view;

	public function getService()
	{
		return $this->_service;
	}

	public function __construct($request, &$view)
	{
		$this->_service = new Pages_Model_Page_Service ();
		$this->_request = $request;
		$this->_view = $view;
	}

	public function getList()
	{
		try
		{
			$view = $this->_getView ();
			$paginator = NW_Pages_Paginator::__fromRequest ( $this->_request );
			$paginator->setIsAjax ( true, "#main_content" );
			$lang = $this->_request->getParam ( "lang", App_Model_Display_Service::getInstance ()->lang () );
			
			$paramsSearch = array (
					"template" => $this->_request->getParam ( "type" ) 
			);
			$pageSearchForm = $this->pageSearchForm ( array (
					"type" => $this->_request->getParam ( "type" ),
					"paginator" => $paginator 
			) );
			$descriptor = NW_Pages_Page::getInstance ()->page ( $this->_request->getParam ( "type" ) );
			$view->canAdd = (! isset ( $descriptor ["limit"] ) || $descriptor ["limit"] == "N");
			
			$listDTO = new Pages_Model_Page_DTO_Page_List ();
			$pages = $this->_service->fetchAll ( $paginator, $paramsSearch );
			$translationService = new Pages_Model_Page_Translation_Service ();
			$listDTO->setPages ( $pages );
			$retrieved = $listDTO->pageIds ();
			if (! empty ( $retrieved ))
				$listDTO->setTranslations ( $translationService->fetchAll ( array (
						$lang => $retrieved 
				) ) );
			else
				$listDTO->setTranslations ( array () );
			$view->dto = $listDTO;
			$view->paginator = $paginator->prepare ();
			$view->formSearch = $pageSearchForm;
			$view->pageTitle = $descriptor->label;
		}
		catch ( Exception $e )
		{
			throw $e;
		}
	}

	public function search($formParams)
	{
		try
		{
			$view = $this->_getView ();
			$lang = $formParams ['lang'];
			$paginator = NW_Pages_Paginator::__fromRequest ( $this->_request );
			$paginator->setIsAjax ( true, "#main_content" );
			$descriptor = NW_Pages_Page::getInstance ()->page ( $this->_request->getParam ( "type" ) );
			$listDTO = new Pages_Model_Page_DTO_Page_List ();
			$pages = $this->_service->fetchAll ( $paginator, $formParams );
			$translationService = new Pages_Model_Page_Translation_Service ();
			$listDTO->setPages ( $pages );
			$retrieved = $listDTO->pageIds ();
			if (! empty ( $retrieved ))
				$listDTO->setTranslations ( $translationService->fetchAll ( array (
						$lang => $retrieved 
				) ), $lang );
			else
				$listDTO->setTranslations ( array () );
			$view->dto = $listDTO;
			$view->paginator = $paginator->prepare ();
			$view->paginationController = 'frmSearch';
			$view->pageTitle = $descriptor->label;
		}
		catch ( Exception $e )
		{
			throw $e;
		}
	}

	public function getNew()
	{
		try
		{
			$type = $this->_request->getParam ( 'type' );
			$view = $this->_getView ();
			$form = $this->pageForm ();
			
			$form->getElement ( 'lang' )->setValue ( App_Model_Display_Service::getInstance ()->lang () );
			$form->getElement ( 'template' )->setValue ( $type );
			$form->getElement ( 'status' )->setValue ( Pages_Model_Page::STATUS_DRAFT );
			$view->form = $form;
			$view->pageLabel = NW_Pages_Page::getInstance ()->page ( $type )->label;
		}
		catch ( Exception $e )
		{
			throw $e;
		}
	}
	// receives id and lang
	public function getTranslation($parentPage = null)
	{
		try
		{
			if (empty ( $parentPage ))
				$parentPage = $this->_request->getParam ( "id" );
			$lang = $this->_request->getParam ( "lang" );
			
			$view = $this->_getView ();
			
			$pageId = $this->_service->translate ( $parentPage, $lang );
			$dto = $this->_service->load ( $pageId );
			// ---- MAINPAGE
			$mainPage = $dto->getPage ();
			$mainForm = $this->pageForm ();
			$mainForm->populate ( $mainPage->getOptions () );
			// ---- TAXONOMIES
			
			// ---- BLOCKS!
			$blocks = array ();
			$taxonomies = array ();
			
			$blockAdminService = new Pages_Model_Block_Admin_Service ( $this->_request, $this->_view );
			
			foreach ( $dto->getBlocks () as $block )
			{
				$foo = $blockAdminService->edit ( $block, true );
				$blocks [$block->getId ()] = $blockAdminService->edit ( $block, true );
			}
			
			// ---- BLOCKS!
			
			// ---- TAXONOMIES
			$taxonomies = array ();
			if (NW_Pages_Page::getInstance ()->pageHasTaxonomiesToDisplay ( $mainPage->getTemplate () ))
				$taxonomies = NW_Pages_Page::getInstance ()->displayableTaxonomies ( $mainPage->getTemplate () );
				
				// ---- TAXONOMIES
			
			$view->form = $mainForm;
			$view->blocks = $blocks;
			$view->taxonomies = $taxonomies;
			$view->mainPage = $mainPage;
			$view->label = NW_Pages_Page::getInstance ()->page ( $mainPage->getTemplate () )->label;
		}
		catch ( Exception $e )
		{
			throw $e;
		}
	}

	public function getEdit($pageId = null)
	{
		try
		{
			if (empty ( $pageId ))
				$pageId = $this->_request->getParam ( "id" );
			
			$view = $this->_getView ();
			$dto = $this->_service->load ( $pageId );
			
			// ---- MAINPAGE
			$mainPage = $dto->getPage ();
			$mainForm = $this->pageForm ();
			$mainForm->populate ( $mainPage->getOptions () );
			// ---- TAXONOMIES
			
			// ---- BLOCKS!
			$blocks = array ();
			$taxonomies = array ();
			
			$blockAdminService = new Pages_Model_Block_Admin_Service ( $this->_request, $this->_view );
			
			foreach ( $dto->getBlocks () as $block )
			{
				$foo = $blockAdminService->edit ( $block, true );
				$blocks [$block->getId ()] = $blockAdminService->edit ( $block, true );
			}
			
			// ---- BLOCKS!
			
			// ---- TAXONOMIES
			$taxonomies = array ();
			if (NW_Pages_Page::getInstance ()->pageHasTaxonomiesToDisplay ( $mainPage->getTemplate () ))
				$taxonomies = NW_Pages_Page::getInstance ()->displayableTaxonomies ( $mainPage->getTemplate () );
				
				// ---- TAXONOMIES
			
			$view->form = $mainForm;
			$view->blocks = $blocks;
			$view->taxonomies = $taxonomies;
			$view->shared = NW_Pages_Page::getInstance ()->pageHasSharedBlocks ( $mainPage->getTemplate () );
			$view->mainPage = $mainPage;
			$view->label = NW_Pages_Page::getInstance ()->page ( $mainPage->getTemplate () )->label;
		}
		catch ( Exception $e )
		{
			throw $e;
		}
	}

	public function saveShared()
	{
		try
		{
			$view = $this->_getView ();
			$pageId = $this->_request->getParam("page");
			$page = $this->_service->find($pageId);
			
			//loads datas from the descriptor
			$descriptor = NW_Pages_Page::getInstance()->page($page->getTemplate());
			$blocks =  $descriptor->shares->blocks->children();
			$positions = $descriptor->shares->positions->children();
			//loads datas from the descriptor
			
			
			$form = new Pages_Form_Block_Shared (array("positions" => $positions));
			$status = new NW_View_RetCode ();
			if ($this->_request->isPost ())
			{
				if ($form->isValid ( $this->_request->getPost () ))
				{
					$blockService = new Pages_Model_Block_Shared_Service();
					$blockService->saveShared($page, $form->getValues ());
					$status->setStatus(NW_View_RetCode::STATUS_SUCCESS)
						->setPayload("salvato con successo");
				}
				else
				{
					$status->setStatus(NW_View_RetCode::STATUS_ERROR)
						->setPayload("Orco Leo");
				}
				
			}
			else {
				$status->setStatus(NW_View_RetCode::STATUS_ERROR)
					->setPayload("Mica ricevuto niente, io, eh!");
			}
			$view->status = $status;
		}
		catch ( Exception $e )
		{
			throw $e;
		}
	}

	protected function _getView()
	{
		return $this->_view;
	}

	public function pageSearchForm($options)
	{
		return new Pages_Form_PageSearch ( $options );
	}

	public function pageForm()
	{
		return new Pages_Form_Page ();
	}

}
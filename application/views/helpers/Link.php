<?php

class Zend_View_Helper_Link extends Zend_View_Helper_Abstract {
    public $view;
    
    public function link() {
        return $this;
    }

    public function setView(Zend_View_Interface $view) {
        $this->view = $view;
    }

    public function scriptPath($script) {
        return $this->view->getScriptPath($script);
    }
    
    public function edit($content, $queryStringAsArray, $options=array())
    {
        $tpl = '<a href="%s" %s>%s</a>';
        $route = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();
        $itemId = (isset($options['id'])) ? sprintf(' id="%s" ', $options['id']) : '';
        $itemClass = (isset($options['class'])) ? sprintf(' classs="%s" ', $options['class']) : '';
        $itemRel = (isset($options['rel'])) ? sprintf(' rel="%s" ', $options['rel']) : '';
        $target = (isset($options['target'])) ? sprintf(' target="%s" ', $options['target']) : '';
        $paramsUrl = array_merge(array("action" => 'edit'), $queryStringAsArray);
        $anchor = $this->view->url($paramsUrl, $route);  
        return sprintf($tpl, $anchor, $itemId.$itemClass.$itemRel.$target, $content);
    }
}

?>

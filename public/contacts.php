<?php
$pageId = 'contacts';
include "inc/required.php";
include "inc/common/contents.php";
?><!doctype html>
<!--[if lt IE 7 ]> <body class="ie6"> <![endif]-->
<!--[if IE 7 ]><body class="ie7"><![endif]-->
<!--[if IE 8 ]><body class="ie8"><![endif]-->
<!--[if IE 9 ]><body class="ie9"><![endif<]-->
<!--[if (gt IE 9)|!(IE)]><!--><!--<![endif]-->

<html>

	<head>
		<meta charset="utf-8"/>
        
		<title>FM Ingegneria Spa</title>
		<meta name="description" content="#" />
		<meta name="keywords" content="#" />
        
        <meta name="author" content="Newwave snc - newwave-media.it">
		<meta name="Copyright" content="Copyright 2013 Newwave snc All Rights Reserved.">
        
        <meta name="viewport" content="width=1024">
        
        <link rel="shortcut icon" href="common/img/favicon.ico">
        <link rel="apple-touch-icon" href="common/img/apple-touch-icon.png">
        
        <link rel="stylesheet" media="all" href="common/css/main.css">
		
        <!--[if lt IE 9]>
			<script src="common/js/html5.js"></script>
		<![endif]-->
		
        
        <!-- SCRIPT -->
		<script type="text/javascript" src="common/js/core.js"></script>
        <script type="text/javascript" src="common/js/jquery.sharrre.min.js"></script>
        <script type="text/javascript" src="common/js/custom.js"></script>
        
        <script type="text/javascript">	
                jQuery(window).load(function() {
                    // START
					loading();
					share();
					fancybox();
                });
        </script>
        
	</head>    
    
	<body lang="it">
    
    	<div id="loader"></div>
    	    	
        <?php include "inc/common/header.php"?>
        
        <section id="main_page">
        	<section class="container">
            
            	<section id="page_title">
                    <h3>Contacts</h3>
                    
                     <?php include "inc/common/social_share.php"?>
                    
                </section><!--page_title-->
                
                <?php 
                
                	foreach($addresses as $address)
                	{
                		$img = $address->_('file');
                		echo sprintf($tpl_address_gmaps,  $address->_('gmaps'),  $address->_('description'));
                	}
                	
                ?>
            </section><!--container-->
        </section><!--main_page-->
        
        <?php include "inc/common/footer.php"?>
		
	</body>
	
</html>
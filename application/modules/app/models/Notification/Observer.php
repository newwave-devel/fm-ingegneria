<?php
class App_Model_Notification_Observer implements SplObserver
{	
	private $_translationAdapter;
	private $_notificationService;
	
	public function __construct()
	{
		$translator = Zend_Registry::get('Zend_Translate');
		$this->_translationAdapter = $translator->getAdapter();
		$this->_notificationService = new App_Model_Notification_Service();
	}
	
	public function update(SplSubject $subject)
	{
		switch(get_class($subject))
		{
		    case 'Purchase_Model_Delivery':
		    	$this->notifyDelivery($subject);
		    	break;
	    	case 'Purchase_Model_OrderRequest':
	    		$this->notifyOrderRequest($subject);
	    		break;
		    case 'Purchase_Model_Purchase':
		    	$this->notifyPurchase($subject);
		    	break;
		}
		
		return $this;
	}
	
	//notification is sent upon "proceed", before status update
	private function notifyOrderRequest(Purchase_Model_OrderRequest &$subject)
	{
		try {
			$notification = new App_Model_Notification();
			$notification->setRead(false)
				->setPriority(App_Model_Notification::PRIORITY_LOW);
			$permService = new App_Model_Permission_Service();
			$vesselService = new Vessels_Model_Vessel_Service();
			$vessel = $vesselService->find($subject->getShipId());
			$users = array();
			$ta = $this->_translationAdapter;
			
			switch($subject->getStatus())
			{
			    case Purchase_Model_OrderRequest::STATUS_OPEN:
			    	$users = $permService->usersOfVessel($subject->getShipId(), App_Model_Role::ROLE_FLEETMASTER,false);//$vesselService->dpa($subject->getShipId(), false);
			    	$message = sprintf($ta->translate("New order for ship %s submitted to dpa for approval by %s"), 
			    									$vessel->getName(), 
			    									$subject->getOriginator()->name);	
			    	break;
			    case Purchase_Model_OrderRequest::STATUS_SENT_TO_DPA:
			    		$users = $permService->usersOfVessel($subject->getShipId(), App_Model_Role::ROLE_SHIPOWNER,false);
			    		$shoff = $permService->usersOfVessel($subject->getShipId(), App_Model_Role::ROLE_SHIPOFFICER,false);
			    		$users = array_merge($users, $shoff);
			    		$message = sprintf($ta->translate("New order for ship %s submitted to dpa for approval by %s"),
			    				$vessel->getName(),
			    				$subject->getOriginator()->name);
			    		break;
			}
			
			if(!empty($users))
			{
				foreach($users as $userId)
				{
					$notification
						->setMessage($message)
						->setTo($userId);
					$this->_notificationService->save($notification);
				}
			}
			
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	private function notifyDelivery(Purchase_Model_Delivery &$subject)
	{
		try {
			$ta = $this->_translationAdapter;
			$notification = new App_Model_Notification();
			$notification->setRead(false)
				->setPriority(App_Model_Notification::PRIORITY_LOW);
				
			$permService = new App_Model_Permission_Service();
			$vesselService = new Vessels_Model_Vessel_Service();
			$requestService = new Purchase_Model_OrderRequest_Service();
			$deliveryService = new Purchase_Model_Delivery_Service();
			
			$request = $requestService->find($subject->getRequest());
			$vessel = $vesselService->find($request->getShipId());
			$subject = $deliveryService->load($subject->getId());
			$users = array();
			switch($subject->getStatus())
			{
				
			   case Purchase_Model_Delivery::STATUS_RECEIVED:
		    		$users = $permService->usersOfVessel($request->getShipId(), App_Model_Role::ROLE_FLEETMASTER, false); //$vesselService->dpa($request->getShipId(), false);
		    		$message = sprintf($ta->translate("Ship %s confirms receiving of %s %s"),
		    				$vessel->getName(), $subject->getRow()->quantity, $subject->getRow()->request);
		    		break;
			}
				
			if(!empty($users))
			{
				foreach($users as $userId)
				{
					$notification
						->setMessage($message)
						->setTo($userId);
					$this->_notificationService->save($notification);
				}
			}
				
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	private function notifyPurchase(Purchase_Model_Purchase &$subject)
	{
		try {
			$ta = $this->_translationAdapter;
			$notification = new App_Model_Notification();
			$notification->setRead(false)
				->setPriority(App_Model_Notification::PRIORITY_LOW);
	
			$vesselService = new Vessels_Model_Vessel_Service();
			$requestService = new Purchase_Model_OrderRequest_Service();
			
			$purchaseService = new Purchase_Model_Purchase_Service();
			$permService = new App_Model_Permission_Service();
			$request = $requestService->find($subject->getRequest());
			$vessel = $vesselService->find($request->getShipId());
			
			$users = array();
			
			switch($subject->getStatus())
			{
			    case Purchase_Model_Purchase::STATUS_WAITING_DELIVERY:
			    	$users = $permService->usersOfVessel($request->getShipId(), App_Model_Role::ROLE_SHIPOFFICER, false);//$vesselService->shipofficers($request->getShipId(), false);
			    	$message = sprintf($ta->translate("Order %s has been shipped"), $subject->getLabel());
			    	break;
		    	case Purchase_Model_Purchase::STATUS_SENT:
		    		$users = $permService->usersOfVessel($request->getShipId(), App_Model_Role::ROLE_SHIPOWNER, false);
		    		$message = sprintf($ta->translate("Order %s is being processed"), $subject->getLabel());
		    		break;
			   
			}
	
			if(!empty($users))
			{
				foreach($users as $userId)
				{
					$notification
						->setMessage($message)
						->setTo($userId);
					$this->_notificationService->save($notification);
				}
			}
	
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	
}

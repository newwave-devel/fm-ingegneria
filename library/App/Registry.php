<?php
class App_Registry{
	private static $_instance = null;
	private $_pool = array();

	public static function registry()
	{
		if(self::$_instance == null)
			self::$_instance = new App_Registry();
		return self::$_instance;
	}
	
	private function __construct() {}
	private function __clone() {}
	
	
	public function get($key)
	{
		if(class_exists($key))
		{
			if(!isset($this->_pool[$key]))
				$this->_pool[$key] = new $key;
			return $this->_pool[$key];
		}
	}
	
	public function service($key)
	{
		$className = sprintf("%s_Service", $key);
		return $this->$className;
	}
	
	public function __get($name) {
        return $this->get($name);
    }
}
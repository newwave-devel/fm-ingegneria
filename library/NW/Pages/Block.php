<?php
class NW_Pages_Block
{
	private $_descriptor;
	private static $_instance=null;
	
	public static function getInstance()
	{
		if (self::$_instance == null)
			self::$_instance = new NW_Pages_Block();
		return self::$_instance;
	}
	
	private function __construct()
	{
		try {
			$xml = APPLICATION_PATH."/etc/blocks.xml";
			$this->_descriptor = simplexml_load_file($xml);
			if($this->_descriptor == false)
				throw new Exception("Cannot parse file '$xml'");
		} catch (Exception $e)
		{
			throw $e;
		}
	}
	
	public function block($name)
	{
		try {
			$query = sprintf('//block[@name="%s"]', $name);
			$result = $this->_descriptor->xpath($query);
			if($result==false) throw new Exception("blocks.xml: invalid query: $query");
			return current($result);
		} catch (Exception $e)
		{
			throw $e;
		}
	}
	
	public function field($block, $field)
	{
		try {
			$query = sprintf('//block[@name="%s"]/fields/%s', $block, $field);
			$result = $this->_descriptor->xpath($query);
			if($result==false) throw new Exception("blocks.xml: invalid query: $query");
			return current($result);
		} catch (Exception $e)
		{
			throw $e;
		}
	}
	
	public function blocks($forSelect = true)
	{
		try {
			$query = sprintf('//blocks/block');
			$result = $this->_descriptor->xpath($query);
			if($result==false) throw new Exception("blocks.xml: invalid query: $query");
			if($forSelect) 
			{
				$blocks = array();
				$count = 0;
				$att = '';
				foreach($result as $res)
				{
					$name = (string) $res['name'];
					$label = (string) $res->label;
					$blocks[$name] = $label;
				}
				return $blocks;
			}
			return $result;
		} catch (Exception $e)
		{
			throw $e;
		}
	}

	public function attachObservers(Pages_Model_Block &$block)
	{
		$descriptor = $this->block($block->getTemplate());
		if($descriptor->observers->count() != 0)
		{
			foreach($descriptor->observers->children() as $obs)
			{
				$className = $obs->getName();
				if(class_exists($className))
					$block->attach(new $className);
			}
		}
	}
}
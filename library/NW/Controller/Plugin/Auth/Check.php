<?php

class NW_Controller_Plugin_Auth_Check extends Zend_Controller_Plugin_Abstract {

    private $_controller;
    private $_module;
    private $_action;

    //gets involved evrt we make a request
    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        $this->_controller = $request->getControllerName();
        $this->_module = $request->getModuleName();
        $this->_action = $request->getActionName();
        
        //$route = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();
        switch ($this->_module) 
        {
            case 'app':
            	if($this->_controller == 'media' && $this->_action == 'upload') 
            		break;
            case 'login':
            case 'default':
            case 'certificates':
                if ($this->_controller == 'document' && $this->_action == 'save')
                    break;
            case 'purchase':
            	if ($this->_action == 'save')
            	{
            		if($this->_controller == 'quote' || $this->_controller=='deliverynote')
            			break;
            	}
            default:
                $this->_checkIdentity();
                break;
        }
    }

    private function _checkIdentity() {
    	//Zend_Registry::get('Log')->log( $this->_controller.",".$this->_module.",".$this->_action." -> CHECKING", Zend_Log::INFO);
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_request->setModuleName('app');
            $this->_request->setControllerName('login');
            $this->_request->setActionName('index');
        } else {
        	$this->_request->setModuleName($this->_module);
            $this->_request->setControllerName($this->_controller);
            $this->_request->setActionName($this->_action);
        }
    }

}

?>

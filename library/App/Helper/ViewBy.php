<?php

class App_Helper_ViewBy extends Zend_Controller_Action_Helper_Abstract {
    
   
    public function viewBy() {
        $session = new Zend_Session_Namespace('viewbehaviour');
        return $session->viewby;
    }
    
    public function viewParam()
    {
        $session = new Zend_Session_Namespace('viewbehaviour');
        return $session->queryItem;
    }
    
    public function view()
    {
        $session = new Zend_Session_Namespace('viewbehaviour');
        return $session;
    }
    
    public function direct()
    {
        return $this->view();
    }
    public function dump()
    {
        $session = new Zend_Session_Namespace('viewbehaviour');
        echo sprintf("<!-- SESSION DUMP: viewby: %s, queryItem: %s-->", $session->viewby, $session->queryItem);
    }
}

?>

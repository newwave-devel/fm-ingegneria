<?php

/**
 * @author Ale
 *
 */
class Pages_Model_Page_Translation
{

	private $_id;

	private $_langs;

	public function __construct()
	{
		$this->_id = null;
		$this->_langs = array ();
	}

	public function getId()
	{
		return $this->_id;
	}

	public function setId($_id)
	{
		$this->_id = $_id;
		return $this;
	}

	public function getLangs()
	{
		return $this->_langs;
	}

	public function setLangs($_langs)
	{
		$this->_langs = $_langs;
		return $this;
	}

	public function addLang($code, $value)
	{
		$this->_langs [$code] = $value;
	}

	public function getLang($code)
	{
		return $this->_langs [$code];
	}
}
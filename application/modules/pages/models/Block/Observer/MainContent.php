<?php

class Pages_Model_Block_Observer_MainContent
{

	public function update(&$subject, $event)
	{
		switch ($event)
		{
			case Pages_Model_Block_Event::EVENT_BEFORE_SAVE :
				break;
			case Pages_Model_Block_Event::EVENT_AFTER_SAVE :
				$this->writeBlockContent($subject);
				break;
			case Pages_Model_Block_Event::EVENT_BEFORE_UPDATE :
				break;
			case Pages_Model_Block_Event::EVENT_AFTER_UPDATE :
				$this->dropBlockContent($subject)
					->writeBlockContent($subject);
				break;
			case Pages_Model_Block_Event::EVENT_ADD :
				break;
			case Pages_Model_Block_Event::EVENT_TRANSLATE :
				break;
			case Pages_Model_Block_Event::EVENT_BEFORE_DELETE :
				break;
			case Pages_Model_Block_Event::EVENT_AFTER_DELETE :
				$this->dropBlockContent($subject);
				break;
		}
	}
	
	public function writeBlockContent(Pages_Model_Block $subject)
	{
		try {
			$descriptor = NW_Pages_Block::getInstance()->block($subject->getTemplate());
			
			$contents = $subject->getContents();
			
			$datas = array();
			
			$sorts = array();
			foreach($descriptor->fields->children() as $field)
			{
				$dbType = isset($field['dbtype']) ? $field['dbtype'] : 'varchar';
				
				if(!isset($field['alias'])) continue;
				$fieldName = $field->getName();
				$fieldAlias = (string)$field['alias'];
				if(isset($contents[$fieldName]))
				{
					$value = $contents[$fieldName];
					$datas[$fieldAlias] = $this->_convert($value, $dbType);
				}
				if(isset($field['defines_sort']))
					$sorts[] = sprintf("%s %s", $fieldAlias, $field['defines_sort']);
				
			}
			if(empty($datas)) return $this;
			$datas["block"] = $subject->getId();
			$datas["page"] = $subject->getPage();
			$datas["status"] = $subject->getStatus();
			$this->_insert($datas);
			if(!empty($sorts))
				$this->_sortPages($sorts, $subject->getPage());
			return $this;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	
	private function _insert($data)
	{
		try{
			$adapter = Zend_Db_Table::getDefaultAdapter();
			$adapter->insert("tbl_blocks_mains", $data);
		}catch(Exception $e)
		{
			throw $e;
		}
	}
	public function dropBlockContent(Pages_Model_Block $subject)
	{
		try{
			$blockId = $subject->getId();
			$adapter = Zend_Db_Table::getDefaultAdapter();
			$adapter->delete("tbl_blocks_mains", "block = $blockId" );
			return $this;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function _sortPages($sorts, $curPageId)
	{
		try{
			$pageService = new Pages_Model_Page_Service();
			$page = $pageService->find($curPageId);
			//build the query to join pages with tbl_blocks_main
			$adapter = Zend_Db_Table::getDefaultAdapter();
			$query = "SELECT tbl_blocks_mains.page, @pos:=@pos+1 AS pos
						FROM tbl_blocks_mains 
						INNER JOIN tbl_pages ON tbl_pages.id=tbl_blocks_mains.`page` ,
						(SELECT @pos:=0) b
						WHERE tbl_pages.`template`='%s' 
						AND tbl_pages.lang='%s'
						ORDER BY %s";
			$sql = sprintf($query, $page->getTemplate(), $page->getLang(), implode(",", $sorts));
			$newSortings = $adapter->fetchAll($sql);

			foreach($newSortings as $ns)
			{
				$data = array("order" => $ns['pos']);
				$where = 'id = '.$ns['page'];
				$adapter->update('tbl_pages', $data, $where);
			}
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	private function _convert($value, $dbType, $params=array())
	{
		switch($dbType)
		{
			default:
				return $value;
			case 'datetime':
				$time = NW_Utils_DateTime::strptime($value, "%d/%m/%Y");
				return strftime("%Y-%m-%d", $time);
		}
	}
}
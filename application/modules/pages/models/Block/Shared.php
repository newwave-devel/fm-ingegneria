<?php

class Pages_Model_Block_Shared
{

    private $_id;

    private $_page;
    
    private $_block;

    private $_order;

    private $_position;

    private $_observers;
    
    public function __construct()
    {
    	$this->_observers = new SplObjectStorage();
    }
    /**
     *
     * @return the unknown_type
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     *
     * @param unknown_type $_id            
     */
    public function setId($_id)
    {
        $this->_id = $_id;
        return $this;
    }
       
    /**
     *
     * @return the unknown_type
     */
    public function getPage()
    {
        return $this->_page;
    }

    /**
     *
     * @param unknown_type $_page            
     */
    public function setPage($_page)
    {
        $this->_page = $_page;
        return $this;
    }
    
    public function getBlock()
    {
    	return $this->_block;
    }

    public function setBlock($block)
    {
    	$this->_block = $block;
    	return $this;
    }
    /**
     *
     * @return the unknown_type
     */
    public function getOrder()
    {
        return $this->_order;
    }

    /**
     *
     * @param unknown_type $_order            
     */
    public function setOrder($_order)
    {
        $this->_order = $_order;
        return $this;
    }   

    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || ! method_exists($this, $method)) {
            throw new Exception('Invalid certificates property: ' . $name);
        }
        $this->$method($value);
    }

    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || ! method_exists($this, $method)) {
            throw new Exception('Invalid certificates property: ' . $name);
        }
        return $this->$method();
    }

    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function getOptions()
    {
        return get_object_vars($this);
    }

    /**
     *
     * @return the unknown_type
     */
    public function getPosition()
    {
        return $this->_position;
    }

    /**
     *
     * @param unknown_type $_status            
     */
    public function setPosition($_position)
    {
        $this->_position = $_position;
        return $this;
    }
    
    public function attach($observer)
    {
    	$this->_observers->attach($observer);
    }
    
    public function detach($observer)
    {
    	$this->_observers->detach($observer);
    }
    
    public function notify($event, $params=array())
    {
    	foreach($this->_observers as $observer)
    		$observer->update($this, $event, $params);
    }
}
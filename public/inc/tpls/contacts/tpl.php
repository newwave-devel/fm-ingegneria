<?php
$tpl_address = <<<TPL
				<article class="contact_box">
                	<section class="map">
                	<img src="%s" class="map"/ >
                    </section><!--map-->
                    <section class="address">
                    	%s
                    </section><!--address-->
                </article><!--contact_box-->	
TPL;

$tpl_address_gmaps = <<<TPL
				<article class="contact_box">
                	<section class="map">
                	%s
                    </section><!--map-->
                    <section class="address">
                    	%s
                    </section><!--address-->
                </article><!--contact_box-->
TPL;

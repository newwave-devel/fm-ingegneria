<?php
require_once ("inc/functions/helpers.php");
try {
	// loads disciplinescat
	$glob_disciplinescatIds = $pageService->collection ()->display ( 'disciplines-cat' );
	$glob_disciplinesCat = $blockService->display ( 'disciplines-cat-main', array (
			'id' => $glob_disciplinescatIds 
	) );
	$regionsCollection = $pageService->collection ()->display ( 'regions' );
	
	// loads disciplines
	$glob_disciplinesIds = $pageService->collection ()->display ( "disclipines" );
	$glob_disciplines = $pageService->display ( "disclipines", array (
			'id' => $glob_disciplinesIds 
	) );
	
	// loads disciplines groups
	$glob_disciplines_groupsIds = $pageService->collection ()->display ( "disclipines-group" );
	$glob_disciplines_groups = $pageService->display ( "disclipines-group", array (
			'id' => $glob_disciplines_groupsIds
	) );
	switch ($pageId) {
		case 'homepage' :
			$curPage = $pageService->displayFirst ( "homepage" );
			$relatedContentsMain = $pageService->displayShared ( $curPage->getId (), array ("position" => 'body' ), true );
			$slides = $blockService->displayByPage ( $curPage->getId (), 'slideshow', array(), array("order" => "ASC"));
			break;
		case 'discipline' :
			$curPage = $pageService->fromRequestSafe('disciplines');
			$mainBlock = $blockService->displayMain($curPage->getId());
			$relatedContentsMain = $pageService->displayShared ( $curPage->getId (), array ("position" => array('sx', 'dx') ), true );
			
			$peoleTaxonomy = new Pages_Model_Taxonomy('peoplediscipline', 'disciplines', $curPage->getId());
			$peoplesCollection = $pageService->collection()->displayDependent($peoleTaxonomy, 'people');
			$peoplesBlocks = $blockService->display('peoples-main', array("page" => $peoplesCollection));
			$slides = $blockService->displayByPage ( $curPage->getId (), 'slideshow' );
			$attachments = $blockService->displayByPage($curPage->getId(), 'common-attachments');
			//--breadcrubms
			$groupcatTaxonomy = new Pages_Model_Taxonomy('disgroupdisc', 'disciplines',$curPage->getId());
			$currentGroupCollection = $pageService->collection()->displayDependent($groupcatTaxonomy, 'disciplines-group');
			$currentGroupBlock = $blockService->display('disciplines-group-main', array("page" => $currentGroupCollection[0]));
			$catTaxonomy = new Pages_Model_Taxonomy('discat', 'disciplines',$curPage->getId());
			$currentCatCollection = $pageService->collection()->displayDependent($catTaxonomy, 'disciplines-cat');
			if(!empty($currentCatCollection))
				$currentCatBlock = $blockService->display('disciplines-cat-main', array("page" => $currentCatCollection[0]));
			else $currentCatBlock = array();
			break;
		case 'homepages' :
			$curPage = $pageService->fromRequestSafe( "disciplines-cat" );
			$relatedContentsMain = $pageService->displayShared ( $curPage->getId (), array ("position" => 'body' ), true );
			$slides = $blockService->displayByPage ( $curPage->getId (), 'slideshow' );
			break;
		case 'project-detail' :
			$curPage = $pageService->fromRequestSafe( "projects" );
			$mainBlock = $blockService->displayMain($curPage->getId());
			$attachments = $blockService->displayByPage($curPage->getId(), 'common-attachments');
			$videos =  $blockService->displayByPage($curPage->getId(), 'common-vimeovideo');

			$peoleTaxonomy = new Pages_Model_Taxonomy('projectpeople', 'projects', $curPage->getId());
			$peoplesCollection = $pageService->collection()->displayDependent($peoleTaxonomy, 'people');
			//$peoplesBlocks = $blockService->display('peoples-main', array("id" => $peoplesCollection));
            $peoplesBlocks = $blockService->display('peoples-main', array("page" => $peoplesCollection));
			
			$galleryBlocks = $blockService->displayByPage($curPage->getId(), 'common-gallery');
			
			$disciplinesTaxonomy = new Pages_Model_Taxonomy('projectdiscipline', 'projects', $curPage->getId());
			$disciplinesCollection = $pageService->collection()->displayDependent($disciplinesTaxonomy, 'disciplines');
			//$disciplinesBlocks = $blockService->display('disciplines-main', array("id" => $disciplinesCollection));
            $disciplinesBlocks = $blockService->display('disciplines-main', array("page" => $disciplinesCollection));
			
			$disciplinesCatTaxonomy = new Pages_Model_Taxonomy('projectdiscat', 'projects', $curPage->getId());
			$disciplinesCatCollection = $pageService->collection()->displayDependent($disciplinesCatTaxonomy, 'disciplines-cat');
			//$disciplinesCatBlocks = $blockService->display('disciplines-cat-main', array("id" => $disciplinesCatCollection));
            $disciplinesCatBlocks = $blockService->display('disciplines-cat-main', array("page" => $disciplinesCatCollection));
			break;
		case 'company':
			$curPage = $pageService->displayFirst ( "company" );

            $gallery_header = $blockService->displayByPage ( $curPage->getId (), 'slideshow-notext' );
			$attachments = $blockService->displayByPage($curPage->getId(), 'common-attachments');
			$mainBlock = $blockService->displayMain($curPage->getId());
			
			$peoleTaxonomy = new Pages_Model_Taxonomy('peoplecompany', 'company', $curPage->getId());
			$peoplesCollection = $pageService->collection()->displayParents($peoleTaxonomy, 'peoples');
			//$peoplesBlocks = $blockService->display('peoples-main', array("id" => $peoplesCollection));
            $peoplesBlocks = $blockService->display('peoples-main', array("page" => $peoplesCollection));
			//news
			$newsTaxonomy = new Pages_Model_Taxonomy('newscompany', 'company', $curPage->getId());
			$newsCollections = $pageService->collection()->displayParents($newsTaxonomy, 'news', array(), array("tbl_pages.order" => "asc"));
			$newsBlocks = $blockService->display('news-main', array("page" => $newsCollections));			
			break;
		case 'people':
			require_once "inc/tpls/peoples/tpl.php";
			$curPage = $pageService->displayFirst ( "people" );
			$mainBlock = $blockService->displayMain($curPage->getId());	
			$staff = $pageService->displayShared ( $curPage->getId (), array ("position" => 'staff' ), true );
			$partners = $pageService->displayShared ( $curPage->getId (), array ("position" => 'partners' ), true );
            $gallery_header = $blockService->displayByPage($curPage->getId(), 'slideshow-notext');
			break;
		case 'people-detail':
			$curPage = $pageService->fromRequestSafe( "peoples" );
			$mainBlock = $blockService->displayMain($curPage->getId());
			
			//tags are only disciplines
			$disciplineTaxonomy = new Pages_Model_Taxonomy('peoplediscipline', 'peoples', $curPage->getId());
			$disciplineCollections = $pageService->collection()->displayParents($disciplineTaxonomy, 'disciplines');
			$tagBlocks = $blockService->display('disciplines-main', array("page" => $disciplineCollections));
			
			$projectTaxonomy = new Pages_Model_Taxonomy('projectpeople', 'peoples', $curPage->getId());
			$projectsCollections = $pageService->collection()->displayParents($projectTaxonomy, 'projects');
			$projectsBlocks = $blockService->display('projects-main', array("page" => $projectsCollections));
			
			$newsTaxonomy = new Pages_Model_Taxonomy('newspeople', 'peoples', $curPage->getId());
			$newsCollections = $pageService->collection()->displayParents($newsTaxonomy, 'news');
			$newsBlocks = $blockService->display('news-main', array("page" => $newsCollections));
			break;
		case 'projects':
			require_once "inc/tpls/projects/tpl.php";
            $filter_disciplines = '';
            $filter_search = '';
            $filter_countries = '';
			if(!filter_has_var(INPUT_GET,'search'))
			{
				$paginator = new NW_Pages_Paginator();
				$paginator->setItemsPerPage(12);
				$projectsPages = $pageService->collection()->fetchAll($paginator, array("template" => "projects", "status" => "active"));
				$projects = $blockService->display('projects-main', array("page" => $projectsPages));
				$paginator->prepare();
			}
			else {

				$curPage = filter_has_var(INPUT_GET,'pag') ? filter_input(INPUT_GET, 'pag', FILTER_SANITIZE_NUMBER_INT) : 1;
				$filter_disciplines = filter_has_var(INPUT_GET,'disciplines') ? filter_input(INPUT_GET, 'disciplines', FILTER_SANITIZE_STRING) : null;
                $filter_countries= filter_has_var(INPUT_GET,'countries') ? filter_input(INPUT_GET, 'countries', FILTER_SANITIZE_STRING) : null;
                $filter_search = filter_has_var(INPUT_GET,'search') ? filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING) : null;
				$curPage=($curPage != 0) ? $curPage : 1;
				$paginator = new NW_Pages_Paginator();
				$paginator->setItemsPerPage(12)
					->setCurPage($curPage);

                $collection = new Pages_Model_Collection();
                $collection->select('projects', $display->getLang());

                if(!empty($filter_search))
                    $collection->mainblockContains($filter_search);
                if(!empty($filter_disciplines))
                    $collection->dependentIn('projectdiscipline', $filter_disciplines);
                if(!empty($filter_countries))
                    $collection->dependentIn('projectregion', $filter_countries);

                $projectsPages = $collection->collection($paginator);

                $projects = $blockService->display('projects-main', array("page" => $projectsPages));
                $paginator->prepare();
			}
            $filter_disciplines_array  = empty($filter_disciplines) ? array() : explode(",", $filter_disciplines);
            $filter_countries_array = empty($filter_countries) ? array() : explode(",", $filter_countries);
            $regions = $blockService->display('regions-main', array("page" => $regionsCollection));

			break;
		case 'news':
			require_once "inc/tpls/news/tpl.php";
            $filter_disciplines = '';
            $filter_search = '';
			if(!filter_has_var(INPUT_GET,'search'))
			{
				$paginator = new NW_Pages_Paginator();
				$paginator->setItemsPerPage(6);
				$newsPages = $pageService->collection()->fetchAll($paginator, array("template" => "news", "status" => "active"));
				$news = $blockService->display('news-main', array("page" => $newsPages));
				$paginator->prepare();
			}
			else {
				$curPage = filter_has_var(INPUT_GET,'pag') ? filter_input(INPUT_GET, 'pag', FILTER_SANITIZE_NUMBER_INT) : 1;
                $filter_disciplines = filter_has_var(INPUT_GET,'disciplines') ? filter_input(INPUT_GET, 'disciplines', FILTER_SANITIZE_STRING) : null;
                $filter_search = filter_has_var(INPUT_GET,'search') ? filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING) : null;
                $curPage=($curPage != 0) ? $curPage : 1;
                $paginator = new NW_Pages_Paginator();
				$paginator->setItemsPerPage(6)
					->setCurPage($curPage);

                $collection = new Pages_Model_Collection();
                $collection->select('news', $display->getLang());

                if(!empty($filter_search))
                    $collection->mainblockContains($filter_search);
                if(!empty($filter_disciplines))
                    $collection->parentIn('disciplinenews', $filter_disciplines);

                $newsPages = $collection->collection($paginator);
                $news = $blockService->display('news-main', array("page" => $newsPages));
                $paginator->prepare();
			}
            $filter_disciplines_array  = empty($filter_disciplines) ? array() : explode(",", $filter_disciplines);
            break;
		case 'news-detail' :
			$curPage = $pageService->fromRequestSafe( "news" );
			$mainBlock = $blockService->displayMain($curPage->getId());
			$attachments = $blockService->displayByPage($curPage->getId(), 'common-attachments');
				
			$peoleTaxonomy = new Pages_Model_Taxonomy('newspeople', 'news', $curPage->getId());
			$peoplesCollection = $pageService->collection()->displayDependent($peoleTaxonomy, 'people');
			$peoplesBlocks = $blockService->display('peoples-main', array("page" => $peoplesCollection));
				
			$galleryBlocks = $blockService->displayByPage($curPage->getId(), 'common-gallery');
				
			$disciplinesTaxonomy = new Pages_Model_Taxonomy('disciplinenews', 'news', $curPage->getId());
			$disciplinesCollection = $pageService->collection()->displayParents($disciplinesTaxonomy, 'disciplines');
			//$disciplinesBlocks = $blockService->display('disciplines-main', array("id" => $disciplinesCollection));
            $disciplinesBlocks = $blockService->display('disciplines-main', array("page" => $disciplinesCollection));
				
			$disciplinesCatTaxonomy = new Pages_Model_Taxonomy('newsdiscat', 'news', $curPage->getId());
			$disciplinesCatCollection = $pageService->collection()->displayDependent($disciplinesCatTaxonomy, 'disciplines-cat');
			//$disciplinesCatBlocks = $blockService->display('disciplines-cat-main', array("id" => $disciplinesCatCollection));
            $disciplinesCatBlocks = $blockService->display('disciplines-cat-main', array("page" => $disciplinesCatCollection));
			break;
		case 'contacts':
			require_once('inc/tpls/contacts/tpl.php');
			$curPage = $pageService->displayFirst( "contacts" );
			$addresses = $blockService->displayByPage($curPage->getId(), 'contact_address');
			break;
        case 'static':
            $type = filter_has_var(INPUT_GET,'id') ? filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING) : 'terms_conditions';
            switch($type)
            {
                case 'terms_conditions':
                    $curPage = $pageService->displayFirst ( "terms_conditions" );
                    break;
                case 'privacy':
                    $curPage = $pageService->displayFirst ( "privacy" );
                    break;
                default:
                    $curPage = $pageService->displayFirst ( "terms_conditions" );
                    break;
            }

            $mainBlock = $blockService->displayByPage ( $curPage->getId (), 'static-main' );
            if(!empty($mainBlock)) $mainBlock = current($mainBlock);
            break;
        case 'careers':
            $paginator = NW_Pages_Paginator::fetchAll();
            $jobsPages = $pageService->collection()->fetchAll($paginator, array("template" => "jobs", "status" => "active"));
            $jobs = $blockService->display('jobs-main', array("page" => $jobsPages));
            break;
        case 'homepage-map':
            $curPage = $pageService->displayFirst ( "homepage" );
			$relatedContentsMain = $pageService->displayShared ( $curPage->getId (), array ("position" => 'body' ), true );
			$slides = $blockService->displayByPage ( $curPage->getId (), 'slideshow', array(), array("order" => "ASC"));
            //extract project having coordinates
            $gpsBlockPaginator = NW_Pages_Paginator::fetchAll();
            $projectsGPSBlocks= $blockService->fetchAll($gpsBlockPaginator, array("template" => "projects-latlang", "status" => "active"));
            $projectsHavingGps = array();
            foreach($projectsGPSBlocks as $block)
            {
                $projectsHavingGps[$block->getPage()] = array("id" => $block->getPage(), "coords" => $block->_("coords"), "link" => "project.php?id=".$block->getPage());
            }
            if(!empty($projectsHavingGps))
            {
                $mainCacheBlockService = new Pages_Model_Block_Main_Service();
                $results = $mainCacheBlockService->query($gpsBlockPaginator, array("page" => array_keys($projectsHavingGps)), array("page", "title" => "varchar1"));

                foreach($results as $res)
                    $projectsHavingGps[$res->page]["title"] = sprintf('<a href="project-detail.php?id=%d" class="gmaplink">%s</a>', $res->page, $res->title);
            }
            $projectsHavingGps = array_values($projectsHavingGps);
            break;
	}
} catch ( Exception $e ) {
	header("Location: /index.php");
	die($e->getMessage());
}

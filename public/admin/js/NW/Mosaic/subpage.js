Mosaic.SubPage = (function() {
    
	var items;
	var libraryController;

	SubPage = function() {
		this.items = new Array();
		this.libraryController = undefined;
	}

	SubPage.prototype = 
	{			
		tabs: function(customOptions, customSelector) {
			options = {
					beforeLoad: function( event, ui ) {
						ui.jqXHR.error(function() {
							ui.panel.html("Errore caricamento" );
						});
					}
				};
			var selector = (customSelector != undefined)  ? customSelector : "#tabs";
			if(customOptions != undefined)
				jQuery.extend(true, options, customOptions );
			$(selector).tabs(options);
			this.items.push(selector);
			return this;
		},
		setLibraryController: function(blockName, target)
		{
			this.libraryController = new LibraryController(blockName, target);
		},
		getLibraryController: function()
		{
			return this.libraryController;
		},
		galleryFromLocal:function(itemRef)
		{
			var ref = $(itemRef).parent().siblings(".previews");
			var blockName = ref.attr("rel");
			var targetId = ref.attr("id");
			this.setLibraryController(blockName, "#"+targetId);
			var href= '/app/media/addtolibrary/block/'+blockName+'/blockId/'+targetId;
			window.main_page.fancybox(href);
		},
		galleryFromRemote: function(itemRef)
		{
			var ref = $(itemRef).parent().siblings(".previews");
			var blockName = ref.attr("rel");
			var targetId = ref.attr("id");
			this.setLibraryController(blockName,"#"+targetId);
			var href= '/app/media/gallery/block/'+blockName+'/blockId/'+targetId;
			window.main_page.fancybox(href);
		},
		libraryFromLocal:function(itemRef)
		{
			var ref = $(itemRef).parent().siblings(".previews");
			var blockName = ref.attr("rel");
			var targetId = ref.attr("id");
			this.setLibraryController(blockName, "#"+targetId);
			var href= '/app/media/addtolibrary/block/'+blockName+'/blockId/'+targetId;
			window.main_page.fancybox(href);
		},
		libraryFromRemote: function(itemRef)
		{
			var ref = $(itemRef).parent().siblings(".previews");
			var blockName = ref.attr("rel");
			var targetId = ref.attr("id");
			this.setLibraryController(blockName,"#"+targetId);
			var href= '/app/media/library/block/'+blockName+'/blockId/'+targetId;
			window.main_page.fancybox(href);
		},
		modifyLibraryItem: function(itemRef, id, blockName)
		{
			var targetId = $(itemRef).closest(".picture");
			this.setLibraryController(blockName, targetId);
			var href= '/app/media/edit/id/'+id+'/blockName/'+blockName;
			window.main_page.fancybox(href);
			return false;
		},
		libraryItemLabel: function(itemId)
		{
			var href= '/app/media/label/id/'+itemId;
			window.main_page.fancybox(href);
			return false;
		},
		uploadItems: function(itemRef)
		{
			var targetId = $(itemRef).parent().siblings().find(".gallery");
			this.setLibraryController('', targetId);
			var href= '/app/media/uploaditems';
			window.main_page.fancybox(href);
			return false;
		},
		replace: function(target, html)
		{
			$(target).replaceWith(html);
		},
		append: function(target, html)
		{
			$(target).fadeOut(200).prepend(html).fadeIn(200);
		}
	}
	
	return {
		getNew: function()
		{
			var subpage = new SubPage();
			return subpage;
		}
	}
}());

var Mosaic_subpage = Mosaic.SubPage;


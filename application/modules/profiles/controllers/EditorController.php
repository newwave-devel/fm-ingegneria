<?php

class Profiles_EditorController extends NW_Controller_Action {

    public function init() {
        $user = Zend_Auth::getInstance()->getIdentity();
        $this->view->user = $user;
        if ($this->_helper->Role() != App_Model_Role::ROLE_EDITOR)
            $this->_redirect('/');

        $this->view->jQuery()
                ->addJavascriptFile('/js/app/dic.js')
                ->addJavascriptFile("/js/common/core.js")
                ->addJavascriptFile("/js/common/custom.js")
                ->addJavascriptFile('/js/app/App.js')
                ->addJavascriptFile('/js/admin/NW/NW.js')
                ->addJavascriptFile('/js/app/certificates/certificate.js')
                ->addJavascriptFile('/js/app/purchase/request.js')
                ->addJavascriptFile('/js/app/vessels/vessel.js')
                ->addJavascriptFile('/js/app/shipowners/shipowner.js')
                ->addJavascriptFile('/js/vendors/tablesorter/jquery.tablesorter.min.js')
                ->addJavascriptFile("/js/vendors/justGage.1.0.1/resources/js/justgage.1.0.1.min.js")
                ->addJavascriptFile("/js/vendors/justGage.1.0.1/resources/js/raphael.2.1.0.min.js");
                
        $this->_addOnload("mainmenu_selection();")
                ->_addOnload("menuTop();")
                ->_addOnload("fancybox();");
    }

    public function indexAction() {
    	try {
    	//	$form = new Dash_Form_ShipOwnerForm();
//     		$session = new Zend_Session_Namespace('viewbehaviour');
//     		$session->viewby = 'vessel';
//     		$session->queryItem = App_Model_Permission_Manager::getPermissions()->current();
    		/*
    		if ($this->getRequest()->isGet())
    		{
    		//	if (!$form->isValid($this->getRequest()->getQuery()))
    		//		$this->_redirect('/');
    	
    			$vesselId = $form->getValue("vessel");
    			//if i have a vessel, I override the query by shipowner
    			if (!empty($vesselId))
    			{
    				$this->view->vessel = $vesselId;
    				$session->viewby = 'vessel';
    				$session->queryItem = $vesselId;
    				$form->getElement('vessel')->setValue($vesselId);
    				if(!empty($shipOwnerId))
    					$form->getElement('shipOwner')->setValue($shipOwnerId);
    			}
    		}
    		*/
    		$this->_helper->layout()->setLayout('internal');
    		//$this->view->contextForm = $form;
    		$this->_asyncRequest("#main_content", "/dash/show/");
    	
    	} catch (Exception $e) {
    		throw $e;
    	}
//         try {
//             $session = new Zend_Session_Namespace('viewbehaviour');
//             $session->viewby = 'vessel';
//             $vesselId = App_Model_Permission_Manager::getPermissions()->current();
//             $session->queryItem = $vesselId;
//             $this->_helper->layout()->setLayout('internal');
//             $this->_asyncRequest("#main_content", "/dash/show/index");
//         } catch (Exception $e) {
//             throw $e;
//         }
    }

}


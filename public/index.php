<?php
$pageId = 'homepage';
include "inc/required.php";
include "inc/common/contents.php";
?>


<?php include "inc/common/head.php" ?>
		
        
        <!-- SCRIPT -->
		<script type="text/javascript" src="/common/js/core.js"></script>
        <script type="text/javascript" src="/common/js/jquery.nivo.slider.pack.js"></script>
        <script type="text/javascript" src="/common/js/custom.js"></script>
        
        
        
        <script type="text/javascript">	
                jQuery(window).load(function() {
                    // START
					loading();
					slideshow();
					fancybox();
                });
        </script>

	</head>    
    
	<body lang="it">
    
    	<div id="loader"></div>
    	    	
        <?php include "inc/common/header.php"?>
        
        
        <div id="menu_controller"></div>
        <section id="main_menu_panel">
			<?php include "inc/common/menu/panel.php"?>
        </section><!--main_menu_panel-->
        <?php include "inc/common/menu/main.php"?>
        
        <section id="main_page" class="home">
        	<section class="container">
            
            	<?php include "inc/common/slider.php"; ?>
                
                <?php include "inc/homepages/main-col.php"?>
                      
            </section><!--container-->
        </section><!--main_page-->
        
        <?php include "inc/common/footer.php"?>
		
	</body>
    
    
	
</html>
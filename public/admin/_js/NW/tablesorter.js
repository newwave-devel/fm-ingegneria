NW.Tablesorter = (function () {
    // Initialize the application
    var items = {};
    
    tablesorter = function(tableRef)
    {
        //the form that actually sends the request
       this.tableRef = tableRef;
       this.options = [];
    }
    
    tablesorter.prototype = 
    {
        sort : function()
        {
            $(this.tableRef).tablesorter(this.options);
            return this;
        },
        addOptions: function(optionkey, optionValue)
        {
            this.options[optionkey] = optionValue;
            return this;
        }
        
    }  
    // Return the public facing methods for the App
    return {
        register: function (tableRef) {
            items[tableRef] = new tablesorter(tableRef);
            return items[tableRef];
        },
        get: function(tableRef)
        {
            return items[tableRef];
        }     
    };
}());


var NW_Tablesorter = NW.Tablesorter;
<?php

class App_Helper_SwitchContextByUser extends Zend_Controller_Action_Helper_Abstract {
    
    public function direct($action = null)
    {
       $action = (empty ($action )) ? Zend_Controller_Front::getInstance()->getRequest()->getActionName() : $action;
       $helper = $this->getActionController()->getHelper('viewRenderer');
       switch(Zend_Auth::getInstance()->getStorage()->read()->getRole())
       { 	
	       	case App_Model_Role::ROLE_ADMINISTRATOR:
	       		$helper->setScriptAction('users/administrator/'.$action);
	            break;
	        case App_Model_Role::ROLE_EDITOR:
	        	$helper->setScriptAction('users/editor/'.$action);
	            break;
	        case App_Model_Role::ROLE_GUEST:
	        	$helper->setScriptAction('users/guest/'.$action);
	            break;
       }
    }
}

?>

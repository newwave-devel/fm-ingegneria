<?php

class App_Form_Asset extends Zend_Form {

	private $_block;
	private $_field;
	private $_blockId;
	private $_descriptor;
	
    public function init() {
    	$this->setMethod('post')
    		->setAttrib('id', 'file_upload')
    		->setAttrib('class', 'form')
    		->setAction("/app/media/upload");
    	$params =  $this->_fieldParams();
    	if($params)
    	{
    		$descriptor = (array) $this->_fieldParams()->params;
    		$file = new NW_Form_Element_FileUploader('file', array("id" => "file", "params" => $descriptor, "target" => $this->_blockId));	
    	}
    	else {
    		$file = new NW_Form_Element_FileUploader('file', array("id" => "file"));
    	}
    	$file->setRequired(true)
    		->setLabel("file");
    	$this->addElement($file);
    }
    public function setBlock($block)
    {
    	$this->_block = $block;
    }
    
    public function setBlockId($blockId)
    {
    	$this->_blockId = sprintf("#%s",$blockId);
    }
    public function setField($block)
    {
    	$this->_field = $block;
    }
    
    private function _fieldParams()
    {
    	try {
    	if(empty($this->_block) || empty($this->_field)) 
    		return false;
    	return NW_Pages_Block::getInstance()->field($this->_block, $this->_field);
    	} catch(Exception $e)
    	{
    		return false;
    	}
    }
}


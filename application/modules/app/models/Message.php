<?php
class App_Model_Message {
	
	protected $_id;
	protected $_body;
	protected $_status;
	protected $_tsCreate;
	protected $_tsSent;
	protected $_tsReceived;
	protected $_sender;
	protected $_type;
	protected $_recipients;
	protected $_subject;
	
	const TYPE_SMS = 'sms';
	const TYPE_EMAIL = 'email';
	
	const STATUS_NEW = 0;
	const STATUS_SENT = 1;
	const STATUS_RECEIVED = 2;
	
	public function __construct()
	{
		$this->_recipients = array();
		$this->_type = self::TYPE_EMAIL;
		$this->_status = self::STATUS_NEW;
		$this->_tsCreate = time();
		$this->_sender = -1;
		$this->_id = null;
	}
	
	 /**
	  * @return the unknown_type
	  */
	 public function getId() {
	  return $this->_id;
	 }
	 
	 /**
	  * @param unknown_type $_id
	  */
	 public function setId($_id) {
	  $this->_id = $_id;
	  return $this;
	 }
	 
	 /**
	  * @return the unknown_type
	  */
	 public function getBody() {
	  return $this->_body;
	 }
	 
	 /**
	  * @param unknown_type $_body
	  */
	 public function setBody($_body) {
	  $this->_body = $_body;
	  return $this;
	 }
	 
	 /**
	  * @return the unknown_type
	  */
	 public function getStatus() {
	  return $this->_status;
	 }
	 
	 /**
	  * @param unknown_type $_status
	  */
	 public function setStatus($_status) {
	  $this->_status = $_status;
	  return $this;
	 }
	 
	 /**
	  * @return the unknown_type
	  */
	 public function getTscreate() {
	  return $this->_tsCreate;
	 }
	 
	 /**
	  * @param unknown_type $_tsCreate
	  */
	 public function setTscreate($_tsCreate) {
	  $this->_tsCreate = $_tsCreate;
	  return $this;
	 }
	 
	 /**
	  * @return the unknown_type
	  */
	 public function getTssent() {
	  return $this->_tsSent;
	 }
	 
	 /**
	  * @param unknown_type $_tsSent
	  */
	 public function setTssent($_tsSent) {
	  $this->_tsSent = $_tsSent;
	  return $this;
	 }
	 
	 /**
	  * @return the unknown_type
	  */
	 public function getTsreceived() {
	  return $this->_tsReceived;
	 }
	 
	 /**
	  * @param unknown_type $_tsReceived
	  */
	 public function setTsreceived($_tsReceived) {
	  $this->_tsReceived = $_tsReceived;
	  return $this;
	 }
	 
	 /**
	  * @return the unknown_type
	  */
	 public function getSender() {
	  return $this->_sender;
	 }
	 
	 /**
	  * @param unknown_type $_sender
	  */
	 public function setSender($_sender) {
	  $this->_sender = $_sender;
	  return $this;
	 }
	 
	 /**
	  * @return the unknown_type
	  */
	 public function getType() {
	  return $this->_type;
	 }
	 
	 /**
	  * @param unknown_type $_type
	  */
	 public function setType($_type) {
	  $this->_type = $_type;
	  return $this;
	 }
	 
	 public function __set($name, $value) {
	 	$method = 'set' . $name;
	 	if (('mapper' == $name) || !method_exists($this, $method)) {
	 		throw new Exception('Invalid user property');
	 	}
	 	$this->$method($value);
	 }
	 
	 public function __get($name) {
	 	$method = 'get' . $name;
	 	if (('mapper' == $name) || !method_exists($this, $method)) {
	 		throw new Exception("Invalid user property '$name'");
	 	}
	 	return $this->$method();
	 }
	 
	 public function addRecipient(App_Model_Message_Recipient $r)
	 {
	 	$this->_recipients[] = $r;
	 	return $this;
	 }
	 
	 public function addRecipients($recipients, $sendAs=App_Model_Message_Recipient::SEND_AS_TO)
	 {
	 	foreach($recipients as $rcpt)
	 	{
	 		if($rcpt instanceof App_Model_User)
	 			$this->addRecipient(new App_Model_Message_Recipient($rcpt->getEmail(), $rcpt->getName(), $sendAs));
	 		else 
	 			$this->addRecipient($rcpt);
	 	}
	 	return $this;
	 }
	 
	 public function getRecipients($rcptAs = null)
	 {
	 	if(empty($rcptAs))
	 		return $this->_recipients;
	 	else 
	 	{
	 		$res = array();
	 		foreach($this->_recipients as $rcpt)
	 		{
	 			if($rcpt->getSendas() == $rcptAs)
	 				$res[] = $rcpt;
	 		}
	 		return $res;
	 	}
	 }
	 
	 public function hasRecipients($rcptAs = App_Model_Message_Recipient::SEND_AS_TO)
	 {
	 	if(empty($rcptAs))
	 		return !empty($this->_recipients);
	 	else
	 	{
	 		foreach($this->_recipients as $rcpt)
	 		{
	 			if($rcpt->getSendas() == $rcptAs)
	 				return true;
	 		}
	 		return false;
	 	}
	 }
	 
	 public function applyBodyTemplate()
	 {
	 	if(func_num_args() < 2) 
	 		$this->_body = '';
	 	else {
		 	$args = func_get_args();
		 	$tpl = trim($args[0]);
		 	unset($args[0]);
		 	$args[] = date("d/m/Y");
		 	$args[] = date("H:i:s");
		 	$this->_body = html_entity_decode(vsprintf($tpl, $args));
	 	}
	 }

	 /**
	  * @return the unknown_type
	  */
	 public function getSubject() {
	 	return $this->_subject;
	 }
 
 /**
  * @param unknown_type $_subject
  */
	 public function setSubject($_subject) {
	 	$this->_subject = $_subject;
	 	return $this;
	 }
 
 
	 public function getOptions()
	 {
	 	$opt = array();
	 	$opt['_body'] = $this->getBody();
	 	$opt['_status'] = $this->getStatus();
	 	$opt['_sender'] = $this->getSender();
	 	$opt['_type'] = $this->getType();
	 	$opt['_subject'] = $this->getSubject();
	 	$opt['_recipients'] = array();
	 	$opt['_id'] = $this->getId();
	 	foreach($this->getRecipients() as $recipient)
	 	{
	 		if($recipient instanceof App_Model_Message_Recipient)
		 		$opt['_recipients'][] = $recipient;
	 		 
	 	}
	 	return $opt;
	 }
	 
	 public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
      
        if(isset($options['to']))
        {
        	if(!is_array($options['to']))
        		$options["to"] = array($options["to"]);
        	foreach($options['to'] as $opt)
        		$this->addTo($opt);
        }
        if(isset($options['cc']))
        {
        	if(!is_array($options['cc']))
        		$options["cc"] = array($options["cc"]);
        	foreach($options['cc'] as $opt)
        		$this->addCc($opt);
        }
        if(isset($options['bcc']))
        {
        	if(!is_array($options['bcc']))
        		$options["bcc"] = array($options["bcc"]);
        	foreach($options['bcc'] as $opt)
        		$this->addBCc($opt);
        }
        return $this;
    }
    
    public function addTo($to)
    {
    	if(empty($to)) return;
    	if(!($to instanceof App_Model_Message_Recipient))
    		$to = new App_Model_Message_Recipient($to);
    	$this->addRecipient($to);
    }
    
    public function addCc($cc)
    {
    	if(empty($cc)) return;
    	if(!($cc instanceof App_Model_Message_Recipient))
    		$cc = new App_Model_Message_Recipient($cc, '', App_Model_Message_Recipient::SEND_AS_CC);
    	$this->addRecipient($cc);
    }
    
    public function addBCc($Bcc)
    {
    	if(empty($Bcc)) return;
    	if(!($Bcc instanceof App_Model_Message_Recipient))
    		$Bcc = new App_Model_Message_Recipient($Bcc, '', App_Model_Message_Recipient::SEND_AS_BCC);
    	$this->addRecipient($Bcc);
    }
}


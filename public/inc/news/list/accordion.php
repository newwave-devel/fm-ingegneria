<?php 
require_once("inc/tpls/news/tpl.php");

$project_list_accordion_html = '';

foreach($glob_disciplinesCat as $disCatBlock):
	$disCatName = $disCatBlock->_("title");
	$disCatId = $disCatBlock->getPage();
	
	$groupsTaxonomy = new Pages_Model_Taxonomy('disgroupcat', 'disciplines-cat', $disCatBlock->getId());
	$groupsCollection = $pageService->collection()->displayParents($groupsTaxonomy, 'disciplines-group');


    $all_is_selected = empty($filter_disciplines_array) ? "current" : '';
    $options = sprintf('<li><a class="filter disciplines %s" href="#" rel="">%s</a></li>', $all_is_selected, $translator->{"Pagina 'Progetti'"}->_t("All"));
    $active = false;
    foreach($groupsCollection as $groupId)
	{
		$disciplineTaxonomy = new Pages_Model_Taxonomy('disgroupdisc', 'disciplines-group', $groupId);
		$disciplineCollection = $pageService->collection()->displayParents($disciplineTaxonomy, 'disciplines');
		$disciplines = $blockService->display('disciplines-main', array('page' => $disciplineCollection));

		foreach($disciplines as $dis)
        {
            if(in_array($dis->getPage(), $filter_disciplines_array))
            {
                $active = true;
                $options .= sprintf('<li><a class="filter disciplines current" href="#" rel="%d">%s</a></li>', $dis->getPage(), $dis->_("title"));
            }
            else
                $options .= sprintf('<li><a class="filter disciplines" href="#" rel="%d">%s</a></li>', $dis->getPage(), $dis->_("title"));
        }
	}
    $class_active = ($active) ? 'active' : '';
    $class_opened = ($active)  ? 'open' : '';
	$project_list_accordion_html .= sprintf($tpl_news_accordion, $class_active, $disCatName, $class_opened, $options);
endforeach;
	
?>
<div id="accordion">
	
	<?php echo $project_list_accordion_html ?>

</div>
<!--/#accordion-->
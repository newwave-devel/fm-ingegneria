<?php

/**
 * @author Ale
 * used only for storing values from form to db
 */
class Pages_Model_Taxonomy_DTO
{

    public $values;

    public $taxonomyName;

    public $invokerRole;

    public $invokerId;

    public function __construct($options=array())
    {
        $this->taxonomyName = isset($options['taxonomy']) ? $options['taxonomy'] : '';
        $this->invokerRole = isset($options['role']) ? $options['role'] : '';
        $this->invokerId = isset($options['pageid']) ? $options['pageid'] : '';
        $this->values = isset($options['related']) ? $options['related'] : '';
    }

    public function getValues()
    {
        return $this->values;
    }

    public function setValues($values)
    {
        $this->values = $values;
        return $this;
    }

    public function getTaxonomyName()
    {
        return $this->taxonomyName;
    }

    public function setTaxonomyName($taxonomyName)
    {
        $this->taxonomyName = $taxonomyName;
        return $this;
    }

    public function getInvokerRole()
    {
        return $this->invokerRole;
    }

    public function setInvokerRole($invokerRole)
    {
        $this->invokerRole = $invokerRole;
        return $this;
    }

    public function getInvokerId()
    {
        return $this->invokerId;
    }

    public function setInvokerId($invokerId)
    {
        $this->invokerId = $invokerId;
        return $this;
    }

    public function addValue($val)
    {
        $this->values[] = $val;
    }
    
}
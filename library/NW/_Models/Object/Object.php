<?php

class NW_Models_Object_Object {

    /**
     *
     * Item id
     * @var int
     */
    protected $_id;

    /**
     *
     * ClassUri
     * @var string
     */
    protected $_classUri;

    /**
     * untranslated content
     * @var array 
     */
    protected $_contents;
    
    /**
     * Database Adapter
     * @var type 
     */
    protected $adapter;
    
    /**
     * object mapper to build query
     * @var type 
     */
    private $mapper;

    public function __construct($classUri) {
        try {
            $this->_classUri = ($classUri instanceof NW_ClassURI) ? $classUri : NW_ClassURI::fromString($classUri);
            $this->_contents = new stdClass();
            $this->_id = '-1';
            $this->mapper = new NW_Models_Object_Mapper($this);
            foreach ($this->mapper->getFieldsName() as $fieldName)
                $this->_contents->$fieldName = '';
            $this->initAdapter();
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function setId($id) {
        $this->_id = $id;
        return $this;
    }

    public function getId() {
        return $this->_id;
    }

    public function getAdapter() {
        return $this->adapter;
    }
    
    public function getMapper()
    {
        return $this->mapper;
    }

    public function __get($name) {
        if ($name == 'id')
            return $this->getId();
        if($name == 'classUri')
            return $this->getClassUri ();
        
        if (isset($this->_contents->$name))
            return $this->_contents->$name;
    }
    
    public function __set($name, $value) {
        if ($name == 'id')
             $this->setId($value);
        if($name == 'classUri')
             $this->setClassUri ($value);
        
        if (isset($this->_contents->$name))
             $this->_contents->$name = $value;
        return $this;
    }

    public function getClassUri() {
        return $this->_classUri;
    }

    public function doInsert($object) {
        try {
           $query = $this->getMapper()->queryInsert($object);
           $this->getAdapter()->performInsertQuery($query);
           print "<li>".$query->getQuery()."</li>";
           return $object;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function doUpdate(&$object) {
        try {
            $query = $this->getMapper()->queryUpdate($object);
            $this->getAdapter()->performUpdateQuery($query);
            print "<li>".$query->getQuery()."</li>";
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function doLoadOne($params) {
        try {
            return $this->getMapper()->queryLoad($params);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function doLoadAll($params, NW_Pages_Paginator &$paginator) {
        try {
           return $this->getMapper()->queryAll($params, $paginator);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function doView($viewName, $params,  &$paginator) {
        try {
           return $this->getMapper()->queryView($viewName, $params, $paginator);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function doLoadByRelDefInTable(NW_Db_Query_Select &$query, $relationName, $view, $callerTable)
    {
        try {
            $this->getMapper()->queryLoadByRelDefInTable($query, $relationName, $view, $callerTable);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function getMainTable()
    {
        return $this->getMapper()->getTable();
    }

    

    protected function initAdapter() {
        
        $connectionName = NW_Application::getSingleton('app/NW_ModuleManager')
                                    ->getModuleDescriptor($this->getClassUri())
                                        ->connectionByName($this->getClassUri(), "default");
        $this->adapter = NW_Application::getSingleton($connectionName);
    }
    
    public function export() {
        return $this->_contents;
    }
    
//    public function toStdClass()
//    {
//        $stdClass = new stdClass();
//        $stdClass->id = $this->getId();
//        $stdClass->classUri = $this->getClassUri();
//        foreach($this->_contents as $k => $v)
//            $stdClass->$k = $v;
//        return $stdClass;
//    }
    
    public function formFields()
    {
        $fields = array();
        $fields[] = new NW_Form_Element_Hidden("id");
        $fields[] = new NW_Form_Element_Hidden("class");
        return $fields;
    }
    
    public function classDescriptor()
    {
        return NW_Application::getSingleton('app/NW_ModuleManager')
                                        ->classDescriptor($this->getClassUri());
    }
    
    public function moduleDescriptor()
    {
        return NW_Application::getSingleton('app/NW_ModuleManager')
                            ->getModuleDescriptor($this->getClassUri());
    }
    
    public function cloneNew(&$object)
    {
        $object->id = $this->getId();
        $object->class = $this->getClassUri();
        
        foreach($this->_contents as $fName => $fValue)
            $object->$fName = $fValue;
        return $object;
    }

}

?>
<?php

class NW_System_File {
    
    const DEFAULT_PERM = 0777;

    public static function exists($path) {
        return file_exists($path);
    }

    public static function getDir($path) {
        return pathinfo($path, PATHINFO_DIRNAME);
    }

    public static function getExtension($path) {
        return pathinfo($path, PATHINFO_EXTENSION);
    }

    public static function dirExists($path) {
        return is_dir($path);
    }

    public static function createDir($path, $mode = self::DEFAULT_PERM) {
        try {
            if (!self::canWrite($path))
                throw new Exception("Dir $path is not writable");
            mkdir($path, $mode, true);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public static function canWrite($path)
    {
        return is_writable($path);
    }

    public static function put($path, $data) {
        try {
            $dir = self::getDir($path);
            if (!self::dirExists($dir)) {
                self::createDir($dir);
            }
            if(!self::canWrite($dir)) throw new Exception("cannot write file $path");
            return file_put_contents($path, $data, LOCK_EX);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public static function get($path) {
        try {
            if (!file_exists($path))
                throw new Exception("File $path not found");
            return file_get_contents($path);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public static function getOnLine($path) {
        try {
            return file_get_contents($path);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public static function append($path, $data) {
        try {
            if (!file_exists($path))
                throw new Exception("File $path not found");
            return file_put_contents($path, $data, LOCK_EX | FILE_APPEND);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public static function move($path, $target) {
        try {
            if (!file_exists($path))
                throw new Exception("File $path not found");
            return rename($path, $target);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public static function copy($src, $dst, $dstPerm = false) {
        try {
            if (!file_exists($src))
                throw new Exception("File $src not found");
            if(!$dstPerm)
            	return copy($src, $dst);
            $res = copy($src, $dst);
            if($res) return chmod($dst,$dstPerm);
            return false;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public static function size($path) {
        try {
            if (!file_exists($path))
                throw new Exception("File $path not found");
            return filesize($path);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public static function mkdir($path, $chmod = self::DEFAULT_PERM) {
        return (is_dir($path)) ? true : mkdir($path, $chmod, true);
    }

    public static function rmDir($path)
    {
    	if (is_dir($path) === true)
    	{
    		$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::CHILD_FIRST);    
    		foreach ($files as $file)
    		{
    			if (in_array($file->getBasename(), array('.', '..')) !== true)
    			{
    				if ($file->isDir() === true)
    					rmdir($file->getPathName());
    				else if (($file->isFile() === true) || ($file->isLink() === true))
    					unlink($file->getPathname());
    			}
    		}
    		return rmdir($path);
    	}
    	else if ((is_file($path) === true) || (is_link($path) === true))
    		return unlink($path);
    	return false;
    }
}

?>

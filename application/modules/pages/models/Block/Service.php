<?php
class Pages_Model_Block_Service
{
	protected $_mapper;

    public function getMapper() {
        return $this->_mapper;
    }

    public function __construct() {
        $this->_mapper = new Pages_Model_Block_Mapper();
    }
	
	public function find($paramID)
	{
		try{
			$model = new Pages_Model_Block();
			$this->_mapper->find($paramID, $model);
			return $model;
		} catch(Exception $e)
		{
			
		}
	}
	
	public function load($id, $lang)
	{
		
	}
	
	public function fetchAll(NW_Pages_Paginator &$paginator, $paramsSearch = array(), $paramsOrder = array(), $load = false)
	{
		try 
		{
			return $this->_mapper->fetchAll($paginator, $paramsSearch, $paramsOrder, $load);
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function save(Pages_Model_Block $block)
	{
		try {
			return $this->_mapper->save($block);
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	public function delete($id)
	{
	    try {
	        $this->getMapper()->delete($id);
	        return true;
	    } catch (Exception $e) 
	    {
	        throw $e;
	    }
	}
	
	public function byPage($pageId, NW_Pages_Paginator &$paginator=null, $paramsSearch = array(), $paramsOrder = array(), $load = false)
	{
		try{
			if(empty($paginator))
				$paginator = NW_Pages_Paginator::fetchAll();
			$paramsSearch['page'] = $pageId;
			$result = $this->fetchAll($paginator,$paramsSearch,$paramsOrder,$load);
			if($paginator->itemsPerPage==1 )
			   return (!empty($result)) ? current($result) : array();
			return $result;
			    
		}catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function assets($assetsIds, NW_Pages_Paginator &$paginator = null)
	{
		if(empty($assetsIds)) return array();
		if(strpos($assetsIds, ",") != false) $assetsIds = explode(",", $assetsIds);
		$paginator = (empty($paginator)) ? NW_Pages_Paginator::fetchAll() : $paginator;
		$assetService = new App_Model_Asset_Service();
		$ids = array($assetsIds) ? $assetsIds : array($assetsIds);
		return $assetService->fetchAll($paginator, array("ids" => $ids));
	}
	
	public function asset($assetsId)
	{
		$assetService = new App_Model_Asset_Service();
		return $assetService->find($assetsId);
	}
	
	public function displayByPage($pageId, $template, $paramsSearch = array(), $paramsOrder = array())
	{
		try{
			$paginator = NW_Pages_Paginator::fetchAll();
			$paramsSearch['page'] = $pageId;
			$paramsSearch['template'] = $template;
			$paramsSearch['status'] = 'active';
			$result = $this->fetchAll($paginator,$paramsSearch, $paramsOrder);
			if($paginator->itemsPerPage==1 )
				return (!empty($result)) ? current($result) : array();
			return $result;
			 
		}catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function displayMain($pageId)
	{
		try{
			$paginator = NW_Pages_Paginator::fetchSingle();
			$paramsSearch['page'] = $pageId;
			$paramsSearch['status'] = 'active';
			$result = $this->fetchAll($paginator,$paramsSearch);
			if($paginator->itemsPerPage==1 )
				return (!empty($result)) ? current($result) : array();
			return $result;
	
		}catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function display($template, $paramsSearch = array())
	{
		try
		{
			$paramsSearch['template'] = $template;
			$paramsSearch['status'] = 'active';
			
			$paginator = NW_Pages_Paginator::fetchAll();
			return $this->fetchAll($paginator, $paramsSearch);
		} catch(Exception $e)
		{
			throw $e;
		}
	}

    public function updateOrder($pageId, $newOrder)
    {
        try {
           $this->_mapper->updateOrder($pageId, $newOrder);
            return true;
        } catch(Exception $e)
        {
            throw $e;
        }
    }
}
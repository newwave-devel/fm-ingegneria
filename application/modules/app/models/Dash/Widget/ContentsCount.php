<?php

/**
 * Description of Portfolio
 *
 * @author Ale
 */
class App_Model_Dash_Widget_ContentsCount extends App_Model_Dash_Widget {

    private $type;
    private $rows;
    private $titles;

    const TYPE_HEADER = 'header';
    const TYPE_NOHEADER = 'noheader';

    

    public function __construct($options) {
        $this->boxTitle = isset($options[static::BOXTITLE]) ? $options[static::BOXTITLE] : "Contents";
        $this->boxPosition = isset($options[static::BOXPOSITION]) ? $options[static::BOXPOSITION] : '';
        $this->type = isset($options['type']) ? $options['type'] : static::TYPE_NOHEADER;
        $this->rows = isset($options['rows']) ? $options['rows'] : array();
        $this->titles = array("left" => '', "right" => '');
        if(empty($this->rows) && $this->type == static::TYPE_HEADER)
            $this->rows = array("left" => array(), "right" => array());
        return $this;
    }
    
    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
        return $this;
    }

    public function getRows() {
        return $this->rows;
    }

    public function setRows($rows) {
        $this->rows = $rows;
        return $this;
    }

    public function getTitles() {
        return $this->titles;
    }

    public function setTitles($titles) {
        $this->titles = $titles;
        return $this;
    }

    public function addValue($label, $value,$pos = '') {
        if($this->type == static::TYPE_NOHEADER)
            $this->rows[$label] = $value;
        else $this->rows[$pos][$label] = $value;
        return $this;
    }

    public function render() {
        if ($this->type == static::TYPE_NOHEADER) {
            $half = count($this->rows) / 2;
            $left = array_slice($this->rows, 0, $half);
            $right = array_slice($this->rows, $half);
            return $this->getHtml($left, $right);
        }
        else
            return $this->getHtml($this->rows['left'], $this->rows['right'], $this->titles['left'], $this->titles['right']);
    }

    public function getHtml($left, $right, $maintitleleft = '', $maintitleright = '') {

        $table = '';

        $content = '';
        foreach ($left as $label => $value)
            $content .= sprintf("<li><h4>%s</h4>%s</li>", $value, ucfirst($label));
        
        foreach ($right as $label => $value)
            $content .= sprintf("<li><h4>%s</h4>%s</li>", $value, ucfirst($label));
        
        $this->boxContent = $content;
        return $this;
    }

}

?>

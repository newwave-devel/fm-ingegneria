<?php

class Pages_Form_Block extends Zend_Form {
	
    public function init() 
    {
       
        $this->setMethod('post')
                ->setAttrib('class', 'form')
                ->setAttrib('id', "form_".$this->getAttrib("id"))
                ->setAction("/pages/block/save");
        
        $lang = new NW_Form_Element_Hidden('lang');
        $lang->setFilters(array('StringTrim'))
                ->setRequired(true);
        $id = new NW_Form_Element_Hidden('id', array("class" => "id"));
        $id->addValidator('Digits',false)
        		->setRequired(true);
        $template = new NW_Form_Element_Hidden('template');
        
        $template->setRequired(true);
        $status = new NW_Form_Element_Select('status');
        $status->setRequired(true)
        	->setLabel("Status")
        	->addMultiOption(Pages_Model_Block::STATUS_ACTIVE, "Active")
        	->addMultiOption(Pages_Model_Block::STATUS_DRAFT, "Draft")
        	->addMultiOption("", "Please Select One");
        
        $page = new NW_Form_Element_Hidden('page');

        $this->addElements(array($lang, $id, $template, $page, $status));
    }

    public function populate(array $values) 
    {    
        $AdaptedValues['lang'] = $values["_lang"];
        $AdaptedValues['id'] = $values["_id"];
        $AdaptedValues['template'] = $values["_template"];
        $AdaptedValues['page'] = $values["_page"];
        $AdaptedValues['status'] = $values["_status"];

        $others = (array)$values['_contents'];
        $all = array_merge($AdaptedValues, $others);
        parent::populate($all);
    }

    public function onLoadOptions($randId)
    {
        $js = <<<TPL
        $("#%s").ajaxForm(
                        {
                            success: currentPage.saveBlock,
                            beforeSerialize:    function()
                                                { 
                                                    if (typeof CKEDITOR != 'undefined') 
                                                    { 
                                                        for( instance in CKEDITOR.instances ) 
                                                        CKEDITOR.instances[instance].updateElement();
                                                    }
                                                }, 
                            dataType: "json" 
                        });
        
TPL;
        return sprintf($js, $randId);
    }
}


<?php
class App_Model_TransactionalEmail_Service extends App_Model_Message_Service
{
	const ERROR_NO_RECIPIENTS = 10;
	const ERROR_NO_BODY = 20;
	const ERROR_NO_SUBJECT = 30;
	
	private $_mapper;
	
	public function __construct()
	{
		parent::__construct();
		$this->_mapper = new App_Model_TransactionalEmail_Mapper();
	}

	
	public function queue(App_Model_TransactionalEmail $message)
	{
		try {
			$messageId = parent::queue($message);			
			$message->setId($messageId);
			$this->_mapper->save($message);
			return $messageId;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function modelHasMessages($model, $id)
	{
		try {
			$params = array("model" => $model, 'item' => $id);
		    $count = $this->_mapper->count(null, $params);
		    return $count != 0;
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	public function fetchAll(NW_Pages_Paginator &$paginator, $paramsSearch=array(), $sortBy = array())
	{
		try{
			return $this->_mapper->fetchAll($paginator,$paramsSearch, $sortBy, true);
		} catch(Exception $e) {
			throw $e;
		}
	}
	
	public function incoming(NW_Pages_Paginator &$paginator, $paramsSearch=array(), $sortBy = array())
	{
		try{
			return $this->_mapper->fetchAll($paginator,$paramsSearch, $sortBy, true);
		} catch(Exception $e) {
			throw $e;
		}
	}
	
	public function sent(NW_Pages_Paginator &$paginator, $paramsSearch=array(), $sortBy = array())
	{
		try{
			return $this->_mapper->fetchAll($paginator,$paramsSearch, $sortBy, true);
		} catch(Exception $e) {
			throw $e;
		}
	}
	
	public function delete($paramId)
	{
		try {
			return $this->_mapper->delete(array("id=?"=> $paramId));
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	
	public function deleteMessage($paramMessageId)
	{
		try {
			return $this->_mapper->delete(array("message = ?"=> $paramMessageId));
		} catch(Exception $e)
		{	
			throw $e;
		}
	}
	public function load($paramId)
	{
		try {
			$message = new App_Model_Message();
			$this->_mapper->load($paramId, $message);
			return $message;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	protected function getMapper()
	{
		return $this->_mapper;
	}
	

	
	
}
?>
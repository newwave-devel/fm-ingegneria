function message(messageId)
{
    this.messageId = messageId;
}
message.requeue = function(messageId, itemRef)
{
	$.post("/app/mailbox/requeue/", {paramId: messageId})
		.done(function(data)
			{
				var response = JSON.parse(data);
				if(response.status=='ok')
					$('#tabs').tabs('load', 1);
				else alert("There has been some errors processing the request");
				
		})
		.fail(function(){alert("There has been some errors processing the request");});
}
function mailbox()
{
}

mailbox.dropIncoming = function(messageId)
{
	$.post("/app/mailbox/delete/", {paramId: messageId})
		.done(function(data){
			var response = JSON.parse(data);
			if(response.status=='ok')
				$('#tabs').tabs('load', 0);
			else alert("There has been some errors processing the request");
		})
    	//.done(function(data){ $('#list', parent.document).html(data);} )
    	.fail(function(){alert("There has been some errors processing the request");});
}
mailbox.dropSent = function(messageId)
{
	$.post("/app/mailbox/delete/", {paramId: messageId})
		.done(function(data){
			var response = JSON.parse(data);
			if(response.status=='ok')
				$('#tabs').tabs('load', 1);
			else alert("There has been some errors processing the request");
		})
    	//.done(function(data){ $('#list', parent.document).html(data);} )
    	.fail(function(){alert("There has been some errors processing the request");});
}
mailbox.updateMail=function()
{
	parent.$.fancybox.close();
	$('#tabs').tabs('load', 1);
}

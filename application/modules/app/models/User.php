<?php

class App_Model_User implements Zend_Acl_Role_Interface {

    protected $_id;
    protected $_name;
    protected $_username;
    protected $_password;
    protected $_salt;
    protected $_date_created;
    protected $_role;
    protected $_pid;
    protected $_status;
    protected $_email;
    protected $_activation_code;

    const STATUS_ENABLED = 'active';
    const STATUS_DISABLED = 'disabled';
    const STATUS_AWAITING = 'awaiting';

    public function __construct(array $options = null) {
        $this->_date_created = date("Y-m-d h:i:s");
        $this->_id = null;
        $this->status = static::STATUS_DISABLED;
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid user property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid user property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $id;
        return $this;
    }

    public function getName() {
        return $this->_name;
    }

    public function setName($name) {
        $this->_name = $name;
        return $this;
    }

    public function getUsername() {
        return $this->_username;
    }

    public function setUsername($username) {
        $this->_username = $username;
        return $this;
    }

    public function getPassword() {
        return $this->_password;
    }

    public function setPassword($password) {
        $this->_password = $password;
        return $this;
    }

    public function getActivationCode() {
        return $this->_activation_code;
    }

    public function setActivationCode($activationcode) {
        $this->_activation_code = $activationcode;
        return $this;
    }

    public function getEmail() {
        return $this->_email;
    }

    public function setEmail($email) {
        $this->_email = $email;
        return $this;
    }

    public function getStatus() {
        return $this->_status;
    }

    public function setStatus($status) {
        $this->_status = $status;
        return $this;
    }

    public function getSalt() {
        return $this->_salt;
    }

    public function setSalt($salt) {
        $this->_salt = $salt;
        return $this;
    }

    public function getDate_created() {
        return $this->_date_created;
    }

    public function setDate_created($date_created) {
        $this->_date_created = $date_created;
        return $this;
    }

    public function getRole() {
        return $this->_role;
    }

    public function setRole($role) {
        $this->_role = $role;
        return $this;
    }

    public function getPid() {
        return $this->_pid;
    }

    public function setPid($pid) {
        $this->_pid = $pid;
        return $this;
    }

    public function toggleStatus() {
        if ($this->getStatus() == static::STATUS_ENABLED)
            $this->setStatus(static::STATUS_DISABLED);
        else
            $this->setStatus(static::STATUS_ENABLED);
        return $this;
    }

    public function checkPassword($givenPassword)
    {
    	return (sha1($givenPassword.$this->_salt) == $this->getPassword());
    }

    public function assignNewPassword($pass) {
        $this->_salt = sha1(time());
        $this->_password = sha1($pass . $this->_salt);
        return $this;
    }

    public function generateActivationCode() {
        $this->_activation_code = sha1($this->getEmail() . time() . $this->getPassword());
        return $this;
    }

    public static function getStatusArray() {
        return array(static::STATUS_AWAITING => "In attesa di conferma", static::STATUS_DISABLED => "Disattivo", static::STATUS_ENABLED => "Attivo");
    }
	/* (non-PHPdoc)
	 * @see Zend_Acl_Role_Interface::getRoleId()
	 */
	public function getRoleId() {
		// TODO Auto-generated method stub
		
	}
	
	public function getOptions()
	{
		return get_object_vars($this);
	}


}


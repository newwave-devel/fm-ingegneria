<?php

class App_Model_Notification_Mapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('App_Model_DbTable_Notification');
        }
        return $this->_dbTable;
    }

    public function fetchAll(NW_Pages_Paginator &$paginator, $paramsSearch = array(), $paramsOrder = array(), $load=false) {
        try {

            $query = $this->getDbTable()->select();
            
            if(!empty($paramsSearch))
            	$this->_filterBySearchParam($select, $paramsSearch);
			if(!$paginator->getFetchAll())
            	$select->limit($paginator->itemsPerPage, $paginator->getStart());
			if(!empty($paramsOrder))
			{
				foreach($paramsOrder as $ordp => $ordd)
					$select->order(sprintf("%s %s", $ordp, $ordd));
			}
            $entries = array();

            $paginator->itemsCount = $this->getDbTable()->count($select);
            
            foreach ($this->getDbTable()->fetchAll($select) as $row) {
                $model = new App_Model_Notification();
                $this->_fromDb($model, $row);
                $entries[] = $model;
            }
            return $entries;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function find($id, App_Model_Notification &$asset) {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $this->_fromDb($asset, $row);          
    }



    public function save(App_Model_Notification $notification) {
        try {
        	
            $data = $this->_toDb($notification);
            
            if ('' == ($id = $notification->getId())) {
                unset($data['id']);
                return $this->getDbTable()->insert($data);
            } else {
                $this->getDbTable()->update($data, array('id = ?' => $id));
            }
        } catch (Exception $e) {
            throw $e;
        }
    }
    public function count(Zend_Db_Select $select) {
    	try {
    		$table = $this->getDbTable ();
    		$select->reset ( Zend_Db_Select::COLUMNS )->reset ( Zend_Db_Select::LIMIT_COUNT )->reset ( Zend_Db_Select::LIMIT_OFFSET )->columns ( array (
    				'num' => new Zend_Db_Expr ( 'COUNT(*)' )
    		) );
    		return $table->fetchRow ( $select )->num;
    	} catch ( Exception $e ) {
    		throw $e;
    	}
    }


    public function delete($id) {
        try {
            $where = $this->getDbTable()->getAdapter()->quoteInto("id=?", $id);
            $this->getDbTable()->delete($where);
            return true;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public static function _fromDb(App_Model_Notification &$obj, $map) {
        
    	$read = ($map['read'] == 'Y');
    	$ts = (!empty($map['ts'])) ? NW_Utils_DateTime::strptime($map['ts'], "%Y-%m-%d %H:%M:%S") : time();
        $obj->setTo($map['to'])
                ->setId($map['id'])
                ->setMessage($map['message'])
                ->setPriority($map['priority'])
                ->setRead($read)
                ->setTs($ts);
    }

    public function _toDb(App_Model_Notification $obj) {
        $map = array(
            'to' => $obj->getTo(),
            'ts' =>  strftime("%Y-%m-%d %H:%M:%S",$obj->getTs()),
            'read' =>  ($obj->getRead()) ? 'Y' : 'N',
            'message' =>  $obj->getMessage(),
            'priority' =>  $obj->getPriority(),
            'id' =>  $obj->getId(),
         );
        return $map;
    }
    
    private function _filterBySearchParam(Zend_Db_Select &$select, $paramsSearch = array()) {
    	if (!empty($paramsSearch)) {   		 
    		if (!empty($paramsSearch['to']))
    			$select->where("to = ?", $paramsSearch['to']);
    		if (!empty($paramsSearch['read']))
    			$select->where("read = ?", $paramsSearch['read']);
    		if (!empty($paramsSearch['priority']))
    			$select->where("priority = ?", $paramsSearch['priority']);
    	}
    }

}

?>

<?php

class App_Form_I18n_Item extends Zend_Form {

    protected $_format;
    protected $_siblings;

    public function init() {
    	$this->setMethod('post')
    		    ->setAction("/app/i18n/update")
            ->setAttrib("class", "form");
    	
    	$key = new NW_Form_Element_Hidden("key");
        $value = new NW_Form_Element_Text("value");
        switch($this->_format)
        {
            case App_Model_I18n::FORMAT_WYSIWYG:
                $value = new NW_Form_Element_CKEditor("value");
                break;
            case App_Model_I18n::FORMAT_TEXTAREA:
                $value = new NW_Form_Element_Textarea("value");
                break;
        }

        $value -> setLabel("Valore Tradotto")
        			->setRequired(false);

        $lang= new NW_Form_Element_Hidden("lang");

        if($this->_siblings):
            $siblings = new NW_Form_Element_Select('siblings');
            $siblings->setAttrib("onchange","parent.window.main_page.getTranslationController().translateSibling(this.value);")
                ->setLabel("Switch to")
                ->setRequired(false);
            foreach($this->_siblings as $sib)
                $siblings->addMultiOption($sib->getId(), $sib->getLang());
        else:
            $siblings = new NW_Form_Element_Hidden('siblings');
        endif;
        $id= new NW_Form_Element_Hidden("id");
        $context= new NW_Form_Element_Hidden("context");
        $format= new NW_Form_Element_Hidden("format");

        $this->addElements(array($key,$value,$siblings, $lang,$id, $context, $format));
    }

    public function setFormat($_format)
    {
        $this->_format = $_format;
    }

    public function setSiblings($siblingItems)
    {
        $this->_siblings = $siblingItems;
    }

    public function populate(array $values) 
    {

        $AdaptedValues['key'] = $values["_key"];
        $AdaptedValues['value'] = $values["_value"];
        $AdaptedValues['context'] = $values["_context"];
        $AdaptedValues['format'] = $values["_format"];
        $AdaptedValues['lang'] = $values["_lang"];
        $AdaptedValues['id'] = $values["_id"];

        parent::populate($AdaptedValues);
    }

}


<?php

class App_Model_TransactionalEmail extends App_Model_Message
{
	protected $_item;
	protected $_model;
	
	public function __construct($item=null, $model = null)
	{
		parent::__construct();
		if(!empty($item))
			$this->_item = $item;
		if(!empty($model))
			$this->_model = $model;
	}  
	
	public function __set($name, $value) {
		$method = 'set' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid property: ' . $name);
		}
		$this->$method($value);
	}
	
	public function __get($name) {
		$method = 'get' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid property: '. $name);
		}
		return $this->$method();
	}

	public function setItem($item)
	{
		$this->_item = $item;
		return $this;
	}
	public function getItem()
	{
		return $this->_item;
	}
	
	public function setModel($model)
	{
		$this->_model = $model;
		return $this;
	}
	public function getModel()
	{
		return $this->_model;
	}
}

?>
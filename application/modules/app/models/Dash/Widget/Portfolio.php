<?php

/**
 * Description of Portfolio
 *
 * @author Ale
 */
class App_Model_Dash_Widget_Portfolio extends App_Model_Dash_Widget {

    private $_REMOTE_URI = 'http://www.newwave-media.it/whats_new.html';
    private $_CACHED_POS = '/assets/portfolio.cached.html';

    public function __construct($options) {
        $this->boxTitle = isset($options[static::BOXTITLE]) ? $options[static::BOXTITLE] : "What's new in NewWave";
        $this->boxPosition = isset($options[static::BOXPOSITION]) ? $options[static::BOXPOSITION] : '';
        $this->boxContent = $this->fetchContent();
        return $this;
    }

    public function fetchContent() {
        try {
            $content = file_get_contents($this->_REMOTE_URI);
            $cache = PUBLIC_PATH . $this->_CACHED_POS;
            if (empty($content) || $content === false) {
                if (NW_System_File::exists($cache))
                    return NW_System_File::get($cache);
                else throw new Exception("Sorry, no content available now!");
            }
            else {
                NW_System_File::put($cache, $content);
                return $content;
            }
        } catch (Exception $e) {
            $content = $e->getMessage();
            return $content;
        }
    }

}

?>

<?php

class Application_Form_Login extends NW_Form
{

    public function init()
    {
         
        $this->setMethod('post')
                ->setAttrib('class', 'form login')
                ->setAttrib('id', 'form')
                ->setAction("login");
        
        $username = new NW_Form_Element_Text('username');
        $username->setLabel('Username')
                ->setRequired(true)
                ->setFilters(array('StringTrim', 'StringToLower'))
                ->setValidators(array('Alpha',array('StringLength', false, array(3, 20))));
        
       $password = new NW_Form_Element_Password('password');
       $password->setLabel('Password')
                ->setRequired(true)
                ->setFilters(array('StringTrim'))
                ->setValidators(array('Alnum',array('StringLength', false, array(6, 20))));              
        $submit = new NW_Form_Element_Submit('submit');
        $submit->setAttrib('class', 'submit_btn')
                ->setValue("Login");
        $clearer = new NW_Form_Element_Html('clearer');
        $clearer->setClearer();
        
        $this->addElement($username)
             ->addElement($password)
                ->addElement($submit)
                ->addElement($clearer);
            
    }


}


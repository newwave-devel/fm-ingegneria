<?php

/**
 * Description of Mapper
 *
 * @author Ale
 */
class NW_Tags_Mapper {

    private $taggedType;

    public function __construct($ttype) {
        $this->taggedType = $ttype;
    }

    public function listTags(NW_Pages_Paginator &$paginator, $params = array()) {
        try {
            $query = new NW_Db_Query_Select();
            $query->select(array('name', 'count(1) as howmany'))
                    ->from(NW_Tags_Tag::TABLE)
                    ->innerJoinOn(NW_Media_Asset::SQL_TABLE, NW_Media_Asset::SQL_TABLE . ".id = " . NW_Tags_Tag::TABLE . ".tagged_id ")
                    ->group(array("name"))
                    ->where(array(NW_Media_Asset::SQL_TABLE . ".type = '?'" => $this->taggedType))
                    ->order(array("howmany desc"))
                    ->limit($paginator->start, $paginator->itemsPerPage);
            if (!empty($params)) {
                foreach ($params as $whereCond)
                    $query->where($whereCond);
            }

            return NW_Application::getSingleton('database/default')->fetchRowsAssociative($query->getQuery());
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function delete($id) {
        try {
            $deleteQuery = new NW_Db_Query_Delete();
            $deleteQuery->delete()->from(NW_Tags_Tag::TABLE)
                    ->where(array('tagged_id=?' => $id));
            NW_Application::getSingleton('database/default')->execute($deleteQuery->getQuery());
        } catch (Exception $e) {
            throw $e;
        }
    }

}

?>

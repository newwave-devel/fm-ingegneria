<?php
class Pages_Model_Page_DTO_Page {
	
    public $page;
	public $blocks;
	private $_tree;
	
	public function __construct() {
		$this->page = null;
		$this->blocks = array ();
		$this->_tree = array();
	}
	
	/**
	 *
	 * @return the unknown_type
	 */
	public function getPage() {
		return $this->page;
	}
	
	/**
	 *
	 * @param unknown_type $page        	
	 */
	public function setPage($page) {
		$this->page = $page;
		return $this;
	}
	
	/**
	 *
	 * @return the unknown_type
	 */
	public function getBlocks() {
		return $this->blocks;
	}
	
	public function getBlock($tpl, $pos=0) {
	    $realPos = $this->_tree[$tpl][$pos];
	    return $this->blocks[$realPos];
	}
	
	/**
	 *
	 * @param unknown_type $blocks        	
	 */
	public function setBlocks($blocks) {
	    foreach($blocks as $block)
	        $this->addBlock($block);
		return $this;
	}
	
	public function addBlock(Pages_Model_Block $block) 
	{
	    $template = $block->getTemplate();
	    if(!isset($this->_tree[$template]))
	        $this->_tree[$template] = array();
	    $this->_tree[$template][] = $block->getId();
	    $this->blocks[$block->getId()] = $block;
		return $this;
	}
}
?>
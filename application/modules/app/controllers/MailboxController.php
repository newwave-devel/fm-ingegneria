<?php

class App_MailboxController extends NW_Controller_Action {
    private $_service;
    
    public function init() {
        $this->_service = new App_Model_Message_Service();
        $this->_translateAdapter = Zend_Registry::get('Zend_Translate');
    }

    public function indexAction() {
        
    }
    
    public function listAction()
    {
    	try {
    		$this->_setAjaxAction()
                    ->_addJsSource('/admin/js/app/mailbox.js');
    		
    		$this->view->main_title = sprintf('MailBox <strong>%s</strong>', Zend_Auth::getInstance()->getIdentity()->getName());
    		$this->_addTabContainer(array("active" => 0))
    				->_addTab("/app/mailbox/incoming", 'Incoming Mail')
    				->_addTab("/app/mailbox/sent", 'Outgoing Mail');
    		
    	} catch(Exception $e)
    	{
    		throw $e;
    	}
    }

    public function incomingAction() {
        try {
            $this->_setAjaxAction();
            $request = $this->getRequest();
            $paginator = NW_Pages_Paginator::__fromRequest($request);
           
    		$paginator->setIsAjax(true, "#mailbox-list-incoming", "/app/mailbox/incoming");

    		$this->view->entries = $this->_service->incoming($paginator, array("recipient" => $this->_helper->LoggedUser()->getEmail()));
            $this->view->paginator = $paginator->prepare();
            $this->view->paginationController = 'frmList-incoming';
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function sentAction() {
    	try {
    		$this->_setAjaxAction();
    		$request = $this->getRequest();
    		$paginator = NW_Pages_Paginator::__fromRequest($request);
    		$paginator->setIsAjax(true, "#mailbox-list-sent","/app/mailbox/sent");  		
    		
    		$this->view->entries = $this->_service->sent($paginator, array("sender" => $this->_helper->LoggedUser()->getId()));
    		$this->view->paginator = $paginator->prepare();
    		$this->view->paginationController = 'frmList-sent';
    	} catch (Exception $e) {
    		throw $e;
    	}
    }
    
    
    public function saveAction()
    {
    	try {
    		$this->_setIframeAction();
    		$request = $this->getRequest();
    		$form = new App_Form_Message();
    		
    		$status = new NW_View_RetCode();
    		if ($request->isPost())
    		{
    			if ($form->isValid($request->getPost()))
    			{
    				if ($this->_hasParam('id') && ($id=$request->getParam('id'))!='') {
    					$model = $this->_service->load($id);
    				}
    				$model->setOptions($form->getValues());
    				$itemId = $this->_service->save($model);
    				
    				$this->_addOnload('parent.mailbox.updateMail()');
    			}
    			else {
    				$this->view->form = $form;
    			}
    			
    		} 
    	} catch(Exception $e)
    	{
    		$this->view->status = NW_View_RetCode::error($e->getMessage());
    	}
    }
    
    public function displayAction()
    {
    	try {
    		$this->_setIframeAction();
    		if(!$this->_hasParam('id')) throw new Exception("no id recevied");
    		$id = $this->_getParam('id');
    		$message = $this->_service->load($id);
    		$form = new App_Form_Message();
    		$form->setReadOnly()->populate($message->getOptions());
    		
    		$this->view->form = $form;
    		$this->view->message = $message;
    	} catch (Exception $e) {
    		throw $e;
    	}
    }

    
    public function editAction()
    {
    	try {
    		$this->_setIframeAction();
    		if(!$this->_hasParam('id')) throw new Exception("no id recevied");
    		$id = $this->_getParam('id');
    		$service = new App_Model_Message_Service();
    		$message = $service->load($id);
    		$form = new App_Form_Message();
    		$form->populate($message->getOptions());    		
    		$this->view->form = $form;
    	} catch(Exception $e)
    	{
    		throw $e;
    	}
    }
    
    public function deleteAction()
    {
    	try {
    		$this->_setJsonAction();
    		if(!$this->_hasParam('paramId')) throw new Exception("no id received");
    		$id = $this->_getParam('paramId');
    		$this->_service->delete($id);
    		$this->view->response = NW_View_RetCode::success();
    	} catch(Exception $e)
    	{
    		$this->view->response = NW_View_RetCode::error();
    		throw $e;
    	}
    }
    
    public function requeueAction()
    {
    	try {
    		$this->_setJsonAction();
    		if(!$this->_hasParam('paramId')) throw new Exception("no id received");
    		$id = $this->_getParam('paramId');
    		$message = $this->_service->load($id);
    		$message->setStatus(App_Model_Message::STATUS_NEW);
    		$this->_service->queue($message);
    		$this->view->response = NW_View_RetCode::success();
    	} catch(Exception $e)
    	{
    		$this->view->response = NW_View_RetCode::error();
    		throw $e;
    	}
    }

}


<?php
$pageId = 'company';
include "inc/required.php";
include "inc/common/contents.php";
?>
<?php include "inc/common/head.php" ?>


        <!-- SCRIPT -->

		<script type="text/javascript" src="/common/js/core.js"></script>
		<script type="text/javascript" src="/common/js/jquery.jcarousel.pack.js"></script>
        <script type="text/javascript" src="/common/js/jquery.sharrre.min.js"></script>
        <script type="text/javascript" src="/common/js/jquery.nivo.slider.pack.js"></script>
        <script type="text/javascript" src="/common/js/custom.js"></script>
        
        <script type="text/javascript">	
                jQuery(window).load(function() {
                    // START
					loading();
					share();
					people_carousel_sidebar();
                    slideshow();
					fancybox();
                });
        </script>
        
	</head>    
    
	<body lang="it">
    
    	<div id="loader"></div>
    	    	
        <?php include "inc/common/header.php"?>
        
        <section id="main_page">
        	<section class="container">
            
            	<section id="page_title">
                    <h3><?php echo $mainBlock->_("title")?></h3>
                    
                   <?php include "inc/common/social_share.php"?>
                    
                </section><!--page_title-->
                
                <?php include "inc/company/main-slider-no-text.php"?>
                <!-- 
            		<section id="main_video">
                  <iframe src="//player.vimeo.com/video/47845783?title=0&amp;byline=0&amp;portrait=0&amp;color=cccccc" width="1000" height="562" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                  </section>
                -->
                
                <section id="main_data">
                
                	<aside class="mainCol">
                    	<section id="discipline_text">
                            <h3><?php echo $mainBlock->_("subtitle")?></h3>
                            <?php echo $mainBlock->_("textsx")?>
                        </section><!--discipline_text-->
                        
                        <?php 
                       		include "inc/common/downloads.php"; 
                       	?>
                    </aside><!--mainCol-->
                    
                    <aside class="sidebar">
                   		<h5 class="view_people"><a href="people.php"><?php $translator->{"Pagina company"}->__("View People") ?></a></h5>
                 	</aside><!--sidebar-->
                
                </section><!--main_data-->
                                
                <section id="home_articles">
                	
                    <h4 class="title"><?php $translator->{"Pagina company"}->__("Related Contents") ?></h4>
                    
                    <?php 
                    include "inc/company/main-col.php";
                    ?>
                    
                </section><!--home_articles-->
                
            </section><!--container-->
        </section><!--main_page-->
        
        <?php include "inc/common/footer.php"?>
		
	</body>
	
</html>
<?php

class App_Helper_Role extends Zend_Controller_Action_Helper_Abstract {
    
   
    public function role() {
        return Zend_Auth::getInstance()->getStorage()->read()->getRole();
    }
    
    public function direct()
    {
        return $this->role();
    }
}

?>

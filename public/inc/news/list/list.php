<span id="newsList">
        <?php if($news): ?>
            <?php
                $news_list_list_html = '';
                foreach($news as $new):
                    $img = $new->_("file");
                    $title = $new->_("title");
                    $date = str_replace("/", ".",  $new->_("date"));
                    $news_list_list_html .= sprintf($tpl_listItem, $new->getPage(), addslashes($title), $blockService->asset($img)->getAssetUrl(), addslashes($title),
                            strtoupper($title), $date, nicesubstring($new->_("description"), 50));
                endforeach;
            ?>
            <?php echo $news_list_list_html ?>
        <?php else: ?>
            <?php $translator->{"Pagina news"}->__("Nessun risultato trovato"); ?>
        <?php endif; ?>
        <div class="clearer"></div>
        <?php include "inc/common/pagination.php"; ?>
</span>



<?php
$chunks = array_chunk($disciplineBlocks, 7);
foreach($chunks as $chunk)
{
	echo "<ul>";
	foreach($chunk as $block)
	{
		echo sprintf('<li><a href="%d">%s</a></li>', $block->getId(), $block->_("title"));
	}
	echo "<ul>";
}
?>
<?php

class App_Model_Asset
{

	protected $id;

	protected $mime;

	protected $assetPath;

	protected $attributes;

	protected $pid;

	protected $creation_dt;

	protected $assetUrl;

	protected $assetName;

	protected $filesize;

	protected $uploadName;

	protected $extension;

	protected $type;

	const MEDIA_TYPE_IMAGE = 'IMAGE';
//	const MEDIA_TYPE_VIDEO = 'video';
//	const MEDIA_TYPE_ARCHIVE = 'archive';
	const MEDIA_TYPE_DOC = 'DOC';

	const PARENT = 0;

	const ATTRIBUTE_NOT_SET = - 1;

	public function __construct()
	{
		$this->id = '';
		$this->type = '';
		$this->assetPath = '';
		$this->assetUrl = '';
		$this->assetName = '';
		$this->uploadName = '';
		$this->attributes = new stdClass ();
		$this->pid = self::PARENT;
		$this->creation_dt = strftime ( "%Y-%m-%d %H:%M:%S", time () );
		$this->filesize = 0;
	}

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	public function getType()
	{
		return $this->type;
	}

	public function setType($type)
	{
		$this->type = $type;
		return $this;
	}

	public function getAssetPath()
	{
		return $this->assetPath;
	}

	public function setAssetPath($asset)
	{
		$this->assetPath = $asset;
		return $this;
	}

	public function getAssetUrl()
	{
		return $this->assetUrl;
	}

	public function setAssetUrl($asset)
	{
		$this->assetUrl = $asset;
		return $this;
	}

	public function getAssetName()
	{
		return $this->assetName;
	}

	public function setAssetName($asset)
	{
		$this->assetName = $asset;
		return $this;
	}

	public function getUploadName()
	{
		return $this->uploadName;
	}

	public function setUploadName($asset)
	{
		$this->uploadName = $asset;
		return $this;
	}

	public function getAttributes()
	{
		return $this->attributes;
	}

	public function setAttributes($attributes)
	{
		$this->attributes = $attributes;
		return $this;
	}

	public function setAttribute($attribute, $value)
	{
		$this->attributes->$attribute = $value;
		return $this;
	}

	public function getAttribute($attribute)
	{
		if (isset ( $this->attributes->$attribute ))
			return $this->attributes->$attribute;
		return self::ATTRIBUTE_NOT_SET;
	}

	public function getPid()
	{
		return $this->pid;
	}

	public function setPid($pid)
	{
		$this->pid = $pid;
		return $this;
	}

	public function getCreation_dt()
	{
		return $this->creation_dt;
	}

	public function setCreation_dt($creation_dt)
	{
		$this->creation_dt = $creation_dt;
		return $this;
	}

	public function getFilesize()
	{
		return $this->filesize;
	}

	public function setFilesize($filesize)
	{
		$this->filesize = $filesize;
		return $this;
	}

	public function getMime()
	{
		return $this->mime;
	}

	public function setMime($mime)
	{
		$this->mime = $mime;
		return $this;
	}

	public function getExtension()
	{
		return $this->extension;
	}

	public function setExtension($extension)
	{
		$this->extension = $extension;
		return $this;
	}

}

?>

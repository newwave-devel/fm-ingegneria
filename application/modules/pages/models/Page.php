<?php

class Pages_Model_Page
{

	protected $_id;

	protected $_label;

	protected $_lang;

	protected $_tsCreation;

	protected $_tsUpdate;

	protected $_tsDelete;

	protected $_status;

	protected $_template;

	protected $_order;

	protected $_trx = 0;

	protected $_taxonomies = array ();

	const STATUS_ACTIVE = 'active';

	const STATUS_DRAFT = 'draft';

	const STATUS_DELETED = 'deleted';

	protected $_observers;

	public function __construct()
	{
		$this->_observers = new SplObjectStorage ();
	}

	/**
	 *
	 * @return the unknown_type
	 */
	public function getId()
	{
		return $this->_id;
	}

	/**
	 *
	 * @param unknown_type $_id        	
	 */
	public function setId($_id)
	{
		$this->_id = $_id;
		return $this;
	}

	/**
	 *
	 * @return the unknown_type
	 */
	public function getLabel()
	{
		return $this->_label;
	}

	/**
	 *
	 * @param unknown_type $_label        	
	 */
	public function setLabel($_label)
	{
		$this->_label = $_label;
		return $this;
	}

	/**
	 *
	 * @return the unknown_type
	 */
	public function getLang()
	{
		return $this->_lang;
	}

	/**
	 *
	 * @param unknown_type $_lang        	
	 */
	public function setLang($_lang)
	{
		$this->_lang = $_lang;
		return $this;
	}

	/**
	 *
	 * @return the unknown_type
	 */
	public function getTsCreation()
	{
		return $this->_tsCreation;
	}

	/**
	 *
	 * @param unknown_type $_tsCreation        	
	 */
	public function setTsCreation($_tsCreation)
	{
		$this->_tsCreation = $_tsCreation;
		return $this;
	}

	/**
	 *
	 * @return the unknown_type
	 */
	public function getTsUpdate()
	{
		return $this->_tsUpdate;
	}

	/**
	 *
	 * @param unknown_type $_tsUpdate        	
	 */
	public function setTsUpdate($_tsUpdate)
	{
		$this->_tsUpdate = $_tsUpdate;
		return $this;
	}

	/**
	 *
	 * @return the unknown_type
	 */
	public function getTsDelete()
	{
		return $this->_tsDelete;
	}

	/**
	 *
	 * @param unknown_type $_tsDelete        	
	 */
	public function setTsDelete($_tsDelete)
	{
		$this->_tsDelete = $_tsDelete;
		return $this;
	}

	/**
	 *
	 * @return the unknown_type
	 */
	public function getStatus()
	{
		return $this->_status;
	}

	/**
	 *
	 * @param unknown_type $_status        	
	 */
	public function setStatus($_status)
	{
		$this->_status = $_status;
		return $this;
	}

	/**
	 *
	 * @return the unknown_type
	 */
	public function getTemplate()
	{
		return $this->_template;
	}

	/**
	 *
	 * @param unknown_type $_template        	
	 */
	public function setTemplate($_template)
	{
		$this->_template = $_template;
		return $this;
	}

	public function __set($name, $value)
	{
		$method = 'set' . $name;
		if (('mapper' == $name) || ! method_exists ( $this, $method ))
		{
			throw new Exception ( 'Invalid certificates property: ' . $name );
		}
		$this->$method ( $value );
	}

	public function __get($name)
	{
		$method = 'get' . $name;
		if (('mapper' == $name) || ! method_exists ( $this, $method ))
		{
			throw new Exception ( 'Invalid certificates property: ' . $name );
		}
		return $this->$method ();
	}

	public function setOptions(array $options)
	{
		$methods = get_class_methods ( $this );
		foreach ( $options as $key => $value )
		{
			$method = 'set' . ucfirst ( $key );
			if (in_array ( $method, $methods ))
			{
				$this->$method ( $value );
			}
		}
		return $this;
	}

	public function getOptions()
	{
		return get_object_vars ( $this );
	}

	public function getTaxonomies()
	{
		return $this->_taxonomies;
	}

	public function getTaxonomy($taxonomy)
	{
		return $this->_taxonomies [$taxonomy];
	}

	public function setTaxonomies($taxonomy, $items)
	{
		$this->_taxonomies [$taxonomy] = $items;
		return $this;
	}

	public function appendToTaxonomy($taxonomy, $items)
	{
		if (! isset ( $this->_taxonomies [$taxonomy] ))
			$this->_taxonomies [$taxonomy] = array ();
		foreach ( $items as $key => $value )
			$this->_taxonomies [$taxonomy] [$key] = $value;
		return $this;
	}

	/**
	 *
	 * @return the unknown_type
	 */
	public function getOrder()
	{
		return $this->_order;
	}

	/**
	 *
	 * @param unknown_type $_order        	
	 */
	public function setOrder($_order)
	{
		$this->_order = $_order;
		return $this;
	}

	public function getTrx()
	{
		return $this->_trx;
	}

	public function setTrx($trx)
	{
		$this->_trx = $trx;
		return $this;
	}

	public function attach($observer)
	{
		$this->_observers->attach ( $observer );
	}

	public function detach($observer)
	{
		$this->_observers->detach ( $observer );
	}

	public function notify($event, $params=array())
	{
		foreach ( $this->_observers as $observer )
			$observer->update ( $this, $event, $params);
	}

}
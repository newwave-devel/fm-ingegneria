<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DatePicker
 *
 * @author Alessio
 */
class NW_Form_Element_DatePicker extends NW_Form_Element_Text
{
    public function __construct($spec, $options = null)
    {
        parent::__construct($spec, $options);
        $this->setDecorators(array(new NW_Form_Decorator_X_DatePicker()));
    }
}

?>

<?php
class Pages_Model_Block_Shared_Mapper
{
	protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Pages_Model_DbTable_Block_Shared');
        }
        return $this->_dbTable;
    }

	public function find($id, Pages_Model_Block_Shared &$model)
	{
		try {
			$result = $this->getDbTable ()->find ( $id );
			if (0 == count ( $result )) {
				return;
			}
			$row = $result->current ();
			$this->_fromDb ( $model, $row );
		} catch ( Exception $e ) {
			throw $e;
		}
	}


	public function fetchAll(NW_Pages_Paginator &$paginator, $paramsSearch, $paramsOrder = null, $load = false) {
		try {
			$table = $this->getDbTable ();
			$select = $table->select ();
				
			$this->_filterBySearchParam ( $select, $paramsSearch )->_filterByUser ( $select );
			if (! $paginator->getFetchAll ())
				$select->limit ( $paginator->itemsPerPage, $paginator->getStart () );
				
			if (! empty ( $paramsOrder )) {
				foreach ( $paramsOrder as $key => $dir ) {
					$sql = sprintf ( "%s %s", $key, strtoupper ( $dir ) );
					$select->order ( $sql );
				}
			}
			$entries = array ();
			
			foreach ( $table->fetchAll ( $select ) as $row ) {
				$model = new Pages_Model_Block_Shared();
				$this->_fromDb ( $model, $row, $load);
				$entries [] = $model;
			}
			$paginator->itemsCount = $this->count ( $select );
				
			return $entries;
		} catch ( Exception $e ) {
			throw $e;
		}
	}

	public function save(Pages_Model_Block_Shared $model)
	{
		try {
			$data = $this->_toDb($model);
			
			if (null === ($id = $model->getId()) || empty($id)) {
				unset($data['id']);
				$id = $this->getDbTable()->insert($data);
				$model->setId($id);
				return $id;
			} else {
				$this->getDbTable()->update($data, array('id = ?' => $id));
				return $id;
			}
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	public function saveShared($models = array(), $update = true, $pageId = null)
	{
		try {
			$this->getDbTable()->getAdapter()->beginTransaction();
			if($update && !empty($pageId))
				$this->dropPage($pageId);
			if(!empty($models))
			{
				foreach($models as $model)
					$this->save($model);
			}
			$this->getDbTable()->getAdapter()->commit();
			return true;
		} catch (Exception $e) {
			$this->getDbTable()->getAdapter()->rollback();
			throw $e;
		}
	}
	
	public function delete($id)
	{
	    try {
	    	
	        $table = $this->getDbTable();
	        
	        $where = $table->getAdapter()->quoteInto('id = ?', $id);
	        $table->delete($where);
	        
	        return true;
	    } catch(Exception $e)
	    {
	        throw $e;
	    }
	}
	
	public function dropPage($id)
	{
		try {
	
			$table = $this->getDbTable();
			 
			$where = $table->getAdapter()->quoteInto('page = ?', $id);
			$table->delete($where);
			return true;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function count(Zend_Db_Select $select) {
		try {
			$table = $this->getDbTable ();
			$select->reset ( Zend_Db_Select::COLUMNS )->reset ( Zend_Db_Select::LIMIT_COUNT )->reset ( Zend_Db_Select::LIMIT_OFFSET )->columns ( array (
					'num' => new Zend_Db_Expr ( 'COUNT(*)' )
			) );
			return $table->fetchRow ( $select )->num;
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	
	private function _filterByUser(Zend_Db_Table_Select &$select)
	{
// 		$loggedUser = Zend_Auth::getInstance()->getStorage()->read();
// 		$loggedUserId = $loggedUser->getId();
// 		return $this;
	}
	
	private function _filterBySearchParam(Zend_Db_Table_Select &$select, $paramsSearch = array()) {
		if (!empty($paramsSearch)) {
			if (!empty($paramsSearch['page']))
			{
				if(is_array($paramsSearch['page']))
					$select->where("page IN (?)", $paramsSearch['page']);
				else $select->where("page = ?", $paramsSearch['page']);
			}
			if (!empty($paramsSearch['block']))
			{
				if(is_array($paramsSearch['block']))
					$select->where("block IN (?)", $paramsSearch['block']);
				else $select->where("block = ?", $paramsSearch['block']);
			}
			if (!empty($paramsSearch['position']))
			{
				if(is_array($paramsSearch['position']))
					$select->where("position IN (?)", $paramsSearch['position']);
				else $select->where("position = ?", $paramsSearch['position']);
			}
		}
		return $this;
	}
	
	private function _fromDb(Pages_Model_Block_Shared &$model, $row, $load=false) {
		
		$model->setId($row->id)
			->setPage($row->page)
			->setOrder($row->order)
			->setBlock($row->block)
			->setOrder($row->order)
			->setPosition($row->position);
		if($load)
		{
			$pageService = new Pages_Model_Page_Service();
			$service = new Pages_Model_Block_Service();
			$block = $service->find($row->block);
			$model->setBlock($block)
				->setPage($pageService->find($block->getPage()));
		}
		
	}
	
	
	private function _toDb(Pages_Model_Block_Shared $model) {
		
	
		$data = array(
				'id' => $model->getId(),
				'page' => $model->getPage(),
				'order' => $model->getOrder(),
				'position' => $model->getPosition(),
				'block' => $model->getBlock()
		);
		return $data;
	}


}
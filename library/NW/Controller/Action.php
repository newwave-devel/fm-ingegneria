<?php

class NW_Controller_Action extends Zend_Controller_Action
{
	protected $_isAjax = false;
	protected $_isIFrame = false;
	
	protected function _asyncRequest($target, $action, $msg_fail = '')
	{
		try
		{
			$jqueryOnload = sprintf ( '$.get("%s").done(function(data){$("%s").html(data).fadeIn(550)})', $action, $target );
			$jqueryOnload .= ($msg_fail != '') ? sprintf ( '.fail(function(){$("%s").html("<h4 class=\'error_msg\'>%s</h4>").fadeIn(550)});', $target, $msg_fail ) : ';';
			$jqueryOnload .= PHP_EOL;
			$this->view->jQuery ()->addOnLoad ( $jqueryOnload );
			return $this;
		}
		catch ( Exception $e )
		{
			throw $e;
		}
	}

	protected function _setAjaxAction()
	{
		
		$this->view->jQuery ()->setRenderMode(ZendX_JQuery::RENDER_JQUERY_ON_LOAD | ZendX_JQuery::RENDER_JAVASCRIPT | ZendX_JQuery::RENDER_SOURCES);
		$this->_helper->layout ()->setLayout ( 'ajaxcontent' );
		$this->_isAjax = true;
		$this->_isIFrame = false;
		return $this;
	}

	protected function _setIframeAction()
	{
		$this->_helper->layout ()->setLayout ( 'iframecontent' );
		$this->_isIFrame = true;
		$this->_isAjax = false;
		return $this->addDefaultJs();
	}

	protected function _setJsonAction()
	{
		$this->_helper->layout ()->setLayout ( 'json' );
		return $this;
	}

	protected function _addOnload($param)
	{
		if (is_array ( $param ))
		{
			foreach ( $param as $onload )
				$this->view->jQuery ()->addOnload ( $onload );
		}
		else
			$this->view->jQuery ()->addOnload ( $param );
		return $this;
	}

	protected function _addJsSource($src)
	{
		$this->view->jQuery ()->addJavascriptFile ( $src );
		return $this;
	}

	protected function _addJavascript($code)
	{
		$this->view->jQuery ()->addJavascript ( $code.PHP_EOL );
		return $this;
	}
	
	protected function _addTabContainer($options =null)
	{
		$options = (empty($options)) ? array() : $options;
		$options['heightStyle'] = "content";
		$jsOptions = Zend_Json::encode($options);
		if($this->_isIFrame)
			$this->_addOnload ( "parent.window.main_page.subPage().tabs($jsOptions);" );
		else
			$this->_addOnload ( "window.main_page.subPage().tabs($jsOptions);" );
		return $this;
	}

	protected function _addTab($href, $label)
	{
		if (! isset ( $this->view->tabs ))
			$this->view->tabs = array ();
		$this->view->tabs [] = array (
				'href' => $href,
				'label' => $label 
		);
		return $this;
	}
	
	protected function addDefaultJs()
	{
		$this->view->jQuery()
				->addJavascriptFile('/admin/js/app/dic.js')
				->addJavascriptFile('/admin/js/NW.js')
				->addJavascriptFile('/admin/js/NW/Mosaic.js')
				->addJavascriptFile('/admin/js/NW/Mosaic/page.js')
				->addJavascriptFile('/admin/js/NW/Mosaic/subpage.js')
				->addJavascriptFile('/admin/js/NW/pagination.js')
				->addJavascriptFile('/admin/js/NW/tablesorter.js')
				->addJavascriptFile('/admin/js/vendors/malsup/ajaxform.js')
				->addJavascriptFile('/admin/js/vendors/tablesorter/jquery.tablesorter.min.js')
				->addJavascriptFile('/admin/js/vendors/albanx/real-ajax-uploader/examples/js/ajaxupload-min.js')
				->addJavascriptFile('/admin/js/vendors/ckeditor/ckeditor.js')
				->addJavascriptFile('/admin/js/vendors/fancyBox/source/jquery.fancybox.pack.js')
				->addJavascriptFile('/admin/js/vendors/alexei/sprintf.js/sprintf.js')
				->addJavascriptFile('/admin/js/app/templates.js')
				->addJavascriptFile('/admin/js/app/libraryController.js')
				->addJavascriptFile('/admin/js/app/library.js');
		return $this;
	}
}

?>

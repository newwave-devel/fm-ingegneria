<?php
class App_Model_Message_Service
{
	const ERROR_NO_RECIPIENTS = 10;
	const ERROR_NO_BODY = 20;
	const ERROR_NO_SUBJECT = 30;
	
	private $_mapper;
	
	public function __construct()
	{
		$this->_mapper = new App_Model_Message_Mapper();
	}

	public function save(App_Model_Message $message)
	{
		try {
			
			if(!$message->hasRecipients(true))
				return false;
			$messageId = $this->_mapper->save($message);
			$recipientService = new App_Model_Message_Recipient_Service();
			$recipientService->dropByMessage($messageId);
			foreach($message->getRecipients() as $recipient)
			{
				$recipient->setMessage($messageId);
				$recipientService->save($recipient);
			}
			return $messageId;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function queue(App_Model_Message $message)
	{
		try {
			
			if(!$message->hasRecipients())
				return false;

			$messageId = $this->_mapper->save($message);
			$recipientService = new App_Model_Message_Recipient_Service();
			foreach($message->getRecipients() as $recipient)
			{
				$recipient->setMessage($messageId);
				$recipientService->save($recipient);
			}
			return $messageId;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function validate(App_Model_Message $message)
	{
		if(!$message->hasRecipients())
			return self::ERROR_NO_RECIPIENTS;
		$body = $message->getBody();
		$subject = $message->getSubject();
		if(empty($body)) return self::ERROR_NO_BODY;
		if(empty($subject)) return self::ERROR_NO_SUBJECT;
		return true;
	}
	
	public function processQueue()
	{
		try {
			$transport = $this->connect();
			
			$messages = $this->queued(true);
			
			if(empty($messages)) 
				return true;

			foreach($messages as $msg)
			{
				$mail = new Zend_Mail();
				$mail->setBodyHtml(nl2br($msg->getBody()));
				$mail->setSubject($msg->getSubject());
				$recipients = $msg->getRecipients();
				if(empty($recipients))
					continue;
				
				foreach($recipients as $recipient)
				{
					if($recipient->getSendas() == App_Model_Message_Recipient::SEND_AS_TO)
						$mail->addTo($recipient->getAddress());
					
					if($recipient->getSendas() == App_Model_Message_Recipient::SEND_AS_CC)
						$mail->addCc($recipient->getAddress());
					
					if($recipient->getSendas() == App_Model_Message_Recipient::SEND_AS_BCC)
						$mail->addBcc($recipient->getAddress());
				}
				$mail->addBcc('debug@alessiodezotti.net', 'Alessio De Zotti');
				$mail->setFrom('shipgest@gmail.com', 'Sender Agent Shipgest');
				$mail->send($transport);
				$msg->setTssent(time())
					->setStatus(App_Model_Message::STATUS_SENT);
				$this->_mapper->save($msg);
			}
			
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function queued($load=false)
	{
		try {
			return $this->_mapper->fetchAll(NW_Pages_Paginator::fetchAll(), array('status' => App_Model_Message::STATUS_NEW), array(), $load ); 
		} catch(Exception $e) {
			throw $e;
		}
	}
	
	public function incoming(NW_Pages_Paginator &$paginator, $paramsSearch=array(), $sortBy = array())
	{
		try{
			return $this->_mapper->fetchAll($paginator,$paramsSearch, $sortBy, true);
		} catch(Exception $e) {
			throw $e;
		}
	}
	
	public function sent(NW_Pages_Paginator &$paginator, $paramsSearch=array(), $sortBy = array())
	{
		try{
			return $this->_mapper->fetchAll($paginator,$paramsSearch, $sortBy, true);
		} catch(Exception $e) {
			throw $e;
		}
	}
	
	public function delete($paramId)
	{
		try {
			$this->_mapper->getDbTable()->getAdapter()->beginTransaction();
			$trService = new App_Model_TransactionalEmail_Service();
			$recipientsService = new App_Model_Message_Recipient_Service();
			
			$trService->deleteMessage($paramId);
			$recipientsService->dropByMessage($paramId);
			$this->_mapper->delete(array("id=?" =>$paramId));
			$this->_mapper->getDbTable()->getAdapter()->commit();
			return true;
		} catch(Exception $e)
		{
			$this->_mapper->getDbTable()->getAdapter()->rollBack();
			throw $e;
		}
	}
	
	public function load($paramId)
	{
		try {
			$message = new App_Model_Message();
			$this->_mapper->load($paramId, $message);
			return $message;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	protected function getMapper()
	{
		return $this->_mapper;
	}
	
	public function connect()
	{
		try {
// 			$bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
// 			$options = $bootstrap->getOptions();
		
			$config = array(
					'ssl' => 'tls',//$options['email']['ssl'],
					'port' => '587', //$options['email']['port'],
					'auth' => 'login', //$options['email']['auth'],
					'username' => 'shipgest@gmail.com', //$options['email']['username'],
					'password' =>  'dupakupa13' //$options['email']['password']
			);
			return new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	
}
?>
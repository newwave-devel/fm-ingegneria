<?php

/**
 * Description of Link
 *
 * @author Ale
 */
class Zend_View_Helper_Link_Ajax_Image_ActionDelete extends Zend_View_Helper_Abstract
{
    
    public function link_ajax_image_actiondelete($id, $options = array()) { 
        $actionName = 'delete';
        $image = 'delete';
        $this->_js($actionName);
        $url = $this->_url($actionName, $id);
        $image = $this->_image($image);
        return $this->_html($options, $id, $actionName, $url, $image);
    }

    private function _js($actionName) {
        $this->view->headScript()->appendFile("/js/admin/NW/actions.js");
        $this->view->headScript()->appendFile("/js/admin/NW/callbacks/actions.js");
        $this->view->jQuery()->addOnload(sprintf("NW_Actions.registerGet('%s', %sAction);", $actionName, $actionName));
    }

    private function _url($actionName, $id) {
        $route = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();       
        return $this->view->url(array("action" => $actionName, "id" => $id), $route);
    }
    
    private function _image($image)
    {
        return sprintf('<img src="/img/admin/icons/actions/%s.png" />', $image);
    }

    private function _html($options, $id, $actionName, $url, $image) {
        $tpl = '<a href="%s"%s>%s</a>';
        $itemId = (isset($options['id'])) ? sprintf(' id="%s%d %s" ', $actionName, $id, $options['id']) : sprintf(' id="%s%d" ', $actionName, $id);
        $itemClass = (isset($options['class'])) ? sprintf(' class="action %s %s" ', $actionName, $options['class']) : sprintf(' class="action %s" ', $actionName);
        $itemRel = (isset($options['rel'])) ? sprintf(' rel="%s" ', $options['rel']) : '';
        $extras = $itemId . $itemClass . $itemRel;
        return sprintf($tpl, $url, $extras, $image);
    }
    

}

?>

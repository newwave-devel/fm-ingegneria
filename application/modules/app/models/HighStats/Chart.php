<?php
/**
 * Its purpose is to be an interface for Highstats chart, whose api are pretty confusing
 *
 * @author Ale
 */
abstract class App_Model_HighStats_Chart {
    
    protected $options;
    protected $series;
    protected $dataX;
    
    public function __construct($options=array()) {
        $this->options=$options;
        $this->series = array();
        $this->dataX = array();
    }
    
    public function getOptions() {
        return $this->options;
    }

    public function setOptions($options) {
        $this->options = $options;
         return $this;
    }
    
    public function addOption($key, $value) {
        $this->options[$key] = $value;
         return $this;
    }

    public function getSeries() {
        return $this->series;
    }
    
    public function setTitle($title, $options=array())
    {
        $this->options['title'] = array('text' => $title);
        if(!empty($options))
        {
            foreach($options as $k => $v)
            $this->options['title'][$k] = $v;
        }
        return $this;
    }
    
    public function setSubTitle($subtitle, $options=array())
    {
        $this->options['subtitle'] = array('text' => $title);
        if(!empty($options))
        {
            foreach($options as $k => $v)
            $this->options['subtitle'][$k] = $v;
        }
        return $this;
    }

    public function setSeries($series) {
        $this->series = $series;
    }

    public function getCurSeries($pos) {
        return $this->series[$pos];
    }

    public function getDataX() {
        return $this->dataX;
    }

    public function setDataX($dataX) {
        $this->dataX = $dataX;
        return $this;
    }
    
    public function addY(array $seriesData)
    {
        $this->series[] = new App_Model_HighStats_Series($seriesData);
        return $this;
    }
    
    public function limit($limit)
    {
        $this->dataX = array_slice($this->dataX, 0, $limit);
        return $this;
    }
    
    public function sort()
    {
        
    }
    abstract public function getJs();
}

class App_Model_HighStats_Series{
    
    private $data;
    private $options;
    
    function __construct($params=array()) {
        $this->data =  $params['data'];
        unset($params['data']);
        $this->options = $params;
    }

    public function getData() {
        return $this->data;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function getOptions() {
        return $this->options;
    }
    
    public function getOption($key) {
        return $this->options[$key];
    }

    public function setOptions($options) {
        $this->options = $options;
    }
    
    public function addOption($key, $value) {
        $this->options[$key] = $value;
        return $this;
    }
    
    public function y($pos)
    {
        return $this->data[$pos];
    }


}

?>

<?php
class App_Controller_Plugin_Acl extends Zend_Controller_Plugin_Abstract {
	
	public function preDispatch(Zend_Controller_Request_Abstract $request) 
	{
		switch($request->getModuleName()) {
		    case 'login':
		    case 'default':
		    	break;
	    	case 'certificates':
	    		if ($request->getControllerName() == 'document' && $request->getActionName() == 'save')
	    			break;
		    case 'purchase':
		    	if($request->getControllerName() == 'quote' && $request->getActionName() == 'save')
		    		break;
		    default:
				$acl = Zend_Registry::get('acl');
				
				$usersNs = new Zend_Session_Namespace('members');
				
				if ($usersNs->userType == '') {
					$roleName = App_ACL_Roles::defaultRole();
				} else {
					$roleName = $usersNs;
				}
				
				$privilageName = $request->getControllerName() .".".$request->getActionName();
				
				if (!$acl->isAllowed($roleName, null, $privilageName)) 
				{
					$request->setControllerName('error');
					$request->setActionName('invalidpermission');
					$request->setModuleName('default');
				}
			break;
		}
	}
}
?>
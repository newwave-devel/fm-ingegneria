<?php

class App_Form_User extends Zend_Form {

    public function init() {
    	$this->setMethod('post')
    			->setAttrib('id', 'adduser_form')
    			->setAttrib('class', 'form')
    				->setAction("/app/users/save");
    	
    	$name = new NW_Form_Element_Text("name");
        $name -> setLabel("Nome")
        	->setRequired(true)
        	->addValidator('Regex',false, array('/([a-zA-Z \-_]+)/'))
                ->setFilters(array('StringTrim'))
              	->setErrorMessages(array('Insert User Name'));
        $username = new NW_Form_Element_Text("username");
        $username -> setLabel("Username")
        			->setRequired(true)
        			->addValidator('Regex',false, array('/([a-zA-Z0-9 \-_]+)/'))
        			->setFilters(array('StringTrim'))
        			->setErrorMessages(array('Insert Username'));
        $password = new NW_Form_Element_Password("password");
        $password ->  setLabel("Password")
        			->setErrorMessages(array('Password must be 6 to 12 char long'))
        			->setRequired(true)
        			->addValidator('Regex',false, array('/([a-zA-Z0-9 \-_]+)/'))
        			->addValidator('StringLength',false, array(6, 12))
        			->setFilters(array('StringTrim'));       		
        $retypePassword = new NW_Form_Element_Password("checkpassword");
        $retypePassword -> setLabel("Retype Password")
        			->setRequired(true)
        			->addValidator('Regex',false, array('/([a-zA-Z0-9 \-_]+)/'))
        			->addValidator('StringLength',false, array(6, 12))
        			->setFilters(array('StringTrim'))
        			->setErrorMessages(array('Check Password must be 6 to 12 char long'));
        $role = new NW_Form_Element_Select('role');
        $role->setLabel("Ruolo")
        	->addMultiOption('', 'Please Select One')
        	->addMultiOption(App_Model_Role::ROLE_ADMINISTRATOR, "Administrator")
        	->setErrorMessages(array('Select a role'))
        	->setRequired(true);
        
        
        $id = new NW_Form_Element_Hidden('id');
        $email = new NW_Form_Element_Text('email');
        $email -> setLabel('Email')
        		->setRequired(true);
        $this->addElements(array($name,$username, $password, $retypePassword, $email, $id, $role));  
    }

    public function populate(array $values) 
    {    
        $AdaptedValues['name'] = $values["_name"];
        $AdaptedValues['username'] = $values["_username"];      
        $AdaptedValues['email'] = $values["_email"];
        $AdaptedValues['role'] = $values["_role"];
        $AdaptedValues['role'] = $values["_role"]->id;

        parent::populate($AdaptedValues);
    }
    
    public function isValid($data)
    {
    	if($data['password'] != '')
    	{
    		if($data['password'] != $data['checkpassword'])	
	    	$this->addError('Passwords does not match')->markAsError();
	    	return parent::isValid($data);
    	}
    	return parent::isValid($data);
    }
    
    public function firstError()
    {
    	foreach($this->getErrors() as $field => $error)
    		if(!empty($error))
    			return $error[0];
    	foreach($this->getErrorMessages() as $em)
    		if(!empty($em))
    			return $em;
    }
}


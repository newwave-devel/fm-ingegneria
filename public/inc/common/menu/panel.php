
<section class="container">
<?php 
	$common_menu_panel_contents_html='';
	$common_menu_panel_count = 1;
	$common_menu_panel_max_items_per_col = 7;
	
	foreach($glob_disciplinesCat as $disciplineCat) //foreach discipline (= tabbed menu)
	{	
		$common_menu_panel_contents_html .= sprintf('<nav class="menu" id="menu%d">', $common_menu_panel_count).PHP_EOL;
				
		$groupsTaxonomy = new Pages_Model_Taxonomy('disgroupcat', 'disciplines-cat', $disciplineCat->getId());
		$groupsCollection = $pageService->collection()->displayParents($groupsTaxonomy, 'disciplines-group');
		$groupsBlocks = $blockService->display('disciplines-group-main', array('page' => $groupsCollection));

        $output = '';
		$first = true;

        foreach($groupsBlocks as $gB)	//foreach group I extract the disciplines
		{
			$disciplineTaxonomy = new Pages_Model_Taxonomy('disgroupdisc', 'disciplines-group', $gB->getPage());
			$disciplineCollection = $pageService->collection()->displayParents($disciplineTaxonomy, 'disciplines');
			$disciplines = $blockService->display('disciplines-main', array('page' => $disciplineCollection));

            $output .= ($first) ? '<ul>'.PHP_EOL : '<ul class="last">'.PHP_EOL;

            $output .= sprintf('<li class="first"><strong>%s</strong></li>', strtoupper($gB->_("title"))).PHP_EOL;
            foreach($disciplines as $disBlock)
                $output .= sprintf('<li><a href="/discipline.php?id=%d">%s</a></li>', $disBlock->getPage(), $disBlock->_("title")).PHP_EOL;

            $output .= "</ul>".PHP_EOL;
            $first = !$first;
		}
		$common_menu_panel_contents_html .= $output;
		$common_menu_panel_contents_html .= '</nav>'.PHP_EOL;
		$common_menu_panel_count +=1;
	}
?>
<?php echo $common_menu_panel_contents_html; ?>
</section>
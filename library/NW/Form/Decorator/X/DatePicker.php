<?php

class NW_Form_Decorator_X_DatePicker extends NW_Form_Decorator
{

    public function render($content)
    {
        $element = $this->getElement();
       
        if (!$element instanceof Zend_Form_Element)
        {
            return $content;
        }
        if (null === $element->getView())
        {
            return $content;
        }

        $separator = $this->getSeparator();
        $placement = $this->getPlacement();
        $label = $this->buildLabel();
        $input = $this->buildInput();
        $errors = $this->buildErrors();


        $desc = $this->buildDescription();
        $extra_class = $this->getOption('class');
        if (is_array($extra_class))
            $itemClass = empty($extra_class) ? 'formBlock' : sprintf("input %s", implode(" ", $extra_class));
        else
            $itemClass = empty($extra_class) ? 'formBlock' : sprintf("input %s", $extra_class);

        $outputtpl = <<<OUTPUT
                <div class="%s">
             		%s
                        %s
                    	%s
        		<script>
        			%s
        		</script>
                </div>
OUTPUT
        ;

        $output = sprintf($outputtpl, $itemClass, $label, $input, $errors, $this->_js());

        switch ($placement)
        {
            case (self::PREPEND):
                return $output . $separator . $content;
            case (self::APPEND):
            default:
                return $content . $separator . $output;
        }
    }
    
    private function _js()
    {
    	$element = $this->getElement();
    	$id = $element->getAttrib('id');
    	$options = array();
    	$extra_options = $element->getAttrib("params");
    	//parse the extra options
    	$full_options = (is_array($extra_options) && !empty($extra_options)) ? array_merge($options, $extra_options) : $options;
    	$res = array();
    	foreach($full_options as $opt => $value)
    		$res[$opt] = $value;
    
    	$json = Zend_Json::encode($res,false,array('enableJsonExprFinder' => true));
    	$json = Zend_Json::prettyPrint($json, array("indent" => " "));
    
    	return sprintf("$('#%s').datepicker(%s);", $id, $json);
    }

}

?>
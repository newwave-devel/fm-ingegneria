<?php

class NW_Models_Foundation_QueryHelper
{

    public static function find($params, &$classUri, &$moduleDescriptor, &$classDescriptor)
    {
        try {
            $maintable = $moduleDescriptor->table($classUri, "mainTable");

            $base_fields = NW_Db_Helper::getBaseSelectFieldsAsSqlForFo('cat');
            $untranslated_fields = NW_Db_Helper::fieldsArrayToSql($classDescriptor->getUnTranslatedFields(), "mainTable");


            $query = new NW_Db_Query_Select();
            $query->select($base_fields)
                    ->select($untranslated_fields)
                    ->from(array($maintable => "mainTable"))
                    ->innerJoinOn(array('tbl_catalog' => 'cat'), 'cat.id = mainTable.id')
                    ->where(array("cat.id = '?'" => $params['id']));
            foreach($params as $querytpl => $param)
            {
                    if($querytpl =='lang' || $querytpl =='id') continue;
                    $query->where(array($querytpl => $param));
            }
            return $query;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public static function all(NW_Pages_Paginator &$paginator, $params, &$classUri, &$moduleDescriptor, &$classDescriptor, &$displayContext)
    {
        try {
          
            $maintable = $moduleDescriptor->table($classUri, "mainTable");
            
            $untranslated_fields = NW_Db_Helper::fieldsArrayToSql($classDescriptor->getUnTranslatedFields($displayContext->getView()), "mainTable");
            
            if (!empty($untranslated_fields))
            {
                $query = new NW_Db_Query_Select();
                $query->select('cat.*')
                        ->select($untranslated_fields)
                        ->from(array($maintable => "mainTable"))
                        ->innerJoinOn(array('tbl_catalog' => 'cat'), 'cat.id = mainTable.id')
                        ->where(array("cat.status NOT IN ('revision', 'deleted')"));
            } else
            {
                $query = new NW_Db_Query_Select();
                $query->select('cat.*')
                        ->from(array('tbl_catalog' => 'cat'))
                        ->where(array("cat.status NOT IN ('revision', 'deleted')"));
            }
            foreach($params as $paramkey => $paramValue)
                $query->where(array($paramkey => $paramValue));
            
            if (!empty($displayContext) && $displayContext->useOrderBy())
                $query->order($displayContext->getOrderBy());
            $query->limit($paginator->getStart(), $paginator->itemsPerPage);
            return $query;
        } catch (Exception $e) {
            throw $e;
        }
    }

}

?>
<?php

class App_Model_Role_Mapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('App_Model_DbTable_Role');
        }
        return $this->_dbTable;
    }

    public function save(App_Model_Role $role) {
        try {
            $data = $role->export();
            
            $id = $role->getId();
            if (empty($id)) {
                unset($data['id']);
                $this->getDbTable()->insert($data);
            } else {
                $this->getDbTable()->update($data, array('id = ?' => $id));
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function find($id) {
        try {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        return App_Model_Role::import($row);       
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function fetchAll($start = 0, $limit = 40) {
        try {
        $resultSet = $this->getDbTable()->fetchAll(null, null, $limit, $start);
        $entries = array();
        foreach ($resultSet as $row)
            $entries[] = App_Model_Role::import($row);
        return $entries;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function count() {
        try {
        $select = $this->getDbTable()->select();
        $select->from($this->getDbTable(), array('count(*) as amount'));
        $rows = $this->getDbTable()->fetchAll($select);
        return($rows[0]->amount);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function delete($id) {
        try {
            $this->getDbTable()->delete(array("id=?" => $id));
        } catch (Exception $e) {
            throw $e;
        }
    }

}


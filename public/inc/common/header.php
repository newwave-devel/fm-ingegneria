<header>
	<section class="container">
		<h1 id="logo">
			<a href="/index.php">FM Ingegneria</a>
		</h1>
		<nav id="sub_menu">
			<ul>
				<li><a href="company.php" <?php echo ($pageId== 'company') ? 'class="current"': ''?>><?php $translator->{"Menu principale"}->__("Company") ?></a></li>
				<li class="separator">|</li>
                <li><a href="services.php" <?php echo ($pageId== 'services') ? 'class="current"': ''?>><?php $translator->{"Menu principale"}->__("Services") ?></a></li>
				<li class="separator">|</li>
                <li><a href="people.php" <?php echo (($pageId== 'people') || ($pageId== 'people-detail')) ? 'class="current"': ''?>><?php $translator->{"Menu principale"}->__("People") ?></a></li>
				<li class="separator">|</li>
                <li><a href="projects.php" <?php echo (($pageId== 'project-detail') || ($pageId== 'projects')) ? 'class="current"': ''?>><?php $translator->{"Menu principale"}->__("Projects") ?></a></li>
				<li class="separator">|</li>
                <li><a href="news.php" <?php echo (($pageId=='news') || ($pageId== 'news-detail')) ? 'class="current"': ''?>><?php $translator->{"Menu principale"}->__("News") ?></a></li>
			</ul>
		</nav>
		<!--sub_menu-->
        
        <nav id="corporate_menu">
        	<ul>
            	<li><a href="careers.php" <?php echo ($pageId== 'careers') ? 'class="current"': ''?>><?php $translator->{"Menu principale"}->__("Careers") ?></a></li>
				<li><a href="contacts.php" <?php echo ($pageId=='contacts') ? 'class="current"': ''?>><?php $translator->{"Menu principale"}->__("Contacts") ?></a></li>
				<li><a target="_blank" href="https://cloud.fm-ingegneria.com/"><?php $translator->{"Menu principale"}->__("Cloud") ?></a></li>
				<li><a target="_blank" href="https://exchange.fm-ingegneria.com/owa"><?php $translator->{"Menu principale"}->__("Web Mail") ?></a></li>
            </ul>
        </nav><!--corporate_menu-->

		<nav id="language_menu">
			<ul>
				<li><a href="" class="language_disabled"><?php $translator->{"Menu principale"}->__("IT") ?></a></li>
                <li>|</li>
				<li><a class="current" href=""><?php $translator->{"Menu principale"}->__("EN") ?></a></li>
			</ul>
		</nav>
		<!--sub_menu-->

		
		<!--main_menu-->

		
	</section>
	<!--container-->

</header>
<?php

class NW_Descriptors_Application extends NW_XmlDescriptor
{
    /**
     * path for application descriptor
     * @var string
     */
    const APP_DESCRIPTOR_PATH = 'etc/app.xml';

    /**
     *
     * Current Runlevel
     * @var unknown_type
     */
    private $_currentRunlevel;

    /**
     *
     * Current Environment
     * @var Environment
     */
    private $_environment;
    
    public static function load()
    {
        $item = parent::fromFile(APP_PATH.DIRECTORY_SEPARATOR.self::APP_DESCRIPTOR_PATH, __CLASS__);
        $item->loadEnvironment();
        return $item;
    }

    public function detectEnvironment()
    {
        return strval($this->descriptor->config->environment);
    }

    public function getEnvironment()
    {
        return $this->_environment;
    }

    public function getCurrentRunlevel()
    {
        return $this->_currentRunlevel;
    }

    public function loadRunLevel($runlevel)
    {
        $this->_currentRunlevel = new NW_Descriptors_RunLevel($this->descriptor->config->runlevels->$runlevel);
        return $this->_currentRunlevel;
    }

    public function loadModuleManager()
    {
        try {
            return new NW_ModuleManager($this->descriptor->modules);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function loadEnvironment()
    {
        try {
            $env_name = $this->detectEnvironment();
            $this->_environment = new NW_Descriptors_Environment($this->descriptor->environments->$env_name);
        } catch (Exception $e) {
            throw $e;
        }
    }

}
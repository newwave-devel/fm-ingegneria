<?php

class Pages_Model_Block_Observer_Content
{

	public function update(&$subject, $event)
	{
		switch ($event)
		{
			case Pages_Model_Block_Event::EVENT_BEFORE_SAVE :
				break;
			case Pages_Model_Block_Event::EVENT_AFTER_SAVE :
				$this->writeBlockContent($subject);
				break;
			case Pages_Model_Block_Event::EVENT_BEFORE_UPDATE :
				break;
			case Pages_Model_Block_Event::EVENT_AFTER_UPDATE :
				$this->dropBlockContent($subject)
					->writeBlockContent($subject);
				break;
			case Pages_Model_Block_Event::EVENT_ADD :
				break;
			case Pages_Model_Block_Event::EVENT_TRANSLATE :
				break;
			case Pages_Model_Block_Event::EVENT_BEFORE_DELETE :
				break;
			case Pages_Model_Block_Event::EVENT_AFTER_DELETE :
				$this->dropBlockContent($subject);
				break;
		}
	}
	
	public function writeBlockContent(Pages_Model_Block &$subject)
	{
		try {
			foreach($subject->getContents() as $label => $value)
			{
				switch($label)
				{
				    case'file':
				    	$values = explode(",", $value);
				    	if($value == $values)
				    		$this->_insert($label, $value, $subject->getPage(), $subject->getId());
				    	else foreach($values as $val)
				    		$this->_insert($label, $val, $subject->getPage(), $subject->getId());
				    	break;
				    default:
				    	$this->_insert($label, $value, $subject->getPage(), $subject->getId());
				}
				
			}
			return $this;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	
	private function _insert($label, $value, $page, $id)
	{
		try{
			$adapter = Zend_Db_Table::getDefaultAdapter();
			$data = array(
					"content" => $value,
					"label" => $label,
					"page" => $page,
					"block" => $id
			);
			$adapter->insert("tbl_block_contents", $data);
		}catch(Exception $e)
		{
			throw $e;
		}
	}
	public function dropBlockContent(Pages_Model_Block $subject)
	{
		try{
			$blockId = $subject->getId();
			$adapter = Zend_Db_Table::getDefaultAdapter();
			$adapter->delete("tbl_block_contents", "block = $blockId" );
			return $this;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
}
<?php
class Pages_Model_Block_Main_Mapper
{
	protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Pages_Model_DbTable_Block_Main');
        }
        return $this->_dbTable;
    }

	public function query(NW_Pages_Paginator &$paginator, $paramsSearch=array(), $fields=array(), $paramsOrder=array())
    {
        try{
            $table = $this->getDbTable ();
            $select = $table->select ();
            if($fields)
                $select->from($table, $fields);

            $this->_filterBySearchParam ( $select, $paramsSearch )->_filterByUser ( $select );
            if (! $paginator->getFetchAll ())
                $select->limit ( $paginator->itemsPerPage, $paginator->getStart () );

            if (! empty ( $paramsOrder )) {
                foreach ( $paramsOrder as $key => $dir ) {
                    $sql = sprintf ( "%s %s", $key, strtoupper ( $dir ) );
                    $select->order ( $sql );
                }
            }
            $entries = array ();

            foreach ( $table->fetchAll ( $select ) as $row ) {
                $model = new stdClass();
                $this->_fromDb ( $model, $row );
                $entries [] = $model;
            }
            $paginator->itemsCount = $this->count ( $select );

            return $entries;
        } catch(Exception $e)
        {
            throw $e;
        }
    }

    private function _filterByUser(Zend_Db_Table_Select &$select)
    {
// 		$loggedUser = Zend_Auth::getInstance()->getStorage()->read();
// 		$loggedUserId = $loggedUser->getId();
// 		return $this;
    }

    private function _filterBySearchParam(Zend_Db_Table_Select &$select, $paramsSearch = array()) {

        if (!empty($paramsSearch)) {
            if (isset($paramsSearch['page']))
            {
                if (!empty($paramsSearch['page']))
                {
                    if(is_array($paramsSearch['page']))
                    {
                        $select->where("page IN (?)", $paramsSearch['page']);
                    }
                    else $select->where("page = ?", $paramsSearch['page']);
                }
                else $select->where("page = ?", '');
            }
            if (!empty($paramsSearch['status']))
            {
                if(is_array($paramsSearch['status']))
                    $select->where("status IN (?)", $paramsSearch['status']);
                else $select->where("status = ?", $paramsSearch['status']);
            }
            if (!empty($paramsSearch['block']))
            {
                if(is_array($paramsSearch['block']))
                    $select->where("block IN (?)", $paramsSearch['block']);
                else $select->where("block = ?", $paramsSearch['block']);
            }
            $searchables = array("varchar1", "varchar2", "varchar3", "varchar4", "varchar5", "text1", "text2", "text3", "file1", "file2", "file3", "date1", "date2", "date3");
            foreach($searchables as $key):
                if (isset($paramsSearch[$key]))
                {
                    if(!empty($paramsSearch[$key])) {
                        if(is_array($paramsSearch[$key]))
                            $select->where("id IN (?)", $paramsSearch[$key]);
                        else $select->where("id = ?", $paramsSearch[$key]);
                    } else $select->where("$key = ?", ''); // in this way, it is safe for query based on collections
                }
            endforeach;

        }
        return $this;
    }

    private function _fromDb(stdClass &$model, $row) {
        foreach ($row as $key => $value)
            $model->$key = $value;
    }

    public function count(Zend_Db_Select $select) {
        try {
            $table = $this->getDbTable ();
            $select->reset ( Zend_Db_Select::COLUMNS )->reset ( Zend_Db_Select::LIMIT_COUNT )->reset ( Zend_Db_Select::LIMIT_OFFSET )->columns ( array (
                'num' => new Zend_Db_Expr ( 'COUNT(*)' )
            ) );
            return $table->fetchRow ( $select )->num;
        } catch ( Exception $e ) {
            throw $e;
        }
    }
}
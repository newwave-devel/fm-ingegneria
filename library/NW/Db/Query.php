<?php
class NW_Db_Query
{
	
	const RIGHT_JOIN_ON = 1;
	const LEFT_JOIN_ON = 2;
	const INNER_JOIN_ON = 3;

	const RIGHT_JOIN_USING = 4;
	const LEFT_JOIN_USING = 5;
	const INNER_JOIN_USING = 6;
        
        const EXTRACT_FIELD_NAME = 10;
	
	protected $_sql;
	
	public function __construct()
	{
		$this -> _sql = '';
	}
	
	protected function getQuery()
	{
		return $this -> _sql;
	}
        
        public static function tokenizeWhere($paramWhere, $extract = self::EXTRACT_FIELD_NAME)
        {
            list($field, $operator, $value) = explode(" ", $paramWhere,2);
            $field = self::extractField($field);
            if($extract == self::EXTRACT_FIELD_NAME)
                return $field;
        }
        
        private static function extractField($field)
        {
            $pos = strpos($field, "(");
            if($pos!==false)
            {
                $field = substr ($field, $pos);
                $field = str_replace(array("(", ")"), "", $field);
            }
            $pos = strpos($field, ".");
            if($pos !== false)
                $field = substr ($field, $pos+1);
            $field = str_replace(array("`"), "", $field);
            return $field;
        }
}
<?php
if($newsBlocks):
	$isBottom = isset($isBottom) ? $isBottom : false;
	require_once("inc/tpls/blocks/list/tpl.php");
	require_once("inc/functions/blocks.php");
	$col_sx = '';
	$col_dx = '';
	$odd = true;
	foreach($newsBlocks as $block)
	{
		if($odd)
		{
			$col_sx .= renderBlockNews($tpl_item_news, $block, $blockService, $odd, $isBottom);
			$odd = false;
		} else {
			$col_dx .= renderBlockNews($tpl_item_news, $block, $blockService, $odd, $isBottom);
			$odd = true;
		}
		
	}
	?>
	<aside id="mainCol">
		<?php echo $col_sx ?>
	</aside>
	<aside id="sidebar">
		<?php echo $col_dx ?>
	</aside>
	<?php unset($col_sx); ?>
	<?php unset($col_sx); ?>
	<?php unset($newsBlocks); ?>
<?php endif; ?>
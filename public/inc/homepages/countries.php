<?php if($regionsCollection): ?>
<div id="tabs">
	<div id="tab_menu">
	<ul>
	<?php 
		$countries_menu_blocks = $blockService->display('regions-main', array("id" => $regionsCollection));
		$countries_menu_count = count($countries_menu_blocks);
		$count = 0;
		foreach($countries_menu_blocks as $block):
			$countries_menu_class = (--$countries_menu_count == 0 ) ? 'class="ultimo"' : '';
			echo sprintf('<li><a %s href="#tabs-%d">%s</a></li>', $countries_menu_class, ++$count, $block->_('title'));
		endforeach;
	?>
	</ul>
    </div><!--tab_menu-->
	<!--  tabs -->
		<?php 
			
		$pos = 1;
		foreach ($regionsCollection as $region) 
		{
			echo sprintf('<div id="tabs-%d">', $pos);
			$curPos = sprintf("reg%d", $pos);
			$blocks = $pageService->displayShared($curPage->getId(), array("position" => $curPos), true, 2);
			include "inc/homepages/blocks/bottom-list.php";
			$pos += 1; 
			echo '</div>';
		}
		?>
	<!--  tabs -->
</div>
<?php endif; ?>
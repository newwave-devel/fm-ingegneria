<?php

class Pages_PageController extends NW_Controller_Action {
    private $_service;
    protected $_request;
    private $_adminService;
    private $_display;
    
    public function init() {
        $this->_service = new Pages_Model_Page_Service();
        $this->_translateAdapter = Zend_Registry::get('Zend_Translate');
        $this->_request = $this->getRequest();
        $this->_adminService = new Pages_Model_Page_Admin_Service($this->_request, $this->view);
        $this->_display = App_Model_Display_Service::getInstance()->getDisplay();
    }

    public function indexAction() {
        // action body
    }

    public function listAction() {
        try {
            $this->_setAjaxAction()
                ->_addOnload('NW_Tablesorter.register(".tabella").sort()');
            
            if(!$this->_hasParam('type')) throw new Exception ("No type specified");
            $this->_adminService->getList();
            $this->_helper->switchContextByUser();
            $this->view->paginationController = 'frmSearch';
            $this->view->content_type = $this->_getParam("type");

        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function editAction() {
    	try {
    		$this->_setAjaxAction();
    		 
    		$this->_helper->switchContextByUser();
    
    		if(!$this->_hasParam('id'))
    			throw new Exception ("No id specified");
    		$pageId = $this->_getParam("id");
    
    		$this->_adminService->getEdit();
    		$taxonomies = $this->view->taxonomies;
    		if(!empty($taxonomies) || $this->view->shared)
    			$this->_addTabContainer();
    		
    		if(!empty($this->view->taxonomies))
    		{
    			foreach($this->view->taxonomies as $taxonomy)
    			{
    				$tabUrl = '/pages/taxonomy/select/pageid/'.$pageId.'/name/'.$taxonomy->getName();
    				$this->_addTab($tabUrl, $taxonomy->getLabel());
    			}
    		}
    		if($this->view->shared)
    		{
    			$tabUrl = '/pages/block/shared/page/'.$pageId;
    			$this->_addTab($tabUrl, "Costruisci Pagina");
    		}
    		$this->_addOnload("window.main_page.setPageId($pageId);");
    	} catch (Exception $e) {
    		throw $e;
    	}
    }
    
    public function singleAction() {
    	try {
    		$this->_setAjaxAction();
    		$this->_helper->switchContextByUser();
        	if(!$this->_hasParam('type')) throw new Exception ("No type specified");
        	$paramsSearch = array("template" => $this->_getParam("type"));
            $entry = $this->_service->first($paramsSearch);
            
            if($entry == false) 
            	$this->_redirect("/pages/page/new/type/".$this->_getParam("type"));
            	
            $pageId = $entry->getId();
            
    		$this->_adminService->getEdit($pageId);
    		
    		if(!empty($this->view->taxonomies))
    		{
    			$this->_addTabContainer();
    			
    			foreach($this->view->taxonomies as $taxonomy)
    			{
    				$taxonomyUrl = '/pages/taxonomy/select/pageid/'.$pageId.'/name/'.$taxonomy->getName();
    				$this->_addTab($taxonomyUrl, $taxonomy->getLabel());
    			}
    		}
    		$this->_addOnload("window.main_page.setPageId($pageId);");
    	} catch (Exception $e) {
    		throw $e;
    	}
    }
    
    public function newAction() {
    	try {
    		$this->_setAjaxAction();
    		$this->_helper->switchContextByUser();
    		
    		if(!$this->_hasParam('type')) throw new Exception ("No type specified");	
    		
    		$this->_adminService->getNew();
    		
    	} catch (Exception $e) {
    		throw $e;
    	}
    }
    
    public function translateAction()
    {
    	try{
    		$this->_helper->switchContextByUser();
    		$this->_setAjaxAction();
    		if(!$this->_hasParam('id'))
    			throw new Exception ("No id specified");
    		if(!$this->_hasParam('lang'))
    			throw new Exception ("No lang specified");
    		
    		$this->_adminService->getTranslation();
    		$pageId = $this->view->mainPage->getId();
    		if(!empty($this->view->taxonomies))
    		{
    			$this->_addTabContainer();
    			 
    			foreach($this->view->taxonomies as $taxonomy)
    			{
    				$taxonomyUrl = '/pages/taxonomy/select/pageid/'.$pageId.'/name/'.$taxonomy->getName();
    				$this->_addTab($taxonomyUrl, $taxonomy->getLabel());
    			}
    		}
    		$this->_addOnload("window.main_page.setPageId($pageId);");
    	} catch(Exception $e)
    	{
    		throw $e;
    	}
    } 
    
    public function addblockAction()
    {
    try {
            $this->_setIframeAction();
            $this->_helper->switchContextByUser();

            
            $this->view->setEscape('stripslashes');
            
            if ($this->_request->isPost()) 
            {
            	if(!$this->_hasParam('pageid')) throw new Exception ("no template specified");
            	//needs to re-add items to the select
            	$pageId = $this->_request->getParam('pageid');
            	$pageDTO = $this->_service->load($pageId);
                $form = new Pages_Form_ChooseBlockTemplate(array("blocks" => $pageDTO->getBlocks()));
            	$order = $this->_request->getParam('order');
            	$form->setTemplates($pageDTO->getPage()->getTemplate());
            	//needs to re-add items to the select
                if ($form->isValid($this->_request->getPost())) 
                {              	
                    $pageId = $this->_request->getParam('pageid');
                    
                    $blocktemplate = $this->_request->getParam('template');
                    $pageBlock = Pages_Model_Block_Factory::getBlock($blocktemplate);
                    $pageBlock
                    	->setLang($pageDTO->getPage()->getLang())
                        ->setOrder($order)
                    	->setPage($pageId);
                    
                    $pageblockService = new Pages_Model_Block_Service();
                    $pageblockService->save($pageBlock);
                    $this->_addOnload('parent.window.main_page.addBlock();');
                }
                else 
                	$this->view->form = $form;
            }
        } catch (Exception $e) {
        	print $e->getMessage();
        }
    }
    
    public function selecttemplateAction()
    {
    	try {
    		$this->_setIframeAction();
    		if(!$this->_hasParam("id")) 
    			throw new Exception("no page id received");
    		$pageId = $this->_getParam("id");
    		$pageDTO = $this->_service->load($pageId);
    		$this->_helper->switchContextByUser();

    		$form = new Pages_Form_ChooseBlockTemplate(array("blocks" => $pageDTO->getBlocks()));
    		$form->getElement("pageid")->setValue($pageId);
    		$form->setTemplates($pageDTO->getPage()->getTemplate());
    		$this->view->form = $form;
    	} catch(Exception $e)
    	{
    		throw $e;
    	}
    }


    public function saveAction()
    {
    	try {
    		$this->_helper->switchContextByUser();
    		$this->_setJsonAction();		
    		$form = new Pages_Form_Page();
    		$status = new NW_View_RetCode();
    		if ($this->_request ->isPost())
    		{
    			if ($form->isValid($this->_request->getPost()))
    			{
    				$pageTemplate = $form->getElement("template")->getValue();
    				$model = Pages_Model_Page_Factory::getPage($pageTemplate);
    				
    				$model->setOptions($form->getValues());
    				$itemId = $this->_service->save($model);
    				
                    $status->setStatusSuccess()
                    	->addToPayload('id', $itemId);
    			}
    			else {		
    				$errors = '';
    				foreach($form->getMessages() as $field => $msg)
    				{
    				    if(is_array($msg))
    				    {
    				        foreach($msg as $label => $error)
    				            if(is_array($error))
    				                $errors .= sprintf("<strong>Campo %s</strong>: %s<br />", $field, current($error));
    				            else $errors .= sprintf("<strong>Campo %s</strong>: %s<br />", $field, $error);
    				    } else 
    				        $errors .= sprintf("<strong>Campo %s</strong>: %s<br />", $field, $msg);
    				}
    				$status->setPayload($errors);
    			}
    			$this->view->status = $status;
    		}
    	} catch(Exception $e)
    	{
    		$this->view->status = NW_View_RetCode::error($e->getMessage());
    	}
    }
    
    public function savesharedAction()
    {
    	try{
    		$this->_helper->switchContextByUser();
    		$this->_setJsonAction();
    		if(!$this->_hasParam("page")) throw new Exception("no page specified");
    		
    		$this->_adminService->saveShared();

    	}
    	catch(Exception $e)
    	{
    		$status = NW_View_RetCode::error($e->getMessage());
    		$this->view->status = $status;
    	}
    }
    
    public function searchAction() {
        try {
            $this->_setAjaxAction();
            $this->_helper->switchContextByUser();
            //--- SEARCH FORM
        	$form = new Pages_Form_PageSearch();
        	
        	//--- SEARCH FORM
            $params = array();
            if ($this->_request->isPost()) 
            {
                if ($form->isValid($this->_request ->getPost())) 
                {
                    $params = $form->getValues();
                    $this->_addOnload('NW_Tablesorter.register(".tabella").sort()');
                    $this->_adminService->search($params);
                    $this->view->formSearch = $form;
                }
                else
                    $this->_forward('list');
            }
        } catch (Exception $e) {
            throw $e;
        }
    }
    
//     public function composeAction()
//     {
//     	try {
//     		$this->_helper->switchContextByUser();
//     		$this->_setAjaxAction();
//     		$this->_adminService->compose();
//     	} catch(Exception $e)
//     	{
//     		throw $e;
//     	}
//     }
    public function deleteAction()
    {
    	try {
    	    $this->_helper->switchContextByUser();
    		$this->_setJsonAction();
    		if(!$this->_hasParam('id')) throw new Exception("no id received");
    		$id = $this->_getParam('id');
    		$page = $this->_service->find($id);
    		$template = $page->getTemplate();
    		$this->_service->delete($id);
    		$status = NW_View_RetCode::success("/pages/page/list/type/$template");
    		$this->view->status = $status;
    	} catch(Exception $e)
    	{
    		$status = NW_View_RetCode::error($e->getMessage());
    		$this->view->status = $status;
    	}
    }
    
    

}


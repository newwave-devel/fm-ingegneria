<span id="prjList">
    <?php if($projects): ?>
        <?php
            $project_list_list_count = 0;
            $project_list_list_html = '';
            foreach($projects as $project):
                $nomargin = (++$project_list_list_count%3 == 0) ? 'no_margin' : '';
                $img = $project->_("file");
                $project_list_list_html .= sprintf($tpl_listItem, $nomargin, $project->getPage(), $blockService->asset($img)->getAssetUrl(), $project->_("title"),  $project->_("year"), $project->_("title"));
            endforeach;
        ?>
        <?php echo $project_list_list_html; ?>
        <?php else : ?>
            <?php $translator->{"Pagina 'Progetti'"}->__("Nessun risultato trovato"); ?>
        <?php endif;  ?>
	<div class="clearer"></div>
	<?php include "inc/common/pagination.php"; ?>
</span>


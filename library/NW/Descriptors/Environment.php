<?php

class NW_Descriptors_Environment extends NW_XmlDescriptor
{

    
    public function resource($type, $name)
    {
        return new NW_Resources_Resource($this->descriptor->resources->$type->$name);
    }
    
    public function defaultResource()
    {
        foreach($this->descriptor->resources->database as $res)
        {
            if($res['default']=='Y') 
                return new NW_Resources_Resource($this->descriptor->resources->$type->$name);
        }
        return false;
    }

    public function getAdminUrl()
    {
        return $this->descriptor->backend_url;
    }

    public function getAppUrl()
    {
        return $this->descriptor->frontend_url;
    }
    
    

    /**
     * run() loads the Environment, that is mainly the resources. (Db, Emails)
     * Actually, it makes them available for loading, I'm still not sure where to make them load.
     * As they are available for the whole app, I think it doesn't matter too much, so for the moment I
     * load them here. ACTUALLY IT WOULD BE BETTER IF:
     * 	-	This could be open for adding extra - resource without modify (Extension of Environment?)
     *
     * Enter description here ...
     */
    public function run()
    {
        try {
            foreach ($this->descriptor->resources->children() as $resourcePool)
            {
                $resourceType = $resourcePool->getName();
                foreach($resourcePool->children() as $resource)
                {
                    if($resource['persistent'] == 'Y')
                        NW_Adapters_AdapterFactory::getAdapter($resource, $resourceType)->enrollSingleton();
                }
            }
            
        } catch (Exception $e) {
            throw $e;
        }
    }

}
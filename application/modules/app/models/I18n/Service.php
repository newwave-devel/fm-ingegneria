<?php
class App_Model_I18n_Service {

    protected $_mapper;
    
    public function getMapper()
    {
    	return $this->_mapper;
    }
    
    public function __construct(){
    	$this->_mapper = new App_Model_I18n_Mapper();
    }

    public function find($paramID, &$model = null)
    {
        try{
            $model = (empty($model)) ? new App_Model_I18n() : $model;
            $this->_mapper->find($paramID, $model);
            return $model;
        } catch(Exception $e)
        {
            throw $e;
        }
    }

    public function delete($paramId)
    {
    	try {
    		$this->_mapper->delete($paramId);
            return true;
    	} catch(Exception $e)
    	{
    		throw $e;
    	}
    }

    public function save(App_Model_I18n $model)
    {
        try {
            $res =  $this->_mapper->save($model);
            return $res;
        } catch (Exception $e) {
            throw $e;
        }
    }


    public function fetchAll(NW_Pages_Paginator &$paginator, $paramsSearch = array(), $paramsOrder =array("context" => "ASC", "key" => "ASC"))
    {
        try
        {
            return $this->_mapper->fetchAll($paginator, $paramsSearch, $paramsOrder);
        } catch(Exception $e)
        {
            throw $e;
        }
    }
    
    public function contextsAsSelect()
    {
        try{
            return $this->_mapper->contextAsSelect();
        } catch(Exception $e)
        {
            throw $e;
        }
    }

    public function __($label, $context)
    {

    }
    


}

?>
<?php

class Pages_Form_Block_Shared extends Zend_Form {
	
	private $_positions;
    
    public function init() 
    {
        $this->setMethod('post')
                ->setAttrib('class', 'form ajaxpagesubmit')
                ->setAction("/pages/page/saveshared")
                ->setAttrib("id", "composableform");
        $this->setDecorators(array('FormElements','Form'));
        
        $pageId = new NW_Form_Element_Hidden('page');
        $pageId->setRequired(true);
        
        foreach($this->_positions as $pos)
        {
        	$key = (string)$pos['id'];
        	$subForm = new Zend_Form_SubForm();
        	$subForm->setDecorators(array('FormElements'));
        	$item = new NW_Form_Element_Hidden("id");
        	$subForm->addElement($item);
        	$this->addSubForm($subForm,$key);
        }
        
        $this->addElement($pageId);
    }
    
    public function setPositions($pos)
    {
    	$this->_positions= $pos;
    }

    public function getValues($suppressArrayNotation = false) {
        $values = parent::getValues($suppressArrayNotation);
        return $values; 
    }
}


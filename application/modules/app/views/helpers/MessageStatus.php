<?php

class Zend_View_Helper_MessageStatus extends Zend_View_Helper_Abstract {

    public $view;
  
    public function setView(Zend_View_Interface $view) {
        $this->view = $view;
    }

    public function messagestatus($status) {
    	switch($status)
    	{
	        case App_Model_Message::STATUS_NEW:
	        	return "New";
	        case App_Model_Message::STATUS_RECEIVED:
	        	return "Received";
	        case App_Model_Message::STATUS_SENT:
	        	return "Sent";
	    }
    }

}

?>

<?php

/**
 * this is the entry point of the application.
 * should just ask for login
 */
class IndexController extends Zend_Controller_Action{

    public function init() {
        
    }

    public function indexAction() {
        try {
            if (!Zend_Auth::getInstance()->hasIdentity())
                $this->_forward('index', 'login', 'app');
            else
            {
                $this->_forward('index', 'adminpanel', 'app');
            }
        } catch (Exception $e) {
            
        }
    }
}


<?php
class App_Model_Message_Recipient_Mapper
{
	protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('App_Model_DbTable_Recipients');
        }
        return $this->_dbTable;
    }
    
    public function save(App_Model_Message_Recipient $recipient)
    {
    	try {
	    	$data = $this->_toDb($recipient);
	        if (null === ($id = $recipient->getId()) || empty($id)) {
	            unset($data['id']);
	            return $this->getDbTable()->insert($data);
	        } else {
	            $this->getDbTable()->update($data, array('id = ?' => $id));
	            return $id;
	        }	
    	} catch(Exception $e)
    	{
    		throw $e;
    	}
    }
    
    public function count(Zend_Db_Select $select=null) {
    	try {
    		$table = $this->getDbTable();
    		if(!empty($select))
    		{
    			$select->reset(Zend_Db_Select::COLUMNS)
    			->reset(Zend_Db_Select::LIMIT_COUNT)
    			->reset(Zend_Db_Select::LIMIT_OFFSET);
    		} else{
    			$select = $this->getDbTable()->select(Zend_Db_Table::SELECT_WITH_FROM_PART);
    		}
    		$select->columns(array('num' => new Zend_Db_Expr('COUNT(*)')));
    		return $table->fetchRow($select)->num;
    	} catch (Exception $e) {
    		throw $e;
    	}
    }
    
    public function delete($criteria_as_array)
    {
    	try {
    		$where = array();
    		foreach($criteria_as_array as $key => $value)
    			$where[] = $this->getDbTable()->getAdapter()->quoteInto($key, $value);
    		$this->getDbTable()->delete($where);
    	    return true;
    	} catch (Exception $e) {
    		throw $e;
    	}
    }
    
    
    private function _toDb(App_Model_Message_Recipient $recipient)
    {	
    	$result = array();
    	$result['id'] = $recipient->getId();
    	$result['address'] = $recipient->getAddress();
    	$result['name'] = $recipient->getName();
    	$result['send_as'] = $recipient->getSendas();
    	$result['message'] = $recipient->getMessage();
    	return $result;
    }
    
    //needs to be public because it is called by message mapper
    public function _loadFromResultSet($data)
    {
    	$recipient = new App_Model_Message_Recipient();
    	$this->_fromDb($recipient, $data);
    	return $recipient;
    }
    
    private function _fromDb(App_Model_Message_Recipient &$recipient, $data)
    {
    	$recipient->setId($data->id);
    	$recipient->setAddress($data->address);
    	$recipient->setName($data->name);
    	$recipient->setSendas($data->send_as);
    	$recipient->setMessage($data->message);
    }
}
?>
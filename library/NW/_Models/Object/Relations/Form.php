<?php

class NW_Models_Object_Relations_Form extends NW_Models_Form {

    private $_helper;
    public $_relName;
    
    public function __construct(NW_ClassURI &$classUri, $relName, $options) {
        parent::__construct($classUri, $options);
        $this->_relName = $relName;
        $this->_helper = new NW_Models_Object_Relations_Form_Helper($this);
    }

    public function create($fieldsName = array(), $fields_options = array()) {
        $model = NW_Application::__model($this->_classUri);
        $this->_helper->addModelFields($fieldsName, $fields_options);
        return $this;
    }
    public function getRelName()
    {
        return $this->_relName;
    }
    
    public function setIsAjaxForm($className)
    {
        parent::setOptions(array("id" => $className));
        $this->getView()->jQuery()->addJavascriptFile('/js/admin/ajaxform.js');
        $this->getView()->jQuery()->addJavascriptFile('/js/admin/NW/ajaxform.js');
        $this->getView()->jQuery()->addOnload($this->_getAjaxFormJs($className));
        return $this;
    }
    
    private function _getAjaxFormJs($className)
    {
        $js = sprintf("NW_AjaxForm.add('%s');", $className);
        $js .= sprintf("NW_AjaxForm.ajaxify('%s');", $className);
        return $js;
    }
    
}

?>

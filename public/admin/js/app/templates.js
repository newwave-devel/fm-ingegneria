var templates =
{
	imagepreview:'<div class="picture" id="item_{parentId}_{id}">'+ 
					'<a class="fancybox.image preview" href="{url}">' +
						'<img class="img" src="/assets/res.php?src={url}&w=120&h=80&zc=2">' +
					'</a>' +
					'<h5>{name}</h5>' +
					'<h6>{size}</h6>' +
					'<ul class="actions">' +
						'<li><a class="edit_icon" onclick="window.main_page.subPage().modifyLibraryItem(this,{id},\'{blockTemplate}\')"><span class="icon_font">&#xf040;</span></a></li>' + 
						'<li><a class="delete_icon" href="#" onclick="window.main_page.removeFile(this); return false;"><span class="icon_font">&#xf014;</span></a></li>' +
					'</ul>' +
					'<input type="hidden" value="{id}" name="contents[file][]" rel="{id}">' +
					'</div><!--picture-->',
	filepreview: '<div class="picture file">' +
					'<h5>{name}</h5>' +
					'<h6>{size}</h6>' +
					'<ul class="actions">' +
						'<li><a class="edit_icon" href="{url}"><span class="icon_font">&#xf019;</span></a></li>' + 
						'<li><a class="delete_icon" href="#" onclick="window.main_page.removeFile(this);return false;"><span class="icon_font">&#xf014;</span></a></li>' +
					'</ul>' +
					'<input type="hidden" value="{id}" name="contents[file][]" rel="{id}">' +
				'</div>'
}
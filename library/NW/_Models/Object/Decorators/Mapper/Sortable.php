<?php

class NW_Models_Object_Decorators_Mapper_Sortable extends NW_Models_Object_Decorators_Mapper
{
    
    public function __construct(NW_Models_Object_Decorators_Ownable &$decoratorRef)
    {
        $this->decoratorRef = $decoratorRef;
        $this->table = 'tbl_catalog';
    }
    
    public function queryInsert(&$object)
    {
       $query = new NW_Db_Query_Update($this->table);
       $query->add(array("ord" => $object->order));
       $query->where(array("id=?" => $object->id));
        return $query;
    }
    
    public function queryUpdate($object)
    {
       $query = new NW_Db_Query_Update($this->table);
       $query->add(array("ord" => $object->order));
       $query->where(array("id=?" => $object->id));
       return $query;
    }
    
    public function queryLoad($query = '')
    {
        if ($query=='') $query = new NW_Db_Query_Select();
        return $query->select("ord", $this->table);
    }
    
    public function queryAll($query='')
    {
        if ($query=='') $query = new NW_Db_Query_Select();
        return $query->select("ord", $this->table);
    }
    
    public function queryView($viewName, $query='')
    {
        if ($query=='') $query = new NW_Db_Query_Select();
        return $query->select("ord", $this->table);
    }
    
    public function queryLoadByRelDefInTable(NW_Db_Query_Select &$query, $relationName)
    {
        if ($query=='') $query = new NW_Db_Query_Select();
        $tableAlias = $this->tableAlias($relationName);
        $whereCond = sprintf("%s.ord = '?'",$tableAlias);
        $query->where(array($whereCond => $this->getDecorator()->getOrder()));
    }
    
//    public function toStdClass(stdClass &$stdClass)
//    {
//        $this->toStdClass($stdClass);
//    }
}

?>

<?php
/**
 * differs from fetchparents, fetchdependends in that here i am not given the invoker ids: these are my results: this class is meant 
 * to answer to questions like : fetch all projects in disciplines (x,y) and in country (z,w) 
 * @author Ale
 *
 */
class Pages_Model_Collection
{
	private $_statement;
	private $_pageTemplate;
	
	public function __construct()
	{
		$this->_pageTemplate = '';
		$this->$_statement = '';
	}
	
	public function select($pageTemplate)
	{
		$this->_pageTemplate = $pageTemplate;
		return $this;
	}
	
	public function addAnd()
	{
		return $this;
	}
	
	public function addOr($txId, $pageIds=array())
	{
		return $this;
	}
	
	public function union($txId, $pageIds=array())
	{
		return $this;
	}
	
	public function getCollection()
	{
		
	}
	
}

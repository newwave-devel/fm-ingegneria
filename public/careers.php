<?php
$pageId = 'careers';
include "inc/required.php";
include "inc/common/contents.php";
?>
<?php include "inc/common/head.php" ?>
		
        
        <!-- SCRIPT -->
		<script type="text/javascript" src="/common/js/core.js"></script>
        <script type="text/javascript" src="/common/js/custom.js"></script>
        
        <script type="text/javascript">	
                jQuery(window).load(function() {
                    // START
					loading();
					fancybox();
                });
        </script>
        
	</head>    
    
	<body lang="it">
    
    	<div id="loader"></div>

        <?php include "inc/common/header.php"?>

        <section id="main_page">
        	<section class="container">
            
            	<section id="page_title">
                    <h3><?php $translator->{"Pagina 'Careers'"}->__("Posizioni Aperte") ?></h3>
                    
                    <div id="candidatura_spontanea">
                    	<h5><?php $translator->{"Pagina 'Careers'"}->__("Candidatura spontanea") ?></h5>
                        <a class="file" data-rel="external"
                           href="mailto:<?php $translator->{"Pagina 'Careers'"}->__("Indirizzo Destinatario CV") ?>?subject=<?php $translator->{"Pagina 'Careers'"}->__("Posizione Generica") ?>">
                            <?php $translator->{"Pagina 'Careers'"}->__("SEND CV") ?>
                        </a>
                    </div><!--candidatura_spontanea-->
                    
                </section><!--page_title-->
                
                <?php include "inc/careers/list.php" ?>

            </section><!--container-->
        </section><!--main_page-->

        <?php include "inc/common/footer.php"?>
	</body>
	
</html>
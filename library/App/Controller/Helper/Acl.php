<?php
class App_Controller_Helper_Acl{
	
	public $acl;
	
	public function __construct() {
		$this->acl = new Zend_Acl();
	}
	
	public function setRoles() {
		$rc = new Zend_Reflection_Class('App_ACL_Roles');
		foreach($rc->getConstants() as $k => $v)
			$this->acl->addRole(new Zend_Acl_Role($v));
		return $this;
	}

	public function setResources() 
	{
		$rc = new Zend_Reflection_Class('App_ACL_Resources');
		foreach($rc->getConstants() as $k => $v)
			$this->acl->add( new Zend_Acl_Resource($v));
		return $this;
	}

	public function setPrivileges() {
		$this->acl->allow(App_ACL_Roles::FLEETMASTER);
		//$this->acl->deny(App_ACL_Roles::FLEETMASTER, null, App_ACL_Resources::CERTIFICATE_CERTIFICATE_DASHLIST);
		return $this;
	}
	
	public function setAcl() {
		Zend_Registry::set('acl', $this->acl);
	}
}
?>
<?php
class Pages_Model_Collection
{
    private $_taxonomyQuery;
    private $_target;
    private $_fieldQuery;
    private $_adapter;

    private $_resultPages = array();
    private $_resultBlocks = array();

    public function __construct()
    {
        $this->_adapter = Zend_Db_Table::getDefaultAdapter();
        //$this->_adapter->setFetchMode(Zend_Db::FETCH_COLUMN);
    }
    public function select($what, $lang)
    {
        $this->_target = $what;
        $activePagesQuery= sprintf("SELECT id FROM tbl_pages WHERE status='%s' AND template='%s' AND lang='%s' ORDER BY `order` ASC", Pages_Model_Page::STATUS_ACTIVE, $this->_target, $lang);
        //Zend_Registry::get("Log")->debug($activePagesQuery);
        $results = $this->_adapter->fetchCol($activePagesQuery);
        $this->_resultPages =  array_values(array_values($results));
        return $this;
    }

    public function parentIn($taxonomyName, $values)
    {
        if(empty($this->_resultPages)) return $this;
        $values = is_array($values) ? implode(",", $values) : $values;
        $parentInQuery= sprintf("SELECT `dependent` as `id` FROM tbl_page_taxonomy WHERE `taxonomy` = '%s' AND `parent` IN(%s) HAVING `dependent` IN(%s) ORDER BY FIELD(`dependent`, %s)", $taxonomyName, $values, implode(',',$this->_resultPages), implode(',',$this->_resultPages));
        //Zend_Registry::get("Log")->debug($parentInQuery);
        $results = $this->_adapter->fetchCol($parentInQuery);
        $this->_resultPages =  array_values(array_values($results));
        return $this;
    }

    public function dependentIn($taxonomyName, $values)
    {
        if(empty($this->_resultPages)) return $this;
        $values = is_array($values) ? implode(",", $values) : $values;
        $dependentInQuery= sprintf("SELECT `parent` as `id` FROM tbl_page_taxonomy WHERE `taxonomy` = '%s' AND `dependent` IN(%s) HAVING `parent` IN(%s) ORDER BY FIELD(`parent`, %s)", $taxonomyName, $values, implode(',',$this->_resultPages), implode(',',$this->_resultPages));
        //Zend_Registry::get("Log")->debug($dependentInQuery);
        $results = $this->_adapter->fetchCol($dependentInQuery);
        $this->_resultPages =  array_values(array_values($results));
        return $this;
    }

    public function mainblockContains($value)
    {
        if(empty($this->_resultPages)) return $this;
        $containsQuery= sprintf("SELECT `page` as `id`
                                        FROM tbl_blocks_mains
                                        WHERE (
                                            (`varchar1` LIKE '%%%s%%')
                                        OR  (`varchar2` LIKE '%%%s%%')
                                        OR  (`varchar3` LIKE '%%%s%%')
                                        OR  (`varchar4` LIKE '%%%s%%')
                                        OR  (`varchar5` LIKE '%%%s%%')
                                        OR (`text1` LIKE '%%%s%%')
                                        OR (`text2` LIKE '%%%s%%')
                                        OR (`text3` LIKE '%%%s%%')
                                        )
                                        AND `page` IN (%s)
                                        ORDER BY FIELD(`page`, %s)",$value,$value,$value,$value,$value,$value,$value,$value,implode(',',$this->_resultPages),implode(',',$this->_resultPages));
        $results = $this->_adapter->fetchCol($containsQuery);
        //Zend_Registry::get("Log")->debug($containsQuery);
        $this->_resultPages =  array_values(array_values($results));
        return $this;
    }

    public function count()
    {
       return count($this->_resultPages);
    }

    public function collection(NW_Pages_Paginator &$paginator)
    {
        $paginator->setItemsCount($this->count());
        $chunks = array_chunk($this->_resultPages, $paginator->getItemsPerPage());
        $curChunk = $paginator->getCurPage() -1;

        if(isset($chunks[$curChunk]))
            return $chunks[$curChunk];
        return array();
    }
}
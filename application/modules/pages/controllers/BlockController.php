<?php

class Pages_BlockController extends NW_Controller_Action {
    private $_service;
    protected $_request;
    private $_adminService;
    private $_display;
    
    public function init() {
        $this->_service = new Pages_Model_Block_Service();
        $this->_translateAdapter = Zend_Registry::get('Zend_Translate');
        $this->_request = $this->getRequest();
        $this->_adminService = new Pages_Model_Block_Admin_Service($this->_request, $this->view);
        $this->display = App_Model_Display_Service::getInstance();
    }

    public function indexAction() {
        // action body
    }


    public function listAction() {
        try {
            $this->_setAjaxAction();
            $paginator = new NW_Pages_Paginator();
            $paginator->setFetchAll(true);
            $this->_addOnload("fancybox()");
            $this->view->entries = $this->_service->fetchChilds($paginator);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function saveAction()
    {
    	try {
    		$this->_helper->switchContextByUser();
    		$this->_setJsonAction();
    		$request = $this->getRequest();
    		if(!$this->_hasParam('template')) throw new Exception ("No type specified");
    		$blockTemplate = $this->_request->getParam("template");
    		$model = Pages_Model_Block_Factory::getBlock($blockTemplate);
    		$form = Pages_Form_Block_Factory::getForm($blockTemplate);
    		
    		$status = new NW_View_RetCode();
    		if ($request->isPost())
    		{
    			if ($form->isValid($request->getPost()))
    			{
    				$model->setOptions($form->getValues());
                    $itemId = $this->_service->save($model);
                    $status->setStatusSuccess()
                    	->addToPayload("id", $itemId);
    			}
    			else {	
    				$errors = '';
    				$status->setStatusError();
    				$errors = '';
    				
    				foreach($form->getMessages() as $fieldContainer => $msg)
    				{
    				    foreach($msg as $field => $error)
    				        $errors .= sprintf("<strong>Campo %s</strong>: %s<br />", $field, current($error));
    				}
    				$status->setPayload($errors);
    			}
    			$this->view->status = $status;
    		}
    	} catch(Exception $e)
    	{
    		$this->view->status = NW_View_RetCode::error($e->getMessage());
    	}
    }
    
    public function editAction()
    {
    	try {
    		$this->_setAjaxAction();
    		if(!$this->_hasParam("id")) throw new Exception("no id specified");
    		$this->_helper->switchContextByUser();
    		
    		$view = $this->_adminService->edit();
    	} catch(Exception $e)
    	{
    		throw $e;
    	}
    }
    
    //this gets called by the tabs
    public function sharedAction() {
    	try {
    		$this->_setAjaxAction();
    		if(!$this->_hasParam("page")) throw new Exception("no page specified");
            $sortDir = $this->_getParam("sort", "DESC");
            $sortDir = ($sortDir=="DESC") ? $sortDir : "ASC";
    		//$this->_addJsSource('/admin/js/vendors/cssnewbie/jquery.equalheights.js');
    		//$this->_addOnload('$( "ul.sortable" ).sortable({connectWith: "ul"});');
    		//$this->_addOnload('$( ".sortable" ).disableSelection().equalHeights();');
    		$this->_helper->switchContextByUser();
    
    		$this->_adminService->shared($sortDir);
    	} catch (Exception $e) {
    		throw $e;
    	}
    }
    
    public function deleteAction()
    {
    	try {
    		$this->_setJsonAction();
    		if(!$this->_hasParam('id')) throw new Exception("no id received");
    		$id = $this->_getParam('id');
    		$this->_service->delete($id);
    		$status = NW_View_RetCode::success();
    		$this->view->status = $status;
    	} catch(Exception $e)
    	{
    		$status = NW_View_RetCode::error($e->getMessage());
    		$this->view->status = $status;
    	}
    }

    public function sortAction()
    {
        try {
            $this->_setJsonAction();
            if(!$this->_hasParam('page')) throw new Exception("no page received");
            $pageId = $this->_getParam('page');
            $newOrder = $this->_getParam('newOrder');
            $newOrderAsArray =  explode(",", $newOrder);
            if($newOrderAsArray === false) return;
            $this->_service->updateOrder($pageId, $newOrderAsArray);

        } catch(Exception $e)
        {
            $status = NW_View_RetCode::error($e->getMessage());
            $this->view->status = $status;
        }
    }

}


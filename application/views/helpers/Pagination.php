<?php

class Zend_View_Helper_Pagination extends Zend_View_Helper_Abstract {

    private $paginator;
    public $view;
    private $pageUrl;
    private $formId;

    public function setView(Zend_View_Interface $view) {
        $this->view = $view;
    }

    public function pagination($formId) {
        $this->paginator = $this->view->paginator;
        $this->pageUrl = sprintf("%s/%s/%s", Zend_Controller_Front::getInstance()->getRequest()->getModuleName(), Zend_Controller_Front::getInstance()->getRequest()->getControllerName(), Zend_Controller_Front::getInstance()->getRequest()->getActionName()
        );
        $this->formId = $formId;
        if ($this->paginator->isAjax) {
            $jquery = sprintf("NW_Pagination.add('#%s', '%s');", $this->formId, $this->paginator->targetId);
            $this->view->jQuery()->addOnload($jquery);
            if($this->paginator->ajaxAction != '')
            	$this->pageUrl = $this->paginator->ajaxAction;
       }
        else {
            $jquery = sprintf("NW_Pagination.add('#%s');", $this->formId);
            $this->view->jQuery()->addOnload($jquery);
        }
        return $this;
    }

    public function previous() {
        $class = ($this->paginator->curPage <= 1) ? 'class="disabilitato"' : sprintf('class="%s"', $this->formId);
        return sprintf('<li class="label"><a %s href="#" rel="%d">%s</a></li>', $class, $this->paginator->curPage - 1, $this->view->translate("Indietro"));
    }

    public function next() {
        $class = ($this->paginator->curPage >= $this->paginator->numPages) ? 'class="disabilitato"' : sprintf('class="%s"', $this->formId);
        return sprintf('<li class="label"><a %s href="#" rel="%d">%s</a></li>', $class, $this->paginator->curPage + 1, $this->view->translate("Avanti"));
    }

    public function pageList() {
        $html = '';
        for ($i = 1; $i <= $this->paginator->numPages; $i++) {
            $classCurrent = ($i == $this->paginator->curPage) ? "current" : '';
            $html .= sprintf('<li><a href="#" class="%s %s" rel="%d">%d</a></li>', $this->formId, $classCurrent, $i, $i);
        }
        $jquery = sprintf("NW_Pagination.get('#%s').registerPageLink('%s');", $this->formId, $this->formId);
        $this->view->jQuery()->addOnload($jquery);
        return $html;
    }

    public function pageSelect() {
        $html = '<select name="pageChange">';
        for ($i = 1; $i <= $this->paginator->numPages; $i++) {
            $classCurrent = ($i == $this->paginator->curPage) ? 'selected="selected"' : '';
            $html .= sprintf('<option %s>%d</option>', $classCurrent, $i);
        }
        $html .= "</select>";
        $jquery = sprintf("NW_Pagination.get('#%s').registerPageSelect('pageChange');", $this->formId);
        $this->view->jQuery()->addOnload($jquery);
        return $html;
    }

    public function ipp() {
        $html = '<select name="itemsPerPageChange">';
        for ($i = 25; $i <= 200; ) {
            $classCurrent = ($i == $this->paginator->itemsPerPage) ? 'selected="selected"' : '';
            $html .= sprintf('<option %s>%d</option>', $classCurrent, $i);
            $i+=25;
        }
        $html .= "</select>";
        $jquery = sprintf("NW_Pagination.get('#%s').registerIppSelect('itemsPerPageChange');", $this->formId);
        $this->view->jQuery()->addOnload($jquery);
        return $html;
    }

    public function pageUrl($params = '') {
        return (empty($params)) ? $this->pageUrl : $this->pageUrl . "/" . $params;
    }

}

?>

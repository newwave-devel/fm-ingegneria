<?php

class NW_Form_Decorator_JQueryFileUpload extends NW_Form_Decorator
{

	
	public function render($content)
	{
		$element = $this->getElement();
		 
		if (!$element instanceof Zend_Form_Element)
		{
			return $content;
		}
		if (null === $element->getView())
		{
			return $content;
		}
	
		$separator = $this->getSeparator();
		$placement = $this->getPlacement();
		$label = $this->buildLabel();
		$input = $this->buildInput();
		$errors = $this->buildErrors();
	
	
		$desc = $this->buildDescription();
		$extra_class = $this->getOption('class');
		if (is_array($extra_class))
			$itemClass = empty($extra_class) ? 'formBlock' : sprintf("input %s", implode(" ", $extra_class));
		else
			$itemClass = empty($extra_class) ? 'formBlock' : sprintf("input %s", $extra_class);
	
		$buttonLabel = ($element->hasFile()) ? sprintf('<span>%s</span>','Override existing file') : sprintf('<span>%s</span>','Upload...');
		$buttonEraseHtml = ($element->hasFile()) ? sprintf('<span class="cancel">%s</span>','Cancel File') : '';
		$buttonClass = ($element->hasFile()) ? 'btn btn-danger fileinput-button"' : 'btn btn-success fileinput-button"';
		$outputtpl = <<<OUTPUT
		<div class="fileElement">
			<h4>%s</h4>
			<div id="progress-%s" class="progress">
					<div class="progress-bar progress-bar-success"></div>
			</div>
					
			<span class="%s">
				<span>%s</span>
				<input class="fileupload" type="file" name="%ss[]" />
					%s
			</span>
			<div id="showhide-%s" class="filebuttons">
				<button class="btn btn-warning cancel" type="reset" onclick="NW_JQueryFileUpload.get('%s').cancelUpload();">
					<span>Cancel upload</span>
				</button>
				<div class="clearer"></div>  
			</div>
			<ul class="uploadedfiles" id="uploaded-%s"></ul>
		</div>
OUTPUT
		;
		
		$output = sprintf($outputtpl, $element->getLabel(), $element->getName(), $buttonClass, $buttonLabel, $element->getName(),$input, $element->getName(),$element->getName(), $element->getName());
	
		switch ($placement)
		{
		    case (self::PREPEND):
		    	return $output . $separator . $content;
		    case (self::APPEND):
		    default:
		    	return $content . $separator . $output;
		}
	}

}

?>
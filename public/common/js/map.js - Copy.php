<script type="text/javascript">

    var map;
    var projects = <?php echo json_encode($projectsHavingGps, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP); ?>;

    function initialize() {
        var mapOptions = {
            center: new google.maps.LatLng(0.0, 0.0),
            zoom: 3,
            styles: [
			{featureType:'poi', elementType:'all', stylers:[{
				hue:'#000000'},
				{saturation:-100},
				{lightness:-100},
				{visibility:'off'}
				]},
				
			{featureType:'poi',elementType:'all',stylers:[{
				hue:'#000000'},
				{saturation:-100},
				{lightness:-100},
				{visibility:'off'}
				]},
				
			{featureType:'administrative', elementType:'all', stylers:[{
				hue:'#000000'},
				{saturation:0},
				{lightness:-100},
				{visibility:'off'}
				]},
				
			{featureType:'road', elementType:'labels', stylers:[{
				hue:'#ffffff'},
				{saturation:-100},
				{lightness:100},
				{visibility:'off'}
				]},
				
			{featureType:'water', elementType:'labels', stylers:[{
				hue:'#000000'},
				{saturation:-100},
				{lightness:-100},
				{visibility:'off'}
				]},
				
			{featureType:'road.local', elementType:'all', stylers:[{
				hue:'#ffffff'},
				{saturation:-100},
				{lightness:100},
				{visibility:'on'}
				]},
				
			{featureType:'water', elementType:'geometry', stylers:[{
				hue:'#ffffff'},
				{saturation:-100},
				{lightness:100},
				{visibility:'on'}
				]},
			{featureType:'transit', elementType:'labels', stylers:[{
				hue:'#000000'},
				{saturation:0},
				{lightness:-100},
				{visibility:'off'}
				]},
			
			{featureType:'landscape', elementType:'labels',	stylers:[{
				hue:'#000000'},
				{saturation:-100},
				{lightness:-100},
				{visibility:'off'}
				]},
				
			{featureType:'road', elementType:'geometry', stylers:[{
				hue:'#bbbbbb'},
				{saturation:-100},
				{lightness:26},
				{visibility:'on'}
				]},
			
			{featureType:'landscape', elementType:'geometry', stylers:[{
				hue:'#dddddd'},
				{saturation:-100},
				{lightness:-3},
				{visibility:'on'}
				]}
				]
        };
		
        map = new google.maps.Map(document.getElementById("map"), mapOptions);
        var bounds  = new google.maps.LatLngBounds();
        var markers = [];
		var icon1 = 'common/img/poi_icon_a.png';
		var icon2 = 'common/img/poi_icon_c.png';

        for (var i  in projects)
        {	
			

            var ltln = projects[i].coords.split(",");
            var desc = projects[i].title;
            var myltln = new google.maps.LatLng(ltln[0], ltln[1]);
            var marker = new google.maps.Marker({
                position: myltln,
                map: map,
				icon: icon1,
				animation: google.maps.Animation.DROP
            });
            var infowindow = new google.maps.InfoWindow({
				content: '<div id="gm_content">'+desc+'</div>'
                //content: desc
            });
			
			
            makeInfoWindowEvent(map, infowindow, marker);

            markers.push(marker);
            bounds.extend(myltln);
			
			google.maps.event.addListener(marker, 'mouseover', function() {
				this.setIcon(icon2);
			});
			
			google.maps.event.addListener(marker, 'mouseout', function() {
				this.setIcon(icon1);
			});
		
        }
		
		
				

        var initial = true;
        google.maps.event.addListener(map, "zoom_changed", function() {
            if (initial == true){
                if (map.getZoom() > 2) {
                    map.setZoom(2);
                    initial = false;
                }
            }
        });
        map.fitBounds(bounds);
		
		
		var clusterStyles = [
			{
			  textColor: '#FFFFFF',
			  textSize: 10,
			  fontWeight: 'bold',
			  url: 'common/img/poi_icon_b.png',
			  height: 23,
			  width: 24
			}
		  ];

            var mcOptions = {
            gridSize: 60,
            styles: clusterStyles,
            maxZoom: 15
            };
            var markerclusterer = new MarkerClusterer(map, markers, mcOptions);		
	
		
		google.maps.event.addListener(map, 'zoom_changed', function()
		{
			if (map.getZoom() < 2){
			   map.setZoom(2);
			}
		});
	
    }
	
	

    function makeInfoWindowEvent(map, infowindow, marker) {
        google.maps.event.addListener(marker, 'click', function() {
			if (infowindow) infowindow.close();
            infowindow.open(map, marker);
        });
    }
	
	
	
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<?php
class App_Model_Message_Mapper
{
	protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('App_Model_DbTable_Message');
        }
        return $this->_dbTable;
    }
    
    public function load($id, App_Model_Message &$message)
    {
    	try {
            $result = $this->getDbTable()->find($id);
            if (0 == count($result)) {
                return;
            }
            $row = $result->current();
            $this->_fromDb($message, $row, true); 
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function find($id, App_Model_Message &$message)
    {
    	try {
    		$result = $this->getDbTable()->find($id);
    		if (0 == count($result)) {
    			return;
    		}
    		$row = $result->current();
    		$this->_fromDb($message, $row);
    	} catch (Exception $e) {
    		throw $e;
    	}
    }
    
    public function delete($paramsAsArray) {
    	try {
    		$where = array();
    		foreach($paramsAsArray as $key => $value)
    			$where[] = $this->getDbTable()->getAdapter()->quoteInto($key, $value);
    	    $this->getDbTable()->delete($where);
    	    return true;
    	} catch (Exception $e) {
    		throw $e;
    	}
    }
    
    
    public function fetchAll(NW_Pages_Paginator &$paginator, $paramsSearch, $paramsOrder=array(), $load = false)
    {
    	try {
    		$table = $this->getDbTable();
    		$select = $table->select();
            $select = $this->qFetchAll($paginator, $paramsSearch, $paramsOrder, $load);
            $entries = array();
            foreach ($table->fetchAll($select) as $row) {
                $model = new App_Model_Message();
                $this->_fromDb($model, $row, $load);
                $entries[] = $model;
            }
            $paginator->itemsCount = $this->count($select);
            return $entries;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    protected function qFetchAll(NW_Pages_Paginator &$paginator, $paramsSearch, $paramsOrder=array(), $load = false)
    {
    	try {
            $table = $this->getDbTable();
            $select = $table->select()->from($table);

            $this -> _filterBySearchParam($select, $paramsSearch);
			if(!$paginator->getFetchAll())
            	$select->limit($paginator->itemsPerPage, $paginator->getStart());
            if(!empty($paramsOrder))
            {
               foreach($paramsOrder as $key => $dir)
               {
                   $sql = sprintf("%s %s", $key, strtoupper($dir));
                   $select->order($sql);
               }
            }
            return $select;
        } catch (Exception $e) {
            throw $e;
        }
    }
    public function save(App_Model_Message $message)
    {
    	try {
	    	$data = $this->_toDb($message);
	        if (null === ($id = $message->getId()) || empty($id)) {
	            unset($data['id']);
	            return $this->getDbTable()->insert($data);
	        } else {
	            $this->getDbTable()->update($data, array('id = ?' => $id));
	            return $id;
	        }	
    	} catch(Exception $e)
    	{
    		throw $e;
    	}
    }
    
    public function count(Zend_Db_Select $select=null) {
    	try {
    		$table = $this->getDbTable();
    		$select = $this->qCount($select);
    		
    		return $table->fetchRow($select)->num;
    	} catch (Exception $e) {
    		throw $e;
    	}
    }
    
    protected function qCount(Zend_Db_Select $select=null) {
    	try {
    		$table = $this->getDbTable();
    		if(!empty($select))
    		{
    			$select->reset(Zend_Db_Select::COLUMNS)
    				->reset(Zend_Db_Select::LIMIT_COUNT)
    				->reset(Zend_Db_Select::LIMIT_OFFSET);
    		} else{
    			$select = $this->getDbTable()
    				->select(Zend_Db_Table::SELECT_WITH_FROM_PART)
    				->reset(Zend_Db_Select::COLUMNS);
    		}
    		$select->columns(array('num' => new Zend_Db_Expr('COUNT(*)')));
    		return $select;
    	} catch (Exception $e) {
    		throw $e;
    	}
    }
    
    
    private function _toDb(App_Model_Message $message)
    {
    	
    	$ts_create = $message->getTscreate();
    	$ts_sent = $message->getTssent();
    	$ts_received = $message->getTsreceived();
    	
    	$result = array();
    	$result['id'] = $message->getId();
    	$result['status'] = $message->getStatus();
    	if(!empty($ts_create))
    		$result['ts_create'] = date("Y-m-d h:i:s", $ts_create);
    	if(!empty($ts_sent))
    		$result['ts_sent'] =  date("Y-m-d h:i:s", $ts_sent);
    	if(!empty($ts_sent))
    		$result['ts_received'] =  date("Y-m-d h:i:s", $ts_received);
    	
    	$result['sender'] = $message->getSender();
    	$result['type'] = $message->getType();
    	$result['subject'] = $message->getSubject();
    	$result['body'] = $message->getBody();
    	
    	return $result;
    }
    
    private function _fromDb(App_Model_Message &$message, $data, $load=false)
    {   	
    	$recipientMapper = new App_Model_Message_Recipient_Mapper();
    	$message->setId($data->id)
    		->setBody($data->body)
    		->setSender($data->sender)
    		->setStatus($data->status)
    		->setSubject($data->subject)
    		->setType($data->type);
    	if($load)
    	{
    		$recipients = $data->findDependentRowset('App_Model_DbTable_Recipients');	
    	
	    	foreach($recipients as $rec)
	    	{	
	    		$recipient = $recipientMapper->_loadFromResultSet($rec);
	    		$message->addRecipient($recipient);
	    	}
    	}
    	$ts_create = $data->ts_create;
    	$ts_sent = $data->ts_sent;
    	$ts_received = $data->ts_received;
    	if(!empty($ts_create))
    		$message->setTscreate(NW_Utils_DateTime::strptime($ts_create, "%Y-%m-%d %H:%M:%S"));
    	if(!empty($ts_sent))
    		$message->setTssent(NW_Utils_DateTime::strptime($ts_sent, "%Y-%m-%d %H:%M:%S"));
    	if(!empty($ts_received))
    		$message->setTsreceived(NW_Utils_DateTime::strptime($ts_received, "%Y-%m-%d %H:%M:%S"));
    }
    
    private function _filterBySearchParam(Zend_Db_Select &$select, $paramsSearch, $is_querycount=false)
    {
    	$tableName = $select->getPart(Zend_Db_Select::FROM);
    	$tableName = is_array($tableName) ? key($tableName) : $tableName;
    	
    	if (!empty($paramsSearch)) {
    		if(isset($paramsSearch['sender']))
    			$select->where('sender=?', $paramsSearch['sender']);
    		if(isset($paramsSearch['recipient']))
    		{
    			$select->setIntegrityCheck(false);
    			if($is_querycount)
    				$select->join("tbl_messages_recipients", "$tableName.id=tbl_messages_recipients.message", array());
    			else 
    				$select->join("tbl_messages_recipients", "$tableName.id=tbl_messages_recipients.message", array("name", "address", "send_as", "message"));
    			$select->where('tbl_messages_recipients.address=?', $paramsSearch['recipient']);
    		}
	    	if(isset($paramsSearch['status']))
	    	{
	    		if(!is_array($paramsSearch['status']))
	    			$select->where('status=?', $paramsSearch['status']);
	    		else 
	    			$select->where('status IN (?)', $paramsSearch['status']);
	    	}
	    	if(isset($paramsSearch['id']))
	    	{
	    		if(!is_array($paramsSearch['id']))
	    			$select->where('id=?', $paramsSearch['id']);
	    		else
	    			$select->where('id IN (?)', $paramsSearch['id']);
	    	}
    	}
    
    	return $this;
    }
   
}
?>
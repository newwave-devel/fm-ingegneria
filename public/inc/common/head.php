<!doctype html>
<!--[if lt IE 7 ]> <body class="ie6"> <![endif]-->
<!--[if IE 7 ]><body class="ie7"><![endif]-->
<!--[if IE 8 ]><body class="ie8"><![endif]-->
<!--[if IE 9 ]><body class="ie9"><![endif<]-->
<!--[if (gt IE 9)|!(IE)]><!--><!--<![endif]-->

<html>

<head>
    <meta charset="utf-8"/>

    <title>F&M Ingegneria Spa</title>
    <meta name="description" content="#" />
    <meta name="keywords" content="#" />

    <meta name="author" content="Newwave snc - newwave-media.it">
    <meta name="Copyright" content="Copyright 2013 Newwave snc All Rights Reserved.">

    <meta name="viewport" content="width=1024">

    <link rel="shortcut icon" href="http://www.fm-ingegneria.com/common/img/favicon.ico">
    <link rel="apple-touch-icon" href="/common/img/apple-touch-icon.png">

    <link rel="stylesheet" media="all" href="/common/css/main.css">

    <!--[if lt IE 9]>
    <script src="/common/js/html5.js"></script>
    <![endif]-->
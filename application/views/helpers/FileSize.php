<?php 
class Zend_View_Helper_FileSize extends Zend_View_Helper_Abstract
{
	public $view;
  
    public function setView(Zend_View_Interface $view) {
        $this->view = $view;
    }
    
    protected static $_prefixes = array('', 'k', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y');
    
    /**
     * Tranformation to huma-readable format
     * @param  int $size Size in bytes
     * @param  int $precision Presicion of result (default 2)
     * @return string Transformed size
     */
    public function fileSize($size, $precision = 2)
    {        
        $result = $size;
        $index = 0; 
        while ($result > 1024 && $index < count(self::$_prefixes)) {
            $result = $result / 1024;
            $index++;
        }
        return sprintf('%1.' . $precision . 'f %sB', $result, self::$_prefixes[$index]);
    }
}
?>
<?php

class NW_Models_SubForm extends NW_Form_SubForm {

    protected $_classUri;
    
    public function __construct($classUri, $options = null) {
        parent::__construct($options);
        $this->_classUri = $classUri;
    }

    public function getClassUri() {
        return $this->_classUri;
    }

}

?>

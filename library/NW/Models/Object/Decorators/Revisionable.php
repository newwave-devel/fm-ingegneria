<?php

class NW_Models_Object_Decorators_Revisionable extends NW_Models_Object_Decorator {

    public $_revId;

    const PARENT_REVID = 0;

    public function __construct($object, $revId = self::PARENT_REVID) {
        $this->_object = $object;
        $this->_revId = $revId;
        $this->_mapper = new NW_Models_Object_Decorators_Mapper_Revisionable($this);
    }

    public function getRevId() {
        return $this->_revId;
    }

    public function setRevId($res) {
        $this->_revId = $res;
        return $this;
    }

    public function __set($name, $value) {
        if ($name == 'revId')
            return $this->setRevId($value);
        $this->getObject()->$name = $value;
        return $this;
    }

    public function __get($name) {
        if ($name == 'revId')
            return $this->getRevId();
        return $this->getObject()->$name;
    }

    public function isRevisionable() {
        return true;
    }

    public function doInsert($object) {
        try {
            $object = $this->getObject()->doInsert($object);
            $query = $this->getMapper()->queryInsert($object);
            $this->getAdapter()->performInsertQuery($query);
            print "<li>".$query->getQuery()."</li>";
            return $object;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function doUpdate(&$object) {
        try {
            
            $query = $this->getMapper()->queryUpdate($object);
            $this->getAdapter()->performUpdateQuery($query);
            print "<li>".$query->getQuery()."</li>";
            return $this->getObject()->doUpdate($object);
        } catch (Exception $e) {
            
            throw $e;
        }
    }

    public function doLoadOne($params) {
        try {
            $query = $this->getObject()->doLoadOne($params);
            return $this->getMapper()->queryLoad($query);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function doLoadAll($params, NW_Pages_Paginator &$paginator, $query = '') {
        try {
            $query = $this->getObject()->doLoadAll($params, $paginator);
            return $this->getMapper()->queryAll($query);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function doView($viewName, $params, &$paginator, $query = '') {
        try {
            $query = $this->getObject()->doView($viewName, $params, $paginator);
            return $this->getMapper()->queryView($viewName, $query);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function doLoadByRelDefInTable(NW_Db_Query_Select &$query, $relationName, $view, $callerTable)
    {
        try {
            $this->getObject()->doLoadByRelDefInTable($query, $relationName, $view, $callerTable);
            $this->getMapper()->queryLoadByRelDefInTable($query, $relationName);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function toStdClass()
    {
        $stdClass = $this->getObject()->toStdClass();
        $stdClass->revId = $this->getRevId();
        return $stdClass;
    }
    
    public function formFields()
    {
         $fields = $this->getObject()->formFields();
        $element = new NW_Form_Element_Hidden('revId');
        $fields[] = $element;
        return $fields;
    }
    
    public function cloneNew(&$object)
    {
        $object->revId = $this->getRevId();
        return $this->getObject()->cloneNew($object);
    }
}

?>

<?php

class App_Form_MyProfile extends Zend_Form {

    public function init() {
    	$this->setMethod('post')
    			->setAttrib('id', 'myprofile_form')
    			->setAttrib("class", "form")
    				->setAction("/app/users/updatemyprofile");
    	//$this->getView()->jQuery()->addOnLoad('$("#myprofile_form").ajaxForm({success: user.prototype.updatemyprofile });');
    	$name = new NW_Form_Element_Text("name");
        $name -> setLabel("Nome")
        	->setRequired(true)
        	->addValidator('Regex',false, array('/([a-zA-Z0-9 \-_]+)/'))
                ->setFilters(array('StringTrim'));
        $username = new NW_Form_Element_Text("username");
        $username -> setLabel("Username")
        			->setRequired(true)
        			->addValidator('Regex',false, array('/([a-zA-Z0-9 \-_]+)/'))
        			->setFilters(array('StringTrim'));
        $oldpassword = new NW_Form_Element_Password("oldpassword");
        $oldpassword ->  setLabel("Old Password")
        			->setRequired(true)
        			->addValidator('Regex',false, array('/([a-zA-Z0-9 \-_]+)/'))
        			->addValidator('StringLength',false, array(6, 12))
        			->setFilters(array('StringTrim'));
        $password = new NW_Form_Element_Password("password");
        $password ->  setLabel("New Password")
			        ->setRequired(false)
			        ->addValidator('Regex',false, array('/([a-zA-Z0-9 \-_]+)/'))
			        ->addValidator('StringLength',false, array(6, 12))
			        ->setFilters(array('StringTrim'));
        $retypePassword = new NW_Form_Element_Password("checkpassword");
        $retypePassword -> setLabel("Retype Password")
        			->setRequired(false)
        			->addValidator('Regex',false, array('/([a-zA-Z0-9 \-_]+)/'))
        			->addValidator('StringLength',false, array(6, 12))
        			->setFilters(array('StringTrim'));
        
        $email = new NW_Form_Element_Text('email');
        $email -> setLabel('Email')
        		->setRequired(true);
        $this->addElements(array($name,$username, $oldpassword, $password, $retypePassword, $email));  
    }

    public function populate(array $values) 
    {    
        $AdaptedValues['name'] = $values["_name"];
        $AdaptedValues['username'] = $values["_username"];
        $AdaptedValues['password'] = '';
        $AdaptedValues['email'] = $values["_email"];
        parent::populate($AdaptedValues);
    }
    
    public function isValid($data)
    {
    	if($data['password']!= '')
    		return (parent::isValid($data) && ($data['password'] == $data['checkpassword']));
    	return parent::isValid($data);
    }
}


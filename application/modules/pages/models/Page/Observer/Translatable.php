<?php

class Pages_Model_Page_Observer_Translatable
{

    public function update(&$subject, $event)
    {
        switch ($event) {
            case Pages_Model_Page_Event::EVENT_BEFORE_SAVE:
            	$this->_registerTrx($subject);
                break;
            case Pages_Model_Page_Event::EVENT_AFTER_SAVE:
                $this->_updatePageTrx($subject);
                break;
            case Pages_Model_Page_Event::EVENT_BEFORE_UPDATE:
                break;
            case Pages_Model_Page_Event::EVENT_AFTER_UPDATE:
                break;
            case Pages_Model_Page_Event::EVENT_BEFORE_TRANSLATE:
            	break;
            case Pages_Model_Page_Event::EVENT_AFTER_TRANSLATE:
            	$this->_updatePageTrx($subject);
            	break;
        }
    }
    
    public function _updatePageTrx(Pages_Model_Page &$subject)
    {
    	try {
    		$adapter = Zend_Db_Table::getDefaultAdapter();
    		$lang = $subject->getLang();
    		$id = $subject->getId();
    		$data = array($lang => $id);
    		$where =$adapter->quoteInto('id = ?', $subject->getTrx());
    		$trx = $adapter->update("tbl_page_translations", $data, $where);
    	} catch(Exception $e)
    	{
    		throw $e;
    	}
    }
    
    
    private function _registerTrx(Pages_Model_Page &$subject)
    {
    	try{
    		$adapter = Zend_Db_Table::getDefaultAdapter();
    		$data = array();
    		$res = $adapter->insert("tbl_page_translations", $data);
    		$trx = $adapter->lastInsertId();
    		$subject->setTrx($trx);
    	}catch(Exception $e)
    	{
    		throw $e;
    	}
    }
}
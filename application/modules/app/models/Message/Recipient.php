<?php
class App_Model_Message_Recipient {
	
	protected $_id;
	
	protected $_name;
	
	protected $_address;
	
	protected $_sendAs;
	
	protected $_message;
	
	const SEND_AS_TO = 'TO';
	
	const SEND_AS_CC = 'CC';
	
	const SEND_AS_BCC = 'BCC';
	
	public function __construct($address= '', $name = '', $sendAs=self::SEND_AS_TO) {
		$this->_address = $address;
		$this->_name = $name;
		$this->_sendAs = $sendAs;
	}
	
	public function getOptions() {
		return get_object_vars ( $this );
	}
	
	public function getId() {
		return $this->_id;
	}
	
	public function setId($_id) {
		$this->_id = $_id;
		return $this;
	}
	
	public function getName() {
		return $this->_name;
	}
	
	public function setName($_name) {
		$this->_name = $_name;
		return $this;
	}
	
	public function getAddress() {
		return $this->_address;
	}
	
	public function setAddress($_address) {
		$this->_address = $_address;
		return $this;
	}
	
	public function getSendas() {
		return $this->_sendAs;
	}
	
	public function setSendas($_sendAs) {
		$this->_sendAs = $_sendAs;
		return $this;
	}
	public function setMessage($messId)
	{
		$this->_message = $messId;
		return $this;
	}
	
	public function getMessage()
	{
		return $this->_message;
	}
	
	public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid certificates property: ' . $name);
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid certificates property: '. $name);
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }
}


<?php
if($blocks):
	require_once("inc/tpls/blocks/list/tpl.php");
	require_once("inc/functions/blocks.php");
	$homepages_blocks_bottom_col_sx = '';
	$homepages_blocks_bottom_col_dx = '';
	$odd = true;
	$homepages_blocks_bottom_found = false;
	foreach($blocks as $sharedBlock)
	{
		$homepages_blocks_bottom_found = true;
		$page = $sharedBlock->getPage();
		if($odd)
		{
			$homepages_blocks_bottom_col_sx .= ($page->getTemplate() == 'news') ? renderSharedBlockNews($tpl_item_news, $sharedBlock, $blockService, $odd, true) : 
																		renderSharedBlockProject($tpl_item_project, $sharedBlock, $blockService, $odd, true);
			$odd = false;
		} else {
			$homepages_blocks_bottom_col_dx .= ($page->getTemplate() == 'news') ? renderSharedBlockNews($tpl_item_news, $sharedBlock, $blockService, $odd, true) :
																	renderSharedBlockProject($tpl_item_project, $sharedBlock, $blockService, $odd, true);
			$odd = true;
		}
		
	}

	?>
	<section class="countries_articles_container">
		<?php echo $homepages_blocks_bottom_col_sx.PHP_EOL ?>
		<?php echo $homepages_blocks_bottom_col_dx.PHP_EOL ?>
	</section>
	<?php unset($homepages_blocks_bottom_col_sx); ?>
	<?php unset($homepages_blocks_bottom_col_dx); ?>
	<?php unset($blocks); ?>
<?php else: ?>
	<?php echo sprintf("<p>%s</p>", "No content found"); ?>
<?php endif;?>
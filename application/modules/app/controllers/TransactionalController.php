<?php

class App_TransactionalController extends NW_Controller_Action {
    private $_service;
    
    public function init() {
        $this->_service = new App_Model_TransactionalEmail_Service();
        $this->_translateAdapter = Zend_Registry::get('Zend_Translate');
    }

    public function incomingAction() {
        try {
        	if (!$this->_hasParam('item')) throw new Exception("no item received");
        	if (!$this->_hasParam('model')) throw new Exception("no model received");
        	
        	$item = $this->_getParam('item');
        	$model = $this->_getParam('model');
            $this->_setAjaxAction()
		           ->_addOnload("fancybox()")
		           ->_addJsSource('/js/admin/NW/tablesorter.js')
		           ->_addOnload('NW_Tablesorter.register("#table-sent").sort()')
		           ->_addJavascript(sprintf("var transIncoming=new transactional('%s', '%d')", $model, $item));
            $paginator = NW_Pages_Paginator::__fromRequest($this->getRequest());
           
    		$paginator->setIsAjax(true, "#mailbox-list-incoming", "/app/mailbox/incoming");

    		$this->view->entries = $this->_service->incoming($paginator, array("recipient" => $this->_helper->LoggedUser()->getEmail(), "model" => $model, "item" => $item));
            $this->view->paginator = $paginator->prepare();
            $this->view->paginationController = 'frmList-incoming';
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function editAction()
    {
    	try {
    		$this->_setIframeAction();
    		if(!$this->_hasParam('id')) throw new Exception("no id recevied");
    		$id = $this->_getParam('id');
    		$service = new App_Model_Message_Service();
    		$message = $service->load($id);
    		$form = new App_Form_Message();
    		$form->setAction("/app/transactional/save");
    		$form->populate($message->getOptions());
    
    		$this->view->form = $form;
    	} catch(Exception $e)
    	{
    		throw $e;
    	}
    }
    
    public function saveAction()
    {
    	try {
    		$this->_setIframeAction();
    		$request = $this->getRequest();
    		$form = new App_Form_Message();
    
    		$status = new NW_View_RetCode();
    		if ($request->isPost())
    		{
    			if ($form->isValid($request->getPost()))
    			{
    				if ($this->_hasParam('id') && ($id=$request->getParam('id'))!='') {
    					$model = $this->_service->load($id);
    				}
    				$model->setOptions($form->getValues());
    				$itemId = $this->_service->save($model);
    
    				$this->_addOnload('parent.transSent.updateMail()');
    			}
    			else {
    				$this->view->form = $form;
    			}
    			 
    		}
    	} catch(Exception $e)
    	{
    		$this->view->status = NW_View_RetCode::error($e->getMessage());
    	}
    }
    
    public function sentAction() {
    	try {
    		if (!$this->_hasParam('item')) throw new Exception("no item received");
        	if (!$this->_hasParam('model')) throw new Exception("no model received");
        	
        	$item = $this->_getParam('item');
        	$model = $this->_getParam('model');
    		$this->_setAjaxAction()
		    		->_addOnload("fancybox()")
		    		->_addJsSource('/js/admin/NW/tablesorter.js')
		    		->_addOnload('NW_Tablesorter.register("#table-sent").sort()')
		    		->_addJavascript(sprintf("var transSent=new transactional('%s', '%d')", $model, $item));
    		
    		$paginator = NW_Pages_Paginator::__fromRequest($this->getRequest());
    		$paginator->setIsAjax(true, "#mailbox-list-sent","/app/mailbox/sent");  		
    		
    		$this->view->entries = $this->_service->sent($paginator, array("sender" => $this->_helper->LoggedUser()->getId(),"model" => $model, "item" => $item));
    		$this->view->paginator = $paginator->prepare();
    		$this->view->paginationController = 'frmList-sent';
    	} catch (Exception $e) {
    		throw $e;
    	}
    }
    
    public function deleteAction()
    {
    	try {
    		$this->_setAjaxAction();
    		if(!$this->_hasParam('paramId')) throw new Exception("no id received");
    		$id = $this->_getParam('id');
    		$this->_service->delete($id);
    	} catch(Exception $e)
    	{
    		throw $e;
    	}
    }

}


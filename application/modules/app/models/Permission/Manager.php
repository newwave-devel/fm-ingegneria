<?php
class App_Model_Permission_Manager
{
	const SESSION_NAME = 'permissions';
	
	public static function getPermissions()
	{
		try {
				$session = new Zend_Session_Namespace(self::SESSION_NAME);
				if(!isset($session->permissions))
				{
					$service = new App_Model_Permission_Service();
					$session->permissions = $service->load();
				} 
				return $session->permissions;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public static function load()
	{
		try {
				$session = new Zend_Session_Namespace(self::SESSION_NAME);
				if(!isset($session->permissions))
				{
					$service = new App_Model_Permission_Service();
					$session->permissions = $service->load();
				}
				return true;
			} catch(Exception $e)
			{
				throw $e;
			}
	}
	
	public static function destroy()
	{
		Zend_Session::namespaceUnset(self::SESSION_NAME);
	}
	
}
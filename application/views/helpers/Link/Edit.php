<?php

/**
 * Description of Link
 *
 * @author Ale
 */
class Zend_View_Helper_Link_Edit extends Zend_View_Helper_Abstract
{
    public function link_edit($id)
    {
        $route = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();
        return $this->view->url(array("action" => 'edit', 'id' => $id), $route); 
    }
    
    
 
}

?>

<?php 
require_once("inc/tpls/projects/tpl.php");

$project_list_accordion_html = '';
$countries_list_accordion_html = '';
foreach($glob_disciplinesCat as $disCatBlock):
	$disCatName = $disCatBlock->_("title");
	$disCatId = $disCatBlock->getPage();
	
	$groupsTaxonomy = new Pages_Model_Taxonomy('disgroupcat', 'disciplines-cat', $disCatBlock->getId());
	$groupsCollection = $pageService->collection()->displayParents($groupsTaxonomy, 'disciplines-group');
	
	$options = '';
    $active = false;
    $all_is_selected = empty($filter_disciplines_array) ? "current" : '';
    $options .= sprintf('<li><a class="filter disciplines %s" href="#" rel="">%s</a></li>', $all_is_selected, $translator->{"Pagina 'Progetti'"}->_t("All"));

    foreach($groupsCollection as $groupId)
	{
		$disciplineTaxonomy = new Pages_Model_Taxonomy('disgroupdisc', 'disciplines-group', $groupId);
		$disciplineCollection = $pageService->collection()->displayParents($disciplineTaxonomy, 'disciplines');
		$disciplines = $blockService->display('disciplines-main', array('page' => $disciplineCollection));

        foreach($disciplines as $dis)
        {
            if(in_array($dis->getPage(), $filter_disciplines_array))
            {
			    $options .= sprintf('<li><a class="filter disciplines current" href="#" rel="%d">%s</a></li>', $dis->getPage(), $dis->_("title"));
                $active = true;
            }
            else
                $options .= sprintf('<li><a class="filter disciplines" href="#" rel="%d">%s</a></li>', $dis->getPage(), $dis->_("title"));
        }
	}
	$class_active = ($active) ? 'active' : '';
    $class_opened = ($active)  ? 'open' : '';
	$project_list_accordion_html .= sprintf($tpl_discipline_accordion, $class_active, $disCatName, $class_opened, $options);
endforeach;

$options = '';

$active = false;
$all_is_selected = empty($filter_countries_array) ? "current" : '';
$options .= sprintf('<li><a class="filter countries %s" href="#" rel="">%s</a></li>', $all_is_selected, $translator->{"Pagina 'Progetti'"}->_t("All"));

foreach($regions as $region):

    if(in_array($region->getPage(), $filter_countries_array))
    {
        $active = true;
        $options .=  sprintf('<li><a class="filter countries current" href="#" rel="%d">%s</a></li>', $region->getPage(), $region->_("title"));
    }
    else
        $options .=  sprintf('<li><a class="filter countries" href="#" rel="%d">%s</a></li>', $region->getPage(), $region->_("title"));
endforeach;
$class_active = ($active) ? 'active' : '';
$class_opened = ($active)  ? 'open' : '';
$countries_list_accordion_html = sprintf($tpl_countries_accordion, $class_active, $translator->{"Pagina 'Progetti'"}->_t("COUNTRIES"), $class_opened, $options)
?>
<div id="accordion">
	
	<?php echo $project_list_accordion_html ?>
    <?php echo $countries_list_accordion_html ?>

</div>
<!--/#accordion-->
<?php

abstract class NW_Models_Object_Decorators_Mapper
{
    protected $decoratorRef;
    protected $table;
    
    protected function getDecorator()
    {
        return $this->decoratorRef;
    }
    
    protected function getTable()
    {
        return $this->table;
    }
    
    //abstract public function queryInsert(&$object);
    
    //abstract public function queryUpdate(&$object);
    
    abstract public function queryLoad();
    
    abstract public function queryAll();
    
    abstract public function queryView($viewName);
    
    protected function fieldAlias($field, $relationName)
    {
        return sprintf("`%s.%s`", $relationName, $field);
    }
    
    protected function tableAlias($relationName, $table='')
    {
        $table = ($table=='') ? $this->table : $table;
        return sprintf("_%s_%s", $relationName, $table);
    }
    
}

?>

<?php
class Pages_Model_Page_Mapper {
	protected $_dbTable;
	private $_taxonomies = array ();
	public function setDbTable($dbTable) {
		if (is_string ( $dbTable )) {
			$dbTable = new $dbTable ();
		}
		if (! $dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception ( 'Invalid table data gateway provided' );
		}
		$this->_dbTable = $dbTable;
		return $this;
	}
	public function getDbTable() {
		if (null === $this->_dbTable) {
			$this->setDbTable ( 'Pages_Model_DbTable_Page' );
		}
		return $this->_dbTable;
	}
	public function addTaxonomy(Pages_Model_Taxonomy $taxonomy) {
		try {
			$this->_taxonomies [] = $taxonomy;
			return $this;
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	public function dropTaxonomies() {
		try {
			$this->_taxonomies = array ();
			return $this;
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	public function find($id, Pages_Model_Page $model) {
		try {
			$result = $this->getDbTable ()->find ( $id );
			if (0 == count ( $result )) {
				return;
			}
			$row = $result->current ();
			$this->_fromDb ( $model, $row );
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	public function load($id, $lang) {
	}
	
	/**
	 *
	 * @param NW_Pages_Paginator $paginator        	
	 * @param type $paramsSearch        	
	 * @return \Purchase_Model_OrderRequest
	 * @throws Exception
	 */
	public function fetchAll(NW_Pages_Paginator &$paginator, $paramsSearch, $paramsOrder = null, $load) {
		try {
			$table = $this->getDbTable ();
			$select = $table->select ()->from ( $table );
			
			$this->_filterBySearchParam ( $select, $paramsSearch )->_filterByUser ( $select )->_addTaxonomies ( $select );
			if (! $paginator->getFetchAll ())
				$select->limit ( $paginator->itemsPerPage, $paginator->getStart () );
			
			if (! empty ( $paramsOrder )) {
				foreach ( $paramsOrder as $key => $dir ) {
					$sql = sprintf ( "%s %s", $key, strtoupper ( $dir ) );
					$select->order ( $sql );
				}
			}
			$entries = array ();
			
			foreach ( $table->fetchAll ( $select ) as $row ) {
				$model = new Pages_Model_Page ();
				$this->_fromDb ( $model, $row, $load );
				$entries [] = $model;
			}
			$paginator->itemsCount = $this->count ( $select );
			
			return $entries;
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	
	public function fetchAvailable(NW_Pages_Paginator &$paginator, $paramsSearch, $paramsOrder = null, $load = false) {
		try {
			$table = $this->getDbTable ();
			$select = $table->select ()->from ( $table );
	
			$taxonomy = $this->_taxonomies [0];
	
			$paramsSearch ['type'] = $taxonomy->getRelated ();
	
			$this->_filterBySearchParam ( $select, $paramsSearch )->_filterByUser ( $select )->_addTaxonomies ( $select, true );
	
			if (! $paginator->getFetchAll ())
				$select->limit ( $paginator->itemsPerPage, $paginator->getStart () );
	
			if (! empty ( $paramsOrder )) {
				foreach ( $paramsOrder as $key => $dir ) {
					$sql = sprintf ( "%s %s", $key, strtoupper ( $dir ) );
					$select->order ( $sql );
				}
			}
			$entries = array ();
			
			foreach ( $table->fetchAll ( $select ) as $row ) {
				$model = new Pages_Model_Page ();
				$this->_fromDb ( $model, $row, $load );
				$entries [] = $model;
			}
			$paginator->itemsCount =$this->count ( $select );
	
			return $entries;
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	
	public function fetchDependent(NW_Pages_Paginator &$paginator, $paramsSearch, $paramsOrder = null, $load = false) {
		try {
			$table = $this->getDbTable ();
			$select = $table->select ()->from ( $table );
				
			$taxonomy = $this->_taxonomies [0];
				
			$paramsSearch ['type'] = $taxonomy->getRelated ();
				
			$this->_filterBySearchParam ( $select, $paramsSearch )->_filterByUser ( $select )->_addTaxonomies ( $select );
				
			if (! $paginator->getFetchAll ())
				$select->limit ( $paginator->itemsPerPage, $paginator->getStart () );
				
			if (! empty ( $paramsOrder )) {
				foreach ( $paramsOrder as $key => $dir ) {
					$sql = sprintf ( "%s %s", $key, strtoupper ( $dir ) );
					$select->order ( $sql );
				}
			}
			$entries = array ();
				
			foreach ( $table->fetchAll ( $select ) as $row ) {
				$model = new Pages_Model_Page ();
				$this->_fromDb ( $model, $row, $load );
				$entries [] = $model;
			}
			$paginator->itemsCount = $this->count ( $select );
				
			return $entries;
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	
	public function fetchParents(NW_Pages_Paginator &$paginator, $paramsSearch, $paramsOrder = null, $load = false) {
		try {
			Zend_Registry::get("Log")->debug("------------------- SONO LA FETCHPARENTS");
			$table = $this->getDbTable ();			
			
			$taxonomy = $this->_taxonomies [0];
			
			$select = $table->select ();
			
			$this->_filterBySearchParam ( $select, $paramsSearch )->_filterByUser ( $select );
			
			$this->_addTaxonomies ( $select, false, true );
			Zend_Registry::get("Log")->debug("QUESTA E' LA SECONDA PARTE (dopo la fetchTAx): ".$select->__toString());
			if (! $paginator->getFetchAll ())
				$select->limit ( $paginator->itemsPerPage, $paginator->getStart () );
			
			if (! empty ( $paramsOrder )) {
				foreach ( $paramsOrder as $key => $dir ) {
					$sql = sprintf ( "%s %s", $key, strtoupper ( $dir ) );
					$select->order ( $sql );
				}
			}
			$entries = array ();
			Zend_Registry::get("Log")->debug("QUESTA E' TOTALE: ".$select->__toString());
			foreach ( $table->fetchAll ( $select ) as $row ) {
				$model = new Pages_Model_Page ();
				$this->_fromDb ( $model, $row, $load );
				$entries [] = $model;
			}
			$paginator->itemsCount = $this->count ( $select );
			
			return $entries;
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	public function save(Pages_Model_Page $model) {
		try {
			if (null === ($id = $model->getId ()) || empty ( $id )) {
			    $model->notify(Pages_Model_Page_Event::EVENT_BEFORE_SAVE);
			    $data = $this->_toDb ( $model );
				unset ( $data ['id'] );
				$this->getMaxOrder ( $data );
				$id = $this->getDbTable ()->insert ( $data );
				$model->setId($id);
				$model->notify(Pages_Model_Page_Event::EVENT_AFTER_SAVE);
				return $id;
			} else {
			    $model->notify(Pages_Model_Page_Event::EVENT_BEFORE_UPDATE);
			    $data = $this->_toDb ( $model );
				if (empty ( $data ['order'] ))
					$this->getMaxOrder ( $data );
				$this->getDbTable ()->update ( $data, array (
						'id = ?' => $id 
				) );
				$model->notify(Pages_Model_Page_Event::EVENT_AFTER_UPDATE);
				return $id;
			}
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	private function getMaxOrder(&$data, $increment = true) {
		try {
			$table = $this->getDbTable ();
			$select = $table->select ();
			$select->from ( $table, array (
					"IFNULL(MAX(`order`),1) AS ord" 
			) )->where ( 'template=?', $data ['template'] );
			$result = $table->fetchRow ( $select );
			if ($increment)
				$data ['order'] = $result->ord + 1;
			else
				$data ['order'] = $result->ord + 1;
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	public function delete($pageId) 
	{
	    try {
	        $table = $this->getDbTable();
	        $blockService = new Pages_Model_Block_Service();
	        $taxonomyService = new Pages_Model_Taxonomy_Service();
	        
	        $table->getAdapter()->beginTransaction();
	        //empties blocks
	        foreach($blockService->byPage($pageId) as $block)
	            $blockService->delete($block->getId());
	        //empties taxonomies
	        $taxonomyService->dropPageFromTaxonomy($pageId);
	        
	        //drop tables
	        $where = $table->getAdapter()->quoteInto('id = ?', $pageId);
	        $table->delete($where);
	        
	        $this->getDbTable()->getAdapter()->commit();
	        return true;
	   } catch ( Exception $e ) {
	       $this->getDbTable()->getAdapter()->rollback();
	       throw $e;
	   }
	    
	}
	public function count(Zend_Db_Select $select) {
		try {
			$table = $this->getDbTable ();
			$select->reset ( Zend_Db_Select::COLUMNS )->reset ( Zend_Db_Select::LIMIT_COUNT )->reset ( Zend_Db_Select::LIMIT_OFFSET )->columns ( array (
					'num' => new Zend_Db_Expr ( 'COUNT(*)' ) 
			) );
			return $table->fetchRow ( $select )->num;
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	private function _filterByUser(Zend_Db_Table_Select &$select) {
		// $loggedUser = Zend_Auth::getInstance()->getStorage()->read();
		// $loggedUserId = $loggedUser->getId();
		return $this;
	}
	private function _filterBySearchParam(Zend_Db_Table_Select &$select, $paramsSearch = array()) {
		$tableName = $select->getPart ( Zend_Db_Select::FROM );
		$tableName = is_array ( $tableName ) ? key ( $tableName ) : $tableName;
		
		if (! empty ( $paramsSearch )) {
			
			if (! empty ( $paramsSearch ['template'] )) {
				if (is_array ( $paramsSearch ['template'] ))
					$select->where ( "$tableName.template IN ?", $paramsSearch ['template'] );
				else
					$select->where ( "$tableName.template = ?", $paramsSearch ['template'] );
			}
			if (! empty ( $paramsSearch ['type'] )) {
				if (is_array ( $paramsSearch ['type'] ))
					$select->where ( "$tableName.template IN ?", $paramsSearch ['type'] );
				else
					$select->where ( "$tableName.template = ?", $paramsSearch ['type'] );
			}
			if (! empty ( $paramsSearch ['free'] ))
				$select->where ( "$tableName.label LIKE ?", '%' . $paramsSearch ['free'] . '%' );
			if (! empty ( $paramsSearch ['status'] )) {
				if (is_array ( $paramsSearch ['status'] ))
					$select->where ( "$tableName.status IN ?", $paramsSearch ['status'] );
				else
					$select->where ( "$tableName.status = ?", $paramsSearch ['status'] );
			}
			if (! empty ( $paramsSearch ['id'] )) {
				if (is_array ( $paramsSearch ['id'] ))
					$select->where ( "$tableName.id IN ?", $paramsSearch ['id'] );
				else
					$select->where ( "$tableName.id = ?", $paramsSearch ['id'] );
			}
		}
		return $this;
	}
	private function _fromDb(Pages_Model_Page &$model, $row, $load = false) {
		$model->setId ( $row->id )->setLabel ( $row->label )->setTscreation ( null )->setTsdelete ( null )->setTsupdate ( null )->setLang ( $row->lang )->setStatus ( $row->status )->setTemplate ( $row->template )->setOrder ( $row->order );
		if (! empty ( $this->_taxonomies )) {
			foreach ( $this->_taxonomies as $taxnomy ) {
				$taxonomyName = $taxnomy->getName ();
				$relatedAliasLabel = sprintf ( "taxonomy-%s-label", $taxonomyName );
				$relatedAliasId = sprintf ( "taxonomy-%s-id", $taxonomyName );
				if (isset ( $row->$relatedAliasLabel ) && isset ( $row->$relatedAliasId ))
					$model->appendToTaxonomy ( $taxonomyName, array (
							$row->$relatedAliasId => $row->$relatedAliasLabel 
					) );
			}
		}
		if ($load) {
		}
		
		if (! is_null ( $row->ts_creation ))
			$model->setTsCreation ( NW_Utils_DateTime::strptime ( $row->ts_creation, "%Y-%m-%d" ) );
		if (! is_null ( $row->ts_update ))
			$model->setTsUpdate ( NW_Utils_DateTime::strptime ( $row->ts_update, "%Y-%m-%d" ) );
		if (! is_null ( $row->ts_delete ))
			$model->setTsDelete ( NW_Utils_DateTime::strptime ( $row->ts_delete, "%Y-%m-%d" ) );
	}
	private function _toDb(Pages_Model_Page $model) {
		$creationDate = $model->getTsCreation ();
		$updateDate = $model->getTsUpdate ();
		$deleteDate = $model->getTsDelete ();
		
		$data = array (
				'id' => $model->getId (),
				'label' => $model->getLabel (),
				'ts_creation' => (is_null ( $creationDate ) || $creationDate == '') ? date ( "Y-m-d h:i:s" ) : strftime ( "%Y-%m-%d %h:%i:%s", $creationDate ),
				'ts_update' => (is_null ( $updateDate ) || $updateDate == '') ? date ( "Y-m-d h:i:s" ) : strftime ( "%Y-%m-%d %h:%i:%s", $updateDate ),
				'ts_delete' => (is_null ( $deleteDate ) || $deleteDate == '') ? date ( "Y-m-d h:i:s" ) : strftime ( "%Y-%m-%d %h:%i:%s", $deleteDate ),
				'template' => $model->getTemplate (),
				'status' => $model->getStatus (),
				'lang' => $model->getLang (),
				'order' => $model->getOrder (),
		        'plang' => $model->getPLang()
		);
		return $data;
	}
	
	private function _addTaxonomies(Zend_Db_Table_Select &$select, $is_fetch_available=false, $log = false) {
		try {
			if (empty ( $this->_taxonomies ))
				return;
			$select->setIntegrityCheck ( false );
			$count = 1;
			if($log) Zend_Registry::get("Log")->debug("ho ".count($this->_taxonomies)." tassi");
			foreach ( $this->_taxonomies as $taxonomy ) 
			{
				$taxonomyName = $taxonomy->getName ();
				$role = $taxonomy->getInvokerrole ();
				$dependent = $taxonomy->getRelated ();
				$tbl_taxonomy_alias = "tbl_page_taxonomy_" . $count;
				$relationedRole = 'dependent';
				$invokerId = $taxonomy->getInvokerId ();
				$join_on_taxonomy = true;
				if($log) Zend_Registry::get("Log")->debug("looping on $taxonomyName, with role $role");
				
				if ($role == 'dependent') 
				{
					$relationedRole = 'parent';
					if (empty ( $invokerId ))
					{
						if($log) Zend_Registry::get("Log")->debug("have no invoker id");
						if(!$is_fetch_available)
							throw new Exception ( "Cannot fetch parents if no id is provided" );
						else {	
							if($log) Zend_Registry::get("Log")->debug("but, luckily, is just a matter of fetching availables!");
							if($log) Zend_Registry::get("Log")->debug($select);// no need to join with other table, no need to keep on looping.
							if($log) Zend_Registry::get("Log")->debug("I exit now");
							return;
						}
					} else 
					{
						//----
						$parentAlias = $taxonomy->getRelated();
						$select->from ( array($parentAlias => "tbl_pages" ));
						$select->where("`$parentAlias`.`template` = ?",  $taxonomy->getRelated ());
						
						//JOIN
						
						$select->joinLeft ( array ($tbl_taxonomy_alias => "tbl_page_taxonomy"), "`$relationedRole`=`$parentAlias`.`id`", array () );
						$select->where("`$tbl_taxonomy_alias`.`$role`=?",$taxonomy->getInvokerId ());
						$select->where("`$parentAlias`.`status` <> ?", 'deleted');
						$join_on_taxonomy = false;
						//----
					}
				} else {
					if (! empty ( $invokerId ))
						$select->where ( "$tbl_taxonomy_alias.$role = ?", $taxonomy->getInvokerId () );
					else
						$join_on_taxonomy = false;
					$relationedRole = 'dependent';
				}
				
				$relatedAliasLabel = sprintf ( "taxonomy-%s-label", $taxonomyName );
				$relatedAliasId = sprintf ( "taxonomy-%s-id", $taxonomyName );
				
				if ($join_on_taxonomy) {
					if($log) Zend_Registry::get("Log")->debug("sono qui i problemi mi sa..., perche' joina con $dependent");
					$select->joinLeft ( array (
							$tbl_taxonomy_alias => "tbl_page_taxonomy" 
					), "`$relationedRole`=`tbl_pages`.`id`", array () )->joinLeft ( array (
							$dependent => "tbl_pages" 
					), "`$tbl_taxonomy_alias`.`$relationedRole`=`$dependent`.id", 

					array (
							$relatedAliasLabel => $dependent . ".label",
							$relatedAliasId => $dependent . ".id" 
					) );
					$select->where ( "`$dependent`.`status` <> ?", 'deleted' );
					$count += 1;
				} else
					$select->where ( "`tbl_pages`.`status` <> ?", 'deleted' );
			}
		} catch ( Exception $e ) {
			throw $e;
		}
	}
}
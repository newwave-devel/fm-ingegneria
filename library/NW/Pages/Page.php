<?php
class NW_Pages_Page
{
	private $_descriptor;
	private static $_instance=null;
	
	public static function getInstance()
	{
		if (self::$_instance == null)
			self::$_instance = new NW_Pages_Page();
		return self::$_instance;
	}
	
	private function __construct()
	{
		try {
			$xml = APPLICATION_PATH."/etc/pages.xml";
			$this->_descriptor = simplexml_load_file($xml);
			if($this->_descriptor == false)
				throw new Exception("Cannot parse file '$xml'");
		} catch (Exception $e)
		{
			throw $e;
		}
	}
	
	public function page($name)
	{
		try {
			$query = sprintf('//page[@type="%s"]', $name);
			$result = $this->_descriptor->xpath($query);
			if($result==false) throw new Exception("Cannot execute xpath query $query");
			return current($result);
		} catch (Exception $e)
		{
			throw $e;
		}
	}
	
	public function blocks($pageTemplate, $forSelect = true)
	{
		try {
			$query = sprintf('//pages/page[@type="%s"]/blocks/block', $pageTemplate);
			$result = $this->_descriptor->xpath($query);
			if($result==false) throw new Exception("page.xml: invalid xpath query: $query");
			if($forSelect) 
			{
				$blocks = array();
				foreach($result as $res)
				{
					if($res['main'] == "Y") continue;
					$id = (string) $res['id'];
					$label = (string) $res;
					$blocks[$id] = $label;
				}
				return $blocks;
			}
			return $result;
		} catch (Exception $e)
		{
			throw $e;
		}
	}

	public function pageHasTaxonomiesToDisplay($pageTemplate)
	{
	    try {
	        $query = sprintf('//pages/page[@type="%s"]/taxonomies/*[@display="Y"]', $pageTemplate);
	        $result = $this->_descriptor->xpath($query);
	        if($result==false || empty($result)) return false;
	        return true;
	    } catch (Exception $e)
	    {
	        throw $e;
	    }
	}
	
	public function pageHasSharedBlocks($pageTemplate)
	{
		try {
			$query = sprintf('//pages/page[@type="%s"]/shares/*', $pageTemplate);
			$result = $this->_descriptor->xpath($query);
			if($result==false || empty($result)) return false;
			return true;
		} catch (Exception $e)
		{
			throw $e;
		}
	}
	
	public function sharedBlocks($pageTemplate)
	{
		try {
			$query = sprintf('//pages/page[@type="%s"]/shares/*', $pageTemplate);
			$result = $this->_descriptor->xpath($query);
			return $result;
		} catch (Exception $e)
		{
			throw $e;
		}
	}
	
	public function displayableTaxonomies($pageTemplate)
	{
	    try {
	        $query = sprintf('//pages/page[@type="%s"]/taxonomies/*[@display="Y"]', $pageTemplate);
	        $result = $this->_descriptor->xpath($query);
	        if($result==false || empty($result)) return false;
	        $taxonomies = array();
	       
			foreach($result as $res)
			    $taxonomies[] =  Pages_Model_Taxonomy::fromXml($res, $pageTemplate);
			   
			return $taxonomies;
	    } catch (Exception $e)
	    {
	        throw $e;
	    }
	}
	
	public function taxonomy($pageTemplate, $taxonomyName)
	{
	    try {
	        $query = sprintf('//pages/page[@type="%s"]/taxonomies/%s', $pageTemplate, $taxonomyName);
	        $result = $this->_descriptor->xpath($query);
	        if($result==false || empty($result)) throw new Exception("No taxonomy found with query $query");
	        $result = current($result);
	        return Pages_Model_Taxonomy::fromXml($result, $pageTemplate);

	    } catch (Exception $e)
	    {
	        throw $e;
	    }
	}
	
	public function blockLabel($blockId)
	{
	    try {
	        $query = sprintf('//block[@id="%s"]', $blockId);
	        $result = $this->_descriptor->xpath($query);
	        if($result==false || empty($result)) throw new Exception("No label found with query $query");
	        $result = current($result);
	        return (string)$result;
	
	    } catch (Exception $e)
	    {
	        throw $e;
	    }
	}
	
	public function attachObservers(Pages_Model_Page &$page)
	{
		$descriptor = $this->page($page->getTemplate());
		
		if($descriptor->observers->count() != 0)
		{
			$childrens = $descriptor->observers->children();
			if(!empty($childrens)) {
				foreach($childrens as $obs)
				{
					$className = $obs->getName();
					
					if(class_exists($className))
						$page->attach(new $className);
				}
			}
		}
	}
}
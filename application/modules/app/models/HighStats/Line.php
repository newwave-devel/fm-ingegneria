<?php
/**
 * Description of Pie
 *
 * @author Ale
 */
class App_Model_HighStats_Line extends App_Model_HighStats_Chart {

    private $type = 'line';

    public function __construct($options = array()) {
        parent::__construct($options);
    }

    public function getJs() {
        $this->options['series'] = (isset($this->options['series'])) ? $this->options['series'] : array();
        $this->options['series']['type'] = $this->type;
        $this->options['series'] = array();
        $this->options['xAxis'] = array();
        $this->_prepareDatas();
        return json_encode($this->options);
    }

    private function _prepareDatas() {
        $this->options['xAxis']['categories'] = $this->getDataX();
        
        $pos = 0;
        foreach ($this->getSeries() as $series) {
            $this->options['series'][$pos] = array('name' => $series->getOption('name'), 'data' => $series->getData());
            $pos +=1;
        }
    }

}

?>

<?php
class App_Model_Notification_Service {
	
	protected $_mapper;
	
	public function getMapper() {
		return $this->_mapper;
	}
	
	public function __construct() {
		$this->_mapper = new App_Model_Notification_Mapper();
	}
	
	public function fetchAll(NW_Pages_Paginator &$paginator, $paramsSearch = array(), $paramsOrder = array())
	{
		try {
			return $this->getMapper()->fetchAll($paginator, $paramsSearch, $paramsOrder);
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	public function fetchNew(NW_Pages_Paginator &$paginator, $userId)
	{
		try {
			$paramsSearch = array("to" => $userId, "read" => "N");
			$paramsOrder = array("ts" => 'desc', "priority" => "desc");
			return $this->getMapper()->fetchAll($paginator, $paramsSearch, $paramsOrder);
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	public function find($id)
	{
		try {
			$model = new App_Model_Notification();
			$this->_mapper->find($id, $model);
			return $model;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function setRead($id)
	{
		try {
			$notification = $this->find($id);
			$notification->setRead(true);
			$this->save($notification);
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function delete($id)
	{
		try {
			$this->_mapper->delete($id);
			return true;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function save(App_Model_Notification $notification)
	{
		try {
			return $this->_mapper->save($notification);
		} catch(Exception $e)
		{
			throw $e;
		}
	}
}
?>
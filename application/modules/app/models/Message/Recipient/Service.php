<?php
class App_Model_Message_Recipient_Service
{
	
	private $_mapper;
	
	public function __construct()
	{
		$this->_mapper = new App_Model_Message_Recipient_Mapper();
	}

	
	public function save(App_Model_Message_Recipient $recipient)
	{
		try {
			return $this->_mapper->save($recipient);
		} catch(Exception $e)
		{
			
			throw $e;
		}
	}
	public function dropByMessage($messageId)
	{
		try {
		    $this->_mapper->delete(array("message = ?" => $messageId));
		} catch (Exception $e) {
			throw $e;
		}
	}
}
?>
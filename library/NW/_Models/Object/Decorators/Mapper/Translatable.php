<?php

class NW_Models_Object_Decorators_Mapper_Translatable extends NW_Models_Object_Decorators_Mapper {

    private $fieldsname;

    public function __construct(NW_Models_Object_Decorators_Translatable &$decoratorRef) {
        $this->decoratorRef = $decoratorRef;
        $this->table = $this->decoratorRef->moduleDescriptor()->table($this->decoratorRef->getClassUri(), "translated");
        $this->fieldsname = $this->decoratorRef->classDescriptor()->byAttribute("translatable", "Y");
    }

    public function getFieldsName() {
        return $this->fieldsname;
    }

    public function queryInsert(&$object) {
        $query = new NW_Db_Query_Insert($this->table);
        foreach($this->fieldsname as $fname)
            $query->addItem($fname,$object->$fname);
        $query->addItem("id", $object->id);
        $query->addItem("lang", $object->lang);
        return $query;
    }

    public function queryUpdate(&$object) {
        
        $query = new NW_Db_Query_Update($this->table);
        foreach($this->fieldsname as $fname)
            $query->add(array($fname => $object->$fname));
        $query->where(array("id=?" => $object->id, "lang='?'" => $object->lang));
        print $query->getQuery(); die();
        return $query;
    }
    
    public function queryDrop(&$object)
    {
        $query = new NW_Db_Query_Delete();
        $query->delete()->from($this->table)->where(array("id=?" => $object->id))
                ->where(array("lang='?'" => $object->lang))
                ->limit(1);
        return $query;
    }

    public function queryLoad($query = '') {
        if ($query == '')
            $query = new NW_Db_Query_Select();
        $query->select($this->fieldsname, $this->table)
                ->select("lang", $this->table)
                ->leftJoinOn($this->table, sprintf("%s.id=%s.id AND %s.lang='%s'", $this->table, $this->getDecorator()->getMainTable(), $this->table, $this->getDecorator()->getLang()));
        return $query;
    }

    public function queryAll($query = '') {
        if ($query == '')
            $query = new NW_Db_Query_Select();
        $query->select($this->fieldsname, $this->table)
                ->select("lang", $this->table)
                ->leftJoinOn($this->table, sprintf("%s.id=%s.id AND %s.lang='%s'", $this->table, $this->getDecorator()->getMainTable(), $this->table, $this->getDecorator()->getLang()));
        return $query;
    }

    public function fieldsInView($viewName) {
        $res = array();
        foreach ($this->fieldsname as $fName) {
            if ($this->decoratorRef->classDescriptor()->inView($viewName, $fName))
                $res[] = $fName;
        }
        return $res;
    }

    public function queryView($viewName, $query = '') {
        if ($query == '')
            $query = new NW_Db_Query_Select();
        $fieldsInView = $this->fieldsInView($viewName);
        if (!empty($viewName)) 
        {
            $query->select($fieldsInView, $this->table)
                    ->select("lang", $this->table)
                    ->leftJoinOn($this->table, sprintf("%s.id=%s.id AND %s.lang='%s'", $this->table, $this->getDecorator()->getMainTable(), $this->table, $this->getDecorator()->getLang()));
        }
        return $query;
    }

    public function queryLoadByRelDefInTable(NW_Db_Query_Select &$query, $relationName, $viewName, $callerTable) {
        if ($query == '')
            $query = new NW_Db_Query_Select();
        $tableAlias = $this->tableAlias($relationName);
        $has_translatedFields = false;

        foreach ($this->decoratorRef->classDescriptor()->view($viewName) as $field) {
            if (in_array($field, $this->fieldsname)) {
                $fieldAlias = $this->fieldAlias($field, $relationName);
                $query->select(array($field => $fieldAlias));
                $has_translatedFields = true;
            }
        }

        if ($has_translatedFields) {
            $query
                    ->leftJoinOn(array($this->table => $tableAlias), sprintf("%s.id = %s.%s AND %s.lang='%s'", $tableAlias, $callerTable, $relationName, $tableAlias, $this->getDecorator()->getLang()));
        }
    }

}

?>




	
<div id="sortabletabs">
	<ul>
		<li><a href="#sortabletabs-1">Elementi Disponibili</a></li>
			<li><a href="#tabs-2">Top</a></li><li><a href="#tabs-3">Area geografica 1</a></li><li><a href="#tabs-4">Area geografica 2</a></li><li><a href="#tabs-5">Area geografica 3</a></li><li><a href="#tabs-6">Area geografica 4</a></li><li><a href="#tabs-7">Area geografica 5</a></li><li><a href="#tabs-8">Area geografica 6</a></li>			
		</ul>
		<div id="sortabletabs-1">
			<div class="column">
				<ul id="sortable_all" class="droptrue sortable"> 
					<li class="ui-state-highlight" rel="84">
<div class="smallItemBox">
	<label class="pageTitle">Upgrading of Road SS 534</label>
	<ul>
	<li><strong>title</strong>UPGRADING OF ROAD SS 534</li><li><strong>year</strong>2013</li><li><strong>project_datas</strong>
	Location: Cosenza, Italy
	Client: Vidoni S.p.A. per A.N.A.S. S.p.A.
	Period: 2013 - in progress
	Activities: Geotechnics, seismic design
	Amount of works: Euro 129.380.170
	Credits: Lotti Ingegnaria ...</li><li><strong>description</strong>The project involves upgrading of the national motorway 534 over a distance of 29.78 km. The motorway forms part of an international route E844 and is included in the First Programme of Strategic Infrastructure ...</li>	
	</ul>
</div>
</li><li class="ui-state-highlight" rel="104">
<div class="smallItemBox">
	<label class="pageTitle">A school for Peace</label>
	<ul>
	<li><strong>title</strong>A SCHOOL FOR PEACE</li><li><strong>year</strong>2012</li><li><strong>project_datas</strong>
	Location: Ghaza, Palestine
	Client: Islamic Development Bank and Kuwait Fund for Development.
	Period: 2012
	Activities: Structural design
	Credits: MCA - arch. Mario Cucinella, Architetture Sostenibili ...</li><li><strong>description</strong>The project involves&nbsp; design of a&nbsp; school and a recreational centre building as a first of a series of 20 schools in Palestine, Jordan, Syria and Lebanon comissioned by&nbsp; the United Nation. ...</li>	
	</ul>
</div>
</li><li class="ui-state-highlight" rel="121">
<div class="smallItemBox">
	<label class="pageTitle">asdfaf</label>
	<ul>
	<li><strong>date</strong>01/11/2013</li><li><strong>title</strong>asdfaf</li><li><strong>description</strong><p>asdfasf</p>
</li>	
	</ul>
</div>
</li>				</ul>
			</div>
		</div><!-- sortabletabs-1 -->
	
	<div id="sortabletabs-2"><div class="column">
	<h5 class="title">Top</h5>
	<ul id="sortable_body" class="dropfalse sortable" rel="body">
			</ul>
</div></div><div id="sortabletabs-3"><div class="column">
	<h5 class="title">Area geografica 1</h5>
	<ul id="sortable_reg1" class="dropfalse sortable" rel="reg1">
			</ul>
</div></div><div id="sortabletabs-4"><div class="column">
	<h5 class="title">Area geografica 2</h5>
	<ul id="sortable_reg2" class="dropfalse sortable" rel="reg2">
			</ul>
</div></div><div id="sortabletabs-5"><div class="column">
	<h5 class="title">Area geografica 3</h5>
	<ul id="sortable_reg3" class="dropfalse sortable" rel="reg3">
			</ul>
</div></div><div id="sortabletabs-6"><div class="column">
	<h5 class="title">Area geografica 4</h5>
	<ul id="sortable_reg4" class="dropfalse sortable" rel="reg4">
			</ul>
</div></div><div id="sortabletabs-7"><div class="column">
	<h5 class="title">Area geografica 5</h5>
	<ul id="sortable_reg5" class="dropfalse sortable" rel="reg5">
			</ul>
</div></div><div id="sortabletabs-8"><div class="column">
	<h5 class="title">Area geografica 6</h5>
	<ul id="sortable_reg6" class="dropfalse sortable" rel="reg6">
			</ul>
</div></div>	
	
	<form action="/pages/page/saveshared"
		method="post"
		id="composableform">
		
<input type="hidden" name="page" value="82" id="page">

<input type="hidden" name="body[id]" value="" id="body-id">

<input type="hidden" name="reg1[id]" value="" id="reg1-id">

<input type="hidden" name="reg2[id]" value="" id="reg2-id">

<input type="hidden" name="reg3[id]" value="" id="reg3-id">

<input type="hidden" name="reg4[id]" value="" id="reg4-id">

<input type="hidden" name="reg5[id]" value="" id="reg5-id">

<input type="hidden" name="reg6[id]" value="" id="reg6-id">	</form>
	<section class="common_actions">
		<ul>
			<li><a class="btn btn-success zoom fancybox.iframe"
				onclick="window.main_page.saveSharedBlocks(82);">
					<span class="icon_font">&#xf055;</span>Salva Pagina		    	</a></li>
		</ul>
	</section>
	
<script>
	$(function() {
		$( "#sortable1, #sortable2" ).sortable().disableSelection();
		var $tabs = $( "#sortabletabs" ).tabs();
		var $tab_items = $( "ul:first li", $tabs ).droppable({
			accept: ".connectedSortable li",
			hoverClass: "ui-state-hover",
			drop: function( event, ui ) 
			{
				var $item = $( this );
				var $list = $( $item.find( "a" ).attr( "href" ) ).find( ".connectedSortable" );
				ui.draggable.hide( "slow", function() {
					$tabs.tabs( "option", "active", $tab_items.index( $item ) );
					$( this ).appendTo( $list ).show( "slow" );
				});
			}
		});
	});
</script>
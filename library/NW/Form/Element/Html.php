<?php

class NW_Form_Element_Html extends Zend_Form_Element_Hidden
{
    
    public function __construct($spec, $options = null)
    {
        parent::__construct($spec, $options);
        $this->setIgnore(true);
        $this->setDecorators(array(new Zend_Form_Decorator_Description(array('escape'=>false, 'tag'=>''))));
    }
    
    public function setHtml($html)
    {
        $this->_description = $html;
    }
    
    public function setClearer()
    {
        $this->_description = '<div class="clearer"></div>';
    }
}

?>

<?php
class Pages_Model_Page_Service
{
	protected $_mapper;
	

    public function getMapper() {
        return $this->_mapper;
    }

    public function __construct() {
        $this->_mapper = new Pages_Model_Page_Mapper();
       
    }
	
	public function find($paramID, &$model = null)
	{
		try{
			$model = (empty($model)) ? new Pages_Model_Page() : $model;
			$this->_mapper->find($paramID, $model);
			return $model;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	//injects a taxonomy on mapper. 
	//@TODO: check necessity of load method
	public function addTaxonomy(Pages_Model_Taxonomy $taxonomy)
	{
		try {
			$taxonomyService = new Pages_Model_Taxonomy_Service();
			$taxonomy = $taxonomyService->load($taxonomy);
			$this->_mapper->addTaxonomy($taxonomy);
			return $this;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function setDisplay(App_Model_Display $display)
	{
		try {
			$this->_mapper->setDisplay($display);
			return $this;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function dropTaxonomies()
	{
		try {
			$this->_mapper->dropTaxonomies();
			return $this;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	/**
	 * loads a page as a Pages_Model_Page_DTO_Page
	 * @param unknown $paramID
	 * @return Pages_Model_Page_DTO_Page
	 */
	public function load($paramID)
	{
		$dto = new Pages_Model_Page_DTO_Page();
		$blockService = new Pages_Model_Block_Service();
		$page = new Pages_Model_Page();
		
		$this->_mapper->find($paramID, $page);
		
		$blocks = $blockService->byPage($paramID, $paginator, array(), array("order"=> "asc"));
		$dto->setPage($page)->setBlocks($blocks);
		return $dto;
	}
	
	public function getTranslation($pageId, $lang, $load=false)
	{
		try{
			$page = $this->find($pageId);
			$trxId = $page->getTrx();
			$translationService = new Pages_Model_Page_Translation_Service();
			$trx = $translationService->find($trxId);
			$translationId = $trx->getLang($lang);
			if($load)
				return $this->find($translationId);
			return $translationId;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	/**
	 * returns a new page as a translation of the current
	 * @param unknown $pageId
	 * @param unknown $lang
	 * @return Pages_Model_Page_DTO_Page
	 */
	public function translate($pageId, $lang)
	{
		try {
			$this->_mapper->getDbTable()->getAdapter()->beginTransaction();
			$parentPageDTO = $this->load($pageId);
			$translatedPage = Pages_Model_Page_Factory::getPage($parentPageDTO->getPage()->getTemplate());
			$currentPage = $parentPageDTO->getPage();
			$translatedPage->setStatus(Pages_Model_Page::STATUS_DRAFT)
							->setLang($lang)
							->setLabel($currentPage->getLabel())
							->setOrder($currentPage->getOrder())
							->setTemplate($currentPage->getTemplate())
							->setTrx($currentPage->getTrx());
			NW_Pages_Page::getInstance()->attachObservers($translatedPage);
			$newPageId = $this->_mapper->saveTranslation($translatedPage);
			$blockService = new Pages_Model_Block_Service();
			//translate blocks
			foreach($parentPageDTO->getBlocks() as $block)
			{
				$block->setPage($newPageId)
					->setStatus(Pages_Model_Block::STATUS_DRAFT)
					->setId(null);
				NW_Pages_Block::getInstance()->attachObservers($block);
				$blockService->save($block);
			}
			//translate taxonomies			
			$taxService = new Pages_Model_Taxonomy_Service();
			$rows = $taxService->taxonomyRows($pageId);
			foreach($rows as $row)
			{
				if($row->getInvokerRole() == 'parent')
				{
					$translationId = $this->getTranslation($row->getDependent(), $lang);
					$row->parent = $newPageId;
					$row->dependent = $translationId;
				}
				else 
				{
					$translationId = $this->getTranslation($row->getParent(), $lang);
					$row->dependent = $newPageId;
					$row->parent = $translationId;
				}
				
				if(empty($translationId) || empty($newPageId)) 
				{
					Zend_Registry::get("Log")->debug("translationId: is empty");
					continue; //there is no translation available, so i skip it
				}
				else 
				{
					Zend_Registry::get("Log")->debug("translationId: ".var_export($translationId, true).", newPageId: $newPageId");
					$taxService->saveRow($row);
				}
			}
			//translates shared blocks
			
			$this->_mapper->getDbTable()->getAdapter()->commit();
			return $newPageId;
		} catch(Exception $e)
		{
			$this->_mapper->getDbTable()->getAdapter()->rollback();
			throw $e;
		}
	}
	
	public function fetchAll(NW_Pages_Paginator &$paginator, $paramsSearch = array(), $paramsOrder = array('order' => 'asc'), $load = false)
	{
		try 
		{
			return $this->_mapper->fetchAll($paginator, $paramsSearch, $paramsOrder, $load);
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function count($paramsSearch = array())
	{
		try {
			return $this->_mapper->requestCount($paramsSearch);
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	public function first($paramsSearch = array(), $load = false)
	{
		try
		{
			$paginator = new NW_Pages_Paginator();
			$paginator->setItemsPerPage(1);
			$result = $this->_mapper->fetchAll($paginator, $paramsSearch, array(), $load);
			if(empty($result)) return false;
			return current($result);
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function save(Pages_Model_Page $page)
	{
		try {
			$this->_mapper->getDbTable()->getAdapter()->beginTransaction();
			$res =  $this->_mapper->save($page);
			$this->_mapper->getDbTable()->getAdapter()->commit();
			return $res;
		} catch (Exception $e) {
			$this->_mapper->getDbTable()->getAdapter()->rollback();
			throw $e;
		}
	}
	
	public function delete($id)
	{
	    try {
	    	$model = $this->find($id);
	    	NW_Pages_Page::getInstance()->attachObservers($model);
	        $this->getMapper()->delete($model);
	        return true;
	    } catch (Exception $e)
	    {
	        throw $e;
	    }
	}
	
	public function fetchDependent(NW_Pages_Paginator &$paginator, $paramsSearch=array(), $paramsOrder = null, $load=false)
	{
	    try {
	        $result = $this->_mapper->fetchDependent($paginator, $paramsSearch, $paramsOrder, $load);
	        if($paginator->itemsPerPage == 1)
	            return (!empty($result)) ? current($result) : array();
	        return $result;
	    } catch(Exception $e)
	    {
	        throw $e;
	    }
	}
	
	public function fetchParents(NW_Pages_Paginator &$paginator, $paramsSearch=array(), $paramsOrder = null, $load=false)
	{
		try {
			$result = $this->_mapper->fetchParents($paginator, $paramsSearch, $paramsOrder, $load);
			if($paginator->itemsPerPage == 1)
			    return (!empty($result)) ? current($result) : array();
			return $result;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function fetchAvailable(NW_Pages_Paginator &$paginator, $paramsSearch=array(), $paramsOrder = null, $load=false)
	{
		try {
			$result = $this->_mapper->fetchAvailable($paginator, $paramsSearch, $paramsOrder, $load);
			if($paginator->itemsPerPage == 1)
				return (!empty($result)) ? current($result) : array();
			return $result;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function displayFirst($template, $paramsSearch=array())
	{
		try {
			$paramsSearch["template"] = $template;
			return $this->first($paramsSearch);
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function display($template, $paramsSearch=array())
	{
		try {
			$paginator = NW_Pages_Paginator::fetchAll();
			$paramsSearch["template"] = $template;
			$paramsSearch["status"] = 'active';
			return $this->fetchAll($paginator, $paramsSearch);
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function displayDependent($taxonomy, $template, $paramsSearch=array())
	{
		try {
			$paginator = NW_Pages_Paginator::fetchAll();
			//$paramsSearch["template"] = $template;
			$paramsSearch["status"] = 'active';
			
			$result = $this->addTaxonomy($taxonomy)->fetchDependent($paginator, $paramsSearch);
			$this->dropTaxonomies();
			if($paginator->itemsPerPage == 1)
				return (!empty($result)) ? current($result) : array();
			return $result;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	/**
	 * 
	 * @param Pages_Model_Taxonomy $taxonomy
	 * @param string $template: the model TO EXTRACT
	 * @param unknown $paramsSearch
	 * @throws Exception
	 * @return Ambigous <multitype:, mixed>|Ambigous <multitype:, mixed, multitype:Pages_Model_Page NULL >
	 */
	public function displayParents($taxonomy, $template, $paramsSearch=array(), $paramsOrder=array())
	{
		try {
			$paginator = NW_Pages_Paginator::fetchAll();
			$paramsSearch["template"] = $template;
			$paramsSearch["status"] = 'active';
			$result = $this->addTaxonomy($taxonomy)->fetchParents($paginator, $paramsSearch,$paramsOrder);
			$this->dropTaxonomies();
			if($paginator->itemsPerPage == 1)
				return (!empty($result)) ? current($result) : array();
			return $result;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	/**
	 * 
	 * @param int $pageId
	 * @param array $paramsSearch
	 * @throws Exception
	 * @return Ambigous <multitype:, mixed, multitype:Pages_Model_Block_Shared >
	 */
	public function displayShared($pageId, $paramsSearch=array(), $load=false, $limit=false)
	{
		try {
			if($limit) 
			{	
				$paginator = new NW_Pages_Paginator();
				$paginator->setItemsPerPage($limit);
			} else $paginator = NW_Pages_Paginator::fetchAll();
			
			$sharedService = new Pages_Model_Block_Shared_Service();
			return $sharedService->byPage($pageId, $paginator, $paramsSearch, array(), $load);
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function collection()
	{
		try {
			$this->_mapper->setAsCollection();
			return $this;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function fromRequestSafe($template, $param='id', $method=INPUT_GET)
	{
		try {
			if(!filter_has_var($method, $param)) throw new Exception();
			$id = filter_input($method, $param, FILTER_SANITIZE_NUMBER_INT);
			if($id === false || empty($id)) throw new Exception();
			$page = $this->find($id);
			$pageTemplate = $page->getTemplate();
			if($pageTemplate != $template) throw new Exception();
			return $page;
		} catch(Exception $e)
		{
			throw $e;
		}
	}
	
}
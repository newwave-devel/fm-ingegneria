<?php
function renderSharedBlockNews($tpl, &$sharedBlock, &$blockService, $odd, $isBottom = false)
{
	$block = $sharedBlock->getBlock();
	$page = $sharedBlock->getPage();
	$img = $blockService->asset($block->_("file"));
	$date = str_replace("/", ".",$block->_("date") );
	$img = ($odd) ? sprintf("/assets/res.php?src=%s&h=320", $img->getAssetUrl()) : sprintf("/assets/res.php?src=%s&w=320&h=320", $img->getAssetUrl());
	$description = ($odd) ? nicesubstring($block->_("description"), 320) : nicesubstring($block->_("description"), 70);
	$itemClass = '';
	if($isBottom)
		$itemClass = ($odd) ? 'big' : 'small';
	return sprintf($tpl, $itemClass, $block->getPage(), $block->_("title"), $img, $block->_("title"), 'News', $block->_("title"), $date, $description);
}

function renderSharedBlockProject($tpl, &$sharedBlock, &$blockService, $odd, $isBottom = false)
{
	$block = $sharedBlock->getBlock();
	$page = $sharedBlock->getPage();
	$img = $blockService->asset($block->_("file"));
	$img = ($odd) ? sprintf("/assets/res.php?src=%s&h=320", $img->getAssetUrl()) : sprintf("/assets/res.php?src=%s&w=320&h=320", $img->getAssetUrl());
	$description = ($odd) ? nicesubstring($block->_("description"), 320) : nicesubstring($block->_("description"), 70);
	$itemClass = '';
	if($isBottom)
		$itemClass = ($odd) ? 'big' : 'small';
	return sprintf($tpl, $itemClass,$block->getPage(), addslashes($block->_("title")), $img, addslashes($block->_("title")), 'Projects',$block->_("title"), $description);
}

function renderBlockNews($tpl, &$block, &$blockService, $odd, $isBottom = false)
{
	
	$page = $block->getPage();
	$img = $blockService->asset($block->_("file"));
	$date = str_replace("/", ".",$block->_("date") );
	$img = ($odd) ? sprintf("/assets/res.php?src=%s&h=320", $img->getAssetUrl()) : sprintf("/assets/res.php?src=%s&w=320&h=320", $img->getAssetUrl());
	$description = ($odd) ? nicesubstring($block->_("description"), 320) : nicesubstring($block->_("description"), 70);
	$itemClass = '';
	if($isBottom)
		$itemClass = ($odd) ? 'big' : 'small';
	return sprintf($tpl, $itemClass, $block->getPage(),  $block->_("title"), $img, $block->_("title"), 'News', $block->_("title"), $date, $description);
}

function renderBlockProject($tpl, &$block, &$blockService, $odd, $isBottom = false)
{
	$page = $block->getPage();
	$img = $blockService->asset($block->_("file"));
	$date = str_replace("/", ".",$block->_("date") );
	$img = ($odd) ? sprintf("/assets/res.php?src=%s&h=320", $img->getAssetUrl()) : sprintf("/assets/res.php?src=%s&w=320&h=320", $img->getAssetUrl());
	$description = ($odd) ? nicesubstring($block->_("description"), 320) : nicesubstring($block->_("description"), 70);
	$itemClass = '';
	if($isBottom)
		$itemClass = ($odd) ? 'big' : 'small';
	return sprintf($tpl, $itemClass,$block->getPage(), addslashes($block->_("title")), $img, addslashes($block->_("title")), 'Projects',$block->_("title"), $description);
}

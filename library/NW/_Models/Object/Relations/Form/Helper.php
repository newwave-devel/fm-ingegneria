<?php

class NW_Models_Object_Relations_Form_Helper {

    private $_form;
    private $_objectDescriptor;

    public function __construct(NW_Models_Object_Relations_Form &$form, $options = null) {
        $this->_form = $form;
        $this->_objectDescriptor = NW_Application::getSingleton("app/NW_ModuleManager")->classDescriptor($this->_form->getClassUri());
    }

    public function addModelFields($fieldsName = array(), $fields_options = array()) {

        $mainField = $this->mainField();

        $id = $this->_form->getField("id", 'hidden', array());
        $classUri = $this->_form->getField("classUri", 'hidden', array());
        $relname = $this->_form->getField("relName", 'hidden', array());
        $this->_form->addElement($mainField)
                ->addElement($id)
                ->addElement($classUri)
                ->addElement($relname);
        return $this;
    }

    public function getForm() {
        return $this->_form;
    }

    private function mainField() {
        $defsDescriptor = NW_Application::getSingleton("defs/NW_Defs_Relations");
        if ($defsDescriptor->relation($this->_form->_relName, 'related') == $this->_form->getClassUri()) {
            $format = $defsDescriptor->relating($this->_form->_relName, 'render');
            $options['required'] = $defsDescriptor->relating($this->_form->_relName, 'required') == 'Y';
            $view = $defsDescriptor->relating($this->_form->_relName, 'view');
            $options['label'] = $defsDescriptor->relating($this->_form->_relName, 'label');
        } else {
            $format = $defsDescriptor->related($this->_form->_relName, 'render');
            $options['required'] = ($defsDescriptor->related($this->_form->_relName, 'required') == 'Y');
            $view = $defsDescriptor->related($this->_form->_relName, 'view');
            $options['label'] = $defsDescriptor->related($this->_form->_relName, 'label');
        }
        $field = $this->_form->getField($this->_form->_relName, $format, $options);
        foreach(NW_Application::__model($this->_form->getClassUri())->view($view, array()) as $rowItem)
            $field->addMultiOption($rowItem["id"],$rowItem['name']);
        return $field;
    }

}

?>
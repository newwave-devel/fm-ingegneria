<?php
class App_Model_I18n_Manager
{
    private $_collections = array();
    private $_i18nService=null;
    private $_lang;
    private static $_instance = null;

    public static function getInstance($lang)
    {
        if(self::$_instance == null)
        {
            if($lang == '')
                $lang = App_Model_Display_Service::getInstance()->lang();
        }

        return new App_Model_I18n_Manager($lang, new App_Model_I18n_Service());
    }

    private function __construct($lang, $service)
    {
        $this->_lang = $lang;
        $this->_i18nService = $service;
        $this->_collections = array();
    }

    public function __get($context)
    {
        if(!isset($this->_collections[$context]))
            $this->_collections[$context] = new App_Model_I18n_Collection($this->_i18nService,$context, $this->_lang);
        return $this->_collections[$context];

    }
}
?>
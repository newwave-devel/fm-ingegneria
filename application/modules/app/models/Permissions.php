<?php
class App_Model_Permissions {
	
	private $_allowed;
	private $_user;
	
	
	public function __construct() {
		$this->_user = null;
		$this->_allowed = array ();
	}
	public function getUser() {
		return $this->_user;
	}
	
	public function setUser($_user) {
		$this->_user = $_user;
		return $this;
	}
	
	public function getAllowed() {
		return $this->_allowed;
	}
	
	public function setAllowed($_allowed) {
		if(!is_array($_allowed))
			$_allowed = array($_allowed);
		$this->_allowed = $_allowed;
		return $this;
	}
	
	public function asCsv()
	{
		return implode(",", $this->_allowed);
	}
	
	public function current()
	{
		if(is_array($this->_allowed))
			return current($this->_allowed);
		return $this->_allowed;
	}
	
}
?>
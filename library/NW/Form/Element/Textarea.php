<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Hidden
 *
 * @author Alessio
 */
class NW_Form_Element_Textarea extends Zend_Form_Element_Textarea
{
    public function __construct($spec, $options = array())
    {
    	if(!isset($options["rows"]))
    		$options["rows"] = 10;
    	if(!isset($options["cols"]))
    		$options["cols"] = 103;
        parent::__construct($spec, $options);
        $this->setDecorators(array(new NW_Form_Decorator_Textarea()));
    }
}

?>

<?php if($attachments): ?>
<section id="download">
	<h5>Download</h5>
	<ul>
	<?php 
	foreach($attachments as $att)
	{
		$file = $blockService->asset($att->_('file'));
		echo sprintf('<li><a href="%s">%s</a></li>', $file->getAssetUrl(), $file->getAssetName());
	}
	?>
	</ul>
</section>
<!--download-->
<?php endif; ?>
<?php

class NW_Models_Object_Decorators_Catalogable extends NW_Models_Object_Decorator {

    /**
     *
     * Object status.
     * Avaialable values are 'active', 'draft', 'revision', 'deleted'
     * @var string
     */
    public $_status;

    /**
     *
     * Creation Timestamp (string, formatted for mysql insertion)
     * @var string
     */
    public $_ts_insert;

    /**
     *
     * Update Timestamp (string, formatted for mysql insertion)
     * @var string
     */
    public $_ts_update;

    /**
     *
     * Deleted Timestamp (string, formatted for mysql insertion)
     * @var string
     */
    public $_ts_delete;
    

    const STATUS_DRAFT = 'draft';
    const STATUS_ACTIVE = 'active';
    const STATUS_DELETED = 'deleted';

    public function __construct($object, $owner = 1) {
        $this->_object = $object;
        $this->_status = NW_Application::getSingleton('app/NW_ModuleManager')->getModuleDescriptor($this->getClassUri())->getClassDefaultStatus($this->getClassUri());
        $this->_ts_insert = date("Y-m-d h:i:s");
        $this->_ts_update = date("Y-m-d h:i:s", 0);
        $this->_ts_delete = date("Y-m-d h:i:s", 0);
        $this->_mapper = new NW_Models_Object_Decorators_Mapper_Catalogable($this);
    }

    public function setStatus($status) {
        $this->_status = $status;
        return $this;
    }

    public function getStatus() {
        return $this->_status;
    }

    public function setTsInsert($ts) {
        $this->_ts_insert = $ts;
        return $this;
    }

    public function getTsInsert() {
        return $this->_ts_insert;
    }

    public function setTsUpdate($ts) {
        $this->_ts_update = $ts;
        return $this;
    }

    public function getTsUpdate() {
        return $this->_ts_update;
    }

    public function setTsDelete($ts) {
        $this->_ts_delete = $ts;
        return $this;
    }

    public function getTsDelete() {
        return $this->_ts_delete;
    }
    
    public function __set($name, $value) {
        switch($name)
        {
            case 'status':
            case 'ts_insert':
            case 'ts_update':
            case 'ts_delete':
                $varname = '_'.$name;
                $this->$varname = $value;
                break;
            default:
                $this->getObject()->$name = $value;
        }
        return $this;
    }
    
     public function __get($name) {
        switch($name)
        {
            case 'status':
            case 'ts_insert':
            case 'ts_update':
            case 'ts_delete':
                $varname = "_".$name;
                return $this->$varname;
                break;
            default:
                return $this->getObject()->$name;
        }
        
    }

    public function export(&$object) {
        $res = array();
        $res['status'] = $object->status;
        $res['ts_insert'] = $object->ts_insert;
        $res['ts_update'] = $object->ts_update;
        $res['ts_delete'] = $object->ts_delete;
        $res['class'] = $object->class;
        return $res;
    }

    public function isCatalogable() {
        return true;
    }

    public function doInsert($object) {
        try {
            $query = $this->getMapper()->queryInsert($object);
            print "<li>".$query->getQuery()."</li>";
            $id = $this->getAdapter()->performInsertQuery($query);
            $object->id = $id;
            return $this->getObject()->doInsert($object);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function doUpdate(&$object) {
        try {
            $query = $this->getMapper()->queryUpdate($object);
            $this->getAdapter()->performUpdateQuery($query);
            print "<li>".$query->getQuery()."</li>";
            return $this->getObject()->doUpdate($object);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function doLoadOne($params) {
        try {
            $query = $this->getObject()->doLoadOne($params);
            return $this->getMapper()->queryLoad($query);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function doLoadAll($params, NW_Pages_Paginator &$paginator) {
        try {
            $query = $this->getObject()->doLoadAll($params, $paginator);
            return $this->getMapper()->queryAll($query);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function doView($viewName, $params, &$paginator) {
        try {
            $query = $this->getObject()->doView($viewName, $params, $paginator);
            return $this->getMapper()->queryView($viewName, $query);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function doLoadByRelDefInTable(NW_Db_Query_Select &$query, $relationName, $view, $callerTable)
    {
        try {
            $this->getObject()->doLoadByRelDefInTable($query, $relationName, $view, $callerTable);
            $this->getMapper()->queryLoadByRelDefInTable($query, $relationName);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
//    public function toStdClass()
//    {
//        $stdClass = $this->getObject()->toStdClass();
//        $stdClass->status = $this->getStatus();
//        $this->ts_insert = $this->getTsInsert();
//        $this->ts_update = $this->getTsUpdate();
//        $this->ts_delete = $this->getTsDelete();
//        return $stdClass;
//    }
//    
    public function formFields()
    {
        $fields = $this->getObject()->formFields();
        $status = new NW_Form_Element_Select("status");
        $status->setLabel("Stato di pubblicazione");
        $status->addMultiOption(self::STATUS_ACTIVE, "active")
                ->addMultiOption(self::STATUS_DRAFT, "draft");
        $fields[] = $status;
        return $fields;
    }
    
    public function cloneNew(&$object)
    {
        $object->status = $this->getStatus();
        $object->ts_insert = $this->getTsInsert();
        $object->ts_update = $this->getTsUpdate();
        $object->ts_delete = $this->getTsDelete();
        return $this->getObject()->cloneNew($object);
    }
}


?>

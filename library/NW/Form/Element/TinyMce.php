<?php

class NW_Form_Element_TinyMce extends Zend_Form_Element_Textarea
{
    
    public function __construct($spec, $options = null)
    {
        parent::__construct($spec, $options);
        $this->setAttrib("class", $this->getAttrib("class")." tinymce");
        $this->setDecorators(array(new NW_Form_Decorator_Textarea()));
        $this->getView()->jQuery()->addOnload($this->getJs());
        
    }
    
    public function getJs()
    {
    	$tpl = <<<TPL
    		      CKEDITOR.replace( '%s' ,
        				{
    						toolbar: '%s',
    						width : 850
        				});
TPL;
    	
    	return sprintf($tpl, $this->getAttrib("id"),$this->getAttrib("class"));
    }
}

?>
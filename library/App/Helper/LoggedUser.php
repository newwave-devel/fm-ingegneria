<?php

class App_Helper_LoggedUser extends Zend_Controller_Action_Helper_Abstract {

	public function direct()
	{
		return Zend_Auth::getInstance()->getStorage()->read();
	}
}

?>

<?php
class App_Model_Display_Service
{
	const SESSIONNAMESPACE = 'display';
	private static $_instance = null;
	private $_display;
	
	public static function getInstance()
	{
		if(self::$_instance == null)
			self::$_instance = new App_Model_Display_Service();
		return self::$_instance;
	}
	//this function is to be used by the frontend: it does not load the default parameters
	public static function initEmpty()
	{
		if(self::$_instance == null)
			self::$_instance = new App_Model_Display_Service(false);
		return self::$_instance;
	}
	
	public function store($display = null)
	{
		$session = new Zend_Session_Namespace(App_Model_Display_Service::SESSIONNAMESPACE);
		$session->display = (empty($display)) ? $this->_display : $display;
	}
	
	private function __construct($default = true)
	{
		$display = ($default)  ? App_Model_Display::defaultDisplay() : new App_Model_Display();
		$this->_display = $display;
	}
	
	public function getDisplay()
	{
		return $this->_display;
	}
	
	public function lang()
	{
		return $this->_display->getLang();
	}
	
	public function langs()
	{
		return $this->_display->getLangs();
	}
	
	public function isMultilanguage()
	{
		return $this->_display->isMultilanguage();
	}
	public function langsAsSelect()
	{
		return $this->_display->langsAsSelect();
	}
}